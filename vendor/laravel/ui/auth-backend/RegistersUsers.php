<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\States;
use App\Models\Countries;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationAdminRegisterUser;
use App\Mail\NotificationCode;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */


    public function showRegistrationForm()
    {
        $country = Countries::where('id', 95)->first();
        return view('auth.register')->with('country',$country);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        //Enviar email con el codigo
        $details = [ 
                    'cedula' => $user->dni,
                    'username' => $user->username,
                    'client' => $user->name.' '.$user->last_name,
                    'code' => $user->code,
                    'url' => route('login')
                ];

        $admin = User::join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                ->where('role_id', 1)->get();


        foreach ($admin as $key => $adm) {

            Mail::to($adm->email)->send(new NotificationAdminRegisterUser($details));
        }
       

        //Mail::to($user->email)->send(new NotificationCode($details));

        return redirect()->route('users.register_success');
        /*return $request->wantsJson()
                    ? new JsonResponse([], 201)
                    : redirect($this->redirectPath());*/
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
