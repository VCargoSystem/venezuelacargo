@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@php( $dashboard_url = View::getSection('dashboard_url') ?? config('adminlte.dashboard_url', 'home') )

@if (config('adminlte.use_route_url', false))
    @php( $dashboard_url = $dashboard_url ? route($dashboard_url) : '' )
@else
    @php( $dashboard_url = $dashboard_url ? url($dashboard_url) : '' )
@endif

<a href="{{ $dashboard_url }}"  style="background: #FFB901 !important; border-bottom:none;"
    @if($layoutHelper->isLayoutTopnavEnabled())
        class="navbar-brand"
    @else
        class="brand-link "
    @endif>

    {{-- Small brand logo --}}
    <img src="{{ asset('vendor/adminlte/dist/img/logo1.png') }}"
         alt=""
         class="brand-image img-circle"
         style="opacity:.99; max-height: 120px !important;">

    {{-- Brand text --}}
    <span class="brand-text font-weight-light {{ config('adminlte.classes_brand_text') }}  font-weight-bold">
        
    </span>

</a>