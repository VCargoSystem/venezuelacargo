@inject('menuItemHelper', 'JeroenNoten\LaravelAdminLte\Helpers\MenuItemHelper')


@if(auth()->user()->hasRole('cliente')==1)

  <li class="nav-item">
      <a href="{{ route('home') }}" class="nav-link">
            <i class="fas fa-home"></i>
            <p> &nbsp;ESCRITORIO</p>
      </a>
  </li>

  <li class="nav-item">
      <a href="{{ route('clients.prealerts.index') }}" class="nav-link">
            <i class="fas fa-exclamation-circle"></i>
            <p> &nbsp;PREALERTAS</p>
      </a>
  </li>
  <li class="nav-item">
      <a class="nav-link  " href="{{ route('clients.packages.index')}}">
         <i class="fas fa-store"></i>
         <p>
              &nbsp;ALMACÉN
         </p>
      </a>
  </li>
  <li class="nav-item">
      <a class="nav-link  " href="{{ route('clients.instrucctions.index')}}">
         <i class="fas fa-edit"></i>
         <p>
              &nbsp;INSTRUCCIONES
         </p>
      </a>
  </li>
  <li class="nav-item">
      <a class="nav-link  " href="{{ route('clients.shippings.index')}}">
         <i class="fas fa-box"></i>
         <p>
              ENVÍOS
         </p>
      </a>
  </li>
  <li class="nav-item">
      <a class="nav-link  " href="{{ route('clients.invoices.index')}}">
         <i class="fas fa-file-invoice"></i>
         <p>
              &nbsp;FACTURACIÓN
         </p>
      </a>
  </li>
@else

<li class="nav-item">
      <a href="{{ route('home') }}" class="nav-link">
            <i class="fas fa-home"></i>
            <p> &nbsp;ESCRITORIO</p>
      </a>
  </li>
	<li class="nav-item">
	    <a class="nav-link  " href="{{ route('admin.users.index')}}">
	       <i class="fas fa-user"></i>
	       <p>
	            &nbsp;USUARIOS
			   </p>
	    </a>
	</li>
 <li class="nav-item">
    <a href="{{ route('prealerts.index') }}" class="nav-link">
      <i class="fas fa-bell"></i>
      <p> &nbsp;PREALERTAS</p>
    </a>
  </li>
  <li class="nav-item">
      <a class="nav-link  " href="{{ route('packages.index')}}">
         <i class="fas fa-warehouse"></i>
         <p>
              &nbsp;ALMACÉN
         </p>
      </a>
  </li>
   <li class="nav-item">
      <a class="nav-link  " href="{{ route('admin.instrucctions.index')}}">
         <i class="fas fa-calendar-check"></i>
         <p>
              &nbsp;INSTRUCCIONES
         </p>
      </a>
  </li>

  <li class="nav-item">
      <a class="nav-link">
        <i class="fas fa-dolly-flatbed"></i>
        <p>
          ENVÍOS
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">

          <a class="nav-link" href="{{ route('admin.shippings.index')}}">
            &nbsp;<i class="fas fa-box-open"></i>
            <p>
                &nbsp;POR FACTURAR
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{ route('admin.shippings_invoices.index')}}">
            &nbsp;<i class="fas fa-box-open"></i>
            <p>
                &nbsp;FACTURADOS 
            </p>
          </a>
        </li>
      </ul>
  </li>
  <li class="nav-item">
      <a class="nav-link  " href="{{ route('admin.invoices.index')}}">
         <i class="fas fa-file-alt"></i>
         <p>
              &nbsp;FACTURACIÓN
         </p>
      </a>
  </li>
  <li class="nav-item">
      <a class="nav-link">
        <i class="fas fa-cog"></i>
        <p>
          &nbsp;CONFIGURACIÓN
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('companies.index') }}" class="nav-link">
            <i class="fas fa-shipping-fast nav-icon"></i>
            <p>&nbsp;EMPRESAS</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.currency.index') }}" class="nav-link">
            <i class="fas fa-dollar-sign nav-icon"></i>
            <p>&nbsp;TASA DEL DÓLAR</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.rate_shippings.index') }}" class="nav-link">
            <i class="fas fa-map-marker-alt nav-icon"></i>
            <p>&nbsp;TASA POR DESTINO</p>
          </a>
        </li>
         <li class="nav-item">
          <a href="{{ route('admin.box.index') }}" class="nav-link">
            <i class="fas fa-truck-loading nav-icon"></i>
            <p>&nbsp;CAJAS</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.status.index') }}" class="nav-link">
            <i class="fas fa-map-marked-alt nav-icon"></i>
            <p>&nbsp;STATUS DE ENVÍO</p>
          </a>
        </li>
        
        <li class="nav-item">
          <a href="{{ route('admin.banner.index') }}" class="nav-link">
            <i class="far fa-images nav-icon"></i>
            <p>&nbsp;BANNER</p>
          </a>
        </li>
      </ul>
  </li>


@endif
<style type="text/css">

[class*='sidebar-dark-'] .nav-sidebar > .nav-item > .nav-link:active {
  color: #C2C7D0;
}

[class*='sidebar-dark-'] .nav-sidebar > .nav-item.menu-open > .nav-link,
[class*='sidebar-dark-'] .nav-sidebar > .nav-item:hover > .nav-link,
[class*='sidebar-dark-'] .nav-sidebar > .nav-item > .nav-link:focus {
  background-color: rgba(255, 255, 255, 0.1);
  color: #ffffff;
}

[class*='sidebar-dark-'] .nav-sidebar > .nav-item > .nav-link.active {
  color: #ffffff;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
}

[class*='sidebar-dark-'] .nav-sidebar > .nav-item > .nav-treeview {
  background: transparent;
}

[class*='sidebar-dark-'] .nav-header {
  background: inherit;
  color: #d0d4db;
}

[class*='sidebar-dark-'] .sidebar a {
  color: #24298D;
  font-weight: bold; 
}

[class*='sidebar-dark-'] .sidebar a:hover, [class*='sidebar-dark-'] .sidebar a:focus {
  text-decoration: none;

}

[class*='sidebar-dark-'] .nav-treeview > .nav-item > .nav-link {
  color: #C2C7D0;
  color: #24298D;
  font-weight: bold; 
}

[class*='sidebar-dark-'] .nav-treeview > .nav-item > .nav-link:hover, [class*='sidebar-dark-'] .nav-treeview > .nav-item > .nav-link:focus {
  background-color: rgba(255, 255, 255, 0.1);
  color: #ffffff;
}

[class*='sidebar-dark-'] .nav-treeview > .nav-item > .nav-link.active, [class*='sidebar-dark-'] .nav-treeview > .nav-item > .nav-link.active:hover, [class*='sidebar-dark-'] .nav-treeview > .nav-item > .nav-link.active:focus {
  background-color: rgba(255, 255, 255, 0.9);
  color: #343a40;
}

[class*='sidebar-dark-'] .nav-flat .nav-item .nav-treeview .nav-treeview {
  border-color: rgba(255, 255, 255, 0.9);
}

[class*='sidebar-dark-'] .nav-flat .nav-item .nav-treeview > .nav-item > .nav-link, [class*='sidebar-dark-'] .nav-flat .nav-item .nav-treeview > .nav-item > .nav-link.active {
  border-color: rgba(255, 255, 255, 0.9);
}
</style>

