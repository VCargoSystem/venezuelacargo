
@if (Auth::user() && Auth::user()->hasRole('admin'))
  <li class="nav-item dropdown">
      <a class="navbar-link" data-toggle="dropdown"href="#">
         <span class="badge badge-warning navbar-badge " id="count_noti">16</span>
        <img src="{{ asset('vendor/adminlte/dist/img/bell.svg') }}" width="20" height="20" alt="" class="mr-4 mt-1">
       
      </a>

      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; left: 0px !important;">
        <span class="dropdown-header"><span id="count_noti"></span> Notificaciones</span>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">
          <i class="fas fa-users mr-3"></i><span id="count_users"> </span>
        </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">
          <i class="fas fa-envelope mr-3"></i><span id="count_prealerts">8 </span> 
        </a>
      </div>
    </li>

@endif
<style type="text/css">
  
  .dropdown-menu-right {
    right: 0;
    left: 0px !important;
  }
</style>