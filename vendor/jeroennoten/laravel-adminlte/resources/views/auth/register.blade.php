@extends('adminlte::auth.auth-page', ['auth_type' => 'register'])

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
@endif


@section('auth_body')

    <form action="{{ $register_url }}" method="post" id="_form_register">
        {{ csrf_field() }}

        {{-- Name field --}}
        <div class="row">
            <input type="hidden" name="longitude" value="" id="longitude">
            <input type="hidden" name="latitude" value="" id="latitude">

            <div class="col-3 mb-3 ">
                <input type="text" name="dni" id="dni" class="form-control {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                       value="{{ old('dni') }}" placeholder="Cedula" autofocus>
              
                @if($errors->has('dni'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('dni') }}</strong>
                    </div>
                @endif
            </div>

            <div class="col-3 mb-3 ">
                <input type="text" name="username" id="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                       value="{{ old('username') }}" placeholder="Usuario" autofocus>
              
                @if($errors->has('username'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </div>
                @endif
            </div>

            <div class="col-3  mb-3">
                <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                       value="{{ old('name') }}" placeholder="Nombre" autofocus>
          
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-3  mb-3">
                <input type="text" name="last_name" class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}"
                       value="{{ old('last_name') }}" placeholder="Apellido" autofocus>
              
                @if($errors->has('last_name'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">

              <div class="col-3  mb-3">
                <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                       value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}">
                  
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
         
            </div>

            <div class="col-3 mb-3">
                <input type="text" name="phone" id="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                       value="{{ old('phone') }}" placeholder="Telefono">
                  
                    @if($errors->has('phone'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </div>
                    @endif
         
            </div>

            <div class="col-3 mb-3">
               {{-- Password field --}}
                <div class="">
                    <input type="password" name="password"
                           class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                           placeholder="{{ __('adminlte::adminlte.password') }}">
                 
                    @if($errors->has('password'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-3 mb-3">
               {{-- Password field --}}
                <div class="">
                    <input type="password" name="password_confirmation"
                           class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                           placeholder="Repita el password">
                
                    @if($errors->has('password_confirmation'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
            
  
         <div class="row">

             <div class="col-3 mb-3">
                <select class="form-control" id="country_id" name="country_id">

                    <option value="{{ $country->id}}">{{$country->name}}</option>

                </select>

                @if($errors->has('country_id'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('country_id') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-3 mb-3">
                <select class="form-control" id="state_id" name="state_id">
                    @foreach($country->states as $state)
                        <option value="{{$state->id}}">{{$state->name}}</option>
                    @endforeach
                </select>

                @if($errors->has('state_id'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('state_id') }}</strong>
                    </div>
                @endif
            </div>

            <div class="col-3 mb-3">
                <select class="form-control" id="city_id" name="city_id">
                    <option value="0">Ciudad</option>
                    <option>2</option>
                  </select>

                @if($errors->has('city_id'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('city_id') }}</strong>
                    </div>
                @endif
            </div>


            <div class="col-12  mb-3">
                <input type="text" name="address" id="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}"
                       value="{{ old('address') }}" placeholder="Ubicación">
                  
                    @if($errors->has('address'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('address') }}</strong>
                        </div>
                    @endif
            </div>
        </div>

        <div class="row">
             <div class="col-11  mb-3">
                <input type="text" name="address1" id="address1" class="form-control {{ $errors->has('address1') ? 'is-invalid' : '' }}"
                       value="{{ old('address1') }}" placeholder="" readonly="readonly">
                   
                    @if($errors->has('address1'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('address1') }}</strong>
                        </div>
                    @endif
            </div>

            <div class="col-md-1">
                <button id="locate" class="btn btn-block btn-primary" style="" type="button" alt="Buscar Direccion">
                    <i class="fa fa-search"></i>
                </button> 
            </div>

        </div>

        <div class="col-xs-10 alert alert-warning">
            Especifique correctamenta la ubicación que sera usada como la dirección unica de entrega de paquetes. Una vez creada, no podra ser modificada sin autorización del administrador
        </div>

        <div class="col-xs-10">
            <iframe class="embed-responsive" id="map_frame" src="{{ route('users.iframeMap') }}" width=auto height=320/></iframe>
        </div>


        {{-- Register button --}}
        <div class="row mt-2  float-right">
            <div class="col-12">
                <button type="button
                " class="btn btn-block btn-primary" id="_btn_save">
                    Guardar
                </button>
            </div>
        </div>

    </form>

@stop

@section('auth_footer')
    <p class="my-0">
        <a href="{{ $login_url }}">
           Ya estoy registrado
        </a>
    </p>
@stop

@section('js')
<script type="text/javascript">
    _id_state=0
    _id_city=0
    _id_location=0
    

    $("#dni").keyup(function (){
         this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    //Select state - return cities
    $("#state_id").on('change, click',function(e){

        e.stopPropagation()
        _id_state = $("#state_id").val()

        $.ajax({
            type: 'get',  
            dataType: 'json',
            url: "<?=url('/findCities')?>/"+ _id_state,

            success: function(res) {
                $("#city_id").html('');
                _cad = '';
                
                $.each(res.cities, function (index, val) {
                    _cad+= `<option value="${val.id}">
                    ${val.name}</option>` 
                });

                $("#city_id").html(_cad);
                //localize_address()
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        }); 
    })


    $("#city_id").on('blur, click',function(e){
        e.stopPropagation()
        //localize_address()
    })

    $('#state_id').trigger('click');
    $('#city_id').trigger('click');
    $('#state_id > option[value="'+_id_state+'"]').prop('selected', 'selected').trigger('click');
    $('#city_id > option[value="'+_id_state+'"]').prop('selected', 'selected').trigger('click');


    //Map and Locator 
    _lat=0
    _lon=0

    function position(){
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(function(objPosition)
            {
                _lon = objPosition.coords.longitude;
                _lat = objPosition.coords.latitude;

            });
        }
    }

    $("#address").keypress(function( event ) {
          if ( event.which == 13 ) {
         event.preventDefault();
          localize_address()
      }
    });

    $("#address" ).blur(function( event ) {
         localize_address()
    });
 
    function localize_address(){
        _cad=''
        $("#address1").val('')
        $("#address1").val('Venezuela ')

        if($("#state_id").val() !== ''){
            _cad +=  $('select[name="state_id"] option:selected').text();
        }

        if($("#city_id").val() !== ''){
            _cad +=  $('select[name="city_id"] option:selected').text();
        }

        if($("#address1").val() !== ''){
            _cad += ' '+$("#address").val()
        }

        _cad = _cad.replace(/\s+/g, ' ')  
        $("#address1").val(_cad)

        $.ajax({
            type: 'get',  
            dataType: 'json',
            url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates',
            data: { 
                SingleLine : $("#address").val(),
                f: 'json',
                outFields: 'Loc_name',
                outSR: {"wkid":102100,"latestWkid":3857}
            },

            success: function(res) {
                console.log(res)
                _long = res.candidates[0].location.x
                _lati = res.candidates[0].location.y

                _base_url = "<?=url('/users/iframeMapFind')?>"

                _url = _base_url+'/'+_long+'/'+_lati

                $('#map_frame').attr('src', _url);
                $("#longitude").val(_long)
                $("#latitude").val(_lati)


            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });
    }

      $("#locate").on('click', function(){
        localize_address()
    });

    $("#btn_save").on('click', function(){
        
        if($("#address").val() =='' || $("#address").val() == undefined){
            return 
        }

        if($("#city_id").val() == 0){
            return false
        }

        if($("#state_id").val() == 0){
            return false
        }
        
        if($("#address1").val() ==''){
            return 
        }

        localize_address()
        
        $("#_form_register").submit();

    });
 
</script>
@stop