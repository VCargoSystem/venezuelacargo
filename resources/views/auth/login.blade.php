@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])

@section('adminlte_css_pre')
    <link rel="stylesheet" href="{{ asset('vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
@stop

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )
@php( $password_reset_url = View::getSection('password_reset_url') ?? config('adminlte.password_reset_url', 'password/reset') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
@endif


      
@section('auth_body')

    <form action="{{ $login_url }}" method="post">
        {{ csrf_field() }}

        <div class="row mb-4">
            <div class="col-3"></div>
            <img src="{{ asset('/vendor/adminlte/dist/img/logo_reporte.svg') }}" class="img-fluid col-6" alt="Responsive image" style="max-height: 30%; margin-top:50px;" >
            <div class="col-3"></div>
        </div><br><br>

        {{-- Email field --}}

        <div class="row">
            <div class="col-2"></div>
            <div class="mb-3 col-8">

                    <input type="text" name="username" class=" form-control form-control{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                           value="{{ old('username') }}" placeholder="CORREO ELECTRÓNICO" autofocus>

                    @if ($errors->has('username'))

                        <div class="invalid-feedback">
                            @foreach ($errors->all() as $key => $message)
                                @if ($message == 'Usuario inactivo o sin código, no puede iniciar sesión')
                                    <strong>Usuario inactivo o sin código, no puede iniciar sesión.</strong>

                                @elseif ($key== 0 && $message != 'Estas credenciales no coinciden con nuestros registros.')
                                    <strong>          {!! trans('auth.username') !!}</strong>

                                @elseif ($message == 'Estas credenciales no coinciden con nuestros registros.')
                                    <strong>Estas credenciales no coinciden con nuestros registros.</strong>
                                @else 
                                    <strong>Usuario inactivo o sin código, no puede iniciar sesión.</strong>
                                @endif
                            @endforeach
                           
                        </div>
                    @else
                        <div class="invalid-feedback">
                            @foreach ($errors->all() as $key => $message)
             
                                @if ($message == 'Usuario inactivo o sin código, no puede iniciar sessión')
                                    <strong>Usuario inactivo o sin código, no puede iniciar sesión.</strong>

                                @elseif ($key== 0 && $message != 'Estas credenciales no coinciden con nuestros registros.')
                                    <strong>Estas credenciales no coinciden con nuestros registros.</strong>

                               
                                @elseif ($message == 'Estas credenciales no coinciden con nuestros registros.')
                                    <strong>Estas credenciales no coinciden con nuestros registros.</strong>
                               

                                @endif
                            @endforeach
                           
                        </div>
                    @endif

                </div>
        </div>

        {{-- Password field --}}
        <div class="row">
            <div class="col-2"></div>
            <div class="mb-3 col-8">
                <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                       placeholder="CONTRASEÑA" value="{{old('password')}}">
               
                @if($errors->has('password'))
                    <div class="invalid-feedback">
                        <strong>{{ __('auth.password')}}</strong>
                    </div>
                @endif
            </div>
        </div>

        {{-- Login field --}}
        <div class="row">
        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
        </div>  
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <button type=submit class="btn btn-block  btn-warning">
                    <b style="color:#2C2E88;"><strong>ENTRAR</strong></b>
                </button>
            </div>
            <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
        </div> 
        </div>
    </form><br>

 

    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
        <a href="{{ route('forget.password.get') }}" style="color:#2C2E88;">
                ¿Olvidaste tu contraseña?
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
            <a href="{{ $register_url }}" style="color:#2C2E88;">
                <b>REGÍSTRATE AQUÍ</b>
            </a>
        </div>
    </div>  <br>
@stop

<style type="text/css">

    .card-primary.card-outline {
        border-top: 1px solid #fff !important;
      
    }

    .login-card-body, .register-card-body {
        background: none !important; 
        border-top: 0;
        color: #666;
      
    }

    body{
        font-family: 'Montserrat', sans-serif !important;
        background-color: #24298D !important;
    }
</style>