@extends('adminlte::master')

@php( $dashboard_url = View::getSection('dashboard_url') ?? config('adminlte.dashboard_url', 'home') )

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

@section('classes_body'){{ ($auth_type ?? 'login') . '-page' }}@stop


@section('body')
    <div class=" col-lg-4 mt-3">

        {{-- Logo --}}
       <div class="{{ $auth_type ?? 'login' }}-logo">
          {{--   <a href="{{ $dashboard_url }}">
                <img src="{{ asset(config('adminlte.logo_img')) }}" height="50">
                {!! config('adminlte.logo', '<b>Venezuelacargo</b>') !!}
            </a>--}}
        </div>

      

        {{-- Card Box --}}
        <div class="card card-success card-outline">
            <div class="card-header">
                <h3 class="card-title float-none text-center">
                     <i class="icon fas fa-check  text-success p-2 "></i><b>Registro exitoso!</b>       
                </h3>
            </div>
   
            {{-- Card Body --}}
            <div class="card-body  ">
                Gracias por su registro, recibirá un correo de confirmación.
            </div>

            {{-- Card Footer --}}
            <div class="card-footer">
               <div class="col-3 mb-1 float-right">
                <a  href="{{route('login')}}" 
                class="btn btn-block  btn-primary ">
                    Aceptar
                </a>
            </div>
            </div>
   

        </div>

    </div>
@stop

@section('adminlte_js')
    @stack('js')
    @yield('js')
@stop
