@extends('adminlte::auth.auth-page', ['auth_type' => 'register'])
@section('adminlte_css_pre')
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
@stop

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
@endif


@section('auth_body')

    <form action="{{ $register_url }}" method="post" id="_form_register">
        {{ csrf_field() }}

        {{-- Name field --}}
        <fieldset>
            <legend style="color:#24298D; font-size:18px; font-weight: 600;">DATOS PERSONALES</legend>
   
                <div class="row">
                    <input type="hidden" name="longitude" value="" id="longitude">
                    <input type="hidden" name="latitude" value="" id="latitude">

                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                value="{{ old('name') }}" placeholder="NOMBRE" style="font-size:14px !important;" autofocus>
                    
                            @if($errors->has('name'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
                            <input type="text" name="last_name" class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}"
                                value="{{ old('last_name') }}" placeholder="APELLIDO" style="font-size:14px !important;" autofocus>
                        
                            @if($errors->has('last_name'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center ">
                            <input type="text" name="dni" id="dni" class="form-control {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                                value="{{ old('dni') }}" placeholder="CÉDULA" style="font-size:14px !important;" autofocus>
                        
                            @if($errors->has('dni'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('dni') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
                            <input type="text" name="phone" id="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                                    value="{{ old('phone') }}" placeholder="TELÉFONO" style="font-size:14px !important;">
                                
                                @if($errors->has('phone'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </div>
                                @endif

                        </div>

                        
                </div>
        </fieldset>
        <hr>
        <fieldset>
            <legend style="color:#24298D; font-size:18px; font-weight: 600;">PERFIL DE ACCESO</legend>
            
                <div class="row">

                <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
                            <input type="text" name="username" id="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                                value="{{ old('username') }}" placeholder="USUARIO" style="font-size:14px !important;" autofocus>
                        
                            @if($errors->has('username'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </div>
                            @endif
                        </div>

                <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
                <input type="email" name="email" style="text-transform:uppercase;" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                        value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}" style="font-size:14px !important;">
                    
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif

                </div>

                

                <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
                {{-- Password field --}}
                <div class="">
                    <input type="password" name="password" style="text-transform:uppercase;"
                            class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                            placeholder="{{ __('adminlte::adminlte.password') }}" style="font-size:14px !important;">
                
                    @if($errors->has('password'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
                </div>

                <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-center">
                {{-- Password field --}}
                <div class="">
                    <input type="password" name="password_confirmation"
                            class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                            placeholder="CONFIRMA TU CONTRASEÑA" style="font-size:14px !important;">
                
                    @if($errors->has('password_confirmation'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </div>
                    @endif
                </div>
                </div>
                </div>
        </fieldset>
        <hr>

       
            
        <fieldset>
            <legend style="color:#24298D; font-size:18px; font-weight: 600;">DIRECCIÓN DE ENTREGA</legend>
            
                <div class="row">

                    <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 text-center">
                        <select class="form-control" id="country_id" name="country_id" style="text-transform:uppercase; font-size:14px !important;">

                            <option value="{{ $country->id}}">{{$country->name}}</option>

                        </select>

                        @if($errors->has('country_id'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('country_id') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 text-center">
                        <select class="form-control" id="state_id" name="state_id" style="text-transform:uppercase; font-size:14px !important;">
                            @foreach($country->states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>

                        @if($errors->has('state_id'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('state_id') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 text-center">
                        <select class="form-control" id="city_id" name="city_id" style="text-transform:uppercase; font-size:14px !important;">
                            <option value="0">Ciudad</option>
                        </select>

                        @if($errors->has('city_id'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('city_id') }}</strong>
                            </div>
                        @endif
                    </div>


                    <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                        <input type="text" name="address" id="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}"
                            value="{{ old('address') }}" placeholder="DIRECCIÓN" style="font-size:14px !important;">
                        
                            @if($errors->has('address'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </div>
                            @endif
                    </div>
                </div>
            <br>

        <div class="row">
             <div class="col-11  mb-3">
                <input type="text" name="address1" id="address1" class="form-control {{ $errors->has('address1') ? 'is-invalid' : '' }}"
                       value="{{ old('address1') }}" placeholder="" readonly="readonly">
                   
                    @if($errors->has('address1'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('address1') }}</strong>
                        </div>
                    @endif
            </div>

            <div class="col-md-1">
                <button id="locate" class="btn btn-block  btn-warning" style="color:#2C2E88;" type="button" alt="Buscar Direccion">
                    <i class="fa fa-search"></i>
                </button> 
            </div>

        </div>
        </fieldset>
            <hr>
        <div class="col-xs-10 alert alert" style="background:#ffebba; border: 2px bold #ffeebba; color:#24298D; font-size:14px; font-weight:600;">
            Especifique correctamenta la ubicación que será usada como la dirección única de entrega de paquetes. Una vez creada, no podrá ser modificada sin autorización del administrador.
        </div>

        <div class="col-xs-10">
            {{--<iframe class="embed-responsive" id="map_frame" src="{{ url('/users/iframeMap') }}" width=auto height=320/></iframe>--}}

            <div id="map_frame" class="embed-responsive" style="width:100%;display:block;background:#F2F4F4; height: 450px !important; border-style: solid; border-width: 0.1px;"></div>
        </div>


        {{-- Register button --}}
        <div class="row mt-2  float-right">
            <div class="col-12">
                <button type="button" class="btn btn-block  btn-warning" style="color:#2C2E88; margin-top:50px; font-weight:600; margin-bottom:10px;" id="_btn_save">
                    <strong>GUARDAR</strong> 
                </button>
                <p class="my-0">
                    <a href="{{ $login_url }}" style="color:#2C2E88; font-weight:400; font-size:14px; margin-bottom:20px;">
                    YA TENGO CUENTA
                    </a>
                </p>
            </div>
        </div>

    </form>

@stop

@section('auth_footer')
    
@stop
@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap'
    rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

<style type="text/css">
    body {
        font-family: 'Montserrat', sans-serif !important;
    }

    .modal {
   width: 500px;
   height: 300px;
   position: absolute;
   left: 50%;
   top: 50%; 
   margin-left: -150px;
   margin-top: -150px;
}


</style>
@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    _id_state=0
    _id_city=0
    _id_location=0
    

    $("#dni").keyup(function (){
         this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    //Select state - return cities
    $("#state_id").on('change, click',function(e){

        //e.stopPropagation();
        _id_state = $("#state_id").val();
        console.log(_id_state);
        $.ajax({
            type: 'get',  
            dataType: 'json',
            async: false,
                        //headers: {
                          //  "cache-control": "no-cache"
                         // },
            url: "<?=url('/findCities')?>/"+ _id_state,

            success: function(res) {
                $("#city_id").html('');
                _cad = '';
                
                $.each(res.cities, function (index, val) {
                    _cad+= `<option value="${val.id}">
                    ${val.name}</option>` 
                });

                $("#city_id").html(_cad);
                console.log(res.cities);
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        }); 
    })


   /*$("#city_id").on('blur, click',function(e){
        console.log(e.stopPropagation());
        //localize_address()
    })*/

    $('#state_id').trigger('click');
    $('#city_id').trigger('click');
    $('#state_id > option[value="'+_id_state+'"]').prop('selected', 'selected').trigger('click');
    $('#city_id > option[value="'+_id_state+'"]').prop('selected', 'selected').trigger('click');


    //Map and Locator 
    _lat=0
    _lon=0

    function position(){
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(function(objPosition)
            {
                _lon = objPosition.coords.longitude;
                _lat = objPosition.coords.latitude;

            });
        }
    }

   
 
    function localize_address(){
        _cad='';
        $("#address1").val('');


        if($("#address").val() !== ''){
            _cad += $("#address").val()+', '

        if($("#city_id").val() !== ''){
           _cad +=  $('select[name="city_id"] option:selected').text()+', '
        }

        if($("#state_id").val() !== ''){
            _cad +=  $('select[name="state_id"] option:selected').text()+', ';
        }

    
        _cad+=' VEN'

        }

        _cad = _cad.replace(/\s+/g, ' ')  
        $("#address1").val(_cad)

        /*/$.ajax({
            type: 'get',  
            dataType: 'json',
            url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates',
            data: { 

                SingleLine : $("#address1").val(),
                f: 'json',
                sourceCountry:'VEN',
                city:$('select[name="city_id"] option:selected').text(),
                region:$('select[name="state_id"] option:selected').text(),
                outFields: '*',
                outSR: {"wkid":102100,"latestWkid":3857}
            },

            success: function(res) {
                console.log(res)
                _long = res.candidates[0].location.x
                _lati = res.candidates[0].location.y

                //_base_url = "<?=url('/users/iframeMapFind')?>"

                //_url = _base_url+'/'+_long+'/'+_lati

                //$('#map_frame').attr('src', _url);
                //$("#longitude").val(_long)
                //$("#latitude").val(_lati)


            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });*/
    }

    $("#_btn_save").on('click', function(){
        
        localize_address()
        $("#_form_register").submit();

    });


    function localizar(elemento,direccion) {
        var geocoder = new google.maps.Geocoder();

        var map = new google.maps.Map(document.getElementById(elemento), {
          zoom: 15,
          scrollwheel: true,
          mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
        });
        
        geocoder.geocode({'address': direccion}, function(results, status) {
            if (status === 'OK') {
                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
                
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });

                $("#longitude").val(resultados_long)
                $("#latitude").val(resultados_lat)


            } else {
                var mensajeError = "";
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                //alert(mensajeError);
            }
        });
    }
    
    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCWV62-MOsXA-ifSUGAb5HXtyikHDVtuJU", function() {


        var mapProp= {
          center:new google.maps.LatLng(6.42375,-66.58973),
          zoom:6,
        };
        var map = new google.maps.Map(document.getElementById("map_frame"),mapProp);

        $("#locate").click(function() {
            localize_address()
            var direccion = $("#address1").val();
            if (direccion !== "") {
                localizar("map_frame", direccion);
            }
        });


        $("#address").keypress(function( event ) {
            if ( event.which == 13 ) {
            event.preventDefault();
            localize_address()
            var direccion = $("#address1").val();
            if (direccion !== "") {
                localizar("map_frame", direccion);
            }
          }
        });

        $("#address").blur(function( event ) {
            localize_address()
            var direccion = $("#address1").val();
            if (direccion !== "") {
                localizar("map_frame", direccion);
            }
        });
    });
  });
</script>
@stop