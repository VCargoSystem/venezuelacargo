@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])

@php( $password_email_url = View::getSection('password_email_url') ?? config('adminlte.password_email_url', 'password/email') )

@if (config('adminlte.use_route_url', false))
    @php( $password_email_url = $password_email_url ? route($password_email_url) : '' )
@else
    @php( $password_email_url = $password_email_url ? url($password_email_url) : '' )
@endif


@section('auth_header', __('adminlte::adminlte.password_reset_message'))

@section('auth_body')

    @if(session('status'))
       <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif


    <form action="" method="post">
        {{ csrf_field() }}

        {{-- Email field --}}
        <div class="input-group mb-3">
            <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                   value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}" autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
            @if($errors->has('email'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
        </div>

       {{-- Send reset link button --}}
        <button type="submit" class="btn btn-block btn-warning">
            <span class="fas fa-share-square"></span>
            <b style="color:#2C2E88;"><strong>ENVIAR CORREO</strong></b>
            
        </button>

    </form>

@stop
<style type="text/css">

    .card-title{
        color: #2C2E88;
        text-transform: uppercase;
    }
    
    .card-primary.card-outline {
        border-top: 1px solid #fff !important;
      
    }

    .login-card-body, .register-card-body {
        background: none !important; 
        /*border-top: 0;*/
        color: #666;
    }

    body{
        font-family: 'Montserrat', sans-serif !important;
        background-color: #24298D !important;
    }
</style>