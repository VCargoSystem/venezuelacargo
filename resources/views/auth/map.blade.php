<!DOCTYPE html>
<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=7, IE=9, IE=10">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
    <title>Simple Map</title>
    

    <style>
      html, body, #map {
        height: 100%;
        width: 100%;
        margin: 0;
        padding: 0;
      }
      body {
        background-color: #FFF;
        overflow: hidden;
        font-family: "Trebuchet MS";
      }
    </style>

    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWV62-MOsXA-ifSUGAb5HXtyikHDVtuJU&callback=myMap"
      async
    ></script>
    <script type="text/javascript">
      function myMap() {
        var mapProp= {
          center:new google.maps.LatLng(51.508742,-0.120850),
          zoom:5,
        };
        var map = new google.maps.Map(document.getElementById("map"),mapProp);
      }
    </script>
  </head>

  <body>

    <div id="map"></div>
  </body>
</html>

          