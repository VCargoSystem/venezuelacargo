@extends('adminlte::page')

@section('title', 'INSTRUCCIONES')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">INSTRUCCIONES</b></h4>
@stop

@section('content')


@if (session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close close2" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            <li> {!! session()->get('success')!!}</li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close1 close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>
<div class="alert alert-warning alert-danger2" style="display: none">
   <button type="button" class="close1 close"  >×</button>
    <span id="msg-danger"></span>
</div>

  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 pr-3">
           <div class="row float-right ">
              @php $date = \Carbon\Carbon::now()->locale('es_VE') @endphp
              <p><b>FECHA: </b> {{   $date->isoFormat('dddd D [de] MMMM [de] YYYY')}}
              <b class="pl-5">HORA: </b> {{   $date->isoFormat('hh:mm A')}}
              </p>
            </div>
          </div> 

          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
                <div class="card-header">
                        <form class="">
                            <div class="row">
                               
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                   <label for="ware_house" style="font-size:15px !important; letter-spacing: -0.5px !important;">WAREHOUSE</label>
                                  <input type="text" name="ware_house" id="ware_house" style="font-size:14px !important; letter-spacing: -0.5px !important;"
                                      class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} mr-1"
                                      value="" placeholder="Nro. Warehouse" autofocus maxlength="40">
                              </div>
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                <label for="date" style="font-size:15px !important; letter-spacing: -0.5px !important;">DESDE</label>
                                    <input type="date" name="date" id="date" style="font-size:14px !important; letter-spacing: -0.5px !important;"
                                        class="form-control form-control-sm  {{ $errors->has('date') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Fecha de registro - Desde" autofocus maxlength="20">
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                <label for="date1" style="font-size:15px !important; letter-spacing: -0.5px !important;">HASTA</label>
                                    <input type="date" name="date1" id="date1"
                                        class="form-control form-control-sm  {{ $errors->has('date1') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Fecha de registro - Hasta" autofocus maxlength="20">
                                </div>
                            </div>
                            <div style="height:7px;"></div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                <label for="tracking" style="font-size:15px !important; letter-spacing: -0.5px !important;">TRACKING</label>
                                    <input type="text" name="tracking" id="tracking" style="font-size:14px !important; letter-spacing: -0.5px !important;"
                                        class="form-control form-control-sm  {{ $errors->has('tracking') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Tracking" autofocus maxlength="50">
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6 pt-4">
                                    <button id="_btn_filter"
                                        class=" ml-1 btn btn-primary btn-sm col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                        style=" color: #fff; background-color: #23298a; border-color: #23298a; box-shadow: none; margin-top: 7px !important; margin-left:0px !important;"><i class="fas fa-search"></i>&nbsp;BUSCAR</button>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6 pt-4">
                                    <button id="_btn_refresh"
                                        class=" ml-1 btn btn-primary btn-sm  col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                        style=" color: #fff; background-color: #23298a; border-color: #23298a; box-shadow: none; margin-top: 7px !important; margin-left:0px !important; min-height: 31px">
                                        <i class="fas fa-eraser"></i>&nbsp;LIMPIAR CAMPOS
                                    </button>
                                </div>
                                </div>
                                </div>
                                
                            </div>
                            <div style="height:7px;"></div>
                            <div class="row">
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                                
                              </div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                            </div>
                    
                        </form>
                </div>


              <div class="card-body">
                 <div class="table-responsive">
                    <table class="table table-striped table-responsive-lg table-responsive-md table-responsive-sm table-responsive-xs table-bordered datatable table-bordered" style="font-size:15px !important; letter-spacing: -0.5px !important;">

                      <!--Table head-->
                      <thead>
                        <tr>
                          <th width="10%" style="background:#24298d; color:#ffb901;">WAREHOUSE</th>
                          <th width="20%" style="background:#24298d; color:#ffb901;">TRACKING</th>
                          <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;">MEDIDAS</th>
                          <th width="10%" class="text-center"style="background:#24298d; color:#ffb901;">CAJAS</th>
                          <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;">HOLD</th>
                          <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;">SEGURO</th>
                          <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;">REEMPAQUE</th>
                          <th width="10%" style="background:#24298d; color:#ffb901;">ENVÍO</th>
                          <th width="10%" style="background:#24298d; color:#ffb901;">ACCIÓN</th>
                        </tr>
                      </thead>
                      <!--Table head-->

                      <!--Table body-->
                      <tbody>
                       
                      </tbody>
                    </table>
                  </div>

              </div>

              <!-- /.card-body -->
              <div class="card-footer clearfix">
            
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

<!--modal asignar code-->
<section class="content">
    <div class="modal fade show" id="modal_preview" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="">
              <button type="button" class="close pr-3 pt-2" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body" style="min-height: 300px !important; max-height: 350px !important">
              <img src=""  border="0" width="300" class="img-fluid" align="center" style="max-width: 300px !important; max-height: 350px !important" id="image_details"/>
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade show" id="modal_instrucction" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">INSTRUCCIONES DE ENVÍO</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="needs-validation" id="_frm_instrucctions" method="post" action="{{ route('clients.instrucctions.store') }}">
                  {{ csrf_field() }}
                  {{ method_field('POST') }}
                  <input type="hidden" name="id" id="id">

                  <div class="col-12">
                    <label style="color:#24298d; font-weight:900;">GUARDAR EN ALMACEN MIAMI</label>
            
                      <select class="form-control" name="hold" required>
                        <option value="" selected="selected">Seleccione</option>
                        <option value="1">Si</option>
                        <option value="0">No</option>
                      </select>
                      <div class="invalid-feedback">Seleccione</div>
                      <small style="color:#ff9901;">Si activa esta instrucción su paquete no será enviado hasta que usted lo indique</small>
                  </div>
                  <div class="col-12 mt-2">
                    <label style="color:#24298d; font-weight:900;">SEGURO</label>
                    
                     <div class="input-group mb-3">
                        <input type="text" step="0.01" name="insured" id="insured" class="form-control {{ $errors->has('insured') ? 'is-invalid' : '' }} decimales"
                          value="{{ old('insured')  }}" placeholder="Ingrese el Monto" autofocus maxlength="10" min="0.01">
                        <div class="input-group-append">
                          <span class="input-group-text">$</span>
                        </div>
                        @if($errors->has('insured'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('insured') }}</strong>
                            </div>
                        @endif
                        <small style="color:#ff9901;">Debe indicar el monto por el cuál asegura su paquete, para el caso contrario haga caso omiso a la instrucción</small>
                    </div>
                  </div>
                  <div class="col-12 mt-2">
                    <label style="color:#24298d; font-weight:900;">REEMPAQUE</label>
                   
                    <select class="form-control" name="repackage" id="_repackage">
                      <option value="2">Seleccione</option>
                      <option value="1">Si</option>
                      <option value="0">No</option>
                    </select>
                     <div class="invalid-feedback">Seleccione</div>
                     <small style="color:#ff9901;">Indiqué si desea que su paquete sea reempacado</small>
                  </div>
                  <div class="col-12 mt-2">
                    <label style="color:#24298d; font-weight:900;">TIPO DE ENVÍO</label>
                    
                    <select class="form-control" name="shippins_id">
                      <option value="">Seleccione</option>
                      <option value="1">MARÍTIMO</option>
                      <option value="2">AÉREO</option>
                      
                    </select>
                     <div class="invalid-feedback">Seleccione</div>
                     <small style="color:#ff9901;">Indiqué el tipo de envío que desea para su paquete</small>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
              <button type="button" class="btn btn-primary save_instrucctions" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;"s>GUARDAR</button>
             
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade show" id="modal_update" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">EDITAR INSTRUCCIONES DE ENVÍO</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="needs-validation" id="_frm_update" method="post" action="">
                  {{ csrf_field() }}
                  {{ method_field('PATCH') }}
                  <input type="hidden" name="id_edit" id="id_edit">

                  <div class="col-12">
                    <label style="color:#24298d; font-weight:900;">GUARDAR EN ALMACEN MIAMI</label>
                    
                      <select class="form-control" name="_hold" id="_hold" required>
                        <option value="" selected="selected">Seleccione</option>
                        <option value="1">Si</option>
                        <option value="0">No</option>
                      </select>
                      <div class="invalid-feedback">Seleccione</div>
                      <small style="color:#ff9901;">Si activa esta instrucción su paquete no será enviado hasta que usted lo indique</small>
                  </div>
                  <div class="col-12 mt-2">
                    <label style="color:#24298d; font-weight:900;">SEGURO</label>
                    
                    <div class="input-group mb-3">
                        <input type="text" step="0.01" name="_insured" id="_insured" class="form-control {{ $errors->has('insured') ? 'is-invalid' : '' }} decimales"
                          value="" placeholder="Ingrese el Monto" autofocus maxlength="20" min="0.01">
                        <div class="input-group-append">
                          <span class="input-group-text">$</span>
                        </div>
                        @if($errors->has('insured'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('insured') }}</strong>
                            </div>
                        @endif
                        <small style="color:#ff9901;">Debe indicar el monto por el cuál asegura su paquete, para el caso contrario haga caso omiso a la instrucción</small>
                    </div>
                  </div>
                  <div class="col-12 mt-2">
                    <label style="color:#24298d; font-weight:900;">REEMPAQUE</label>
                    
                    <select class="form-control" name="_repackage" id="_repackage" required>
                     <option value="">Seleccione</option>
                      <option value="1">Si</option>
                      <option value="0">No</option>
                    </select>
                     <div class="invalid-feedback">Seleccione</div>
                     <small style="color:#ff9901;">Indiqué si desea que su paquete sea reempacado</small>
                  </div>
                  <div class="col-12 mt-2">
                    <label style="color:#24298d; font-weight:900;">TIPO DE ENVÍO</label>
                    
                    <select class="form-control" name="_shippins_id" id="_shippins_id" required>
                      <option value="">Seleccione</option>
                      <option value="1">MARÍTIMO</option>
                      <option value="2">AÉREO</option>
                      
                    </select>
                     <div class="invalid-feedback">Seleccione</div>
                     <small style="color:#ff9901;">Indiqué el tipo de envío que desea para su paquete</small>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
              <button type="button" class="btn btn-primary update_instrucctions" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR</button>
             
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

       <!--  tracking -->
    <div class="modal fade show modal-md" id="modal_tracking" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">SEGUIMIENTO DEL PAQUETE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row col-12">
                <iframe class="embed-responsive" src="{{ route('packages.iframeMap') }}" height="320" width="auto" id="map_frame"></iframe>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</section>
@stop

@section('css')
  <link rel="stylesheet" href="/css/admin_custom.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

</head>
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }
  .tracking{
    word-break: break-all;
  }

  .select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;
    min-height: 30px;
  }

  .form-control {
    display: block;
    width: 100%;
    height: calc(1.95rem + 1px) !important;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-shadow: inset 0 0 0 transparent;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
  }


  .input-group-append {
      margin-left: -1px;
      max-height: 32px !important;
  }

  .table td, .table th {
        padding: .40rem !important;
        vertical-align: middle !important;
        border-top: 1px solid #dee2e6;
        font-size:15px;
    }

    div.dataTables_wrapper .dataTables_info {
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination{
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-green {
    border-top: solid 10px #38c53e !important;
 }
</style>

@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>

<script type="text/javascript">

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(document).ready(function() {
  $(".alert2").css('display', 'none')
  $(".alert-danger2").css('display', 'none')

    $('.select2').select2();
      $('.select2-selection').css('max-height','25px')

      $('.select2').select2({
            placeholder: 'Seleccionar Usuario',
            ajax: {
                url: "{{ route('clients.users.usersAutocomplete') }}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name+' '+item.last_name+' - '+item.code,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

    // init datatable. 
    var dataTable = $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        pageLength: 20,
        searching: false, 
        paging: true,
        bLengthChange: false,
        "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        // scrollX: true,
        "order": [[ 0, "desc" ]],
        ajax: {
          url: "{{ route('clients.instrucctions.getFilterPackages') }}",
          data: function(d){
            d.tracking = $('#tracking').val(),
            d.ware_house = $('#ware_house').val(),
            
            d.date = $('#date').val(),
            d.date1 = $('#date1').val()
          }
        },
        columns: [

            {data: 'ware_house', name: 'ware_house',},
            {data: 'tracking', name: 'tracking',},
            {data: 'dimensions', sClass:'text-center', 'searchable': false, orderable:false, render:function ( data, type, row ) {  return row.width+' x '+row.high+' x '+row.long;
            }},
            {data: 'nro_box', sClass:'text-center', name: 'nro_box'},
            {data: 'hold', 'searchable': false, render:function ( data, type, row ) {  
              return row.hold==1 ? 'Si':'No';
            }},
            {data: 'insured', sClass:'text-center', 'searchable': false, render:function ( data, type, row ) { 

              _seguro = ''
              if (row.insured > 0 ){
                _seguro = row.insured
              }

              return _seguro;
            }},
            {data: 'repackage', sClass:'text-center', 'searchable': false, render:function ( data, type, row ) {  
              return row.repackage==1 ? 'Si':'No';
            }},
            {data: 'shippins_id', 'searchable': false, render:function ( data, type, row ) {  
              return row.shippins_id==1 ? 'MARITIMO':'AEREO';
            }},

            {data: 'actions', name: 'actions',orderable:false,serachable:false,sClass:'text-center'}
        ]
    });
   
  //Delete user
  $('body').on('click', '.delete', function(e) {
    e.preventDefault();
    _id = $(this).attr("data-id");

    $.confirm({
          title: 'Eliminar Paquete!',
          content: 'Seguro desea eliminar el registro?',
          type: 'red',
          typeAnimated: true,
          buttons: {
              delete: {
                  text: 'Eliminar',
                  btnClass: 'btn-red',
                  action: function(){
                    deletePackage(_id)
                  }
              },
              cancelar: function () {
              }
          }
    });

  });


  $('.close1').on('click', function(e) {
    $(this).parent().fadeToggle("slow");
  });

  $('#search').on('keyup', function(e) {
      e.preventDefault();
      dataTable.draw();
  });

  //assing instrucctions
  $('body').on('click', '.instrucction', function(e) {
    e.preventDefault();
    $("#id").val($(this).attr('data-id'))

    $.ajax({
        url: _url = "<?=url("/cliente/instrucctions/assing")?>/"+$("#id").val(),
        type: "GET",
        dataType: 'json',
      
        success: function (data) {
            if(data.success==0){
              $("#msg-danger").html(data.message)
              $(".alert-danger2").css('display', 'block')
            }else{
               $("#modal_instrucction").modal("show");
            }
            //dataTable.draw() 
        },
        error: function (data) {

        }
    });
  });

  $('.save_instrucctions').on('click', function(e) {
    e.preventDefault();

    var form = $("#_frm_instrucctions")

    if (form[0].checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
       form.addClass('was-validated');
      return false;
    }else{
        if($("#_repackage").val() == 1){
        $.confirm({
            title: 'Guardar Paquete!',
            content: 'Seguro desea guardar el registro?',
            type: 'warning',
            typeAnimated: true,
            buttons: {
                aceptar: {
                    text: 'aceptar',
                    btnClass: 'btn-blue',
                    action: function(){
                      $("#_frm_instrucctions").submit()
                    }
                },
                cancelar: function () {
                }
            }
        });

      }else{
         $("#_frm_instrucctions").submit()
      }
    }

  });

   //assing instrucctions
  $('body').on('click', '.edit', function(e) {
    e.preventDefault();
    _id = $(this).attr('data-id')
    $("#id_edit").val(_id)

    $.ajax({
        url: _url = "<?=url("/cliente/instrucctions")?>/"+_id+"/edit",
        type: "GET",
        dataType: 'json',
      
        success: function (data) {
            if(data.success==0){
              $("#msg-danger").html(data.message)
              $(".alert-danger2").css('display', 'block')
            }else{
              $('#_hold option[value="'+data.package.hold+'"]').attr("selected", true);
              $('#_insured').val(data.package.insured);
              $('#_repackage option[value="'+data.package.repackage+'"]').attr("selected", true);
              $('#_shippins_id option[value="'+data.package.shippins_id+'"]').attr("selected", true);

              $("#modal_update").modal("show");
            }
            //dataTable.draw() 
        },
        error: function (data) {

        }
    });
  });

  $('.update_instrucctions').on('click', function(e) {
    e.preventDefault();
    form = $("#_frm_update")

    if (form[0].checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
       form.addClass('was-validated');
      return false;
    }else{
        _url = "{{ route('clients.instrucctions.update',"+ $('#id_edit').val()+") }}"

      $("#_frm_update").attr('action', _url)

      if($("#_repackage").val() == 1){
              $.confirm({
                  title: 'Guardar Paquete!',
                  content: 'Seguro desea guardar el registro?',
                  type: 'warning',
                  typeAnimated: true,
                  buttons: {
                      aceptar: {
                          text: 'aceptar',
                          btnClass: 'btn-blue',
                          action: function(){
                            $("#_frm_update").submit()
                          }
                      },
                      cancelar: function () {
                   
                      }
                  }
              });

      }else{
         $("#_frm_update").submit()
      }
    }

  });

    $('#_btn_filter').on('click', function(e) {
    e.preventDefault();
    arr = [];
    let _cont = 0
    let sw =0
    arr[0] = $("#ware_house").val()
    arr[1] = $("#tracking").val()
    
  

    arr[3] = $("#date").val()
    arr[4] = $("#date1").val()
  


    for (let i in arr) {
      if(i < 5 &&  arr[i] != ""){
        _cont++
      }
     
    }

    if(_cont > 1 && (arr[3] =="" && arr[4] =="")){
      sw = 1
    }else if(_cont > 2 && (arr[3] !="" && arr[4] !="")){
      sw = 1
    }else if(_cont > 1 && (arr[3] !="" && arr[4] =="")){
      sw = 1
    }else if(_cont > 1 && (arr[3] =="" && arr[4] !="")){
      sw = 1
    }

    if(sw == 1){
        $.confirm({
            title: 'Filtro de busquedas!',
            content: 'No puede seleccionar mas de dos criterios',
            type: 'blue',
            typeAnimated: true,

            buttons: {
              cancelar: function () {

              }
            }
        });
      
      return false;
    }

    if(_cont == 0 ){
      $.confirm({
          title: 'Filtro de busquedas!',
          content: 'Debe seleccionar algun criterio',
          type: 'blue',
          typeAnimated: true,

          buttons: {
            cancelar: function () {

            }
          }
      });
      return false;
    }

  if(_cont == 1 && (arr[3] !="" && arr[4] =="")){
      $.confirm({
          title: 'Filtro de busquedas!',
          content: 'Debe seleccionar las dos fechas',
          type: 'blue',
          typeAnimated: true,

          buttons: {
            cancelar: function () {

            }
          }
      });
      return false;
    }

     if(arr[3] !="" && arr[4] !=""){
      let date1 = new Date($("#date").val());
      let date2 = new Date($("#date1").val());

      if(date1 > date2){
        $.confirm({
          title: 'Filtro de busquedas!',
          content: 'Fecha Hasta no puede ser menor que Fecha Desde',
          type: 'blue',
          typeAnimated: true,

          buttons: {
            cancelar: function () {

            }
          }
        });
        return false;
      }
    }

    dataTable.draw()  
  });

  
  $('body').on('click', '.shipping', function(e) {
    e.preventDefault();

    _lati =  $(this).attr('data-latitude')
    _long = $(this).attr('data-longitude')

    _base_url = "<?=url('cliente/instrucctions/iframeMapFind')?>"
    _url = _base_url+'/'+_long+'/'+_lati


    $('#map_frame').attr('src', _url);

    $("#modal_tracking").modal("show");
    $("#map").css('width','600')
    $("#map_root").width(600)

  });

  $('#_btn_refresh').on('click', function(e) {
      e.preventDefault();
      
      $("#date").val('')
      $("#date1").val('')
      $("#tracking").val('')
      $("#ware_house").val('')

      $("#usuario").val('').trigger('change')

      dataTable.draw()
  });

});
    
</script>
@stop
