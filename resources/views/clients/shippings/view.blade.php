@extends('adminlte::page')
@section('title', 'Envios')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">DETALLE DEL ENVÍO</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content" style="font-size:15px !important; letter-spacing: -0.5px !important;">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                     
                          <div class="row">
                             <div class="row col-5">
                             
                              <div class="col-12">
                              <label><strong>WAREHOUSE:</strong> </label><span class="pl-2">{{ $invoice->id }}</span><br>
      
                              <label> <strong> NRO DE FACTURA:</strong> </label><span class="pl-2">{{ $invoice->id }}</span>
                              <hr>
                                <label class="text-primary">DATOS BÁSICOS</label>
                                <ul class="nav nav-pills flex-column">
                                  <li>
                                      <label>Cliente nro: </label> <span class="pl-2">{{ $user->code }}
                                      </span>
                                  </li>
                                  <li>
                                      <label>Cliente: </label> <span class="pl-2">{{  \Str::upper($user->name.' '.$user->last_name) }}
                                      </span>
                                  </li>
                                 
                                  
                                </ul>
                                <hr>
                              
                                
  
                            
                                <label class="text-primary">MEDIDAS</label>
                                <ul class="nav nav-pills flex-column">
                        
                                  <li>
                                      <label>Peso: </label> <span class="pl-2">{{ $invoice->total_w }} LBS
                                      </span>
                                  </li>
                                  <li>
                                      <label>Volumen: </label> <span class="pl-2">{{ $invoice->total_volume }}
                                      </span>
                                  </li>
                                  <li>
                                      <label>Pie cúbico: </label> <span class="pl-2">{{ $invoice->total_pie }}
                                      </span>
                                  </li>
                                  <li>
                                      <label>Número de piezas: </label> <span class="pl-2">{{ $invoice->total_pieces }}
                                      </span>
                                  </li>
                                  <li>
                                      <label>Contenido: </label> <span class="pl-2">
                                      <p>
                                        @foreach($packages as $pak) 
                                          @if($pak->package_id > 0)
                                              {{ $pak->description }},&nbsp;
                                          @else
                                              @foreach($pak->descriptions as $w) 
                                                  {{ $w }},&nbsp;
                                              @endforeach
                                          @endif
                                        @endforeach
                                        </p>
                                      </span>
                                  </li>
                                </ul>
                                <hr>
                              </div>
                      
                                  
                              <div class="col-12">
                                <label class="text-primary">INSTRUCCIONES</label>
                                  <ul class="nav nav-pills flex-column">
                                    <li class="nav-item">
                                        <label>Almacén: </label> <span class="pl-2">{{ $invoice->hold==1? 'Guardar en Almacen': 'No Guardar en Almacen' }} 
                                        </span>
                                    </li>
                                    <li class="nav-item">
                                        <label>Seguro: </label> <span class="pl-2">{{ $invoice->cost_insured ? $invoice->cost_insured : 0}} 
                                        </span>
                                    </li>
                                    <li class="nav-item">
                                        <label>Reempaque: </label> <span class="pl-2">{{ $invoice->invoice_repackage == 1? 'Con reempaque': 'Sin reempaque' }} 
                                        </span>
                                    </li>
                                    <li class="nav-item">
                                        <label>Tipo de envío: </label> <span class="pl-2">{{ $invoice->shippins_id == 2? 'AÉREO': 'MARÍTIMO' }} 
                                        </span>
                                    </li>
                          
                                    <li class="nav-item">
                                        <label>Número de Container: </label> <span class="pl-2">{{ $invoice->nro_contenedor}} 
                                        </span>
                                    </li>
                                  </ul>
                              </div>
                            </div>
                              
                            <div class="col-7">
                            <div class="col-12">
                                <label class="text-primary">LISTADO DE RECIBOS</label>
                                  <div class="row card mb-2 border rounded-lg  bg-light" style="max-height: 170px !important; min-height: 160px !important; font-size: 13px">

                                    <div class="card-body pt-2" style="display: block;">
                                        <ul id="list_tracking" class="nav nav-pills flex-column">

                                           <li class="nav-item ">
                                           <p>
                                            @foreach($packages as $pak) 
                                                
                                                @if($pak->package_id > 0)
                                                    {{ $pak->ware_house }}&nbsp;
                                                @endif
                                            @endforeach

                                            @foreach($packages as $pak)
                                                @if($pak->package_id > 0) 
                                                    &nbsp;
                                                @endif
                                            @endforeach

                                            @foreach($packages as $pak) 
                                                @if($pak->package_id == 0)
                                                    @foreach($pak->wh as $w) 
                                                       {{ $w }} &nbsp;
                                                    @endforeach
                                                @endif
                                            @endforeach
                                            </p>
                                          </li>
                                       
                                        </ul>
                                    </div>
                                </div>
                              </div>
                              
                              
                              <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                
                                
                                  @foreach($packages as $pak) 
                                  @if(count($pak->images) == 1)
                                      <div class="carousel-item @if ($loop->index==0) active @endif">
                                              <img src="{{ $pak->images[0] }}" class="d-block w-100"> 
                                          </div>
                                  @else
                                    @foreach($pak->images as $imag) 
                                          <div class="carousel-item @if ($loop->index==0) active @endif">
                                              <img src="{{ $imag }}" class="d-block w-100">
                                          </div>
                                    @endforeach
                                  @endif
                                        
                                @endforeach
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                              </div>
                             
                             
                             
                             
                              <small style="color:red;">Puedes visualizar la imagen en la calidad original con solo dar click sobre ella, esta opción te permite descargarla</small>
                            </div>
                             
                             
                             
                              
                            </div>
                          
                          </div>

                    {{-- Save button --}}
                    <div class="row mb-3 mt-1 float-right ">
                      <div class="col-12">
                        <a href="{{ route('clients.shippings.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">Atras
                        </a>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }
    .card .nav.flex-column>li{
       border-bottom:white !important;
    }

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">
$(document).ready(function() {


});
 
</script>
@stop
-