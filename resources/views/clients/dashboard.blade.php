@extends('adminlte::page')

@section('title', 'ESCRITORIO')

@section('content_header')
          <div class="row">
          <div class="col-9 col-sm-9 col-md-9">
          <h4><b style="color:#24298D; font-weight: 900;">ESCRITORIO</b></h4>
          </div>
          <div class="col-3 col-sm-3 col-md-3">
          <h4 align="right"><b style="color:black; font-weight: 900;">Código de usuario: {{ $code[0]->code }}</b></h4>
          </div>
        </div>
        <hr>
    
@stop

@section('content')
<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">El costo estimado de su envío es:</h5>
        
      </div>
      <div class="modal-body">
       <center> <h1 id="respuesta"></h1></center>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-secondary" id="button_cerrar" data-dismiss="modal" style="color: #24298d;
                              background-color: #ffb901;
                              border-color:#ffb901;
                              box-shadow: none; font-size:15px !important; font-weight: 900;">CERRAR</button>
      </div>
    </div>
  </div>
</div>
<section class="content">
      <div class="container-fluid">
        <div class="row" style="padding-bottom:20px;">
        <div class="col-12 col-sm-12 col-md-12" style="background:#2e3192ff;">
        <center><img src="../public/vendor/adminlte/dist/img/TITULO.jpg" /></center> 
        </div>
        </div>
      <hr>
        
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
            <span class="info-box-icon bg-danger elevation-1">
                        <img src="{{ asset('vendor/adminlte/dist/img/bell.svg') }}" class="" width="30" height="30">
                    </span>

              <div class="info-box-content">
              <span class="info-box-text" style="color:24298D; font-weight: 600;">PREALERTAS</span>
                <a href="{{ route('clients.prealerts.index') }}">
                  <span class="info-box-number h5" id="count_codes">
                    {{ $prealerts }}
                  </span>
                </a>
              </div>
              <!-- /.info-box-content -->
            </div>
             
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1">
                        <img src="{{ asset('vendor/adminlte/dist/img/almacen.svg') }}" class="" width="30" height="30">
                    </span>

               <div class="info-box-content">
               <span class="info-box-text" style="color:24298D; font-weight: 600;">ALMACÉN</span>
                <a href="{{ route('clients.packages.index') }}">
                  <span class="info-box-number h5" id="count_codes">
                    {{ $package }}
                  </span>
                </a>
              </div>

              <!-- /.info-box-content -->

            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
            <span class="info-box-icon bg-success elevation-1">
                        <img src="{{ asset('vendor/adminlte/dist/img/instrucciones.svg') }}" class="" width="30"
                            height="30">
                    </span>

              <div class="info-box-content">
              <span class="info-box-text" style="color:24298D; font-weight: 600;">INSTRUCCIONES</span>
                <a href="{{ route('clients.instrucctions.index') }}">
                  <span class="info-box-number">{{ $instrucctions }}</span>
                </a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

          <!-- /.col -->
        </div><hr>

<div class="card">

    <div class="card-body">
        <center>
            <h5 class=""><b style="color:#24298D; font-weight: 900;">CALCULADORA</b></h5>
        </center>
        <hr>
        
        <div class="alert alert-danger error-calculator" role="alert" style="display:none;">
                </div>
        
        <div>
        {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <center>
                        <h6 style="font-weight: 900;">TIPO DE ENVÍO</h6><br>

                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="option" id="option1" value="1"
                                style="position: absolute; opacity: 0;" checked>
                                <label class="form-check-label" for="option1" value="1"
                                style="width:140px; height:140px; background:#24298d; border: solid 8px #24298d; border-radius: 15px;"><img
                                     id ="imagen-radio1" src="{{ asset('vendor/adminlte/dist/img/ship-light.svg') }}" height="100%"
                                    width="100%"></label>
                                    </div>  
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="option" id="option2" value="2"
                        style="position: absolute; opacity: 0;">
                                <label class="form-check-label" for="option2" value="2"
                                style="width:140px; height:140px; background:white; border: solid 8px #24298d; border-radius: 15px;"><img
                                id ="imagen-radio2" src="{{ asset('vendor/adminlte/dist/img/airplane-blue.svg') }}"
                                    height="100%" width="100%"></label>
                    </div>    
                             

                          </center>    

                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                <center>
                <h6 style="font-weight: 900; padding:7px;">SELECCIONA EL DESTINO</h6>
                          <select class="form-control" name="destiner_id" id="destiner_id">
                                <option value="">Seleccione</option>
                                @foreach ($destiner as $sta)>
                              
                                <option value="{{ $sta->id }}">{{ $sta->name }}</option>
                                
                                @endforeach    
                          </select><br>
                          <h6 style="font-weight: 900; padding:7px;">MEDIDAS</h6>
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-3">
                        <input type="text" name="width" id="width"
                                  class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                                   placeholder="ANCHO" autofocus maxlength="40">
                    </div>x
                    <div class="col-lg-3 col-md-3 col-xs-3"> 
                        <input type="text" name="lenght" id="lenght"
                        class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                        placeholder="LARGO" autofocus maxlength="40">
                      </div>x
                    <div class="col-lg-3 col-md-3 col-xs-3">
                        <input type="text" name="height" id="height"
                          class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                          placeholder="ALTO" autofocus maxlength="40">
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-xs-2">
                        <input type="text" name="weight" id="weight"
                          class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                          placeholder="PESO" autofocus maxlength="40">
                    </div>
                  </div><br>
                  <h6 style="font-weight: 900; padding:7px;">DESEA REEMPAQUE</h6>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="reemp" id="reemp1" value="1">
                        <label class="form-check-label" for="inlineRadio1">SI</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="reemp" id="reemp2" value="2" checked>
                        <label class="form-check-label" for="inlineRadio2">NO</label>
                      </div>

                </div>
                
            </div>
            </center>  
            <hr>

        
                <center><button class="btn btn-primary" id="_btn_calculator" style=" color: #24298d;
                      background-color: #ffb901;
                      border-color:#ffb901;
                      box-shadow: none; font-size:15px !important; font-weight: 900;">CALCULAR
                   </button></center>
    </div>
    
</div>

<div class="col-lg-4 col-md-4 col-xs-4">

</div>
</div>


</div>
</div>
</div>


<!-- /.row -->
</div>
<!--/. container-fluid -->
</section>



@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>

<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
 

 // funcion para radio button y el style

 function checkRadio(){
    if($('#option1').prop('checked')){
    $('#option1').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option1]").attr({
        'style': 'width:140px; height:140px; background:#24298d; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio1").attr({
        'src': 'vendor/adminlte/dist/img/ship-light.svg',
        'height':'100%',
        'width':'100%'
    });
    }else{
        $('#option1').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option1]").attr({
        'style': 'width:140px; height:140px; background:white; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio1").attr({
        'src': 'vendor/adminlte/dist/img/ship-blue.svg',
        'height':'100%',
        'width':'100%'
    });
    }

    if($('#option2').prop('checked')){
    $('#option2').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option2]").attr({
        'style': 'width:140px; height:140px; background:#24298d; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio2").attr({
        'src': 'vendor/adminlte/dist/img/airplane-light.svg',
        'height':'100%',
        'width':'100%'
    });
    }else{
        $('#option2').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option2]").attr({
        'style': 'width:140px; height:140px; background:white; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio2").attr({
        'src': 'vendor/adminlte/dist/img/airplane-blue.svg',
        'height':'100%',
        'width':'100%'
    });
    }

 }
 $("input[name=option]").change(function() {
    checkRadio();
});
 



 $("#button_cerrar").on('click',function(){
        $('.modal').hide();
      });

  $("#_btn_calculator").on('click',function(){
      console.log($("input[name=option]:checked").val());
      console.log($("input[name=reemp]:checked").val());
        _url = "<?=url('/cliente/calculatorClient')?>"
        
        $.ajax({
        url: _url,
        type: "post",
        data: {
            "_token": "{{ csrf_token() }}",
            "width": $("#width").val(),
            "height": $("#height").val(),
            "lenght": $("#lenght").val(),
            "weight": $("#weight").val(),
            "trip_id": $("input[name=option]:checked").val(),
            "destiner_id": $("#destiner_id").val(),
            "reemp": $("input[name=reemp]:checked").val(),
        },
        dataType: 'json',

        success: function (data) {
            if(data.error == 1){
                $('.error-calculator').html(data.message);
			          $('.error-calculator').fadeIn('fast');
                setTimeout(function(){ $('.error-calculator').fadeOut('fast'); }, 2000);
            }else{
                $('.modal').show();
                $('#respuesta').text('$'+data.success);
            }           
            
        },
       
        })
    });

  //create shippings
  

  
  

  
});

</script>

@stop
