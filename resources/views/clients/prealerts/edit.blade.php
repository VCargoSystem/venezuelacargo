@extends('adminlte::page')
@section('title', 'EDITAR PREALERTA')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">EDITAR PREALERTA</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content" style="color:#24298D; font-weight: 900;">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                               <form  method="post" id="_form_register" action="{{ route('clients.prealerts.update', $prealerts->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                {{-- Name field --}}
                                <input type="hidden" name="id" value="{{ $prealerts->id }}">
                                <div class="row mb-2">
                                    <div class="col-lg-6 col-md-12 col-xs-12 col-sm-12">
                                        <label>CÓDIGO</label>
                                        <input type="text" name="code"  id="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}"
                                               value="{{ $prealerts->code }}" placeholder="Código" autofocus readonly>
                                  
                                        @if($errors->has('code'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </div>
                                        @endif
                                    </div>

                                     <div class="col-lg-6 col-md-12 col-xs-12 col-sm-12">
                                      <label>TRACKING</label>
                                        <input type="text" name="tracking" id="tracking" class="form-control {{ $errors->has('tracking') ? 'is-invalid' : '' }}"
                                               value="{{ $prealerts->tracking  }}" placeholder="Tracking" autofocus maxlength="40">
                                      
                                        @if($errors->has('tracking'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('tracking') }}</strong>
                                            </div>
                                        @endif
                                  </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-lg-6 col-md-12 col-xs-12 col-sm-12">
                                      <label>FECHA ESTIMADA DE LLEGADA</label>

                                        <input type="date" name="date_in" id="date_in" class="form-control {{ $errors->has('date_in') ? 'is-invalid' : '' }}"
                                               value="{{ $prealerts->date_in  }}" placeholder="Fecha estimada de Llegada" autofocus  min="{{ date('Y-m-d')}}">
                                      
                                        @if($errors->has('date_in'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('date_in') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                   
                                    <div class="col-lg-5 col-md-11 col-xs-11 col-sm-11">
                                          <label>EMPRESA</label>
                                         <select class="form-control select2 select2-hidden-accessible" style="width: 100%; min-height: 70px !important;"  tabindex="-1" aria-hidden="true"  name="company_id" id="company_id">
                                            @foreach($companies as $com)
                                              <option value="{{ $com->id }}" @if($prealerts->company_id ==$com->id) selected  @endif>{{ $com->name }}</option>
                                            @endforeach
                                          </select>
                                           
                                          @if($errors->has('company_id'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('company_id') }}</strong>
                                            </div>
                                          @endif
                                    </div>

                                    <div class="col-lg-1 col-md-1 col-xs-1 col-sm-1 mt-2">
                                            <label></label>
                                            <button id="_btn_add_company" class="btn btn-block btn-primary" type="button" alt="Agregar Empresa" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">
                                                <i class="fa fa-plus"></i>
                                            </button> 
                                   
                                    </div>
                                </div>
                                
                              <!--  <div class="row mb-3">
                                    <div class="col-lg-6 col-md-12 col-xs-12 col-sm-12">
                                          <label>Estatus</label>
                                          <select class="form-control" style="width: 100%;" name="status_id" id="status_id">
                                              <option value="0">Activo</option>
                                              <option value="1">Inactivo</option>

                                          </select>
                                           
                                          @if($errors->has('status_id'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('status_id') }}</strong>
                                            </div>
                                          @endif
                                    </div>
                                  </div>-->
                              

              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('clients.prealerts.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>


<!--modal asignar code-->
<section>
    <div class="modal fade" id="modal_company" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modelHeading"><b>AGREGAR EMPRESA</b></h6>
            </div>
            <div class="modal-body">
              <div id="content" style="display: none">
                <div class="loading text-center"><img src="{{ asset('img/ajax-loader.gif') }}" alt="loading" /></div>
              </div>

                 <form  method="post" id="_form_company" >
                      {{ csrf_field() }}

                      <div class="form-group">
                            <label class="form-control-label" for="company_name">NOMBRE</label>
                            <input type="text" class="form-control" name="company_name" id="company_name" required>
                            <div class="invalid-feedback">Campo requerido.</div>
                      </div>
                                  
                      <div class="form-group">
                          <label class="form-control-label" for="company_code">DESCRIPCIÓN</label>
                          <textarea name="company_description" id="company_description" class="form-control {{ $errors->has('company_description') ? 'is-invalid' : '' }}"
                                   value="{{ old('value') }}" placeholder="Descripción" autofocus></textarea>
                         
                          <div class="invalid-feedback">Campo requerido.</div>
                      </div>

                    {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <button type="button" class="btn btn-default" id="btn_cancel_company">CANCELAR
                        </button> 
                        <button type="button" class="btn btn-primary" id="_btn_save_company" value="create">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>

<script type="text/javascript">
  $(document).ready(function() {

    $('.select2').select2();
    $('.select2-selection').css('min-height','37px')

    $('#_btn_add_company').on('click', function(){
      $("#company_name").val('')
      $("#company_code").val('')
      $("#company_description").text('')
      $("#modal_company").modal("show");
    });

    $('#btn_cancel_company').on('click', function(){
      $("#modal_company").modal("hide");
    });

    $('#_btn_save_company').on('click', function(){

          // Fetch form to apply custom Bootstrap validation
          var form = $("#_form_company")

          if (form[0].checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
             form.addClass('was-validated');
            return false;
          }else{

              form.addClass('was-validated');

              $('#content').css('display','block')

              $.ajax({
                data: {
                  "name": $("#company_name").val(),
                  "code": $("#company_code").val(),
                  "description": $("#company_description").val(),
                  "_token": "{{ csrf_token() }}"
                },
                url: "{{ route('clients.prealerts.storeCompany') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
               
                    if(data.success==1){
                          $.confirm({
                            title: 'Crear empresa!',
                            content: 'Empresa creada correctamente',
                            type: 'blue',
                            typeAnimated: true,
                            buttons: {
                                aceptar: {
                                    text: 'Aceptar',
                                    btnClass: 'btn-blue',
                                    action: function(){
                                      $('#modal_company').modal('hide');
                                      $('#content').css('display','none')
                                    }
                                }
                            }
                        });

                        selectData(data.companies, data.company_id)
                    }else{
                        $.confirm({
                            title: 'Crear empresa!',
                            content: 'Error al crear la empresa',
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                delete: {
                                    text: 'Aceptar',
                                    btnClass: 'btn-red',
                                    action: function(){
                                      $('#modal_company').modal('hide');
                                      $('#content').css('display','none')
                                    }
                                }
                            }
                        });
                    }
                },
                error: function (data) {

                }
              });
          }
           
        });

  });//Fin button click

  function selectData(companies, company_id){
    _cad=''
    $("#company_id").html('');
                
    $.each(companies, function (index, val) {
        _selected=  (company_id==val.id?'selected':'')
        _cad+= `<option value="${val.id}" ${_selected}>
        ${val.name}</option>` 
    });

    $("#company_id").html(_cad);
  }

</script>
@stop
