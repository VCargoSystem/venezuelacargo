@extends('adminlte::page')

@section('title', 'PREALERTAS')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">PREALERTAS</b></h4>
@stop

@section('content')


@if (session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            <li> {!! session()->get('success')!!}</li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>


  <section class="content">

      <div class="container-fluid">
        <div class="row">
           <div class="col-12 mb-1 pr-3">
           <div class="row mb-1 float-right ">
              <a href="{{ url('/cliente/prealerts/create') }}" class="btn btn-sm btn-primary" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">AGREGAR</a>
  
            </div>
          </div>
          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
               <div class="card-header">
                    <form class="form-group">
              
                        <div class="row">
                          
                          <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 pl-2 ">
                            <label class="col-md-12 col-xs-12 col-sm-12 float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">TRACKING</label>
                            <input style="font-size:14px !important; letter-spacing: -0.5px !important;" type="text" name="tracking" id="tracking" class="form-control form-control-sm  {{ $errors->has('tracking') ? 'is-invalid' : '' }} mr-1" value="" placeholder="Tracking" autofocus maxlength="50">
                          </div>

                          <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                            <label  class="float-left col-md-12 col-xs-12 col-sm-12" style="font-size:15px !important; letter-spacing: -0.5px !important;">DESDE</label>
                            <input style="font-size:14px !important; letter-spacing: -0.5px !important;" type="date" name="date" id="date" class=" form-control form-control-sm  {{ $errors->has('date') ? 'is-invalid' : '' }} mr-1" value="" autofocus>
                          </div>

                          <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                            <label  class="float-left col-md-12 col-xs-12 col-sm-12" style="font-size:15px !important; letter-spacing: -0.5px !important;">HASTA</label>
                            <input style="font-size:14px !important; letter-spacing: -0.5px !important;" type="date" name="date1" id="date1" class=" form-control form-control-sm  {{ $errors->has('date1') ? 'is-invalid' : '' }} mr-1" value="" autofocus>

                          </div>

                         <div class=" col-lg-2 col-md-2 col-xs-12 col-sm-12 pt-4 mr-3">
                             <button id="_btn_filter" class=" ml-1 btn btn-primary btn-sm col-lg-12  col-sm-12 col-xs-12 col-md-12 " style=" color: #fff;
      background-color: #23298a;
      border-color: #23298a;
      box-shadow: none; margin-top: 7px !important;"><i class="fas fa-search"></i>&nbsp;BUSCAR</button>
                        
                          </div>

                          <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 pt-3">
                              <button id="_btn_refresh" class=" ml-1 btn btn-primary btn-sm  col-lg-12  col-sm-12 col-xs-12 col-md-12" style=" color: #fff;
      background-color: #23298a;
      border-color: #23298a;
      box-shadow: none; margin-top: 15px !important;" >
        
        <i class="fas fa-eraser"></i>&nbsp;LIMPIAR CAMPOS
                              </button>
                          </div>
                    </div>
                  </form>
        
            </div>

              <div class="card-body">
                <div class="table-responsive">
                <table class="table table-striped table-responsive-lg table-responsive-md table-responsive-sm table-responsive-xs table-bordered datatable table-bordered" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                    <thead>                  
                      <tr>
                        <th width="15%" style="background:#24298d; color:#ffb901;">CÓDIGO</th>
                        <th width="20%" style="background:#24298d; color:#ffb901;">CLIENTE</th>
                        <th width="25%" style="background:#24298d; color:#ffb901;">TRANSPORTE</th>
                        <th width="10%" style="background:#24298d; color:#ffb901;">INGRESO</th>
                        <th width="12%" style="background:#24298d; color:#ffb901;">ACCIÓN</th>
                      </tr>
                    </thead>
                    <tbody>
             
                          <tr>
    	                    
                          </tr>
    						    
                    </tbody>
                  </table>
                  
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
            
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>

<style type="text/css">
  body{
        font-family: 'Montserrat', sans-serif !important;
    }
  .tracking{
    word-break: break-all;
  }

  .select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;
    min-height: 30px;
  }

  .form-control {
    display: block;
    width: 100%;
    height: calc(1.95rem + 1px) !important;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-shadow: inset 0 0 0 transparent;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
  }

  .table td, .table th {
        padding: .40rem !important;
        vertical-align: middle !important;
        border-top: 1px solid #dee2e6;
        font-size:15px;
    }

    div.dataTables_wrapper .dataTables_info {
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination{
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-green {
    border-top: solid 10px #38c53e !important;
     }
  
</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {


      $('.select2').select2();
      $('.select2-selection').css('max-height','25px')


      $('.select2').select2({
            placeholder: 'Seleccionar Usuario',
            ajax: {
                url: "{{ route('clients.users.usersAutocomplete') }}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name+' '+item.last_name+' - '+item.code,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
      
        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 20,
            searching: false,
            paging: true,
            bLengthChange: false,
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: {
              url: "{{ route('clients.prealerts.getFilterPrealerts') }}",
              data: function(d){
                d.tracking = $('#tracking').val(),
               
                d.date = $('#date').val(),
                d.date1 = $('#date1').val()
              }
            },
            columns: [
              {data: 'code_user', 'searchable': false, render:function ( data, type, row ) {  
                return '<span class="tracking">'+row.code_user+'</span>';
              }},
              {data: 'cliente', 'searchable': false, render:function ( data, type, row ) {  
                return '<span class="tracking">'+row.cliente+'</span>';
              }},
              {data: 'company', 'searchable': false, render:function ( data, type, row ) {  
                return '<span class="tracking">'+row.company+'</span>';
              }},
              {data: 'date_in', name: 'date_in'},
                /*{data: 'status', name:'status'},*/
              {data: 'actions', name: 'actions',orderable:false,serachable:false,sClass:'text-center'}
            ]
        });


        //Delete user
        $('body').on('click', '.delete', function(e) {
          e.preventDefault();

          _id = $(this).attr("data-id");

          $.confirm({
                title: 'Eliminar Prealerta!',
                content: 'Seguro desea eliminar el registro?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    delete: {
                        text: 'Eliminar',
                        btnClass: 'btn-red',
                        action: function(){
                          deletePrealerts(_id)
                        }
                    },
                    cancelar: function () {
                    }
                }
            });

        });

        $('#_btn_refresh').on('click', function(e) {
          e.preventDefault();
          
          $("#date").val('')
          $("#date1").val('')
          $("#tracking").val('')
         

          dataTable.draw()
        });

    $('#_btn_filter').on('click', function(e) {
        e.preventDefault();
        sw = 0;

        if($("#date").val() !="" && $("#date1").val() ==""){
          sw = 2
        }else if($("#date").val() =="" && $("#date1").val() !=""){
          sw = 2
        }else if($("#date").val() !="" && $("#date1").val() !=""){
            let date1 = new Date($("#date").val());
            let date2 = new Date($("#date1").val());

            if(date1 > date2){
              $.confirm({
                title: 'Filtro de busquedas!',
                content: 'Fecha Hasta no puede ser menor que Fecha Desde',
                type: 'blue',
                typeAnimated: true,

                buttons: {
                  cancelar: function () {

                  }
                }
              });
              return false;
            }
        }

        if(sw == 2){
          $.confirm({
              title: 'Filtro de busquedas!',
              content: 'Debe seleccionar las dos fechas',
              type: 'blue',
              typeAnimated: true,

              buttons: {
                cancelar: function () {

                }
              }
          });
          return false;
        }

        dataTable.draw()
      });  


        function deletePrealerts(_id){

            $.ajax({
              url:  "<?=url("/cliente/prealerts/delete")?>/"+_id,
              type: "DELETE",
              dataType: 'json',
              data: {
                "id": _id,
                "_token": "{{ csrf_token() }}"
              },
              success: function (data) {

                  if(data.success==1){
                    $("#msg").html(data.message)
                    $(".alert2").css('display', 'block')
                    $('#content').css('display','none')
                    dataTable.draw() 
                  }
              },
              error: function (data) {

              }
          });

        }
  });


    
</script>
@stop
