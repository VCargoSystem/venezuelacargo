@extends('adminlte::page')
@section('title', 'TRANSFERENCIA')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">PAGO CON TRANSFERENCIA/PAGO MÓVIL</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content" style="font-size:15px !important; letter-spacing: -0.5px !important;">
      <div class="container-fluid">
      <div class="card">
          <div class="card-body">
          <table style="font-size:11px !important;">
                <tr>
                    <td valign="top" style="text-align: left; padding-left: 10px;">
                        <li class="fcolor" style="text-align: left; font-size: 6;  line-height: 1.2em; list-style-type: none;"><strong>MÉTODOS DE PAGO</strong></li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em"><strong>TITULAR: Carlos Eduardo Restrepo Ruiz - CI: V-17.632.959 - TELÉFONO: (0412)1812469</strong></li>
                        <hr style="color: #ccc; line-height: 0.5em">
                        <li class="fcolor" style="text-align: left; font-size: 6;  line-height: 1.2em; list-style-type: none;"><strong>BANCOS VENEZOLANOS (PAGOS EN BOLÍVARES)</strong></li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">BANCO BANESCO</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NRO DE CUENTA: 0134 0869 6486 9302 5833  TIPO: CTA CORRIENTE</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NOTAS: PAGOS POR  TRANSFERENCIAS Y PAGO MÓVIL</li>
                        
                        
                
                    </td>
                   
                    <td valign="top" style="text-align: left; padding-left: 70px;">
                    <li class="fcolor" style="text-align: left; font-size: 6;  line-height: 1.2em; list-style-type: none;"><strong>BANCOS AMERICANOS (PAGOS EN DIVISAS)</strong></li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">BANK OF AMERICA</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NRO DE CUENTA: 3340 6659 9986  TIPO: CTA CORRIENTE</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">ABA ROUTING NUMBER: 061000052</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NOTAS: NO COLOCAR NADA EN EL ASUNTO DE LA TRANSFERENCIA</li>
                        <hr style="color: #ccc; line-height: 0.5em">
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">ZELLE</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">CORREO ELECTRÓNICO: venezuelacargo@icloud.com</li>
                   
                    
                
                
                    </td>
                   
                </tr>
            </table> 
          </div>
        </div>
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                               <form  method="post" id="_form_payStore" action="{{ route('clients.invoices.transferStore') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('post') }}
                                
                                {{-- Name field --}}
                                <div class="row mb-2">
                                  <input type="hidden" name="id" value="{{ $invoice->id }}">
                                  <div class="col-6">
                                      <label>BANCO</label>
                                      
                                        <select name="bank" class="form-control">
                                          @foreach($banks as $b)
                                            <option value="{{ $b->id }}">{{ $b->code.' -'.$b->name }}</option>
                                          @endforeach
                                        </select>
                                      
                                        @if($errors->has('bank'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('bank') }}</strong>
                                            </div>
                                        @endif
                                  </div>

                                  <div class="col-6">
                                      <label>REFERENCIA</label>
                                  
                                        <input type="text" name="reference" id="reference" class="form-control {{ $errors->has('reference') ? 'is-invalid' : '' }}"
                                               value="{{ old('value')  }}" placeholder="Reference" autofocus maxlength="50">
                                      
                                        @if($errors->has('reference'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('reference') }}</strong>
                                            </div>
                                        @endif
                                  </div>

                                  <div class="col-12 mt-3">
                                    <label>COMPROBANTE DE PAGO</label>
                                    <div class="border rounded-lg text-center p-3" style="max-height: 250px !important; max-width: auto !important;">
                                        <img src="//placehold.it/140?text=IMAGE" class="img-fluid" id="img-upload" style="max-height: 230px !important; max-width: auto  !important;"/>
                                    </div>
                                    <div class="custom-file mt-1">
                                        <input type="file" class="custom-file-input form-control form-control {{ $errors->has('capture') ? 'is-invalid' : '' }}" id="capture" name="capture" accept="image/png, image/jpeg, image/jpeg, image/svg">
                                        <label class="custom-file-label" for="customFile">Seleccione el archivo</label>
                                        @if($errors->has('capture'))
                                            <div class="invalid-feedback">
                                              <strong>{{ $errors->first('capture') }}</strong>
                                            </div>
                                        @endif
                                         <div id="error_img" class="text-danger invalid-feedback" style="display: none; font-weight: 700">
                                          <strong>Formato invalido</strong></div>
                                      </div>
                                       
                                    </div>
                                   
                                </div>

              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('clients.invoices.payment', $invoice->id) }}}}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">
  $(document).ready(function() {
    $(".integer").inputNumberFormat({ 'decimal': 0, 'decimalAuto': 0 });

    $("#capture").on('change',function(){
       $('#error_img').css("display", "none");
       $(this).removeClass('is-invalid');

        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(png|jpeg|jpg|svg|bmp)$");

        if (!(regex.test(val))) {
            $(this).val('');
            $('#error_img').text('Formato invalido')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        }else if($(this)[0].files[0].size/1024 > 6000){
            $(this).val('');
            $('#error_img').text('Limite superado (6MB))')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        }else{
            var input = $(this)[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result).fadeIn('slow');
                }
                reader.readAsDataURL(input.files[0]);

            }
        }
    });
   



  });//Fin jquery


</script>
@stop
