@extends('adminlte::page')
@section('title', 'FACTURACIÓN')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">PAGAR FACTURA</b></h4>
@stop

@section('content') 

@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content" style="font-size:15px !important; letter-spacing: -0.5px !important;">
      <div class="container-fluid">
        <div class="card">
          <div class="card-body">
          <table style="font-size:11px !important;">
                <tr>
                    <td valign="top" style="text-align: left; padding-left: 10px;">
                        <li class="fcolor" style="text-align: left; font-size: 6;  line-height: 1.2em; list-style-type: none;"><strong>MÉTODOS DE PAGO</strong></li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em"><strong>TITULAR: Carlos Eduardo Restrepo Ruiz - CI: V-17.632.959 - TELÉFONO: (0412)1812469</strong></li>
                        <hr style="color: #ccc; line-height: 0.5em">
                        <li class="fcolor" style="text-align: left; font-size: 6;  line-height: 1.2em; list-style-type: none;"><strong>BANCOS VENEZOLANOS (PAGOS EN BOLÍVARES)</strong></li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">BANCO BANESCO</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NRO DE CUENTA: 0134 0869 6486 9302 5833  TIPO: CTA CORRIENTE</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NOTAS: PAGOS POR  TRANSFERENCIAS Y PAGO MÓVIL</li>
                        
                        
                
                    </td>
                   
                    <td valign="top" style="text-align: left; padding-left: 70px;">
                    <li class="fcolor" style="text-align: left; font-size: 6;  line-height: 1.2em; list-style-type: none;"><strong>BANCOS AMERICANOS (PAGOS EN DIVISAS)</strong></li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">BANK OF AMERICA</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NRO DE CUENTA: 3340 6659 9986  TIPO: CTA CORRIENTE</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">ABA ROUTING NUMBER: 061000052</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">NOTAS: NO COLOCAR NADA EN EL ASUNTO DE LA TRANSFERENCIA</li>
                        <hr style="color: #ccc; line-height: 0.5em">
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">ZELLE</li>
                        <li style="text-align: left; padding-left: 10px; font-size: 6; line-height: 1.2em">CORREO ELECTRÓNICO: venezuelacargo@icloud.com</li>
                   
                    
                
                
                    </td>
                   
                </tr>
            </table> 
          </div>
        </div>
     
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">

                          <input type="hidden" name="id" value="{{ $invoice->id }}" id="id">

                          <div class="row">
                            <div class="col-sm">
                               <label class="pr-2">NÚMERO: </label>{{ $invoice->id }}
                            </div>
                            <div class="col-sm">
                               <label class="pr-2">FECHA: </label>{{ 
                                \Carbon\Carbon::parse($invoice->created_at)->format('d-m-Y')
                             }}
                            </div>
                            <div class="col-sm">
                              <label class="pr-2">USUARIO: </label>{{ $user->name.' '.$user->last_name}} 
                            </div>
                            <div class="col-sm">
                              <label class="text-warning pr-2 pl-2">ENVIO:</label>
                             {{ $type==1? 'MARITIMO':'AEREO'}}
                
                            </div>
                          </div>

                        <br><br>
                       
                        <div class="table-responsive">

                           <table class="table table-sm  datatable table-responsive-md table-responsive-sm table-responsive-xs  table-bordered">
                              <thead class="h6">                  
                                <tr class="text-center">
                                  <th width="8%">TOTAL PIEZAS</th>
                                  <th width="8%">TOTAL PESO</th>
                                  <th width="8%">TOTAL VOLUMEN</th>
                                  <th width="8%">TOTAL PIE CUBICO</th>
                                </tr>
                              </thead>
                              <tbody>
                                    <tr class="text-center">
                                      <td>{{ $invoice->total_pieces }}</td>
                                      <td>{{ $invoice->total_w }}</td>
                                      <td>{{ $invoice->total_volume }}</td>
                                      <td>{{ $invoice->total_pie }}</td>
                        
                                    </tr>
                              </tbody>
                            </table>


                            <table class="table table-sm table-striped  datatable table-responsive-md table-responsive-sm table-responsive-xs  table-bordered">
                              <thead class="h6">                  
                                <tr>
                                  <th width="8%">LARGO</th>
                                  <th width="8%">ANCHO</th>
                                  <th width="8%">ALTO</th>
                                  <th width="8%">PESO</th>
                                  <th width="8%">LB VOL</th>
                                  <th width="10%">PIE CÚBICO</th>
                                  <th width="9%">UNI</th>
                                  <th width="35%">CONTENIDO</th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                @if($packages)
                                  @foreach($packages as $p)
                                    <tr>
                                      <td>{{ $p->long }}</td>
                                      <td>{{ $p->width }}</td>
                                      <td>{{ $p->high }}</td>
                                      <td>{{ $p->weight }}</td>
                                      <td>{{ $p->subtotal_lb }}</td>
                                      <td>{{ $p->total_pie3 }}</td>
                                      <td>{{ $p->nro_pieces }}</td>
                                      <td>{{ $p->description }}</td>


                                    </tr>
                                  @endforeach
                                @endif

                                @if($repackages)
                                  <tr>
                                    <td colspan="8">Paquetes con reempaque</td>
                                  </tr>
                                  @foreach($repackages as $r)
                                    <tr>
                                      <td>{{ $r->long }}</td>
                                      <td>{{ $r->width }}</td>
                                      <td>{{ $r->high }}</td>
                                      <td>{{ $r->weight }}</td>
                                      <td>{{ $r->subtotal_lb }}</td>
                                      <td>{{ $r->total_pie3 }}</td>
                                      <td>{{ $r->nro_pieces }}</td>
                                      <td>{{ $r->description }}</td>


                                    </tr>
                                  @endforeach
                                @endif
                              </tbody>
                            </table>

                          </div>

                          <table class="table table-sm   table-responsive-md table-responsive-sm table-responsive-xs ">
                              <tbody>
                                    <tr class="text-right" style="border-top: 0px solid #dee2e6;">
                                      <td>
                                        <label class="mr-2">Subtotal paquetes </label> $ {{ number_format($invoice->subtotal_package,2) }}</td>
                                    </tr>
                                    <tr class="text-right">
                                      <td>
                                        <label class="mr-2">Subtotal reempaque </label> $
                                        {{ $invoice->subtotal_repackage }}</td>
                                    </tr>
                                    <tr class="text-right">
                                      <td><label class="mr-2">Subtotal cajas </label> $ {{ $invoice->amount_box }}</td>
                                    </tr>
                                    <tr class="text-right">
                                      <td><label class="mr-2">Subtotal adicional </label> $ {{ $invoice->additional }}</td>
                                    </tr>
                                    <tr class="text-right">
                                      <td><label class="mr-2">Subtotal costo reempaque </label> $ {{ $invoice->cost_repackage }}</td>
                                    </tr>
                                    <tr class="text-right  caption-top" style="border-top: 1px solid #23298a !important; ">
                                      <td><label class="mr-2" style="color:#23298a; font-size: 18px !important">Total  $ </label>
                                        <label  style="font-size: 18px !important">{{ number_format($invoice->amount, 2) }}</label></td>
                                    </tr>
                                    <tr class="text-right">
                                      <td><label class="mr-2" style="color:#23298a; font-size: 18px !important">Total (VES) Bs </label>
                                        <label style="font-size: 18px !important">{{ number_format(($invoice->amount_ves), 2) }}</label></td>
                                    </tr>
                              </tbody>
                            </table>
          
                      <div class="row pt-5">
                        <div class="col clearfix">
                            <span class="float-left"></span>
                            <span class="float-right">
                               <form action="" method="get" id="_form_pay">
                    
                                <button  class=" ml-1 btn btn-primary btn-sm" style=" color: #fff;  background-color: #23298a; border-color: #23298a; box-shadow: none;" id="_btn_paypal" disabled>PAGAR CON PAYPAL</button>

                            </form>
                            </span>
                        </div>
                        <div class="col clearfix">
                            <span class="float-left">
                              <form action="{{ route('clients.invoices.paymentTransfer', $invoice->id) }}" method="get" id="_form_pay">

                                <button class=" ml-1 btn btn-primary btn-sm" style=" color: #fff; background-color: #23298a; border-color: #23298a; box-shadow: none;" id="_btn_transfer">PAGAR CON TRANSFERENCIA</button>
                              </form>
                            </span>
                            <span class="float-right">
                               
                            </span>
                        </div>
                    </div>
              
                   <div class="row pt-4 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('clients.invoices.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                       
                      </div>
                    </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>


@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>

<style type="text/css">
  .table td{
    padding: .1rem;
    vertical-align: top;
    border-top: 0px solid #dee2e6; 
}

body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">
  $(document).ready(function() {



  });

</script>
@stop
