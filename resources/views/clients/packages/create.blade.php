@extends('adminlte::page')
@section('title', 'ALMACEN')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">CREAR PAQUETE</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content" style="font-size:15px !important; letter-spacing: -0.5px !important;">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                      <form  method="post" id="_form_register" action="{{ route('packages.store') }}"  accept-charset="UTF-8" enctype="multipart/form-data">
                                {{ csrf_field() }}
                          <div class="row">
                            <div class="col-8">
                              <input type="hidden" name="tracking1[]" id="tracking1" value="{{ old('tracking1.0')  }}">
                              <input type="hidden" name="tracking1[]" id="tracking2" value="{{ old('tracking1.1')  }}">
                              <input type="hidden" name="tracking1[]" id="tracking3" value="{{ old('tracking1.2')  }}">
                              <input type="hidden" name="tracking1[]" id="tracking4" value="{{ old('tracking1.3')  }}">
                  
                              <div class="row mb-2">
                                <div class="col-6">
                                        <label>WAREHOUSE</label>
                                        <input type="text" name="ware_house"  id="ware_house" class="form-control {{ $errors->has('ware_house') ? 'is-invalid' : '' }}"
                                               value="{{ old('ware_house') }}" placeholder="Nro. Warehouse" autofocus>
                                  
                                        @if($errors->has('ware_house'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('ware_house') }}</strong>
                                            </div>
                                        @endif
                                </div>

                                <div class="col-6">
                                    <label>TRACKING</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="tracking" id="tracking" class="form-control {{ $errors->has('tracking') ? 'is-invalid' : '' }}"
                                               value="{{ old('tracking')  }}" placeholder="Tracking" autofocus maxlength="50">
                                      
                                        <div class="input-group-append">
                                            <span class="btn-primary btn"  data-toggle="tooltip" data-placement="top" title="Agregar otro tracking" id="btn_add-tracking">+</span>
                                        </div>
                                        @if($errors->has('tracking'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('tracking') }}</strong>
                                            </div>
                                        @endif
                                </div>
                              </div>
                              </div>

                              <div class="row mb-2">
                                    <div class="col-6">
                                      <label>CÓDIGO DE USUARIO</label>
                                        <input type="text" name="code" id="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}"
                                               value="{{ old('code')  }}" placeholder="Código de usuario" autofocus maxlength="30">
                                      
                                        @if($errors->has('code'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                   
                                    <div class="col-6">
                                      <label>CLIENTE</label>

                                        <input type="text" name="username" id="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                                               value="{{ old('username')  }}" placeholder="Cliente" autofocus maxlength="100">
                                      
                                        @if($errors->has('username'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                              </div>

                                <div class="row mb-2">
                                  <div class="col-12">
                                        <label>CONTENIDO</label>
                                          <textarea name="description" id="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                                 value="" placeholder="Contenido" autofocus maxlength="200">{{ old('description')  }}</textarea>
                                        
                                          @if($errors->has('description'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('description') }}</strong>
                                              </div>
                                          @endif
                                  </div>
                                </div>

                                <label>MEDIDAS</label>
                                <div class="row mt-1 border-top">
                                  <div class="col-4 mt-1">
                                        <label>ANCHO</label>
                                        <div class="input-group mb-3">
                                          <input type="text" step="0.01" name="width" id="width" class="form-control {{ $errors->has('width') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ old('width')  }}" placeholder="Ancho" autofocus maxlength="10" min="0.01">
                                          <div class="input-group-append">
                                            <span class="input-group-text">IN</span>
                                          </div>
                                          @if($errors->has('width'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('width') }}</strong>
                                              </div>
                                          @endif
                                      </div>
                                  </div>

                                  <div class="col-4 mt-1">
                                      <label>ALTO</label>
                                      <div class="input-group mb-3">
                                          <input type="text" name="high" id="high" class="form-control {{ $errors->has('high') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ old('high')  }}" placeholder="Ancho" autofocus maxlength="20" step="0.01">
                                          <div class="input-group-append">
                                            <span class="input-group-text">IN</span>
                                          </div>

                                          @if($errors->has('high'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('high') }}</strong>
                                              </div>
                                          @endif
                                      </div>
                                  </div>

                                  <div class="col-4 mt-1">
                                        <label>LARGO</label>
                                        <div class="input-group mb-3">
                                          <input type="text" name="long" id="long" class="form-control {{ $errors->has('long') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ old('long')  }}" placeholder="Largo" autofocus maxlength="10" step="0.01">
                                           <div class="input-group-append">
                                            <span class="input-group-text">IN</span>
                                          </div>
                                        
                                          @if($errors->has('long'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('long') }}</strong>
                                              </div>
                                          @endif

                                        </div>
                                  </div>
                                </div>

                                <div class="row mb-3">
                                  <div class="col-4 mt-1">
                                        <label>PESO</label>
                                        <div class="input-group mb-3">
                                          <input type="text" step="0.01" name="weight" id="weight" class="form-control {{ $errors->has('weight') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ old('weight')  }}" placeholder="Peso" autofocus maxlength="10" min="0.01">
                                          <div class="input-group-append">
                                            <span class="input-group-text">LB</span>
                                          </div>
                                          @if($errors->has('weight'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('weight') }}</strong>
                                              </div>
                                          @endif
                                          
                                        </div>
                                          
                                  </div>
                                  <div class="col-4 mt-1">
                                        <label>VOLUMEN</label>
                                        <div class="input-group mb-3">
                                          <input type="text"  name="volume" id="volume" class="form-control {{ $errors->has('volume') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ old('volume')  }}" placeholder="Volumen" autofocus maxlength="10" min="0.01" step="0.01">

                                         
                                        
                                          @if($errors->has('volume'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('volume') }}</strong>
                                              </div>
                                          @endif
                                      </div>
                                    </div>
                                  <div class="col-4 mt-1">
                                        <label>NÚMERO DE CAJAS</label>
                                        <div class="input-group mb-3 integer">
                                          <input type="text" name="nro_box" id="nro_box" class="form-control {{ $errors->has('nro_box') ? 'is-invalid' : '' }}"
                                                 value="{{ old('nro_box')  }}" placeholder="Cajas" autofocus maxlength="20" min="1">

                                         
                                        
                                          @if($errors->has('nro_box'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('nro_box') }}</strong>
                                              </div>
                                          @endif
                                      </div>
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="col-4 mt-1">
                                        <label>NÚMERO DE PIEZAS</label>
                                        <div class="input-group mb-3 integer">
                                          <input type="text" name="nro_pieces" id="nro_pieces" class="form-control {{ $errors->has('nro_pieces') ? 'is-invalid' : '' }} integer"
                                                 value="{{ old('nro_pieces')  }}" placeholder="Piezas" autofocus maxlength="20" min="1">

                                          <div class="input-group-append">
                                            <span class="input-group-text"></span>
                                          </div>
                                        
                                          @if($errors->has('nro_pieces'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('nro_pieces') }}</strong>
                                              </div>
                                          @endif
                                      </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 col-xs-12">
                              <label>LISTADO DE TRACKINGS</label>
                                  <div class="row card mb-2 border rounded-lg  bg-light" style="max-height: 200px !important; min-height: 190px !important; font-size: 13px">

                                    <div class="card-body p-0" style="display: block;">
                                        <ul id="list_tracking" class="nav nav-pills flex-column">
            
                                        </ul>
                                    </div>
                                  </div>

                              <label class="pt-2">IMAGEN DEL PAQUETE</label>
                              <div class="border rounded-lg text-center p-3" style="max-height: 400px !important; max-width: auto !important;">
                                  <img src="//placehold.it/140?text=IMAGE" class="img-fluid" id="img-upload" style="max-height: 300px !important; max-width: auto  !important;"/>
                              </div>

                                <div class="custom-file mt-1">
                                  <input type="file" class="custom-file-input form-control form-control {{ $errors->has('image_package') ? 'is-invalid' : '' }}" id="image_package" name="image_package" accept="image/png, image/jpeg, image/jpeg, image/svg">
                                  <label class="custom-file-label" for="customFile">Seleccione el archivo</label>
                                  @if($errors->has('image_package'))
                                      <div class="invalid-feedback">
                                        <strong>{{ $errors->first('image_package') }}</strong>
                                      </div>
                                  @endif
                                   <div id="error_img" class="text-danger invalid-feedback" style="display: none; font-weight: 700">
                                    <strong>Formato invalido</strong></div>
                              </div>
                            </div>
                          
                          </div>

                    {{-- Save button --}}
                    <div class="row mb-3 mt-1 float-right ">
                      <div class="col-12">
                        <a href="{{ route('packages.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">
$(document).ready(function() {
  _arr_tracking=[]
  $('#error_img').css("display", "none");
  loadTrackings()

  //Delete tracking
  $('body').on('click', '.del-tracking', function(e) {
    e.preventDefault();
    _id = $(this).attr('data-id')

    $("#tracking"+_id).val('')
    $(this).closest("li").remove();
  });


  $("#btn_add-tracking").on('click',function(){
    let _tracking = $("#tracking").val()
    let _exist=0
    let _count = $("#list_tracking").children().length;
    

    if(_tracking ==''){
      return
    }else{
      if(_count==60){
           $.confirm({
                'title':"Agregar Tracking",
                content: 'Limite de tracking alcanzados Max(5)',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    delete: {
                        text: 'Aceptar',
                        btnClass: 'btn-red',
                        action: function(){
                        }
                    }
                }
            });
           return
      }

      $("input[name='tracking2[]']").each(function(indice, elemento) {
        if(elemento.value ==  _tracking){
          _exist=1
          return false
        }
      });

      if(!_exist){
        _cad= ''
        _nro = $("#list_tracking").children().length+1
        _pos = 0
            //find position free
        $("input[name='tracking1[]']").each(function(indice, elemento) {
            if(elemento.value ==''){
              _pos = indice+1
              return false;
            }
        });

        _cad=`<li class="nav-item ">
                <a href="#" class="nav-link">
                 ${_tracking}
                 <i class="float-right fa fa-trash text-danger del-tracking" data-id="${_nro}"></i>
                </a>
                <input type="hidden" name="tracking2[]" value="${_tracking}">
              </li>`

        $("#list_tracking").append(_cad)

        $("#tracking"+_pos).val(_tracking)
         $.confirm({
                title: 'Agregar Tracking',
                content: 'Se agrego nuevo tracking',
                type: 'blue',
                typeAnimated: true,
                buttons: {
                    delete: {
                        text: 'Aceptar',
                        btnClass: 'btn-blue',
                        action: function(){
                        }
                    }
                }
            });
      }else{
           $.confirm({
                title: 'Agregar Tracking',
                content: 'Ya existe el tracking',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    aceptar: function () {
                    }
                }
            });
      }
    }
  });

  $(".decimales").inputNumberFormat({ 'decimal': 2, 'decimalAuto': 2 });
   $(".integer").inputNumberFormat({ 'decimal': 0, 'decimalAuto': 0 });
  

});

function loadTrackings(){
    _cad= ''
    _nro = $("#list_tracking").children().length+1

 
    $("input[name='tracking1[]']").each(function(indice, elemento) {

      if(elemento.value !=''){
        _cad+=`<li class="nav-item ">
                <a href="#" class="nav-link">
                 ${elemento.value}
                 <i class="float-right fa fa-trash text-danger del-tracking" data-id="${_nro}"></i>
                </a>
                 <input type="hidden" name="tracking2[]" value="${elemento.value}">
              </li>`
      }
      
    });

    $("#list_tracking").append(_cad)
}


   $("#image_package").on('change',function(){
       $('#error_img').css("display", "none");
       $(this).removeClass('is-invalid');

        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(png|jpeg|jpg|svg|bmp)$");

        if (!(regex.test(val))) {
            $(this).val('');
             $('#error_img').text('Formato invalido')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        }else if($(this)[0].files[0].size/1024 > 6000){
            $(this).val('');
            $('#error_img').text('Limite superrado (6MB))')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        }else{
            var input = $(this)[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result).fadeIn('slow');
                }
                reader.readAsDataURL(input.files[0]);

            }

        }
         
       
    });
  
 

</script>
@stop
