@extends('adminlte::page')
@section('title', 'DETALLE DE USUARIO')

@section('content_header')
    <h5><b style="color:#24298D; font-weight: 600;"> <strong>DETALLE DE USUARIO</strong></b></h5>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">

                      <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-4">
                            <div class="card" style="height:220px !important;">
                              <div class="card-body">
                              <h5 style="color:#24298D;" >Datos personales</h5>
                            <hr>
                            <label style="font-size:15px;">CÉDULA: </label><span class="pl-2" style="font-size:15px;">{{  $user->dni }}</span><br>
                            <label style="font-size:15px;">NOMBRE: </label> <span class="pl-2" style="font-size:15px;">{{ $user->name }}
                            </span><br>
                            <label style="font-size:15px;">APELLIDO: </label> <span class="pl-2" style="font-size:15px;">{{ $user->last_name  }}
                            </span><br>
                            <label style="font-size:15px;">TELÉFONO: </label> <span class="pl-2" style="font-size:15px;">{{ $user->phone }}
                            </span><br>
                              </div>
                            </div>
                            
                            
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <div class="card" style="height:220px !important;">
                              <div class="card-body">
                            <h5 style="color:#24298D;">Perfíl de acceso</h5>
                            <hr>
                            <label style="font-size:15px;">USUARIO: </label><span class="pl-2" style="font-size:15px;">{{  $user->username}}</span><br>
                            <label style="font-size:15px;">EMAIL: </label> <span class="pl-2" style="font-size:15px;">{{ $user->email }}
                                    </span>
                                    <label style="font-size:15px;"></label> <span class="pl-2" style="font-size:15px;">
                                    </span>
                                    <label style="font-size:15px;"></label> <span class="pl-2" style="font-size:15px;">
                                    </span>
                                    </div>
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <div class="card" style="height:220px !important;">
                              <div class="card-body">
                            <h5 style="color:#24298D;">Dirección de entrega</h5>
                            <hr>
                            <label style="font-size:15px;">ESTADO: </label> <span class="pl-2" style="font-size:15px;">
                                    @if(count($user->address) > 0)
                                         {{ $user->address[0]->state->name }}
                                    @endif
                                    </span><br>
                                    <label style="font-size:15px;">CIUDAD: </label> <span class="pl-2" style="font-size:15px;">
                                     @if(count($user->address) > 0)
                                      {{  $user->address[0]->city[0]->name }}

                                      <input type="hidden" id="latitude" name="latitude" value="{{ $user->address[0]->latitude }}">
                                      <input type="hidden" id="longitude" name="longitude" value="{{ $user->address[0]->longitude }}">
                                    @endif
                                    </span><br>
                                    <label style="font-size:15px;">DIRECCIÓN: </label> <span class="pl-2" style="font-size:15px;">
                                     @if(count($user->address) > 0)
                                      {{  $user->address[0]->address }}
                                    @endif
                                    </span>

                                    </div>
                      </div>

                          </div>
                      </div>
                    <hr>
                  
                             
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                 <iframe class="embed-responsive" id="map_frame" src="{{ url('admin/iframeMap/user') }}" width=auto height=320/></iframe>
                            </div>
                     
                    </div>
                    
                    {{-- Save button --}}
                    <div class="row mb-12 mt-1 float-right ">
                    <div class="col-lg-11 col-md-11 col-xs-11">
                       
                      </div>
                      <div class="col-lg-1 col-md-1 col-xs-1 float-right">
                        <a href="{{  route('admin.users.index') }}" class="btn btn-warning" id="btn_cancel" value="cancel" style="color:#2C2E88;"> <strong>ATRÁS</strong> 
                        </a>
                      </div>
                    </div>

                </form>
                <br><br>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<style type="text/css">
  body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>

<script type="text/javascript">
$(document).ready(function() {

  _lati = $("#latitude").val()
  _long = $("#longitude").val()

  _base_url = "<?=url('admin/users/iframeMapFind')?>"
  _url = _base_url+'/'+_long+'/'+_lati

  if((_lati != "" && _long != "" && _lati != undefined && _long != undefined)){
    $('#map_frame').attr('src', _url);
  }

});
 
</script>
@stop
