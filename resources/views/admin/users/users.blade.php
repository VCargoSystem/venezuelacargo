@extends('adminlte::page')

@section('title', 'USUARIOS')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">USUARIOS</b></h4>
@stop

@section('content')


@if (session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            <li> {!! session()->get('success')!!}</li>
        </ul>
    </div>
@endif

<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

<div class="alert alert-warning alert-danger2" style="display: none">
   <button type="button" class="close1 close"  >×</button>
    <span id="msg-danger"></span>
</div>


  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <!-- /.card-header -->
              <div class="card-header">
                  <form class="">

                    
                      <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">CLIENTE</label>
                              <select class="form-control select2 select2-hidden-accessible" style="width: 100%; min-height: 70px !important; font-size:14px;"  tabindex="-1" aria-hidden="true"  name="usuario" id="usuario">
                              </select>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">ESTADO</label>
                              <select class="form-control" name="state_id" id="state_id" style="font-size:14px;">
                                <option value="" style="color:#9e9e9e;">Seleccione una opción</option>
                                @foreach($states as $state)
                                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">CIUDAD</label>
                              <select class="form-control" name="city_id" id="city_id" style="font-size:14px;">
                                <option value="" style="color:#9e9e9e;">Seleccione una opción</option>
                                
                              </select>
                          </div>
                         

                          
                         
                        </div>
                        <div style="height:7px;"></div>
                          <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">STATUS</label>
                            <select class=" form-control" name="status_user" id="status_user" style="font-size:14px;">
                              <option value="" style="color:#9e9e9e;">Seleccione una opción</option>
                              <option value="1">Activo</option>
                              <option value="0">Inactivo</option>
                            </select>
                          </div>
                          
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">DESDE</label>
                            <input type="date" style="font-size:14px;" name="date" id="date" class="form-control form-control-sm  {{ $errors->has('date') ? 'is-invalid' : '' }} mr-1" value="" placeholder="Fecha de registro - Desde" autofocus maxlength="20">
                          </div>
                          <div style="height:7px;"></div>
                          
                          <div style="height:7px;"></div>
                          <div class="col-lg-4 col-md-4 col-xs-4">
                          <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">HASTA</label>
                            <input style="font-size:14px;" type="date" name="date1" id="date1" class="form-control form-control-sm  {{ $errors->has('date1') ? 'is-invalid' : '' }} mr-1" value="" placeholder="Fecha de registro - Hasta" autofocus maxlength="20">
                          </div>
                          </div>
                          
                          
                          <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-4"> </div>
                            <div class="col-lg-4 col-md-4 col-xs-4">
                              <div class="row">
                              <div class="col-lg-6 col-md-6 col-xs-6 col-sm-1 pt-3">
                              <button id="_btn_filter" class=" ml-1 btn btn-primary btn-sm col-lg-12  col-sm-12 col-xs-12 col-md-12" style=" color: #fff;
      background-color: #23298a;
      border-color: #23298a;
      box-shadow: none; margin-top: 7px !important; font-size:14px; margin-left:0px !important;"><i class="fas fa-search"></i>&nbsp;BUSCAR</button>
                          </div>

                          <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6 pt-3">
                              <button id="_btn_refresh" class=" ml-1 btn btn-primary btn-sm  col-lg-12  col-sm-12 col-xs-12 col-md-12" style=" color: #fff;
      background-color: #23298a;
      border-color: #23298a;
      box-shadow: none; margin-top: 7px !important; min-height: 31px !important; margin-left:0px !important;" >
        
        <i class="fas fa-eraser"></i>&nbsp;LIMPIAR CAMPOS 
                              </button>
                                </div>
                          </div>
                           
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-4"> </div>

                          </div>
                         
                         
                         

                          
                    
                  
                  </form>
              </div>

              <div class="card-body">
                <table class="table table-sm table-striped  datatable table-responsive-md table-responsive-sm table-responsive-xs table-bordered" style="letter-spacing: -0.5px !important;">
                  <thead>                  
                    <tr>
                      <th width="14%" style="background:#24298d; color:#ffb901;"> USUARIO</th> 
                      <th width="18%" style="background:#24298d; color:#ffb901;"> NOMBRE</th>
                      
                      <th width="21%" style="background:#24298d; color:#ffb901;"> CORREO ELECTRÓNICO</th>
                      <th width="9%" style="background:#24298d; color:#ffb901;"> TELÉFONO</th>
                      <th width="9%" class="text-center" style="background:#24298d; color:#ffb901;"> CÓDIGO</th>
                      <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;"> STATUS</th>
                      <th width="9%" class="text-center" style="background:#24298d; color:#ffb901;">F. REGISTRO</th>
                      <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;"> ACCIONES</th>
                    </tr>
                  </thead>

                  <tbody id="table-users">
                    <tr>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix text-center">
            
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

  <!--modal asignar code-->
  <section>
    <div class="modal fade" id="modal_code" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modelHeading"><b style="color:#24298D; font-weight: 600;"> <strong>ASIGNAR CÓDIGO</strong> </b></h6>
            </div>
            <div class="modal-body">
              <div id="content" style="display: none">
                <div class="loading text-center"><img src="{{ asset('img/ajax-loader.gif') }}" alt="loading" /></div>
              </div>

                <form id="frm_code" novalidate="" name="frm_code" class="form-horizontal" action="" method="POST">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                   <input type="hidden" name="user_id" id="user_id">
      
      
                  <div class="form-group">
                      <label class="form-control-label" for="code" style="font-size:12px;">CÓDIGO</label>
                      <input type="text" class="form-control" name="code" id="code" placeholder="ESCRIBE EL CÓDIGO DEL USUARIO" required>
                      <div class="valid-feedback">Correcto!.</div>
                      <div class="invalid-feedback">Campo requerido.</div>
                  </div>
                    <div class="row float-right">
                      <div class="col-sm-12">
                        <button type="button" class="btn btn-default" id="btn_cancel" value="cancel" style="color:#2C2E88;"> <strong>CANCELAR</strong> 
                        </button>
                        <button type="button" class="btn btn-warning" id="btn_save_code" value="create" style="color:#2C2E88;
                           box-shadow: none;"> <strong>GUARDAR</strong>
                        </button>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
  <script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
  <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop


<style type="text/css">
  .select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;
    min-height: 30px;
  }

  body{
        font-family: 'Montserrat', sans-serif !important;
        
    }


  .form-control {
    display: block;
    width: 100%;
    height: calc(1.95rem + 1px) !important;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-shadow: inset 0 0 0 transparent;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}

  div.dataTables_wrapper div.dataTables_info{
    font-size:14px !important;
  }

  .table td,
    .table th {
        padding: .40rem !important;
        vertical-align: middle !important;
        border-top: 1px solid #dee2e6;
        font-size: 15px;
    }

    div.dataTables_wrapper .dataTables_info {
        font-size: 15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination {
        font-size: 15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-green {
        border-top: solid 10px #38c53e !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-blue {
        border-top: solid 10px #f44336 !important;
    }

    hr {
        background: #e3e3e3;
    }

</style>
@section('js')

<link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<script type="text/javascript">

$(document).ready(function() {
  _id_state=0
  _id_city=0

  $('.select2').select2();
  $('.select2-selection').css('min-height','67px')

  $('.select2').select2({
        placeholder: 'Seleccionar Usuario',
        ajax: {
            url: "{{ route('users.usersAutocomplete') }}",
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name+' '+item.last_name+' - '+item.code,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });


  // init datatable.
  var dataTable = $('.datatable').DataTable({
      processing: true,
      serverSide: true,
      autoWidth: false,
      pageLength: 20,
      searching: false,
      ordering: true,
      paging: true,
      bLengthChange: false,
      //pageLength: 5,
      "language": {
      "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      },
      // scrollX: true,
      "order": [[ 0, "desc" ]],
      ajax: {
          url: "{{ route('users.getFilterUsers') }}",
          data: function(d){
            d.usuario = $('#usuario').val(),
            d.state_id = $('#state_id').val(),
            d.city_id = $('#city_id').val(),
            d.status_user = $('#status_user').val(),
            d.date = $('#date').val(),
            d.date1 = $('#date1').val()
          }
      },
      columns: [
          
          {data: 'username', name: 'username'},
          /*{data: 'name', name: 'name'},*/
          
          {data: 'name', render:function ( data, type, row ) {  return row.name+' '+row.last_name;
          }},
          {data: 'email', name: 'email'},
          {data: 'phone', name: 'phone'},
          {data: 'code', name: 'code',sClass:'text-center'},
          {data: 'status', name:'status',sClass:'text-center'},
          {data: 'created_date', name: 'created_date',sClass:'text-center'},
          {data: 'actions', name: 'actions',orderable:false,serachable:false,sClass:'text-center'}
      ]
  });

       //Assign code
        $('body').on('click', '.assing-code', function(e) {
          e.preventDefault();
          $("#code").val('')
          let user_id= $(this).attr('data-id');

          $("#user_id").val(user_id)
          $('#modal_code').modal('show');

        });

        $('#btn_save_code').on('click', function(){

          // Fetch form to apply custom Bootstrap validation
          var form = $("#frm_code")

          if (form[0].checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.addClass('was-validated');

          $('#content').css('display','block')

          $.ajax({
            data: {
              "code" : $('#code').val(),
              "user_id": $("#user_id").val(),
              "_token": "{{ csrf_token() }}"
            },
            url: "{{ route('users.codeUpdate') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#frm_code').trigger("reset");
                
                if(data.success==1){
                  $('#modal_code').modal('hide');
                  $("#msg").html(data.message)
                  $(".alert2").css('display', 'block')
                  $('#content').css('display','none')
                  dataTable.draw() 
                }
            },
            error: function (data) {

            }
          });

           
        });

        $('#btn_cancel').on('click', function(e) {
          $('#modal_code').modal('hide');
        });

        //Delete user
        $('body').on('click', '.delete', function(e) {
          e.preventDefault();

          _id = $(this).attr("data-id");
          

          $.confirm({
                title: '',
                content: '¿Desea eliminar el registro?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    delete: {
                        text: 'Eliminar',
                        btnClass: 'btn-red',
                        action: function(){
                          deleteUser(_id)
                        }
                    },
                    cancelar: function () {
                    }
                }
            });

        });

   //Select state - return cities
    $("#state_id").on('change, click',function(e){

        e.stopPropagation()
        _id_state = $("#state_id").val()

        $.ajax({
            type: 'get',  
            dataType: 'json',
            url: "<?=url('admin/findCities')?>/"+ _id_state,

            success: function(res) {
                $("#city_id").html('');
                _cad = '<option value="" disabled>Seleccione una opción</option>';
                
                $.each(res.cities, function (index, val) {
                    _cad+= `<option value="${val.id}">
                    ${val.name}</option>` 
                });

                $("#city_id").html(_cad);
                //localize_address()
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        }); 
    })

    $('#state_id').trigger('click');
    $('#city_id').trigger('click');
    $('#state_id > option[value="'+_id_state+'"]').prop('selected', 'selected').trigger('click');


    $('body').on('click', '.edit-staus', function(e) {
      e.preventDefault();
      _id = $(this).attr('data-id')
      _title='';
      _content=''
      _status = 0

      if($(this).attr('data-status') == 1){
          _title='';
          _content='¿Desea inhabilitar el usuario?'
          _status = 0

      }else{
          _title='';
          _content='¿Desea habilitar el usuario?'
          _status = 1
      }

         $.confirm({
              title: _title,
              content: _content,
              type: 'blue',
              typeAnimated: true,
              buttons: {
                  delete: {
                      text: 'ACEPTAR',
                      btnClass: 'btn-blue',
                      action: function(){

                        changeStatus(_id, _status)
                      }
                  },
                  cancelar: function () {
                  }
              }
          });
         
    });

    //Filter data search
    $('#_btn_filter').on('click', function(e) {
        e.preventDefault();
        sw = 0;

        if($("#date").val() !="" && $("#date1").val() ==""){
          sw = 2
        }else if($("#date").val() =="" && $("#date1").val() !=""){
          sw = 2
        }else if($("#date").val() !="" && $("#date1").val() !=""){
            let date1 = new Date($("#date").val());
            let date2 = new Date($("#date1").val());

            if(date1 > date2){
              $.confirm({
                title: '!',
                content: 'Indique un rango de fecha válido',
                type: 'blue',
                typeAnimated: true,

                buttons: {
                  cancelar: function () {

                  }
                }
              });
              return false;
            }
        }

        if(sw == 2){
          $.confirm({
              title: '',
              content: 'Indique un rango de fecha válido',
              type: 'blue',
              typeAnimated: true,

              buttons: {
                cancelar: function () {

                }
              }
          });
          return false;
        }

        dataTable.draw()
      });  

  });

    function deleteUser(_id){

            $.ajax({
              url:  "<?=url("/admin/users/")?>/"+_id,
              type: "DELETE",
              dataType: 'json',
              data: {
                "id": _id,
                "_token": "{{ csrf_token() }}"
              },
              success: function (data) {
                  $('#frm_code').trigger("reset");
                  
                  if(data.success==1){
                    $('#modal_code').modal('hide');
                    $("#msg").html(data.message)
                    $(".alert2").css('display', 'block')
                    $('#content').css('display','none')
                    dataTable.draw() 
                  }
              },
              error: function (data) {

              }
          });

    }

    function changeStatus(id, status){


            $.ajax({
              url: "{{ route('users.changeStatus') }}",
              type: "post",
              dataType: 'json',
              data: {
                "id": id,
                "status": status,
                "_token": "{{ csrf_token() }}"
              },
              success: function (data) {
                  
                  if(data.success==1){
                    $('#modal_code').modal('hide');
                    $("#msg").html(data.message)
                    $(".alert2").css('display', 'block')
                    $('#content').css('display','none')
                    //dataTable.draw() 
                    window.location.reload() 
                  }else{
                    $('#modal_code').modal('hide');
                    $("#msg-danger").html(data.message)
                    $(".alert-danger2").css('display', 'block')
                    $('#content').css('display','none')
       
                  }
              },
              error: function (data) {

              }
          });
          
          //window.location.reload() 
    }
    
    $('.close1').on('click', function(e) {
      $(this).parent().fadeToggle("slow");
    });

    $('#_btn_refresh').on('click', function(e) {
          e.preventDefault();
          
          $("#date").val('')
          $("#date1").val('')
          $("#state_id").val('').trigger('change')
          $("#city_id").val('').trigger('change')
          $("#status_user").val('').trigger('change')
          $("#usuario").val('').trigger('change')

          $("#_btn_filter").trigger('click')
          dataTable.draw()

      });
</script>
@stop
