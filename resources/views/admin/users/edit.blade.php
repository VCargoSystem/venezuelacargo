@extends('adminlte::page')
@section('title', 'Usuarios')

@section('content_header')
    <h5><b style="color:#24298D; font-weight: 600;"> <strong>EDITAR DATOS DEL CLIENTE</strong> </b></h5>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                               <form  method="post" id="_form_register" action="{{ route('admin.users.update', $user->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                {{-- Name field --}}
                                <div class="row">
                                    <input type="hidden" name="id" value="{{$user->id}}">
                                    <input type="hidden" name="country_id" value="{{$country->id}}">
                                    <input type="hidden" name="longitude" value="{{$user->address[0]->longitude}}" id="longitude">
                    <input type="hidden" name="latitude" value="{{$user->address[0]->latitude}}" id="latitude">

                                    <div class="col-3 mb-3 ">
                                        <label>CÉDULA</label>
                                        <input type="text" name="dni" id="dni" class="form-control {{ $errors->has('dni') ? 'is-invalid' : '' }}"
                                               value="{{ $user->dni }}" placeholder="Cédula" autofocus>
                                      
                                        @if($errors->has('dni'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('dni') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-3  mb-3">
                                        <label>USUARIO</label>
                                        <input type="text" name="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                                               value="{{ $user->username }}" placeholder="Usuario" autofocus>
                                  
                                        @if($errors->has('username'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-3  mb-3">
                                      <label>NOMBRE</label>
                                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                               value="{{ $user->name }}" placeholder="Nombre" autofocus>
                                  
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-3  mb-3">
                                      <label>APELLIDO</label>
                                        <input type="text" name="last_name" class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}"
                                               value="{{  $user->last_name
                                                }}" placeholder="Apellido" autofocus>
                                      
                                        @if($errors->has('last_name'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="col-3  mb-3">
                                        <label>EMAIL</label>
                                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                               value="{{ $user->email }}" placeholder="{{ __('adminlte::adminlte.email') }}">
                                          
                                            @if($errors->has('email'))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </div>
                                            @endif
                                 
                                    </div>
                                    <div class="col-3 mb-3">
                                        <label>TELÉFONO</label>
                                        <input type="text" name="phone" id="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                                               value="{{ $user->phone }}" placeholder="Teléfono">
                                          
                                            @if($errors->has('phone'))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </div>
                                            @endif
                                 
                                    </div>

                                    <div class="col-3 mb-3">
                                       {{-- Password field --}}
                                        <div class="">
                                          <label>CONTRASEÑA</label>
                                            <input type="password" name="password"
                                                   class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                                   placeholder="Contraseña" value="" minlength="4">
                                         
                                            @if($errors->has('password'))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-3 mb-3">
                                       {{-- Password field --}}
                                        <div class="">
                                          <label>REPETIR CONTRASEÑA</label>
                                            <input type="password" name="password_confirmation"
                                                   class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                                                   placeholder="Repita la clave" value="">
                                        
                                            @if($errors->has('password_confirmation'))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                              </div>

                              <div class="row">
                                 <div class="col-3 mb-3">
                                        <label>PAÍS</label>
                                        <select class="form-control" id="country_id" name="state_id">
                                              <option value="{{$country->id}}">{{$country->name}}</option>
                                        </select>

                                        @if($errors->has('country_id'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('country_id') }}</strong>
                                            </div>
                                        @endif
                                </div>

                                <div class="col-3 mb-3">
                                    <label>ESTADO</label>
                                    <select class="form-control" id="state_id" name="state_id">
                                   
                                        @foreach($country->states as $state)
                                          @if(!empty($user->address[0]->states_id))
                                            <option value="{{ $state->id }}" @if($state->id ==          $user->address[0]->states_id) selected @endif> {{ $state->name }}</option>
                                          @else
                                             <option value="{{ $state->id }}"> {{ $state->name }}</option>
                                          @endif
                                        @endforeach

                                     
                                    </select>

                                    @if($errors->has('state_id'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('state_id') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-3 mb-3">
                                    <label>CIUDAD</label>
                                    <select class="form-control" id="city_id" name="city_id">

                                       @if(!empty($user->address[0]))
                                          @foreach($country->states[$user->address[0]->states_id -1]->cities as $city)
                                            <option value="{{ $city->id }}" @if($city->id ==          $user->address[0]->cities_id) selected @endif>{{ $city->name }}
                                            </option>
                                          @endforeach
                                        @else
                                            <option value="">
                                            </option>
                                        @endif
                                    </select>
                                      </select>

                                    @if($errors->has('city_id'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('city_id') }}</strong>
                                        </div>
                                    @endif
                                </div>


                                <div class="col-12  mb-3">
                                  <label>DIRECCIÓN</label>
                                    <input type="text" name="address" id="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}"

                                      value = "{{ !empty($user->address[0]->address) ? $user->address[0]->address : '' }}" placeholder="Ubicación">
                                      
                                        @if($errors->has('address'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </div>
                                        @endif
                                </div>
                              </div>

                              <div class="row">
                                 <div class="col-11  mb-3">
                                    <input type="text" name="address1" id="address1" class="form-control {{ $errors->has('address1') ? 'is-invalid' : '' }}"
                                            value = "{{ !empty($user->address[0]->address1) ? $user->address[0]->address1 : '' }}" placeholder="" readonly="readonly">
                                       
                                        @if($errors->has('address1'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('address1') }}</strong>
                                            </div>
                                        @endif
                              </div>

                              <div class="col-md-1">
                                  <button id="locate" class="btn btn-block btn-primary" type="button" alt="Buscar Direccion" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">
                                      <i class="fa fa-search"></i>
                                  </button> 
                              </div>

                        </div>

                        <div class="col-xs-10 alert alert-warning">
                            Especifique correctamenta la ubicación que sera usada como la dirección unica de entrega de paquetes. Una vez creada, no podra ser modificada sin autorización del administrador
                        </div>

                        <div class="col-xs-10">
                            <div id="map_frame" class="embed-responsive" style="width:100%;display:block;background:#F2F4F4; height: 450px !important; border-style: solid; border-width: 0.1px;"></div>
                        </div>


              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ url()->previous() }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

    _id_state=0
    _id_city=0
    _id_location=0
    
    localize_address()
    $("#locate").trigger('click')

    $("#dni").keyup(function (){
         this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    //Select state - return cities
    $("#state_id").on('change, click',function(e){

        //e.stopPropagation()
        _id_state = $("#state_id").val()

        $.ajax({
            type: 'get',  
            dataType: 'json',
            url: "<?=url('admin/findCities')?>/"+ _id_state,

            success: function(res) {
                $("#city_id").html('');
                _cad = '';
                
                $.each(res.cities, function (index, val) {
                    _cad+= `<option value="${val.id}">
                    ${val.name}</option>` 
                });

                $("#city_id").html(_cad);
                //localize_address()
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        }); 
    })


    $("#city_id").on('blur, click',function(e){
        e.stopPropagation()
        //localize_address()
    })

    //Map and Locator 

 
    function localize_address(){
        _cad='';
        $("#address1").val('');


        if($("#address").val() !== ''){
            _cad += $("#address").val()+', '

        if($("#city_id").val() !== ''){
           _cad +=  $('select[name="city_id"] option:selected').text()+', '
        }

        if($("#state_id").val() !== ''){
            _cad +=  $('select[name="state_id"] option:selected').text()+', ';
        }

    
        _cad+=' VEN'

        }

        _cad = _cad.replace(/\s+/g, ' ')  
        $("#address1").val(_cad)

        /*/$.ajax({
            type: 'get',  
            dataType: 'json',
            url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates',
            data: { 

                SingleLine : $("#address1").val(),
                f: 'json',
                sourceCountry:'VEN',
                city:$('select[name="city_id"] option:selected').text(),
                region:$('select[name="state_id"] option:selected').text(),
                outFields: '*',
                outSR: {"wkid":102100,"latestWkid":3857}
            },

            success: function(res) {
                console.log(res)
                _long = res.candidates[0].location.x
                _lati = res.candidates[0].location.y

                //_base_url = "<?=url('/users/iframeMapFind')?>"

                //_url = _base_url+'/'+_long+'/'+_lati

                //$('#map_frame').attr('src', _url);
                //$("#longitude").val(_long)
                //$("#latitude").val(_lati)


            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });*/
    }

    $("#btn_save").on('click', function(){
        
        if($("#address").val() =='' || $("#address").val() == undefined){
            return 
        }

        if($("#city_id").val() == 0){
            return false
        }

        if($("#state_id").val() == 0){
            return false
        }
        
        if($("#address1").val() ==''){
            return 
        }

        localize_address()
        var direccion = $("#address1").val();
        if (direccion !== "") {
            localizar("map_frame", direccion);
        }
        $("#_form_register").submit();

    });

     $("#locate").click(function(e) {
            localize_address()
            var direccion = $("#address1").val();

            localizar("map_frame", direccion);
            
        });


    function localizar(elemento,direccion) {
        var geocoder = new google.maps.Geocoder();

        var map = new google.maps.Map(document.getElementById(elemento), {
          zoom: 15,
          scrollwheel: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        geocoder.geocode({'address': direccion}, function(results, status) {
            if (status === 'OK') {
                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
                
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });

                $("#longitude").val(resultados_long)
                $("#latitude").val(resultados_lat)


            } else {
                var mensajeError = "";
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                //alert(mensajeError);
            }
        });
    }
    
    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCWV62-MOsXA-ifSUGAb5HXtyikHDVtuJU", function() {

        
        localizar("map_frame", $("#address1").val());

        $("#locate").click(function() {
            localize_address()
            var direccion = $("#address1").val();
            if (direccion !== "") {
                localizar("map_frame", direccion);
            }
        });


        $("#address").keypress(function( event ) {
            if ( event.which == 13 ) {
            event.preventDefault();
            localize_address()
            var direccion = $("#address1").val();
            if (direccion !== "") {
                localizar("map_frame", direccion);
            }
          }
        });

        $("#address").blur(function( event ) {
            localize_address()
            var direccion = $("#address1").val();
            if (direccion !== "") {
                localizar("map_frame", direccion);
            }
        });
    });

  });//Ready
     
</script>

<style type="text/css">
  body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>
@stop
