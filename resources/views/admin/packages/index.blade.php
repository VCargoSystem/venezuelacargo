@extends('adminlte::page')

@section('title', 'ALMACÉN')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">ALMACÉN</b></h4>
@stop

@section('content')


@if (session('errors'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (session()->has('success'))
<div class="alert alert-success">
    <button type="button" class="close close2" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>
        <li> {!! session()->get('success')!!}</li>
    </ul>
</div>
@endif
<div class="alert alert-success alert2" style="display: none">
    <button type="button" class="close1 close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>
<div class="alert alert-warning alert-danger2" style="display: none">
    <button type="button" class="close1 close">×</button>
    <span id="msg-danger"></span>
</div>


<section class="content">

    <div class="container-fluid">

        <div class="row">
            <div class="col-12 mb-1 pr-3">
                <div class="row mb-1 float-right ">
                    <a href="{{ route('packages.create') }}" class="btn btn-sm btn-primary" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;"> <strong>AGREGAR</strong> </a>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <form class="">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                    <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">CLIENTE</label>
                                    <select class="form-control select2 select2-hidden-accessible"
                                        style="width: 100%; min-height: 70px !important;" tabindex="-1"
                                        aria-hidden="true" name="usuario" id="usuario">
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">WAREHOUSE</label>
                                  <input type="text" name="ware_house" id="ware_house"
                                      class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} mr-1"
                                      value="" placeholder="Nro. Warehouse" autofocus maxlength="40">
                              </div>
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">DESDE</label>
                                    <input type="date" name="date" id="date"
                                        class="form-control form-control-sm  {{ $errors->has('date') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Fecha de registro - Desde" autofocus maxlength="20">
                                </div>
                            </div>
                            <div style="height:7px;"></div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">INSTRUCCIONES</label>
                                   <select name="instrucctions" id="instrucctions" class="form-control">
                                       <option value="" selected="selected">Seleccione</option>
                                       <option value="0">SIN INSTRUCCIONES</option>
                                       <option value="1">CON INSTRUCCIONES</option>
                                   </select>
                                </div>
                        
                            <div class="col-lg-4 col-md-4 col-xs-4">
                            <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">TRACKING</label>
                                <input type="text" name="tracking" id="tracking"
                                    class="form-control form-control-sm  {{ $errors->has('tracking') ? 'is-invalid' : '' }} mr-1"
                                    value="" placeholder="Tracking" autofocus maxlength="50">
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-4">
                                <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">HASTA</label>
                                <input type="date" name="date1" id="date1"
                                    class="form-control form-control-sm  {{ $errors->has('date1') ? 'is-invalid' : '' }} mr-1"
                                    value="" placeholder="Fecha de registro - Hasta" autofocus maxlength="20">
                            </div>
                            </div>
                            <div style="height:7px;"></div>
                            <div class="row">
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                    <button id="_btn_filter"
                                        class=" ml-1 btn btn-primary btn-sm col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                        style=" color: #fff; background-color: #23298a; border-color: #23298a; box-shadow: none; margin-top: 7px !important; margin-left:0px !important;"><i class="fas fa-search"></i>&nbsp;BUSCAR</button>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                    <button id="_btn_refresh"
                                        class=" ml-1 btn btn-primary btn-sm  col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                        style=" color: #fff; background-color: #23298a; border-color: #23298a; box-shadow: none; margin-top: 7px !important; margin-left:0px !important; min-height: 31px">
                                        <i class="fas fa-eraser"></i>&nbsp;LIMPIAR CAMPOS
                                    </button>
                                </div>
                                </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                            </div>
                    
                        </form>
                    </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-responsive-lg table-responsive-md table-responsive-sm table-responsive-xs table-bordered datatable table-bordered" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                            <thead>
                                <tr>
                                    <th width="13%" style="background:#24298d; color:#ffb901;">
                                            CLIENTE</th>     
                                    <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            WAREHOUSE</th>
                                    
                                    <th width="22%" class="text-center" style="background:#24298d; color:#ffb901;">CONTENIDO</th>
                                    <th width="7%" class="text-center" style="background:#24298d; color:#ffb901;">
                                        FT<sup>3</sup></th>
                                    <th width="8%" class="text-center" style="background:#24298d; color:#ffb901;">
                                        VOL</th>
                                    <th width="8%" class="text-center" style="background:#24298d; color:#ffb901;">
                                        PESO LB</th>
                                    <th width="9%" class="text-center" style="background:#24298d; color:#ffb901;">
                                        FECHA</th>
                                    <th width="10%" class="text-center" style="background:#24298d; color:#ffb901;">
                                        STATUS</th>
                                    <th width="11%" class="text-center" style="background:#24298d; color:#ffb901;">
                                        ACCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">

                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>

<!--modal asignar code-->
<section class="content">
    <div class="modal fade show" id="modal_preview" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="">
                    <button type="button" class="close pr-3 pt-2" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="min-height: 300px !important; max-height: 350px !important">
                    <img src="" border="0" width="300" class="img-fluid" align="center"
                        style="max-width: 300px !important; max-height: 350px !important" id="image_details" />
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade show" id="modal_confirm" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">CONFIRMAR PAQUETE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="_frm_confirm" method="post" action="{{ route('packages.confirmed') }}">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <label>SELECCIONE EL ESTATUS</label>
                        <input type="hidden" name="id" id="id">
                        <select class="form-control" name="confirmed">
                            
                            <option value="POR CONFIRMAR">POR ASIGNAR</option>
                            <option value="CONFIRMADO">ASINGADO</option>
                        </select>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save_confirm" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade show" id="modal_instrucction" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            <h4 style="color:#24298d; font-weight:900;"><b>INSTRUCCIONES DE ENVÍO</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="needs-validation" id="_frm_instrucctions" method="post" action="{{ route('admin.packages.instruction') }}">
                  {{ csrf_field() }}
                  {{ method_field('POST') }}
                  <input type="hidden" name="id_package" id="id_package">

                  <div class="col-12">
                  <label style="color:#24298d; font-weight:900;">GUARDAR EN ALMACEN MIAMI</label>
                    
                      <select class="form-control" name="hold" required>
                        <option value="" selected="selected">Seleccione</option>
                        <option value="1">Si</option>
                        <option value="0">No</option>
                      </select>
                      <div class="invalid-feedback">Seleccione una opción</div>
                      <small style="color:#ff9901;">Si activa esta instrucción su paquete no será enviado hasta que usted lo indique</small>
                  </div>
                  <div class="col-12 mt-2">
                  <label style="color:#24298d; font-weight:900;">SEGURO</label>
                    
                    <div class="input-group mb-3">
                        <input type="text" step="1" name="insured" id="insured" class="form-control {{ $errors->has('insured') ? 'is-invalid' : '' }} decimales"
                        value="0"  autofocus maxlength="10" min="1">
                        <div class="input-group-append">
                          <span class="input-group-text">$</span>
                        </div>
                        @if($errors->has('insured'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('insured') }}</strong>
                            </div>
                        @endif
                        <small style="color:#ff9901;">Debe indicar el monto por el cuál asegura su paquete, para el caso contrario haga caso omiso a la instrucción</small>
                    </div>
              
                  </div>
                  <div class="col-12 mt-2"> 
                    <label style="color:#24298d; font-weight:900;">REEMPAQUE</label>
                    
                    <select class="form-control" name="repackage" id="_repackage" required>
                      <option value="">Seleccione</option>
                      <option value="1">Si</option>
                      <option value="0">No</option>
                    </select>
                     <div class="invalid-feedback">Seleccione una opcion</div>
                     <small style="color:#ff9901;">Indiqué si desea que su paquete sea reempacado</small>
                  </div>
                  <div class="col-12 mt-2">
                  <label style="color:#24298d; font-weight:900;">TIPO DE ENVÍO</label>
                    
                    <select class="form-control" name="shippins_id" required>
                     <option value="">Seleccione</option>
                      <option value="1">MARITIMO</option>
                      <option value="2">AEREO</option>
                      
                    </select>
                     <div class="invalid-feedback">Seleccione una opcion</div>
                     <small style="color:#ff9901;">Indiqué el tipo de envío que desea para su paquete</small>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary save_instrucctions"  style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</section>
@stop

<style type="text/css">
    .tracking {
        word-break: break-all;
    }

    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 4px;
        min-height: 30px;
    }

    .form-control {
        display: block;
        width: 100%;
        height: calc(1.95rem + 1px) !important;
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        box-shadow: inset 0 0 0 transparent;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    .table td, .table th {
      padding: .40rem !important;
      vertical-align: middle !important;
      border-top: 1px solid #dee2e6;
  } 

  div.dataTables_wrapper .dataTables_info {
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination{
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-blue {
    border-top: solid 10px #f44336 !important;
 }

</style>
@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap'
    rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $(document).ready(function () {
        $(".alert2").css('display', 'none')
        $(".alert-danger2").css('display', 'none')

        $('.select2').select2();
        $('.select2-selection').css('max-height', '25px')

        $('.dataTables_length').css('display', 'none')

        $('.select2').select2({
            placeholder: 'Seleccionar Usuario',
            ajax: {
                url: "{{ route('users.usersAutocomplete') }}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name + ' ' + item.last_name + ' - ' + item.code,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 20,
            searching: false,
            paging: true,
            bLengthChange: false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            // scrollX: true,
            "order": [
                [0, "desc"]
            ],
            ajax: {
                url: "{{ route('packages.getFilterPackages') }}",
                data: function (d) {
                    d.usuario = $('#usuario').val(),
                    d.tracking = $('#tracking').val(),
                    d.ware_house = $('#ware_house').val(),         
                    d.instrucctions = $('#instrucctions').val(),
                    d.date = $('#date').val(),
                    d.date1 = $('#date1').val()
                }
            },
            columns: [
                {
                    data: 'cliente',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.cliente + '</span>';
                    }
                },

                {
                    data: 'ware_house',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.ware_house + '</span>';
                    }
                },
                
                /*{
                    data: 'width',
                    sClass:'text-center',
                    name: 'width'
                },
                {
                    data: 'high',
                    sClass:'text-center',
                    name: 'high'
                },
                {
                    data: 'long',
                    sClass:'text-center',
                    name: 'long'
                },*/
                {data: 'description', sClass:'text-center',  name: 'description', rows:'3'},
                {
                    data: 'pie_cubico',
                    sClass:'text-center',
                    name: 'pie_cubico'
                },
                {
                    data: 'lb_volume',
                    sClass:'text-center',
                    name: 'lb_volume'
                },
                {
                    data: 'weight',
                    sClass:'text-center',
                    name: 'weight'
                },
                {
                    data: 'date',
                    sClass:'text-center',
                    name: 'date'
                },
                {
                    data: 'status',
                    'searchable': false,
                    sClass:'text-center',
                    render: function (data, type, row) {
                        if(row.status == 'POR CONFIRMAR')
                            return '<span class="tracking">POR ASIGNAR</span>';
                        else
                             return '<span class="tracking"> ASIGNADO</span>';
                    }
                },

                
                /*{data: 'path',   name: "path" ,'searchable': false, render: function ( data, type, row ) {
                      
                          return '<img src="'+row.url+'"  border="0" width="40" class="img-fluid img-mini" align="center" style="cursor:pointer"  data-toggle="tooltip" data-placement="top" title="Mostrar imagen"/>';
                  }
                },*/
                /*{data: 'dimensions', 'searchable': false, render:function ( data, type, row ) {  return row.width+' x '+row.high+' x '+row.long;
                }},*/
                {
                    data: 'actions',
                    name: 'actions',
                    orderable: false,
                    serachable: false,
                    sClass: 'text-center'
                }
            ]
        });


        //preview image
        $('body').on('click', '.img-mini', function (e) {
            e.preventDefault();
            _url = $(this).attr('src')
            $("#image_details").attr('src', _url)
            $("#modal_preview").modal("show");
        });

        //Delete user
        $('body').on('click', '.delete', function (e) {
            e.preventDefault();
            _id = $(this).attr("data-id");

            $.confirm({
                title: '',
                content: '¿Desea eliminar el registro?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    delete: {
                        text: 'Eliminar',
                        btnClass: 'btn-red',
                        action: function () {
                            deletePackage(_id)
                        }
                    },
                    cancelar: function () {}
                }
            });

        });

        function deletePackage(_id) {
            $(".alert2").css('display', 'none')
            $(".alert-danger2").css('display', 'none')

            $.ajax({
                url: _url = "<?=url("/admin/packages/")?>/" + _id,
                type: "DELETE",
                dataType: 'json',
                data: {
                    "id": _id,
                    "_token": "{{ csrf_token() }}"
                },

                success: function (data) {
                    if (data.success == 1) {
                        $("#msg").html(data.message)
                        $(".alert2").css('display', 'block')
                    } else {
                        $("#msg-danger").html(data.message)
                        $(".alert-danger2").css('display', 'block')

                    }
                    dataTable.draw()
                },
                error: function (data) { 
                    
                }
            });

        }

        $('.close1').on('click', function (e) {
            $(this).parent().fadeToggle("slow");
        });


        $('#_btn_filter').on('click', function (e) {
            e.preventDefault();
            arr = [];
            let _cont = 0
            let sw = 0
            arr[0] = $("#ware_house").val()
            arr[1] = $("#tracking").val()

            if ($("#usuario").val() == null)
                arr[2] = '';
            else
                arr[2] = $("#usuario").val();

            arr[3] = $("#instrucctions").val()
            arr[4] = $("#date").val()
            arr[5] = $("#date1").val()

            for (let i in arr) {
                if (i < 6 && (arr[i] != "")) {
                    _cont++
                }

            }

            if (_cont > 1 && (arr[4] == "" && arr[5] == "")) {
                sw = 1
            } else if (_cont > 2 && (arr[4] != "" && arr[5] != "")) {
                sw = 1
            } else if (_cont > 1 && (arr[4] != "" && arr[5] == "")) {
                sw = 1
            } else if (_cont > 1 && (arr[4] == "" && arr[5] != "")) {
                sw = 1
            }

            if (sw == 1) {
                $.confirm({
                    title: '',
                    content: 'No puede seleccionar más de dos criterios',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });

                return false;
            }

            if (_cont == 0) {
                $.confirm({
                    title: '',
                    content: 'Debe seleccionar algún criterio',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            if (_cont == 1 && (arr[4] != "" && arr[5] == "")) {
                $.confirm({
                    title: '',
                    content: 'Indique un rango de fechas válido',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            if (_cont == 1 && (arr[4] == "" && arr[5] != "")) {
                $.confirm({
                    title: '',
                    content: 'Indique un rango de fechas',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            if (arr[4] != "" && arr[5] != "") {
                let date1 = new Date($("#date").val());
                let date2 = new Date($("#date1").val());

                if (date1 > date2) {
                    $.confirm({
                        title: '',
                        content: 'Indique un rango de fechas válido',
                        type: 'blue',
                        typeAnimated: true,

                        buttons: {
                            cancelar: function () {

                            }
                        }
                    });
                    return false;
                }
            }

            dataTable.draw()
        });

        $('#search').on('keyup', function (e) {
            e.preventDefault();
            dataTable.draw();
        });

        //confirmed
        $('body').on('click', '.confirma', function (e) {
            e.preventDefault();
            $("#id").val($(this).attr('data-id'))

            _inst = $(this).attr('instrucctions')

            if(_inst == 0){
                $.confirm({
                    title: '',
                    content: 'El paquete no tiene asignada una instrucción, verifique nuevamente',
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        aceptar: {
                            text: 'ACEPTAR',
                        }

                    }
                });
            }else{
                $("#modal_confirm").modal("show");
            }

        });

        $('.save_confirm').on('click', function (e) {
            e.preventDefault();

            $("#_frm_confirm").submit()

        });

        $('#_btn_refresh').on('click', function (e) {
            e.preventDefault();

            $("#date").val('')
            $("#date1").val('')
            $("#ware_house").val('')
            $("#tracking").val('')
            $('#instrucctions').val('').trigger('change')
            $("#usuario").val('').trigger('change')

            //$("#_btn_filter").trigger('click')
            dataTable.draw()

        });

        $('body').on('click', '.instrucction', function(e) {
            e.preventDefault();
            $("#id_package").val($(this).attr('data-id'))

                    $.ajax({
                        url: _url = "<?=url("/admin/packages/assing")?>/"+$("#id_package").val(),
                        type: "GET",
                        dataType: 'json',
                    
                        success: function (data) {
                            if(data.success==0){
                            $("#msg-danger").html(data.message)
                            $(".alert-danger2").css('display', 'block')
                            }else{
                            $("#modal_instrucction").modal("show");
                            }
                            //dataTable.draw() 
                        },
                        error: function (data) {

                        }
                    });
                });

                $('.save_instrucctions').on('click', function(e) {
                    e.preventDefault();

                    var form = $("#_frm_instrucctions")

                    if (form[0].checkValidity() === false) {
                    event.preventDefault()
                    event.stopPropagation()
                    form.addClass('was-validated');
                    return false;
                    }else{

                    if($("#_repackage").val() == 1){
                        $.confirm({
                            title: 'Guardar Paquete!',
                            content: 'Seguro desea guardar el registro?',
                            type: 'warning',
                            typeAnimated: true,
                            buttons: {
                                aceptar: {
                                    text: 'aceptar',
                                    btnClass: 'btn-blue',
                                    action: function(){
                                    $("#_frm_instrucctions").submit()
                                    }
                                },
                                cancelar: function () {
                                }
                            }
                        });

                    }else{
                        $("#_frm_instrucctions").submit()
                    }
                    
                    }


                });


    });

</script>
<style type="text/css">
    body {
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
@stop
