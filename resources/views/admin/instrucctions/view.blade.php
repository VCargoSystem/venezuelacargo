@extends('adminlte::page')
@section('title', 'INSTRUCCIONES')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">DETALLE DE INSTRUCCIÓN</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                      
                          <div class="row">
                             <div class="row col-8">
                              <div class="col-6">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">DATOS BÁSICOS</label>
                                
                                  <hr>
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">WAREHOUSE: </label><span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{  $package->ware_house }}</span><br>
                                  
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">TRACKING: </label><span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{  $package->trackings[0]->tracking }}</span><br>
                                  
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">CÓDIGO DE CLIENTE: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->user->code }}
                                      </span><br>
                                  
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">NOMBRE DE CLIENTE: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->user->name.' '.$package->user->last_name }}
                                      </span><br>
                                  
                                 
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">CONTENIDO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->description }}
                                      </span><br>
                                  
                                
                              </div>
                                
  
                              <div class="col-6">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">MEDIDAS</label>
                                <hr>
                               
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">ANCHO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->width }} IN
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">ALTO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->high }} IN
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">LARGO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->long }} IN
                                      </span><br>
                                 
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">PESO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->weight }} LB
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">NÚMERO DE PIEZAS: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->nro_pieces }}
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">LB VOLUMÉTRICA: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{  number_format((($package->long * $package->width * $package->high)/ 166) * $package->nro_pieces, 3) }}
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">PIE CÚBICO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{  number_format((($package->long * $package->width * $package->high)/ 1728) * $package->nro_pieces, 3) }}
                                      </span><br>
                                 
                              </div>

                              <div class="col-12">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">INSTRUCCIONES</label>
                                 <hr>
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">ALMACÉN: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->hold==1? 'Guardar en Almacen': 'No Guardar en Almacen' }} 
                                        </span>
                                        <br>
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">SEGURO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">${{ $package->insured }} 
                                        </span>
                                   <br>
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">REEMPAQUE: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->repackage == 1? 'Con reempaque': 'Sin reempaque' }} 
                                        </span>
                                   <br>
                                   
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">TIPO DE ENVÍO: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->shippins_id == 1? 'MARÍTIMO': 'AÉREO' }} 
                                        </span>
                                    
                              </div>
                            </div>
                              
                            <div class="col-4">
                              <div class="col-12">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">LISTADO DE TRACKING</label>
                                  <div class="row card mb-2 border rounded-lg  bg-light" style="max-height: 170px !important; min-height: 160px !important; font-size: 15px">

                                    <div class="card-body pt-2" style="display: block;">
                                        <ul id="list_tracking" class="nav nav-pills flex-column">

                                          @foreach($package->trackings as $item)
                                           <li class="nav-item ">
                                             <b class="pl-3">{{ $item->tracking }}</b>
                                          </li>
                                          @endforeach
                                        </ul>
                                    </div>
                                </div>
                              </div>

                              <label class="pt-2 text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">IMAGEN DEL PAQUETE</label>
                              <div class="col-12 border rounded-lg text-center p-3" style="max-height: 185px !important; max-width: auto !important;">
                                  <img src="{{ $package->images[0]->url }}" class="img-fluid" id="img-upload" style="max-height: 150px !important; max-width: auto  !important;"/>
                              </div>
                            </div>
                          
                          </div>

                    {{-- Save button --}}
                    <div class="row mb-3 mt-1 float-right ">
                      <div class="col-12">
                        <a href="{{ route('admin.instrucctions.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">VOLVER
                        </a>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">
$(document).ready(function() {


});
 
</script>
@stop
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
        
        text-transform: uppercase !important;
    }
</style>
