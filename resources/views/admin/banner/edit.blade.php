@extends('adminlte::page')
@section('title', 'EDITAR BANNER')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">EDITAR BANNER</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                          <form  method="post" id="_form_register" action="{{ route('admin.banner.update', $banner->id) }}" ccept-charset="UTF-8" enctype="multipart/form-data" class="needs-validation">
                              {{ method_field('PATCH') }}

                              {{ csrf_field() }}

                                <input type="hidden" name="id" value="{{ $banner->id }}">

                                <div class="row mb-2">
                                    <div class="col-12">
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">URL</label>
                                        <input type="text" name="description" style="font-size:14px !important;"  id="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                               value="{{ $banner->description }}" placeholder="URL" autofocus required>
                                  
                                        @if($errors->has('description'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </div>
                                        @endif

                                         <div class="invalid-feedback">
                                                <strong>Campo requerido</strong>
                                          </div>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                  <div class="col-12">
                                    <label style="font-size:15px !important; letter-spacing: -0.5px !important;">IMAGEN</label>
                                    <div class="border rounded-lg text-center p-3" style="max-height: 250px !important; max-width: auto !important;">
                                        <img src="{{ $banner->url }}" class="img-fluid" id="img-upload" style="max-height: 230px !important; max-width: auto  !important;"/>
                                    </div>
                                    <div class="custom-file mt-1">
                                        <input type="file" class="custom-file-input form-control form-control {{ $errors->has('image') ? 'is-invalid' : '' }}" id="image" name="image" accept="image/png, image/jpeg, image/jpeg, image/svg" >
                                        <label class="custom-file-label" for="customFile">Seleccione el archivo</label>
                                        @if($errors->has('image'))
                                            <div class="invalid-feedback">
                                              <strong>{{ $errors->first('image') }}</strong>
                                            </div>
                                        @endif

                                        <div class="invalid-feedback">
                                                <strong>Campo requerido</strong>
                                        </div>

                                        <div id="error_img" class="text-danger invalid-feedback" style="display: none; font-weight: 700">
                                          <strong>Formato invalido</strong></div>
                                      </div>
                                  </div>
                                </div>
                           


              {{-- Save button --}}
                    <div class="row mt-4 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('admin.banner.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="button" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {
     $(".decimales").inputNumberFormat({ 'decimal': 2, 'decimalAuto': 2 });


       $("#image").on('change',function(){
       $('#error_img').css("display", "none");
       $(this).removeClass('is-invalid');

        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(png|jpeg|jpg|svg|bmp)$");

        if (!(regex.test(val))) {
            $(this).val('');
            $('#error_img').text('Formato invalido')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        }else if($(this)[0].files[0].size/1024 > 6000){
            $(this).val('');
            $('#error_img').text('Limite superado (6MB))')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        }else{
            var input = $(this)[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result).fadeIn('slow');
                }
                reader.readAsDataURL(input.files[0]);

            }
        }
    });


  //Guardar Banner
  $('#_btn_save').on('click', function(){
 
      var form = $("#_form_register")

      if (form[0].checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
            form.addClass('was-validated');
            return false;

      }else{

          form.metod = 'post'
          form.submit()
      }

   });
   
  });

</script>
@stop

<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>
