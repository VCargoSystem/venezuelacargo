@extends('adminlte::page')
@section('title', 'EDITAR STATUS DE ENVÍO')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">EDITAR STATUS DE ENVÍO</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                               <form  method="post" id="_form_register" action="{{ route('admin.status.update', $status->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                <input type="hidden" name="id" value="{{ $status->id }}">
                                <div class="row mb-2">
                                    <div class="col-6">
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">NOMBRE</label>
                                        <input type="text" name="name" style="font-size:14px !important;"  id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                               value="{{ $status->name}}" placeholder="nombre" autofocus>
                                  
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                           
                                    <div class="col-6">
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">CÓDIGO</label>
                                        <input type="text" name="code" style="font-size:14px !important;"  id="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}"
                                               value="{{ $status->code}}" placeholder="CÓDIGO" autofocus>
                                  
                                        @if($errors->has('code'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-6">
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">LATITUD</label>
                                        <input type="text" name="latitude" style="font-size:14px !important;"  id="latitude" class="form-control {{ $errors->has('latitude') ? 'is-invalid' : '' }}"
                                               value="{{ $status->latitude}}" placeholder="LATITUD">
                                  
                                        @if($errors->has('latitude'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('latitude') }}</strong>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-6">
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">LONGITUD</label>
                                        <input type="text" name="longitude" style="font-size:14px !important;"  id="longitude" class="form-control {{ $errors->has('longitude') ? 'is-invalid' : '' }}"
                                               value="{{ $status->longitude}}" placeholder="LONGITUD" autofocus>
                                  
                                        @if($errors->has('longitude'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('longitude') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">

                                  <div class="col-11  mb-3">
                                    <label>UBICACIÓN</label>
                                    <input type="text" name="address1" id="address1" class="form-control {{ $errors->has('address1') ? 'is-invalid' : '' }}"
                                            value="" placeholder="">
                                       
                                        @if($errors->has('address1'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('address1') }}</strong>
                                            </div>
                                        @endif
                              </div>

                                <div class="col-md-1 pt-4 mt-2">
                                  <button id="locate" class="btn btn-block btn-primary btn-lg" type="button" alt="Buscar Direccion" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">
                                      <i class="fa fa-search"></i>
                                  </button> 
                                </div>
                            </div>

                            <div class="row mb-2">
                              <div class="col-12">
                                <div id="map_frame" class="embed-responsive" style="width:100%;display:block;background:#F2F4F4; height: 450px !important; border-style: solid; border-width: 0.1px;"></div>
                              </div>        
                            </div>    

              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('admin.status.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {
    //Map and Locator 
    $(".decimales").inputNumberFormat({ 'decimal': 2, 'decimalAuto': 2 });




  });

      $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCfw4y4wtDVO4K4MMqm79Wuou1HsnwZ9lc", function() {

   
        initLocate()

        $("#locate").click(function() {
 
            var direccion = $("#address1").val();
            if (direccion !== "") {
                localizar("map_frame", direccion);
            }
        });

    });

  function localizar(elemento,direccion) {
        
      var geocoder = new google.maps.Geocoder();
        
      geocoder.geocode({'address': direccion}, function(results, status) {
            if (status === 'OK') {

               var map = new google.maps.Map(document.getElementById("map_frame"), {
                    center:new google.maps.LatLng(_lat,_lon),
                    zoom: 16,
                    scrollwheel: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
                
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });

                  
                $("#longitude").val(resultados_long)
                $("#latitude").val(resultados_lat)


            } else {

                var mensajeError = "";
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                //alert(mensajeError);
            }
        });
    }

    //Busqueda inicial
  function initLocate() {
      _lat = parseFloat("<?=$status->latitude?>")
      _lon = parseFloat("<?=$status->longitude?>")

      var map = new google.maps.Map(document.getElementById("map_frame"), {
          center:new google.maps.LatLng(_lat,_lon),
          zoom: 16,
          scrollwheel: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });

        
      var marker = new google.maps.Marker({
          map: map,
            position:  {lat:_lat , lng: _lon},
      });
     
      var geocoder = new google.maps.Geocoder();

      geocoder.geocode({
          'location': {
            lat: _lat,
            lng: _lon
          }
        }, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
             
              $("#address1").val(results[1].formatted_address)

            } 
          } 
        });
  }

</script>
@stop

<style type="text/css">
    body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>
