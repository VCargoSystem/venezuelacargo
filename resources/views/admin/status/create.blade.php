@extends('adminlte::page')
@section('title', 'EDITAR STATUS DE ENVÍO')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 600;">EDITAR STATUS DE ENVÍO</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                               <form  method="post" id="_form_register" action="{{ route('admin.box.update', $box->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                <input type="hidden" name="id">
                                <div class="row mb-2">
                                    <div class="col-12">
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">NOMBRE</label>
                                        <input type="text" name="name" style="font-size:14px !important;"  id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                               value="{{ $status->name}}" placeholder="Nombre" autofocus>
                                  
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                  <div class="row mb-2">
                                    <div class="col-12">
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">CÓDIGO</label>
                                        <input type="text" name="name" style="font-size:14px !important;" id="name" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}"
                                               value="{{ $status->code}}" placeholder="Código" autofocus>
                                  
                                        @if($errors->has('code'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mt-2">
                                  <div class="col-12">
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">LATITUD</label>
                                      <div class="input-group mb-3">
                                          <input type="text" name="latitude" style="font-size:14px !important;" id="latitude" class="form-control {{ $errors->has('cost') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ $status->latitude }}" placeholder="Latitud" autofocus maxlength="20" min="1">
                                          <div class="input-group-append">
                                            <span class="input-group-text">$</span>
                                          </div>
                                          @if($errors->has('latitude'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ 
                                                    $errors->first('latitude') }}
                                                  </strong>
                                              </div>
                                          @endif
                                  </div>
                                </div>
                                </div>


              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('admin.box.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
                            background-color: #23298a;
                            border-color: #23298a;
                            box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {
     $(".decimales").inputNumberFormat({ 'decimal': 2, 'decimalAuto': 2 });
  });

  function localizar(elemento,direccion) {
        
      var geocoder = new google.maps.Geocoder();
        
      geocoder.geocode({'address': direccion}, function(results, status) {
            if (status === 'OK') {

               var map = new google.maps.Map(document.getElementById("map_frame"), {
                    center:new google.maps.LatLng(_lat,_lon),
                    zoom: 16,
                    scrollwheel: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
                
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });

                  
                $("#longitude").val(resultados_long)
                $("#latitude").val(resultados_lat)


            } else {

                var mensajeError = "";
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                //alert(mensajeError);
            }
        });
    }
</script>
@stop

<style type="text/css">
    body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>
