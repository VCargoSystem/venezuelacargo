@extends('adminlte::page')
@section('title', 'INSTRUCCIONES')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">DETALLE DE LA INSTRUCCIÓN</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                      
                          <div class="row" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                             <div class="row col-6">
                              <div class="col-6">
                                <label class="text-primary">DATOS BÁSICOS</label>
                                <ul class="nav nav-pills flex-column">
                                  <li class="nav-item ">
                                      <label>WARE HOUSE: </label><span class="pl-2">{{  $package->ware_house }}</span>
                                  </li>
                                  <li class="nav-item">
                                      <label>TRACKING: </label><span class="pl-2">{{  $package->trackings[0]->tracking }}</span>
                                  </li>
                                  <li class="nav-item">
                                      <label>CÓDIGO DE CLIENTE: </label> <span class="pl-2">{{ $package->user->code }}
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>NOMBRE DE CLIENTE: </label> <span class="pl-2">{{ $package->user->name.' '.$package->user->last_name }}
                                      </span>
                                  </li>
                                 
                                  <li class="nav-item">
                                      <label>CONTENIDO: </label> <span class="pl-2">{{ $package->description }}
                                      </span>
                                  </li>
                                </ul>
                              </div>
                                
  
                              <div class="col-6" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                                <label class="text-primary">MEDIDAS</label>
                                <ul class="nav nav-pills flex-column">
                                  <li class="nav-item">
                                      <label>ANCHO: </label> <span class="pl-2">{{ $package->width }} In
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>ALTO: </label> <span class="pl-2">{{ $package->high }} In
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>LARGO: </label> <span class="pl-2">{{ $package->long }} In
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>PESO: </label> <span class="pl-2">{{ $package->weight }} Lbs
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>VOLUMEN: </label> <span class="pl-2">{{ $package->lb_volume }} Lbs
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>PIE CÚBICO: </label> <span class="pl-2">{{ $package->pie_cubico }} In
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>NÚMERO DE CAJAS: </label> <span class="pl-2">{{ $package->nro_box }} 
                                      </span>
                                  </li>
                                  <li class="nav-item">
                                      <label>NÚMERO DE PIEZAS: </label> <span class="pl-2">{{ $package->nro_pieces }}
                                      </span>
                                  </li>

                                </ul>
                              </div>

                              <div class="col-12" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                                <label class="text-primary">INSTRUCCIONES</label>
                                  <ul class="nav nav-pills flex-column">
                                    <li class="nav-item">
                                        <label>ALMACEN: </label> <span class="pl-2">{{ $package->hold==1? 'Guardar en Almacen': 'No Guardar en Almacen' }} 
                                        </span>
                                    </li>
                                    <li class="nav-item">
                                        <label>SEGURO: </label> <span class="pl-2">{{ $package->insured }} 
                                        </span>
                                    </li>
                                    <li class="nav-item">
                                        <label>REEMPAQUE: </label> <span class="pl-2">{{ $package->repackage == 1? 'Con reempaque': 'Sin reempaque' }} 
                                        </span>
                                    </li>
                                    <li class="nav-item">
                                        <label>TIPO DE ENVÍO: </label> <span class="pl-2">{{ $package->shippins_id == 2? 'AEREO': 'MARITIMO' }} 
                                        </span>
                                    </li>
                                  </ul>
                              </div>
                            </div>
                              
                            <div class="col-6" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                              <div class="col-12">
                                <label class="text-primary">LISTADO DE TRACKINGS</label>
                                  <div class="row card mb-2 border rounded-lg  bg-light" style="max-height: 170px !important; min-height: 160px !important; font-size: 13px">

                                    <div class="card-body pt-2" style="display: block;">
                                        <ul id="list_tracking" class="nav nav-pills flex-column">

                                          @foreach($package->trackings as $item)
                                           <li class="nav-item ">
                                             <b class="pl-3">{{ $item->tracking }}</b>
                                          </li>
                                          @endforeach
                                        </ul>
                                    </div>
                                </div>
                              </div>

                              <label class="pt-2 text-primary">IMAGEN DEL PAQUETE</label>
                              <div class="col-12 border rounded-lg text-center p-3" style="max-height: 400px !important; max-width: auto !important;">
                                  <img src="{{ $package->images[0]->url }}" class="img-fluid" id="img-upload" style="max-height: 300px !important; max-width: auto  !important;"/>
                              </div>
                            </div>
                          
                          </div>

                    {{-- Save button --}}
                    <div class="row mb-3 mt-1 float-right ">
                      <div class="col-12">
                        <a href="{{ route('clients.instrucctions.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">ATRAS
                        </a>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>

@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">
$(document).ready(function() {


});
 
</script>
@stop
