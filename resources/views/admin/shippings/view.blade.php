@extends('adminlte::page')
@section('title', 'Envios')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">Detalle del Envío</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                      
                          <div class="row">
                             <div class="row col-8">
                              <div class="col-6">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">Datos Básicos</label>
                               <hr>
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Warehouse: </label><span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{  $package->ware_house }}</span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Tracking: </label><span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{  $package->trackings[0]->tracking }}</span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Código de Cliente: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->code }}
                                      </span><br>
                                 
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Nombre de Cliente: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->name.' '.$package->last_name }}
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Contenido: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->description }}
                                      </span>
                                  
                              </div>
                                
  
                              <div class="col-6">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">Medidas</label>
                               <hr>
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Ancho: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->width }} IN
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Alto: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->high }} IN
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Largo: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->long }} IN
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Peso: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->weight }} LB
                                      </span><br>
                                 
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Volumen: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->lb_volumen }}
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Pie cúbico: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->pie_cubico }}
                                      </span><br>
                                  
                                      <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Número de piezas: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->nro_pieces }}
                                      </span>
                                  
                              </div>

                              <div class="col-12">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">Instrucciones</label>
                                 <hr> 
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Almacén: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->hold==1? 'Guardar en Almacen': 'No Guardar en Almacen' }} 
                                        </span><br>
                                    
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Seguro: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">${{ $package->insured ? $package->insured :0}} 
                                        </span><br>
                                    
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Reempaque: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->invoice_repackage == 1? 'Con reempaque': 'Sin reempaque' }} 
                                        </span><br>
                                    
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Tipo de envío: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->shippins_id == 2? 'AÉREO': 'MARÍTIMO' }} 
                                        </span><br>
                                    
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Fecha estimada de llegada a Venezuela: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{
                                         \Carbon\Carbon::parse($package->date_delivery)->format('d-m-Y')}} 
                                        </span><br>
                                    
                                        <label style="font-size:15px !important; letter-spacing: -0.5px !important;">Número de Container: </label> <span class="pl-2" style="font-size:15px !important; letter-spacing: -0.5px !important;">{{ $package->nro_container!=''?$package->nro_container : 'Sin Asignar'}} 
                                        </span>
                                    
                              </div>
                            </div>
                              
                            <div class="col-4">
                              <div class="col-12">
                                <label class="text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">Listado de Trackings</label>
                                  <div class="row card mb-2 border rounded-lg  bg-light" style="max-height: 170px !important; min-height: 160px !important; font-size: 15px">

                                    <div class="card-body pt-2" style="display: block;">
                                        <ul id="list_tracking" class="nav nav-pills flex-column">

                                          @foreach($package->trackings as $t)
                                           <li class="nav-item ">
                                             <b class="pl-3">{{ $t->tracking }}</b>
                                          </li>
                                          @endforeach
                                        </ul>
                                    </div>
                                </div>
                              </div>

                              <label class="pt-2 text-primary" style="color:#24298D !important; font-size:15px !important; letter-spacing: -0.5px !important;">Imagen del paquete</label>
                              <div class="col-12 border rounded-lg text-center p-3" style="max-height: 185px !important; max-width: auto !important;">
                                  <img src="{{ $images->url }}" class="img-fluid" id="img-upload" style="max-height: 150px !important; max-width: auto  !important;"/>
                              </div>
                            </div>
                          
                          </div>

                    {{-- Save button --}}
                    <div class="row mb-3 mt-1 float-right ">
                      <div class="col-12">
                        <a href="{{ route('admin.shippings.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">VOLVER
                        </a>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">
$(document).ready(function() {


});
 
</script>
@stop
-
<style type="text/css">

  body{
        font-family: 'Montserrat', sans-serif !important;
       
        text-transform: uppercase !important;
    }

</style>