@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
    <h5><b>Usuarios</b></h5>
@stop

@section('content')

@if (session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            <li>{!! session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered datatable">
                  <thead>                  
                    <tr>
                      <th width="5%">#</th>
                      <th>Cedula</th>
                      <th>Usuario</th>
                      <th>Teléfono</th>
                      <th>Email</th>
                      <th>Código</th>
                      <th width="15%">Accion</th>
                    </tr>
                  </thead>
                  <tbody>
           
                        <tr>
  	                    
                        </tr>
  						    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
            
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

  <!--modal asignar code-->
  <section>
    <div class="modal fade" id="modal_code" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modelHeading"><b>Agregar Codigo</b></h6>
            </div>
            <div class="modal-body">
              <div id="content" style="display: none">
                <div class="loading text-center"><img src="{{ asset('img/ajax-loader.gif') }}" alt="loading" /></div>
              </div>

                <form id="frm_code" novalidate="" name="frm_code" class="form-horizontal" action="" method="POST">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                   <input type="hidden" name="user_id" id="user_id">
      
      
                  <div class="form-group">
                      <label class="form-control-label" for="code">Codigo</label>
                      <input type="text" class="form-control" name="code" id="code" required>
                      <div class="valid-feedback">Success! You've done it.</div>
                      <div class="invalid-feedback">No, you missed this one.</div>
                  </div>
                    <div class="row float-right">
                      <div class="col-sm-12">
                        <button type="button" class="btn btn-default" id="btn_cancel" value="cancel">Cancelar
                        </button>
                        <button type="button" class="btn btn-primary" id="btn_save_code" value="create">Guardar
                        </button>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: "{{ route('users.getFilterUsers') }}",
            columns: [
                {data: 'id', name: 'id', orderable:false},
                {data: 'dni', name: 'dni'},
                {data: 'name', render:function ( data, type, row ) {  return row.name+' '+row.last_name;
                }},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'codes', name: 'codes'},
                {data: 'actions', name: 'actions',orderable:false,serachable:false,sClass:'text-center'}
            ]
        });

       //Assign code
        $('body').on('click', '.assing-code', function(e) {
          e.preventDefault();
          $("#code").val('')
          let user_id= $(this).attr('data-id');

          $("#user_id").val(user_id)
          $('#modal_code').modal('show');

        });

        $('#btn_save_code').on('click', function(){

          // Fetch form to apply custom Bootstrap validation
          var form = $("#frm_code")

          if (form[0].checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.addClass('was-validated');

          $('#content').css('display','block')

          $.ajax({
            data: {
              "code" : $('#code').val(),
              "user_id": $("#user_id").val(),
              "_token": "{{ csrf_token() }}"
            },
            url: "{{ route('users.codeUpdate') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#frm_code').trigger("reset");
                
                if(data.success==1){
                  $('#modal_code').modal('hide');
                  $("#msg").html(data.message)
                  $(".alert2").css('display', 'block')
                  $('#content').css('display','none')
                  dataTable.draw() 
                }
            },
            error: function (data) {

            }
          });

           
        });

        $('#btn_cancel').on('click', function(e) {
          $('#modal_code').modal('hide');
        });
  });


    
</script>
@stop
