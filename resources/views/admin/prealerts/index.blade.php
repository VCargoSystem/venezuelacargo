@extends('adminlte::page')

@section('title', 'PREALERTAS')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">PREALERTAS</b></h4>
@stop

@section('content')


@if (session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            <li> {!! session()->get('success')!!}</li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

<section class="content">
      <div class="container-fluid">
        <div class="row">
          {{--<div class="col-12 mb-1 pr-3">
           <div class="row mb-1 float-right ">
              <a href="{{ route('prealerts.create') }}" class="btn btn-sm btn-primary" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">AGREGAR</a>
  
            </div>
          </div>--}}

          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
               <div class="card-header">
                 
                    <form>

                      
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">CLIENTE</label>
                              <select class="form-control select2 select2-hidden-accessible" style="width: 100%; min-height: 70px !important; font-size:14px;"  tabindex="-1" aria-hidden="true"  name="usuario" id="usuario">
                              </select>
                          </div>

                          <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">TRACKING</label>
                            <input type="text" name="tracking" style="font-size:14px;" id="tracking" class="form-control form-control-sm  {{ $errors->has('tracking') ? 'is-invalid' : '' }} mr-1" value="" placeholder="Tracking" autofocus maxlength="50">
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                          <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">DESDE</label>

                          <input type="date" name="date" id="date" style="font-size:14px;" class="form-control form-control-sm  {{ $errors->has('date') ? 'is-invalid' : '' }} mr-1" value="" placeholder="Fecha de registro - Desde" autofocus maxlength="20">
                        </div>
                          </div>
                          <div style="height:7px;"></div>
                          <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                            <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                              <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">CÓDIGO</label>
                              <input type="text" name="code" id="code" style="font-size:14px;" class="form-control form-control-sm  mr-1" value="" placeholder="Código" autofocus maxlength="30">
                            </div>
                            <div style="height:7px;"></div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                              <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">HASTA</label>
                              <input type="date" name="date1" id="date1" style="font-size:14px;" class="form-control form-control-sm  {{ $errors->has('date1') ? 'is-invalid' : '' }} mr-1" value="" placeholder="Fecha de registro - Hasta" autofocus maxlength="20">
                              </div>
                           </div>
                           <div style="height:7px;"></div>
                           <div class="row">
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                              <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                  <button id="_btn_filter" class=" ml-1 btn btn-primary btn-sm col-lg-12  col-sm-12 col-xs-12" style=" color: #fff;
                                          background-color: #23298a;
                                          border-color: #23298a;
                                          box-shadow: none; margin-top: 7px !important; margin-left:0px !important;"><i class="fas fa-search"></i>&nbsp;BUSCAR</button>
                                </div>

                                <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                  <button id="_btn_refresh" class=" ml-1 btn btn-primary btn-sm col-lg-12 col-sm-12 col-xs-12" style=" color: #fff;
                                          background-color: #23298a;
                                          border-color: #23298a;
                                          box-shadow: none; margin-top: 7px !important; min-height:31px !important; margin-left:0px !important;" >
                
                                      <i class="fas fa-eraser"></i>&nbsp;LIMPIAR CAMPOS
                                    </button>
                                </div>
                              </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                          </div>
                  </form>
                 
                </div>
              <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped table-responsive-lg table-responsive-md table-responsive-sm table-responsive-xs table-bordered datatable table-bordered" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                  <thead>
                    <tr>
                      <th width="20%" style="background:#24298d; color:#ffb901;">CLIENTE</th>
                      <th width="20%" style="background:#24298d; color:#ffb901;">TRACKING</th>
                      <th width="15%" class="text-center" style="background:#24298d; color:#ffb901;">CÓDIGO</th>                      
                      <th width="15%" class="text-center" style="background:#24298d; color:#ffb901;">TRANSPORTE</th>
                      <th width="15%" class="text-center" style="background:#24298d; color:#ffb901;">FECHA ESTIMADA DE LLEGADA</th>
                      <th width="15%"class="text-center" style="background:#24298d; color:#ffb901;">ACCIONES</th>
                    </tr>
                  </thead>

                  <tbody>

                  </tbody>
                </table>
         
              </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
            
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

<style type="text/css">
  .tracking{
    word-break: break-all;
  }

   .select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;
    min-height: 30px;
  }


  .form-control {
    display: block;
    width: 100%;
    height: calc(1.95rem + 1px) !important;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-shadow: inset 0 0 0 transparent;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
  }

  body{
        font-family: 'Montserrat', sans-serif !important;
    }

    .table td, .table th {
        padding: .40rem !important;
        vertical-align: middle !important;
        border-top: 1px solid #dee2e6;
    }

    div.dataTables_wrapper .dataTables_info {
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination{
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-blue {
    border-top: solid 10px #f44336 !important;
}
</style>

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

      $('.select2').select2();
      $('.select2-selection').css('min-height','67px')

      $('.select2').select2({
        placeholder: 'Seleccionar Usuario',
        ajax: {
            url: "{{ route('users.usersAutocomplete') }}",
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                  return {
                      results: $.map(data, function (item) {
                          return {
                              text: item.name+' '+item.last_name+' - '+item.code,
                              id: item.id
                          }
                      })
                  };
              },
              cache: true
          }
      });


        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            paging: false,
            paging: true,
            bLengthChange: false,
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: {
              url: "{{ route('prealerts.getFilterPrealerts') }}",
              data: function(d){
                d.code = $('#code').val(),
                d.usuario = $('#usuario').val(),
                d.tracking = $('#tracking').val(),
                d.date = $('#date').val(),
                d.date1 = $('#date1').val()
              }
            },
            columns: [
              {data: 'cliente', 'searchable': false, render:function ( data, type, row ) {  
                  return '<span class="tracking">'+row.cliente+'</span>';
                }},
                {data: 'tracking', 'searchable': false, render:function ( data, type, row ) {  
                  return '<span class="tracking">'+row.tracking+'</span>';
                }},
                {data: 'code_user', sClass:'text-center', 'searchable': false, render:function ( data, type, row ) {  
                  return '<span class="tracking">'+row.code_user+'</span>';
                }},
                
                {data: 'company', sClass:'text-center', 'searchable': false, render:function ( data, type, row ) {  
                  return '<span class="tracking" style="text-transform:uppercase;">'+row.company+'</span>';
                }},
                {data: 'date_in', sClass:'text-center', name: 'date_in'},
                {data: 'actions', name: 'actions',orderable:false,serachable:false,sClass:'text-center'}
            
            ]
        });

        

        //Delete user
        $('body').on('click', '.delete', function(e) {
          e.preventDefault();

          _id = $(this).attr("data-id");

          $.confirm({
                title: '',
                content: '¿Desea eliminar el registro?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    delete: {
                        text: 'Eliminar',
                        btnClass: 'btn-red',
                        action: function(){
                          deletePrealerts(_id)
                        }
                    },
                    cancelar: function () {
                    }
                }
            });

        });

        function deletePrealerts(_id){

            $.ajax({
              url: "<?=url("/admin/prealerts/")?>/"+_id,
              type: "DELETE",
              dataType: 'json',
              data: {
                "id": _id,
                "_token": "{{ csrf_token() }}"
              },
              success: function (data) {

                  if(data.success==1){
                    $("#msg").html(data.message)
                    $(".alert2").css('display', 'block')
                    $('#content').css('display','none')
                    dataTable.draw() 
                  }
              },
              error: function (data) {

              }
          });

        }

      $('#_btn_filter').on('click', function(e) {
        e.preventDefault();
        sw = 0;

        if($("#date").val() !="" && $("#date1").val() ==""){
          sw = 2
        }else if($("#date").val() =="" && $("#date1").val() !=""){
          sw = 2
        }else if($("#date").val() !="" && $("#date1").val() !=""){
            let date1 = new Date($("#date").val());
            let date2 = new Date($("#date1").val());

            if(date1 > date2){
              $.confirm({
                title: '',
                content: 'Indique un rango de fecha válido',
                type: 'blue',
                typeAnimated: true,

                buttons: {
                  cancelar: function () {

                  }
                }
              });
              return false;
            }
        }

        if(sw == 2){
          $.confirm({
              title: '',
              content: 'Indique un rango de fechas',
              type: 'blue',
              typeAnimated: true,

              buttons: {
                cancelar: function () {

                }
              }
          });
          return false;
        }

        dataTable.draw()
      });  

      $('#_btn_refresh').on('click', function(e) {
          e.preventDefault();
          
          $("#date").val('')
          $("#date1").val('')
          $("#tracking").val('')
          $("#code").val('')
          $("#usuario").val('')
          $("#usuario").val('').trigger('change')
          dataTable.draw()

      });

  });


    
</script>
@stop
