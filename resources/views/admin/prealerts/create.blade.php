@extends('adminlte::page')
@section('title', 'Prealertas')

@section('content_header')
    <h5><b style="color:#24298D; font-weight: 600;"> <strong>CREAR PREALERTA</strong> </b></h5>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif


  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                               <form  method="post" id="_form_register" action="{{ route('prealerts.store') }}">
                                {{ csrf_field() }}

                                {{-- Name field --}}

                                <div class="row mb-2">
                         
                                  <div class="col-6">
                                      <label>Tracking</label>
                                        <input type="text" name="tracking" id="tracking" class="form-control {{ $errors->has('tracking') ? 'is-invalid' : '' }}"
                                               value="{{ old('value')  }}" placeholder="Tracking" autofocus maxlength="30">
                                      
                                        @if($errors->has('tracking'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('tracking') }}</strong>
                                            </div>
                                        @endif
                                  </div>

                                  <div class="col-6">
                                      <label>Fecha estimada de Llegada</label>

                                        <input type="date" name="date_in" id="date_in" class="form-control {{ $errors->has('date_in') ? 'is-invalid' : '' }}"
                                               value="{{ old('value')  }}" placeholder="Fecha estimada de Llegada" autofocus >
                                      
                                        @if($errors->has('date_in'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('date_in') }}</strong>
                                            </div>
                                        @endif
                                  </div>

                                </div>

                                <div class="row mb-2">
                                 
                                  <div class="col-6">
                                      <label>Usuario</label>
                                      <select class="form-control select2 select2-hidden-accessible" style="width: 100%; min-height: 70px !important;"  tabindex="-1" aria-hidden="true"  name="user_id" id="user_id">
                                            @foreach($users as $user)
                                              <option value="{{ $user->id }}">{{ $user->name.' '.$user->last_name}} ({{ $user->username }})</option>
                                            @endforeach
                                      </select>
                                           
                                      @if($errors->has('user_id'))
                                          <div class="invalid-feedback">
                                              <strong>{{ $errors->first('user_id') }}</strong>
                                          </div>
                                      @endif
                                  </div>

                                  <div class="col-5 ">
                                          <label>Empresa</label>
                                          <select class="form-control select2 select2-hidden-accessible" style="width: 100%; min-height: 70px !important;"  tabindex="-1" aria-hidden="true"  name="company_id" id="company_id">
                                            @foreach($companies as $com)
                                              <option value="{{ $com->id }}">{{ $com->name }}</option>
                                            @endforeach
                                        </select>
                                           
                                        @if($errors->has('company_id'))
                                          <div class="invalid-feedback">
                                              <strong>{{ $errors->first('company_id') }}</strong>
                                          </div>
                                        @endif
                                  </div>

                                  <div class="col-1 mt-2">
                                            <label></label>
                                            <button id="_btn_add_company" class="btn btn-block btn-primary"  type="button" alt="Agregar Empresa" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">
                                                <i class="fa fa-plus"></i>
                                            </button> 
                                   
                                  </div>

                                </div>

                                    {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('prealerts.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">Cancelar
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">Guardar
                        </button>
                      </div>
                    </div>
                              </div>

                

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>


<!--modal asignar code-->
<section>
    <div class="modal fade" id="modal_company" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modelHeading"><b>Agregar Empresa</b></h6>
            </div>
            <div class="modal-body">
              <div id="content" style="display: none">
                <div class="loading text-center"><img src="{{ asset('img/ajax-loader.gif') }}" alt="loading" /></div>
              </div>

                 <form  method="post" id="_form_company" >
                      {{ csrf_field() }}

                      <div class="form-group">
                            <label class="form-control-label" for="company_name">Nombre</label>
                            <input type="text" class="form-control" name="company_name" id="company_name" required>
                            <div class="invalid-feedback">Campo requerido.</div>
                      </div>
                                  
                      <div class="form-group">
                          <label class="form-control-label" for="company_code">Código</label>
                          <input type="text" class="form-control" name="company_code" id="company_code" required>
                    
                          <div class="invalid-feedback">Campo requerido.</div>
                      </div>

                      <div class="form-group">
                          <label class="form-control-label" for="company_code">Descripción</label>
                          <textarea name="company_description" id="company_description" class="form-control {{ $errors->has('company_description') ? 'is-invalid' : '' }}"
                                   value="{{ old('value') }}" placeholder="Descripción" autofocus required></textarea>
                         
                          <div class="invalid-feedback">Campo requerido.</div>
                      </div>

                    {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <button type="button" class="btn btn-default" id="btn_cancel_company">Cancelar
                        </button> 
                        <button type="button" class="btn btn-primary" id="_btn_save_company" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">Guardar
                        </button>
                      </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">

</head>

<script type="text/javascript">
  $(document).ready(function() {

    if ( $('.alert-danger ul li').length > 1 ) {
      return false;
    }

    $('.select2').select2();
    $('.select2-selection').css('min-height','37px')

    $('#_btn_add_company').on('click', function(){
      $("#company_name").val('')
      $("#company_code").val('')
      $("#company_description").text('')
      $("#modal_company").modal("show");
    });

    $('#btn_cancel_company').on('click', function(){
      $("#modal_company").modal("hide");
    });

    $('#_btn_save_company').on('click', function(){

          // Fetch form to apply custom Bootstrap validation
          var form = $("#_form_company")

          if (form[0].checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
             form.addClass('was-validated');
            return false;
          }else{

              form.addClass('was-validated');

              $('#content').css('display','block')

              $.ajax({
                data: {
                  "name": $("#company_name").val(),
                  "code": $("#company_code").val(),
                  "description": $("#company_description").val(),
                  "_token": "{{ csrf_token() }}"
                },
                url: "{{ route('prealerts.storeCompany') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#_form_register').trigger("reset");
                    
                    if(data.success==1){
                          $.confirm({
                            title: 'Crear empresa!',
                            content: 'Empresa creada correctamente',
                            type: 'blue',
                            typeAnimated: true,
                            buttons: {
                                aceptar: {
                                    text: 'Aceptar',
                                    btnClass: 'btn-red',
                                    action: function(){
                                      $('#modal_company').modal('hide');
                                      $('#content').css('display','none')
                                    }
                                }
                            }
                        });

                        selectData(data.companies, data.company_id)
                    }else{
                        $.confirm({
                            title: 'Crear empresa!',
                            content: 'Error al crear la empresa',
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                delete: {
                                    text: 'Aceptar',
                                    btnClass: 'btn-red',
                                    action: function(){
                                      $('#modal_company').modal('hide');
                                      $('#content').css('display','none')
                                    }
                                }
                            }
                        });
                    }
                },
                error: function (data) {

                }
              });
          }
           
        });



  });//Fin button click

  function selectData(companies, company_id){
    _cad=''
    $("#company_id").html('');
                
    $.each(companies, function (index, val) {
        _selected=  (company_id==val.id?'selected':'')
        _cad+= `<option value="${val.id}" ${_selected}>
        ${val.name}</option>` 
    });

    $("#company_id").html(_cad);
  }

</script>
@stop

<style type="text/css">

  body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
