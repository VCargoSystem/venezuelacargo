@extends('adminlte::page')
@section('title', 'PAGO')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;">COMPROBANTE DEL PAGO</b></h4>
@stop

@section('content')


@if (\session('errors'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (session()->has('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>{!! session()->get('success')!!}
        <li> </li>
    </ul>
</div>
@endif
<div class="alert alert-success alert2" style="display: none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">



                    <div class="card-body">
                        <form>
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="row">
                                <div class="col-4">







                                    <div class="row mb-2">
                                        <div class="col-6">
                                            <label
                                                style="font-size:15px !important; letter-spacing: -0.5px !important;">CÓDIGO
                                                DE CLIENTE</label>
                                            <input type="text" name="code" id="code" style="font-size:14px !important;"
                                                class="form-control" value="{{ $invoice->client_id }}"
                                                placeholder="Código de usuario" autofocus maxlength="30" readonly>

                                            @if($errors->has('code'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </div>
                                            @endif
                                            <div class="invalid-feedback">
                                                <strong>
                                                    Código requerido
                                                </strong>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <label
                                                style="font-size:15px !important; letter-spacing: -0.5px !important;">CLIENTE</label>

                                            <input type="text" name="username" id="username"
                                                style="font-size:14px !important;"
                                                class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                                                value="{{ $invoice->user->name.' '.$invoice->user->last_name  }}"
                                                placeholder="Cliente" autofocus maxlength="50" readonly>

                                            @if($errors->has('username'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </div>
                                            @endif
                                            <div class="invalid-feedback">
                                                <strong>
                                                    Cliente requerido
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="height:10px;"></div>
                                    <div class="row">
                                        <div class="col-12">
                                            <label
                                                style="font-size:15px !important; letter-spacing: -0.5px !important;">OPERACIÓN</label>
                                            <textarea name="description" id="description" rows="4"
                                                style="font-size:14px !important; height:75px !important;"
                                                class="form-control"
                                                value="" placeholder="Contenido"
                                                readonly>{{ $invoice->descriptions  }}</textarea>

                                            @if($errors->has('description'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </div>
                                            @endif
                                            <div class="invalid-feedback">
                                                <strong>
                                                    Contenido requerido
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="height:10px;"></div>
                                    <hr>

                                    <div class="row">

                                        <div class="col-6 mt-1">

                                            <label
                                                style="font-size:15px !important; letter-spacing: -0.5px !important;">MONTO
                                                PAGADO</label>
                                            <div class="input-group mb-3">
                                                <input type="number" step="1" name="width" id="width"
                                                    style="font-size:14px !important;" class="form-control"
                                                    value="{{ $invoice->amount }}" placeholder="" readonly>

                                                @if($errors->has('width'))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first('width') }}</strong>
                                                </div>
                                                @endif
                                                <div class="invalid-feedback">
                                                    <strong>
                                                        Ancho requerido
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 mt-1">
                                            <label
                                                style="font-size:15px !important; letter-spacing: -0.5px !important;">NRO
                                                DE REFERENCIA</label>
                                            <div class="input-group mb-3">
                                                <input type="text" name="high" id="high"
                                                    style="font-size:14px !important;" class="form-control"
                                                    value="{{ $invoice->reference  }}" placeholder="" autofocus
                                                    readonly>


                                                @if($errors->has('high'))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first('high') }}</strong>
                                                </div>
                                                @endif
                                                <div class="invalid-feedback">
                                                    <strong>
                                                        Alto requerido
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                        



                                    </div>







                                </div>
                                <div class="col-8">
                                <label style="font-size:15px !important; letter-spacing: -0.5px !important;"
                                            class="pt-2">IMAGEN DEL PAQUETE</label>
                                        <div class="border rounded-lg text-center p-3"
                                            style="max-height: 400px !important; max-width: auto !important;">
                                            <a href="{{ $invoice->capture }}" download><img  src="{{ $invoice->capture }}" class="img-fluid zoom" id="img-upload"
                                                style="max-height: 350px !important; max-width: auto  !important;" /></a>
                                </div>
                                    <small style="color:red;">Puedes visualizar la imagen en la calidad original con solo dar click sobre ella, esta opción te permite descargarla</small>
                                </div>

                                {{-- Save button --}}


                        </form>

                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap'
    rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

<style type="text/css">
    <style type="text/css">.select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 4px;
        min-height: 38px;
    }

    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 38px;
        user-select: none;
        -webkit-user-select: none;
    }

    .form-control {
        display: block;
        width: 100%;
        height: calc(2.33rem + 1px) !important;
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        box-shadow: inset 0 0 0 transparent;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    body {
        font-family: 'Montserrat', sans-serif !important;

    }

   
    img.zoom {
    width: 350px;
    height: 200px;
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    -ms-transition: all .2s ease-in-out;
}
 
.transition {
    -webkit-transform: scale(1.8); 
    -moz-transform: scale(1.8);
    -o-transform: scale(1.8);
    transform: scale(1.8);
}

</style>
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>


<link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap'
    rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>

<script type="text/javascript">
    $(document).ready(function () {

        $('.zoom').hover(function() {
        $(this).addClass('transition');
    }, function() {
        $(this).removeClass('transition');
    });

        $('.select2').select2();

        $('.select2-selection').css('max-height', '25px')
        var data = {
            id: 1,
            text: $("#username").val() + ' - ' + $("#code").val()
        };

        var newOption = new Option(data.text, data.id, false, false);
        $('.select2').append(newOption).trigger('change');

        $('.select2').select2({
            placeholder: 'Seleccionar Usuario',
            ajax: {
                url: "{{ route('users.usersAutocomplete') }}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name + ' ' + item.last_name + ' - ' + item.code,
                                id: item.id,
                                value_name: item.name + ' ' + item.last_name,
                                value_code: item.code,
                                value: item.id,
                            }
                        })
                    };
                },
                cache: true
            }
        });

        _arr_tracking = []
        $('#error_img').css("display", "none");
        loadTrackings()

        //Delete tracking
        $('body').on('click', '.del-tracking', function (e) {
            e.preventDefault();
            _id = $(this).attr('data-id')
            $("#tracking" + _id).val('')
            $(this).closest("li").remove();
        });

        $("#btn_add-tracking").on('click', function () {
            _tracking = $("#tracking1").val();
            _exist = 0
            let _count = $("#list_tracking").children().length;


            if (_tracking == '') {
                return
            } else {
                if (_count == 5) {
                    $.confirm({
                        'title': "Agregar Tracking",
                        content: 'Limite de tracking alcanzados Max(5)',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            delete: {
                                text: 'Aceptar',
                                btnClass: 'btn-red',
                                action: function () {}
                            }
                        }
                    });
                    return
                }

                $("input[name='tracking2[]']").each(function (indice, elemento) {

                    if (elemento.value == _tracking) {
                        _exist = 1
                        return false
                    }
                });


                if (_exist == 0) {
                    _cad = ''
                    _nro = $("#list_tracking").children().length
                    _pos = 0;

                    //find position free
                    $("input[name='tracking1[]']").each(function (indice, elemento) {
                        if (elemento.value == '') {
                            _pos = indice + 1
                            return false;
                        }
                    });


                    _cad = `<li class="nav-item ">
                <a href="#" class="nav-link">
                 ${_tracking}
                 <i class="float-right fa fa-trash text-danger del-tracking" data-id="${_pos}"></i>
                </a>
                <input type="hidden" name="tracking2[]"  value="${_tracking}">
              </li>`

                    $("#list_tracking").append(_cad)

                    $("#tracking" + _pos).val(_tracking)
                    $.confirm({
                        title: 'Agregar Tracking',
                        content: 'Se agrego nuevo tracking',
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            delete: {
                                text: 'Aceptar',
                                btnClass: 'btn-blue',
                                action: function () {}
                            }
                        }
                    });
                } else {
                    $.confirm({
                        title: 'Agregar Tracking',
                        content: 'Ya existe el tracking',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            aceptar: function () {}
                        }
                    });
                }
            }
        });

        $(".decimales").inputNumberFormat({
            'decimal': 2,
            'decimalAuto': 2
        });
        $(".integer").inputNumberFormat({
            'decimal': 2,
            'decimalAuto': 2
        });

        $('#width').on('keyup', function (e) {
            e.preventDefault();
            lbVolumen_pie3()
        });

        $('#high').on('keyup', function (e) {
            e.preventDefault();
            lbVolumen_pie3()
        });

        $('#long').on('keyup', function (e) {
            e.preventDefault();
            lbVolumen_pie3()
        });

        $('#nro_pieces').on('keyup', function (e) {
            e.preventDefault();

            lbVolumen_pie3()
        });



        $('.select2').on('change', function () {

            _cad = $(".select2 option:selected").text()

            _cad = _cad.split('-')

            $('#username').val(_cad[0])
            $('#code').val(_cad[1])

        })

        //Buscar cliente
        $('#_btn_filter').on('click', function (e) {
            e.preventDefault();

            if ($("#client").val() == "") {
                $.confirm({
                    title: 'Buscar cliente',
                    content: 'Debe llenar un cliente',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        aceptar: function () {

                        }
                    }
                });

                return false
            }

            $.ajax({
                url: _url = "<?=url(" / admin / user ")?>/" + $("#client").val(),
                type: "get",
                dataType: 'json',
                success: function (data) {
                    if (data.success == 1) {
                        $("#msg").html(data.message)
                        $(".alert2").css('display', 'block')

                        $('#username').val(data.user.name + ' ' + data.user.last_name)
                        $('#code').val(data.user.code)

                    } else {
                        $("#msg-danger").html(data.message)
                        $(".alert-danger2").css('display', 'block')

                    }
                    dataTable.draw()
                },
                error: function (data) {

                }
            });

        });


        //Guardar registro del paquete
        $('#_btn_save').on('click', function () {

            var form = $("#_form_register")

            if (form[0].checkValidity() === false) {
                event.preventDefault()
                event.stopPropagation()
                form.addClass('was-validated');
                return false;

            } else {

                form.metod = 'post'
                form.submit()
            }

        });


    });

    function loadTrackings() {
        _cad = ''

        $("input[name='tracking1[]']").each(function (indice, elemento) {

            if (elemento.value != '') {
                _cad += `<li class="nav-item ">
                <a href="#" class="nav-link">
                 ${elemento.value}
                 <i class="float-right fa fa-trash text-danger del-tracking" data-id="${indice+1}"></i>
                </a>
                <input type="hidden" name="tracking2[]"  value="${elemento.value}">
              </li>`
            }

        });

        $("#list_tracking").append(_cad)
    }


    $("#image_package").on('change', function () {
        $('#error_img').css("display", "none");
        $(this).removeClass('is-invalid');

        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(png|jpeg|jpg|svg|bmp)$");

        if (!(regex.test(val))) {
            $(this).val('');
            $('#error_img').text('Formato invalido')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        } else if ($(this)[0].files[0].size / 1024 > 6000) {
            $(this).val('');
            $('#error_img').text('Limite superrado (6MB))')
            $('#error_img').css("display", "block");
            $(this).addClass('is-invalid');
        } else {
            var input = $(this)[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result).fadeIn('slow');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }


    });


    function lbVolumen_pie3() {
        let lb_volume = 0;
        let pie_cubico = 0;
        let long = $("#long").val()
        let width = $("#width").val()
        let high = $("#high").val()

        pie_cubico = ((long * width * high) / 1728);

        lb_volume = ((long * width * high) / 166);

        if (pie_cubico < 1)
            pie_cubico = 1

        if (lb_volume < 1)
            lb_volume = 1

        $("#pie_cubico").val(pie_cubico.toFixed(2))
        $("#lb_volume").val(lb_volume.toFixed(2))

    }

    $('.integer').on('keyup', function (e) {
        e.preventDefault();
        ele = $(this).val()

        $(this).val(ele.replace(/^(0+)/g, ''))
        return
    });

    $('.decimales').on('keyup', function (e) {
        e.preventDefault();
        ele = $(this).val()

        $(this).val(ele.replace(/^(0+)/g, ''))
        return
    });

    $('.decimales').on('blur', function (e) {
        e.preventDefault();
        ele = $(this).val()

        $(this).val(ele.replace(/^(0.+)/g, ''))
        return
    });

</script>
@stop
