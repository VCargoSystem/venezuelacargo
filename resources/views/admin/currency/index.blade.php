@extends('adminlte::page')

@section('title', 'TASA DE DOLAR')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">TASA DEL DÓLAR</b></h4>
@stop

@section('content')


@if (session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close close2" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            <li> {!! session()->get('success')!!}</li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close1 close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>
<div class="alert alert-warning alert-danger2" style="display: none">
   <button type="button" class="close1 close"  >×</button>
    <span id="msg-danger"></span>
</div>


  <section class="content">

      <div class="container-fluid">
          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-responsive-lg table-responsive-md table-responsive-sm table-responsive-xs table-bordered datatable table-bordered">
                        <thead>                  
                          <tr>
                            <th width="20%" class="text-center" style="background:#24298d; color:#ffb901;">CÓDIGO</th>
                            <th width="20%" class="text-center" style="background:#24298d; color:#ffb901;">NOMBRE</th>
                            <th id="tasa_dolar_value" width="40%" class="text-center" style="background:#24298d; color:#ffb901;">TASA</th>
                            <th width="20%" class="text-center" style="background:#24298d; color:#ffb901;">ACCIÓN</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                          </tr>
                        </tbody>
                    </table>
                  </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
            
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
    $(document).ready(function() {
      $(".alert2").css('display', 'none')
      $(".alert-danger2").css('display', 'none')
        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            searching: false,  
            paging:false, 
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: {
              url: "{{ route('admin.currency.getFilterCurrency') }}"
            },
            columns: [

                {data: 'code', name: 'code', sClass:'text-center'},
                {data: 'name', name: 'name', sClass:'text-center'},
                {data: 'value_bs', name: 'value_bs', sClass:'text-center',
                  render: function (data, type, row) {
                        return '<span class="tracking">' + 
                        row.value_bs.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");  
                        + '</span>';
                    }},
                {data: 'actions', name: 'actions',orderable:false,serachable:false,sClass:'text-center'}
            ]
        });


  //preview image
  $('body').on('click', '.img-mini', function(e) {
    e.preventDefault();
    _url = $(this).attr('src')
    $("#image_details").attr('src', _url)
    $("#modal_preview").modal("show");
  });

  //Delete user
  $('body').on('click', '.delete', function(e) {
    e.preventDefault();
    _id = $(this).attr("data-id");

    $.confirm({
          title: 'Eliminar Paquete!',
          content: 'Seguro desea eliminar el registro?',
          type: 'red',
          typeAnimated: true,
          buttons: {
              delete: {
                  text: 'Eliminar',
                  btnClass: 'btn-red',
                  action: function(){
                    deletePackage(_id)
                  }
              },
              cancelar: function () {
              }
          }
    });

  });

  function deletePackage(_id){
    $(".alert2").css('display', 'none')
    $(".alert-danger2").css('display', 'none')

    $.ajax({
        url: _url = "<?=url("/admin/packages/")?>/"+_id,
        type: "DELETE",
        dataType: 'json',
        data: {
          "id": _id,
          "_token": "{{ csrf_token() }}"
        },

        success: function (data) {
            if(data.success==1){
              $("#msg").html(data.message)
              $(".alert2").css('display', 'block')
            }else{
              $("#msg-danger").html(data.message)
              $(".alert-danger2").css('display', 'block')
            
            }
            dataTable.draw() 
        },
        error: function (data) {

        }
    });

  }



});
    
</script>
@stop
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }

    .table td,
    .table th {
        padding: .40rem !important;
        vertical-align: middle !important;
        border-top: 1px solid #dee2e6;
        font-size: 15px;
    }

    div.dataTables_wrapper .dataTables_info {
        font-size: 15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination {
        font-size: 15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-green {
        border-top: solid 10px #38c53e !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-blue {
        border-top: solid 10px #f44336 !important;
    }

    hr {
        background: #e3e3e3;
    }
</style>
