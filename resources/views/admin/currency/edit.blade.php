@extends('adminlte::page')
@section('title', 'MODIFICAR TASA')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">MODIFICAR TASA</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                        <form  method="post" id="_form_update" action="{{ route('admin.currency.update', $currency->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                <input type="hidden" name="id" value="$currency->id">
                                <div class="row">
                                      <label class="col-12 mt-1" style="font-size:15px !important; letter-spacing: -0.5px !important;" >TASA DEL DÓLAR</label>
                                      <div class="col-12 input-group mb-3">
                                          <input type="text" name="value_bs" id="value_bs" class="form-control {{ $errors->has('value_bs') ? 'is-invalid' : '' }} decimal"
                                                 value="{{ $currency->value_bs }}" placeholder="Tasa">
                                          <div class="input-group-append">
                                            <span class="input-group-text" style="font-size:12px !important;">BS</span>
                                          </div>
                                          @if($errors->has('value_bs'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('value_bs') }}</strong>
                                              </div>
                                          @endif
                                  </div>
                                </div>
     

              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                        <a href="{{ route('admin.currency.index') }}" class="btn btn-default " id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary  " id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {
   $(".decimales").inputNumberFormat({ 'decimal': 2, 'decimalAuto': 2 });
            var number1 = $('#value_bs').val();
            var numero1 =number1.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            $('#value_bs').val(numero1);
  });
  
  $('#value_bs').on('keyup', function (e) {
            var number = $('#value_bs').val();
            var numero =number.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            $('#value_bs').val(numero);
            console.log($('#value_bs').val());
        });

</script>
@stop

<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>
