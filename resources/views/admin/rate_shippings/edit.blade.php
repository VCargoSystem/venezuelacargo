@extends('adminlte::page')
@section('title', 'MODIFICAR TASA POR REGIÓN')

@section('content_header')
    <h4><b style="color:#24298D; font-weight: 900;">MODIFICAR TASA POR DESTINO</b></h4>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                        <form  method="post" id="_form_update" action="{{ route('admin.rate_shippings.update', $rate_shippings->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                <input type="hidden" name="id" value="$rate_shippings->id">
                                  <div class="row">
                                  <label style="font-size:15px !important; letter-spacing: -0.5px !important;">REGIÓN</label>

                                  <div class="input-group mb-3">
                                          <input type="text" step="0.01" style="font-size:14px !important;" name="regions" id="regions" class="form-control {{ $errors->has('regions') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ $rate_shippings->regions }}"   readonly="">
                                         
                                          @if($errors->has('regions'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('regions') }}</strong>
                                              </div>
                                          @endif
                                  </div>
                                </div>

                                <div class="row">
                                  <label style="font-size:15px !important; letter-spacing: -0.5px !important;">TIPO DE ENVÍO</label>

                                      <div class="input-group mb-3">
                                          <input type="text" step="0.01" style="font-size:14px !important;" name="regions" id="regions" class="form-control {{ $errors->has('regions') ? 'is-invalid' : '' }} decimales"
                                                 value="@if($rate_shippings->trip_id == 1) MARITIMO @else AEREO @endif"   readonly="">
                                         
                                          @if($errors->has('regions'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('regions') }}</strong>
                                              </div>
                                          @endif
                                  </div>
                                </div>
                              
                                <div class="row">
                                  <label style="font-size:15px !important; letter-spacing: -0.5px !important;">TASA DE ENVÍO POR REGIÓN</label>

                                      <div class="input-group mb-3">
                                          <input type="text" step="0.01" name="cost" id="cost" class="form-control {{ $errors->has('cost') ? 'is-invalid' : '' }} decimales"
                                                 value="{{ $rate_shippings->cost }}" style="font-size:14px !important;" placeholder="Tasa" autofocus maxlength="40" min="1">
                                          <div class="input-group-append">
                                            <span class="input-group-text">$</span>
                                          </div>
                                          @if($errors->has('cost'))
                                              <div class="invalid-feedback">
                                                  <strong>{{ $errors->first('cost') }}</strong>
                                              </div>
                                          @endif
                                  </div>
                                </div>
     

              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('admin.rate_shippings.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $(".decimales").inputNumberFormat({ 'decimal': 2, 'decimalAuto': 2 });

  });

</script>
@stop

<style type="text/css">

  body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
