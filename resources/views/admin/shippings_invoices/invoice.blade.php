@extends('adminlte::page')
@section('title', 'ENVÍOS')

@section('content_header')
    <h5><b style="color:#24298D; font-weight: 600;">CREAR FACTURA</b></h5>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">

                          <div class="row col-12">
                          
                          </div>
                          <div class="row">
                            <div class=" col-3">
                                <label class="pr-2">USUARIO: </label>{{ $user->name.' '.$user->last_name}} 

                            </div> 
  
                            <div class=" col-3">
                                 <label class="pr-2">CÓDIGO DE USUARIO:</label>{{ $user->code}} 
                            </div> 
                            
                            <div class=" col-3">
                              <label class="pr-2">DESTINATARIO:</label>{{ $user->name.' '.$user->last_name }}
                            </div> 
                            

                            <div class=" col-3">
                              <label class="pr-2">TELÉFONO:</label>{{ $user->phone }}
                            </div> 
                      </div>
                              
                      <div class="row pt-1">
                          <div class=" col-6">
                               <label class="pr-2">DIRECCIÓN: </label>{{ $user->address[0]->address }} 
                          </div>

                          <div class=" col-6">
                              <label class="pr-2">PAÍS, ESTADO Y CIUDAD: </label>{{ $user->address[0]->country->name.', '.$user->address[0]->state->name.', '.$user->address[0]->city[0]->name }} 
                          </div>
                      </div>  

                      <div class="row pt-3">
                         <label class="text-warning pr-2 pl-2">ENVIO:</label>
                         {{ $type== 1 ? 'MARITIMO':'AEREO'}}
            
                      </div>


                    @if($count_p > 0)
                      <div class="row pt-3" id="div_package">

                        <div class="table-responsive">
                          <label>RECIBOS DE ENVÍO DIRECTO</label>
                          <table class="table table-bordered">
                              <thead class="h6">                  
                                <tr>
                                  <th width="15%">W-HOUSE</th>
                                  <th width="15%">PESO(LB)</th>
                                  <th width="15%">LB VOL</th>
                                  <th width="15%">PIE CÚBICO</th>
                                  <th width="15%">CANT. PIEZAS</th>
                                  <th width="15%">SEGURO</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($packages as $item)
                                  <tr>
                                    <td>
                                      {{ $item->ware_house }}
                                    </td>
                                    <td>{{ $item->weight }}</td>
                                    <td>{{  number_format($item->lb_volume, 2) }}</td>
                                    <td>{{  number_format($item->pie, 2) }}</td>
                                    <td>{{ $item->nro_pieces }}</td>
                                    <td>{{ $item->insured  }}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                         
                          <div class="row col-lg-12">
                              <div class="col-lg-8 col-sm-12 col-md-12 col-xs-12"></div>
                              <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                                <table  class="table table-sm" >
                                    <tr >
                                      <td width="70%" style="border-top: 0px !important; text-align: right;">
                                        <b>
                                          SUBTOTAL SEGURO
                                        </b>
                                      </td>
                                      <td class="float-right" style="border-top: 0px !important">
                                        <b>
                                          $ {{ $invoice->subtotal_secure}}
                                        </b>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td width="70%" style="border-top: 0px !important; text-align: right;">
                                        <b>SUBTOTAL ENVÍO</b>
                                      </td>
                                      <td class="float-right" style="border-top: 0px !important">
                                        <b>
                                          $ {{ $invoice->subtotal}}
                                        </b>
                                      </td>
                                    </tr>
                               
                              </table>

                              </div>
                          </div>

                        </div>
                  
                        <div class="row  col-12">
                          <div class="col-8"></div>
                          <div class="col-4">
                            <input type="hidden" name="total_pie3_paq" id="total_pie3_paq" value="{{ $invoice->total_pie3 }}">
                            <input type="hidden" name="_subtotal" id="_subtotal" value="{{ $invoice->subtotal + $invoice->subtotal_secure }}">

                            <input type="hidden" name="_subtotal_secure" id="_subtotal_secure" value="{{ $invoice->subtotal_secure }}">
                          </div>
                        </div>
                     
                      </div>
                    @endif

                    @if($count_rp > 0)
                      <div id="div_repackage">
                        <div class="table-responsive">
                            <input type="hidden" name="sw_repackage" id="sw_repackage" value="0">
                            <label>RECIBOS ENVÍO CON REEMPAQUE</label>
                            <table class="table table-bordered">
                                  <thead class="h6">                  
                                    <tr>
                                      <th width="15%">W-HOUSE</th>
                                      <th width="15%">PESO(LB)</th>
                                      <th width="15%">LB VOL</th>
                                      <th width="15%">PIE CÚBICO</th>
                                      <th width="15%">CANT. PIEZAS</th>
                                      <th width="15%">SEGURO</th>
                                    </tr>
                                  </thead>
                                  <tbody id="_body_rpackage">
                                    @foreach($repackages as $item)
                                      <tr>
                                        <td>
                                          {{ $item->ware_house }}
                                        </td>
                                        <td>{{ $item->weight }}</td>
                                        <td>{{  number_format($item->lb_volume, 2) }}</td>
                                        <td>{{  number_format($item->pie_cubico, 2) }}</td>
                                        <td>{{ $item->nro_pieces }}</td>
                                        <td>{{ $item->insured }}</td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                            </table>

                             <div class="row col-lg-12" >
                              <div class="col-lg-8 col-sm-12 col-md-12 col-xs-12"></div>
                              <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" id="_table_repackage" style="display: none">
                                <table  class="table table-sm" >
                                    <tr >
                                      <td width="70%" style="border-top: 0px !important; text-align: right;">
                                        <b>
                                          SUBTOTAL SEGURO
                                        </b>
                                      </td>
                                      <td class="float-right" style="border-top: 0px !important">
                                        <b id="_repackage_secure">
                                         
                                        </b>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td width="70%" style="border-top: 0px !important; text-align: right;">
                                        <b>SUBTOTAL ENVÍO</b>
                                      </td>
                                      <td class="float-right" style="border-top: 0px !important">
                                        <b id="_subtotal_repackage">
                                         
                                        </b>
                                      </td>
                                    </tr>

                                    <input type="hidden" name="subtotal_repackage" id="subtotal_repackage" value="">
                                    <input type="hidden" name="subtotal_repackage_secure" id="subtotal_repackage_secure" value="">

                              </table>

                              </div>
                          </div>


                        </div>    

                        <div class="row float-right pt-2">
                           <button id="btn_repackage" class="btn btn-primary btn-block 2" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">REEMPACAR</button>
                           <input type="hidden" name="_repackage_count" id="_repackage_count" value="1">
                        </div>
                      </div>
                    @endif


                    <div class="row pt-3 col-12">
                      <div class="col-3 mt-2">
                        <label>FECHA ESTIMADA DE LLEGADA</label>
                          <input type="date" class="form-control" name="date_delivery" required id="date_delivery"  value="{{$date_aprox}}" min="{{$date_aprox}}">

                      </div>
                      <div class="col-3 mt-2">
                        <label>NÚMERO DE CONTAINER</label>
                        <input type="text" class="form-control" name="nro_container" required id="nro_container">
                          <div class="invalid-feedback">
                            Ingrese un Número de container
                          </div>
                      </div>
                    </div>

                    <div class="row pt-3 col-12">
                        <label class="pr-2 pl-2">GASTOS EXTRAS</label>
                    </div>

                   
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <div class="row pt-3 col-12">
                       
                          <div class="col-5">
                              <label class="float-right">TIPO DE CAJA</label>
                          </div>
                          <div class="col-2">
                            <select class="form-control" name="cost_box" id="cost_box">
                              @foreach ($box as $b)
                                <option value="{{ $b->cost }}" data-id="{{ $b->id }}">{{ $b->name }}</option>
                              @endforeach

                            </select>
                          </div>

                          <div class="col-2">
                              <label class="float-right">CANTIDAD</label>
                          </div>
                          <div class="col-2">
                              <input type="text" name="box_count" id="box_count"  class="form-control decimal" maxlength="10" min="0" placeholder="0">
                             
                          </div>
                           <button class="btn btn-sm btn-primary" id="add_box">+</button>
                        </div>


                        <div id="div_cajas" class="row pt-3 col-12 offset-4">
                       
                       
                        </div>

                        <div class="row pt-3 col-12">
                          <div class="col-5">
                              <label class="float-right">VALOR DE REEMPAQUE</label>
                          </div>
                          <div class="col-2">
                              <input type="text" name="cost_repackage" id="cost_repackage" class="form-control integer" maxlength="5" min="0" placeholder="0" disabled="">
                          </div>

                       
                            <div class="col-3">
                              <label class="float-right">OTROS GASTOS</label>
                          </div>
                          <div class="col-2">
                              <input type="text" name="aditional_cost"  id="aditional_cost"  class="form-control integer" maxlength="10" min="0" placeholder="0">
                          </div>
                        </div>

                        <div class="row pt-3 col-12">
                          <div class="col-12">
                            <label class="float-right">TOTAL A FACTURAR</label>
                          </div>
                        </div>

                        <div class="row pt-3 col-12">
                          <div class="col-6">
                          </div>
                          
                            <b class="col-3 float-right"> MONTO ($)</b><input type="text" name="subtotal"  id="subtotal"  class="form-control col-3" maxlength="10" min="0" value="@if($count_p > 0){{$invoice->subtotal+$invoice->subtotal_secure  }}@endif" disabled="disabled">

                        </div>

                        <div class="row pt-3 col-12">
                          <div class="col-6">
                          </div>
  
                          <b class="col-3 float-right"> MONTO (Bs)</b>   
                            <input type="text" name="subtotal_bs" id="subtotal_bs"  class="form-control  col-3" maxlength="20" min="0" disabled="disabled">
    
                        </div>

                        
                        {{-- Save button --}}
                         <div class="row mb-3 mt-4 float-right ">
                          <div class="col-12">
                            <a href="{{ route('admin.shippings_invoices.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">VOLVER
                            </a>
                            <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                            </button>
                            <a href="" class="btn btn-primary" id="_btn_print" target="blank" style="display: none">IMPRIMIR
                            </a>
                          </div>
                        </div>

                      </div>  

                    </div>
                </form>


              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->

     
    
  </section>

 <section>
        <div class="modal fade" id="aditional_repackage" aria-modal="true" role="dialog" style="padding-right: 17px;"  aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">AGREGAR MEDIDAS DE REEMPAQUE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div id="content" style="display: none">
                    <div class="loading text-center"><img src="{{ asset('img/ajax-loader.gif') }}" alt="loading" /></div>
                  </div>

                  <form  method="POST" id="_form_repackage" class="needs-validation" novalidat >

                      <div class="form-group">
                          <label class="form-control-label" for="width">WARE-HOUSE</label>
                        <div class="">
                            <input type="text"  name="ware_house" id="ware_house" class="form-control {{ $errors->has('width') ? 'is-invalid' : '' }}"
                              value="" placeholder="ware house" autofocus maxlength="20" required>
                         
                            @if($errors->has('ware_house'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('ware_house') }}</strong>
                                </div>
                            @endif
                        </div>
                      </div>

                      <div class="row">
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-control-label" for="width">ANCHO</label>
                          <div class="input-group">
                              <input type="number" step="0.01" name="width" id="width" class="form-control {{ $errors->has('width') ? 'is-invalid' : '' }} decimales"
                                     value="" placeholder="Ancho" autofocus maxlength="10" min="1" required>
                              <div class="input-group-append">
                                <span class="input-group-text">IN</span>
                              </div>
                              @if($errors->has('width'))
                                  <div class="invalid-feedback">
                                      <strong>{{ $errors->first('width') }}</strong>
                                  </div>
                              @endif
                          </div>
                        </div>
                    
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                          <label class="form-control-label" for="high">ALTO</label>
                            <div class="input-group">
                                <input type="text" name="high" id="high" class="form-control {{ $errors->has('high') ? 'is-invalid' : '' }} decimales"
                                       value="{{ old('high')  }}" placeholder="Ancho" autofocus maxlength="20" step="0.01" required min="1">
                                <div class="input-group-append">
                                  <span class="input-group-text">IN</span>
                                </div>

                                @if($errors->has('high'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('high') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                          <label class="form-control-label" for="long">LARGO</label>
                          <div class="input-group">
                              <input type="text" name="long" id="long" class="form-control {{ $errors->has('long') ? 'is-invalid' : '' }} decimales"
                                     value="{{ old('long')  }}" placeholder="Largo" autofocus maxlength="10" step="1" required>
                               <div class="input-group-append">
                                <span class="input-group-text">IN</span>
                              </div>
                            
                              @if($errors->has('long'))
                                  <div class="invalid-feedback">
                                      <strong>{{ $errors->first('long') }}</strong>
                                  </div>
                              @endif

                          </div>
                        </div>


                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-control-label" for="long">PESO</label>
                             <div class="input-group mb-3">
                            <input type="text" step="0.01" name="weight" id="weight" class="form-control {{ $errors->has('weight') ? 'is-invalid' : '' }} decimales"
                                   value="{{ old('weight')  }}" placeholder="Peso" autofocus maxlength="10" min="1" required>
                            <div class="input-group-append">
                              <span class="input-group-text">LB</span>
                            </div>
                            @if($errors->has('weight'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('weight') }}</strong>
                                </div>
                            @endif
                            
                            </div>
                          </div>

                          <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-control-label" for="long">NÚMERO DE PIEZAS</label>
                             <div class="input-group mb-3">
                            <input type="text" step="1" name="nro_pieces" id="nro_pieces" class="form-control {{ $errors->has('nro_pieces') ? 'is-invalid' : '' }} integer"
                                   value="{{ old('nro_pieces')  }}" placeholder="Peso" autofocus maxlength="10" min="1" required>
                            <div class="input-group-append">
                              <span class="input-group-text"></span>
                            </div>
                            @if($errors->has('nro_pieces'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('nro_pieces') }}</strong>
                                </div>
                            @endif
                            
                            </div>
                          </div>
       
                      </div>


                      <div class="row">
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-control-label" for="width">PIE CÚBICO</label>
                          <div class="input-group">
                              <input type="text" step="0.01" name="pie_cubico" id="pie_cubico" class="form-control {{ $errors->has('pie_cubico') ? 'is-invalid' : '' }} decimales"
                                     value="{{ old('pie_cubico')  }}" placeholder="Ancho" autofocus maxlength="10" min="1" required>
                              <div class="input-group-append">
                                <span class="input-group-text">IN</span>
                              </div>
                              @if($errors->has('pie_cubico'))
                                  <div class="invalid-feedback">
                                      <strong>{{ $errors->first('pie_cubico') }}</strong>
                                  </div>
                              @endif
                          </div>
                        </div>
                    
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                          <label class="form-control-label" for="high">LB VOLUMEN</label>
                            <div class="input-group">
                                <input type="text" name="lb_volume" id="lb_volume" class="form-control {{ $errors->has('lb_volume') ? 'is-invalid' : '' }} decimales"
                                       value="{{ old('lb_volume')  }}" placeholder="Ancho" autofocus maxlength="20" step="0.01" required min="1">
                                <div class="input-group-append">
                                  <span class="input-group-text">IN</span>
                                </div>

                                @if($errors->has('lb_volume'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('lb_volume') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                      </div>

                      {{-- Save button --}}
                      <div class="row mt-2 float-right">
                        <div class="col-sm-12">
                          <button type="submit" class="btn btn-default" id="btn_cancel_aditional" data-dismiss="modal">CANCELAR
                          </button> 
                          <button type="button" class="btn btn-primary" id="save_aditional" value="create" style="color: #fff;
      background-color: #23298a;
      border-color: #23298a;
      box-shadow: none;"> GUARDAR
                          </button>
                        </div>
                      </div>
              </div>
              
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>
</section>
    

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<style type="text/css">
  
  body{
        font-family: 'Montserrat', sans-serif !important;
    }

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ asset('js/plugins/input-number/input-number-format.jquery.min.js') }}"></script>
</head>

<script type="text/javascript">

let _items_repackage =[]; 
let _total_repackage = 0
let _total_box = 0
let _total_additional = 0
let _amount = 0
_cost_repackage = 0
_total_pie_paq = 0;
_subtotal_lb = 0;
_subtotal_w = 0;
_dollar = "{{$dollar}}"



@if(!empty($invoice->cost_repackage))
  _cost_repackage="{{$invoice->cost_repackage}}"
  _total_pie_paq = "{{$invoice->total_pie3}}"
  _subtotal_lb = "{{$invoice->subtotal_lb}}";
  _subtotal_w = "{{$invoice->subtotal_w}}";
@endif

$(document).ready(function() {
  $(".decimales").inputNumberFormat({ 'decimal': 2, 'decimalAuto': 2 });
  $(".integer").inputNumberFormat({ 'decimal': 0, 'decimalAuto': 2 });

  $("#cost_repackage").val(_cost_repackage)

  //Reempacar
  $("#btn_repackage").on('click',function(){
    let sw = $("sw_repackage").val();


    if(sw ==1){
        $.confirm({
          title: 'Crear reempaque',
          content: 'Ya se asignaron medidas de reempaque',
          type: 'red',
          typeAnimated: true,

          buttons: {
            cancelar: function () {

            }
          }
      });
      return false;
    }

    $("#long").val(1);
    $("#high").val(1);
    $("#width").val(1);
    $("#weight").val(1);
    $("#pie_cubico").val(1);
    $("#lb_volume").val(1);
    $("#ware_house").val("<?=$ware_house?>");

    lbVolumen_pie3()
    $("#aditional_repackage").modal("show")

  });

  $('#save_aditional').on('click', function(){
      let form = $("#_form_repackage")
      let type= "<?=$type?>"

      if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
         form.addClass('was-validated');
        return false;
      }

      let items = [
        @foreach ($repackages as $item)
             "{{ $item->id }}", 
        @endforeach
      ];
     
      _url = "<?=url('/admin/shippings/repackage')?>"

      $.ajax({
        url: _url,
        type: "get",
        data: {
          "_token": "{{ csrf_token() }}",
          "items":items,
          "long": $("#long").val(),
          "high": $("#high").val(),
          "width": $("#width").val(),
          "weight":  $("#weight").val(),
          "type":type,
          "ware_house": $("#ware_house").val(),
          "pie_cubico": $("#pie_cubico").val(),
          "lb_volume": $("#lb_volume").val(),

          "city" : "<?=$user->address[0]->city[0]->name?>"
        },
        dataType: 'json',
      
        success: function (data) {
            if(data.success==0){
              $("#msg-danger").html(data.message)
              $(".alert-danger2").css('display', 'block')
            }else{
             
              data = data.data
              
              _items_repackage['subtotal'] = data.subtotal
              _items_repackage['type'] = "<?=$type?>"
              _items_repackage['ware_house'] = data.ware_house
              _items_repackage['high'] = data.high
              _items_repackage['width'] = data.width
              _items_repackage['long'] = data.long
              _items_repackage['weight'] = data.weight
              _items_repackage['volume'] = data.volume
              _items_repackage['insured'] = data.insured
              _items_repackage['total_pieces'] = data.total_pieces
              _items_repackage['subtotal_lb'] = data.subtotal_lb
              _items_repackage['subtotal_w'] = data.subtotal_w
              _items_repackage['total_pie3'] = data.total_pie3
              _items_repackage['packages'] = data.packages
              _items_repackage['cost_repackage'] = data.cost_repackage
              _items_repackage['pie_cubico'] = data.pie_cubico
              _items_repackage['lb_volume'] = data.lb_volume
              _items_repackage['subtotal_secure'] = data.lb_volume




              _cad = `<tr>
                      <td>
                        ${data.ware_house}
                      </td>
                      <td>${data.subtotal_w}</td>
                      <td>${data.subtotal_lb}</td>
                      <td>${data.total_pie3}</td>
                      <td>${data.total_pieces}</td>
                      <td>${(data.insured )}</td>
                      <input type="hidden" id="_repackages" value="${data.data}">
                    </tr>`

                $("#cost_repackage").val(data.cost_repackage)
                $("#total_pie3_paq2").val(data.total_pie3)



              $("#_body_rpackage").html(_cad)

              $("#_table_repackage").css('display', 'block')
              $("#_subtotal_repackage").html(data.subtotal+data.subtotal_secure)
              $("#_repackage_secure").html(data.subtotal_secure)
              $("#subtotal_repackage_secure").val(data.subtotal_secure)


              $("#subtotal_repackage").val(data.subtotal)

              _total = parseFloat($("#subtotal_repackage").val()) + parseFloat($("#_subtotal").val())
              $("#subtotal").val(_total)
              $("#subtotal_bs").val(_total)

              $("#_repackage_count").val('0')

              calculateAditional()

              $("#aditional_repackage").modal("hide")
              $("#btn_repackage").css("display", "none")
            }
          
        },
        error: function (data) {

        }
    });
      /*
      $.ajax({
        url: _url,
        type: "POST",
        dataType: 'json',
        data:{
          "_token": "{{ csrf_token() }}",
          "items":items,
          "long": $("#long").val(),
          "high": $("#high").val(),
          "width": $("#width").val(),
          "weight": (type == 2? $("#weight").val():0),
          "type":type,
          "ware_house": $("#ware_house").val(),
          "city" : "<?=$user->address[0]->city[0]->name?>"
        },
        success: function (data) {
            if(data.success==0){
              $("#msg-danger").html(data.message)
              $(".alert-danger2").css('display', 'block')
            }else{
             
              data = data.data
              
              _items_repackage['subtotal'] = data.subtotal
              _items_repackage['type'] = "<?=$type?>"
              _items_repackage['ware_house'] = data.ware_house
              _items_repackage['high'] = data.high
              _items_repackage['width'] = data.width
              _items_repackage['long'] = data.long
              _items_repackage['weight'] = data.weight
              _items_repackage['volume'] = data.volume
              _items_repackage['insured'] = data.insured
              _items_repackage['total_pieces'] = data.total_pieces
              _items_repackage['subtotal_lb'] = data.subtotal_lb
              _items_repackage['subtotal_w'] = data.subtotal_w
              _items_repackage['total_pie3'] = data.total_pie3
              _items_repackage['packages'] = data.packages
              _items_repackage['cost_repackage'] = data.cost_repackage


              _cad = `<tr>
                      <td>
                        ${data.ware_house}
                      </td>
                      <td>${data.subtotal_w}</td>
                      <td>${data.subtotal_lb}</td>
                      <td>${data.total_pie3}</td>
                      <td>${data.total_pieces}</td>
                      <td>${(data.insured?'SI':'NO')}</td>
                      <input type="hidden" id="_repackages" value="${data.data}">
                    </tr>`

                $("#cost_repackage").val(data.cost_repackage)
                $("#total_pie3_paq2").val(data.total_pie3)


              let dollarUSLocale = Intl.NumberFormat('en-US');
              $("#_body_rpackage").html(_cad)
              $("#_subtotal_repackage").html('<span class="pl-2 float-right"><b>Subtotal: </b>'+dollarUSLocale.format(data.subtotal)+'</span>')

              $("#subtotal_repackage").val(dollarUSLocale.format(data.subtotal))

              _total = parseFloat($("#subtotal_repackage").val()) + parseFloat($("#_subtotal").val())
              $("#subtotal").val(dollarUSLocale.format(_total))
              $("#subtotal_bs").val(dollarUSLocale.format(_total))

              $("#_repackage_count").val('0')

              calculateAditional()

              cost_repackage()
              $("#aditional_repackage").modal("hide")
              $("#btn_repackage").css("display", "none")
            }
          
        },
         error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
        alert(thrownError);
      }
      });
      */
  });

  $("#_btn_save").on('click',function(e){
    event.stopPropagation();

    //Fecha de llegada y nro_tracking
    if($("#date_delivery").val() == ''){
        $.confirm({
              title: 'Facturar',
              content: 'Campo fecha de llegada vacia',
              type: 'red',
              typeAnimated: true,

              buttons: {
                cancelar: function () {

                }
              }
          });
          return false;
    }

    if($("#nro_container").val() == ''){
        $.confirm({
              title: 'Facturar',
              content: 'Campo número de contenedor vacio',
              type: 'red',
              typeAnimated: true,

              buttons: {
                cancelar: function () {

                }
              }
          });
          return false;
    }

      if($("#_repackage_count").val() == 1){
       
         $.confirm({
              title: 'Reempaque',
              content: 'Existen paquetes para reempaque',
              type: 'red',
              typeAnimated: true,

              buttons: {
                cancelar: function () {

                }
              }
          });
          return false;
      
      }

     calculateAditional()


    if( $("#box_count").val() == 0 && $("#cost_box").val() == 0 && $("#cost_repackage").val() == 0 && $("#aditional_cost").val() == 0){

      $.confirm({
          title: 'Costos adicionales!',
          content: '',
          content: 'No desea ingresar un costo adicional?',
          type: 'blue',
          typeAnimated: true,

          buttons: {
            Cancelar: function () {
                 return;
            },
            Aceptar: {
                btnClass: 'btn-blue',
                action: function(){
                  storeInvoice()
                }
            }
          }
      });
   
    }else{
      storeInvoice()
    }


  });



   $('#width').on('keyup', function(e) {
      e.preventDefault();
      lbVolumen_pie3()
  });

  $('#high').on('keyup', function(e) {
      e.preventDefault();
      lbVolumen_pie3()
  });

  $('#long').on('keyup', function(e) {
      e.preventDefault();
      lbVolumen_pie3()
  });

});

function storeInvoice(){

  let items_packages;
  let items_repackages;
  let invoice;

  @if($count_p > 0) 
    items_packages =  @json($invoice->items_invoice) 
    invoice = @json($invoice);
  @endif;

  @if($count_rp > 0) 
    items_repackages = JSON.stringify(Object.assign({}, _items_repackage))
  @endif;

  _type = @if($type == 'AEREO') 2 @else 1 @endif

  _subtotal_secure = parseFloat($("#subtotal_repackage_secure").val()) + parseFloat($("#_subtotal_secure").val())


    $.ajax({
        url: "<?=url("/admin/invoice/storeInvoice")?>",
        type: "POST",
        dataType: 'json',
        data:{
          "_token": "{{ csrf_token() }}",
          "items_packages":items_packages,
          "items_repackages": items_repackages,
          "invoice": invoice,
          "box_count": $("#box_count").val()==''?0:$("#box_count").val(),
          "cost_box":  $("#cost_box").val()==''?0:$("#cost_box").val(),
          "total_box" : _total_box,
          "total_additional" :  $("#aditional_cost").val(),
          "amount" : $("#subtotal").val(),
          "subtotal_bs" : $("#subtotal_bs").val(),
          "client_id" : "<?=$user->id?>",
          "shippings" : "<?=$type?>",
          "nro_container" : $("#nro_container").val(),
          "date_delivery" : $("#date_delivery").val(),
          "total_repackage": $("#cost_repackage").val(),
          "cost_insured": $("#cost_repackage").val(),
          "subtotal_secure" : _subtotal_secure

        },
        success: function (data) {
            if(data.success==0){
              $("#msg-danger").html(data.message)
              $(".alert-danger2").css('display', 'block')
            }else{
              $("#msg").html(data.message)
              $(".alert2").css('display', 'block')
              $("#_btn_save").css('display', 'none')
              $("#btn_cancel").css('display', 'none')


              _url = "<?=url("/admin/printInvoice")?>"+"/"+data.id;
              $("#_btn_print").attr('href', _url)

               $.confirm({
                  title: 'Crear Factura',
                  content: 'Factura creada correctamente',
                  type: 'green',
                  typeAnimated: true,

                  buttons: {
                    acerptar: function () {
                  
                      document.getElementById("_btn_print").click(); 
                   
                    }
                  }
              });

                
              setTimeout(function(){ 
                  window.location.href="<?=url("/admin/shippings")?>";
              }, 5000);
            }
          
        },
        error: function (data) {

        }
      });

}

function calculateAditional(){
  _box = parseInt($("#box_count").val()); 
  _box_cost = parseFloat($("#cost_box").val()); 
  _other = parseFloat(0);
  _cost_repackage = parseFloat(0);
  _total = parseFloat(0);

  _total_box =  parseFloat(0);
  _costo_caja = parseFloat(0);
  _repackage = 0
  _package = 0

  if((_box > 0 && _box_cost == 0) || (_box == 0 && _box_cost > 0)){
     $.confirm({
          title: 'Costo y Cantidad por Caja!',
          content: 'El costo o la cantidad debe ser mayor a cero',
          type: 'red',
          typeAnimated: true,

          buttons: {
            cancelar: function () {

            }
          }
      });
      return false;
  }

  $("#div_cajas div").each(function(){

    if( $(this).attr('data-total') > 0){
      _total_box = (_total_box + parseFloat($(this).attr('data-total')))
    }

  });


  if(_total_box > 0){
     _total_box =  _total_box -_costo_caja
  }
 

  if($("#cost_repackage").val() > 0){
      _cost_repackage = parseFloat($("#cost_repackage").val())
   }


   if($("#aditional_cost").val() > 0){
      _other = parseFloat( $("#aditional_cost").val())
   }


  if(parseFloat($("#subtotal_repackage").val()) > 0)
    _repackage = parseFloat($("#subtotal_repackage").val()) 

  if(parseFloat($("#_subtotal").val()) > 0)
    _package =  parseFloat($("#_subtotal").val())

  _total = (_repackage +_package)


  _t = (_total+_total_box+_cost_repackage + _other)


  let dollarUSLocale = Intl.NumberFormat('es-ES');
  $("#subtotal").val(_t)

  let dollarUS = Intl.NumberFormat('es-ES');
  $("#subtotal_bs").val(_t * _dollar )
  
}
 
function dateAir(){
  let f =  new Date();
  let dif = 0
  let month = ""

  if(f.getDay() < 5){
    dif = 5 - f.getDay()
    f.setDate(f.getDate() + dif);

  }else{
     if(f.getDay() == 5){
        f.setDate(f.getDate() + 7);
     }else{
        f.setDate(f.getDate() + 6);
     }
  }

  if((f.getMonth() +1) < 10){
    month = '0'+(f.getMonth() +1)
  }else{
    month = (f.getMonth() +1)
  }

  $("#date_delivery").val(f.getDate() + "-" + month + "-" + f.getFullYear())
  
}

function dateMar(){
  let f =  new Date();
  let dif = 0
  let month = ""

  if(f.getDay() < 5){
    dif = 5 - f.getDay()
    f.setDate(f.getDate() + (dif+7));

  }else{
     if(f.getDay() == 5){
        f.setDate(f.getDate() + 14);
     }else{
        f.setDate(f.getDate() + 13);
     }
  }

  if((f.getMonth() +1) < 10){
    month = '0'+(f.getMonth() +1)
  }else{
    month = (f.getMonth() +1)
  }

  $("#date_delivery").val(f.getDate() + "-" + month + "-" + f.getFullYear())
  
}

$("#cost_box").change(function() {
  calculateAditional()
});

$("#box_count").blur(function() {
  calculateAditional()
});

$("#aditional_cost").blur(function() {
  calculateAditional()
});


$('#add_box').click (function(e) {
    e.preventDefault();     //prevenir novos clicks

    if($('#box_count').val() == 0){
       $.confirm({
          title: 'Agregar Caja',
          content: 'Debe ingresar la cantidad',
          type: 'red',
          typeAnimated: true,

          buttons: {
            cancelar: function () {

            }
          }
      });
      return false;

    }

    _cost_box = parseFloat($('#cost_box').val())
    _cant = parseFloat($('#box_count').val())
    _total = parseFloat(_cost_box * _cant)
    _tipo =  $("#cost_box option:selected").text();
    _id_tipo =  $("#cost_box option:selected").attr('data-id');
    existe=0

    $("#div_cajas div").each(function(){
      if($(this).attr('data-id-tipo') == _id_tipo){
        existe=1
        return false;
      }
    });

    if(existe == 0){
      $('#div_cajas').append(`<div class="row pt-3 col-8" style="border-bottom: 1px solid;" data-id-tipo="${_id_tipo}" data-total="${_total}">
                            <div class="col-2">
                            </div>
                            <div class="col-3">
                              <label class="float-right">${_tipo}</label>
                            </div>
                            <div class="col-3">
                              <label class="float-right">${_cant} * ${_cost_box} </label>
                            </div>

                            <div class="col-3 pr-4">
                                <label class="float-right">${_total}</label>
                            </div>
                        
                          <a class="btn btn-xs btn-danger  mt-2 mb-2 delete_caja">
                            <i class="fa fa-trash text-white "></i>
                          </a>
                  </div>`)      
    }

    calculateAditional()
  });

  // Remover o div anterior
  $('body').on("click",".delete_caja", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    _resta = parseFloat( $(this).parent('div').attr('data-total'))

    _t = parseFloat($("#subtotal").val() - _resta)


    let dollarUSLocale = Intl.NumberFormat('es-ES');
    $("#subtotal").val(_t)

    let dollarUS = Intl.NumberFormat('es-ES');
    $("#subtotal_bs").val(_t * _dollar) 

  });

  

function lbVolumen_pie3(){
    let lb_volume=0;
    let pie_cubico=0;
    let long  = parseFloat($("#long").val())
    let width = parseFloat($("#width").val())
    let high  = parseFloat($("#high").val())
    let nro_pieces  = 1

    pie_cubico = ((long * width * high)/ 1728 ) * nro_pieces; 

    lb_volume  = ((long * width * high)/ 166 ) * nro_pieces;  

    if(pie_cubico<1)
      pie_cubico = 1

    if(lb_volume<1)
      lb_volume = 1
    
    $("#pie_cubico").val(pie_cubico.toFixed(2))
    $("#lb_volume").val(lb_volume.toFixed(2))

 }


</script>
@stop
-