@extends('adminlte::page')

@section('title', 'ENVÍOS')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 900;"> ENVÍOS FACTURADOS </b></h4>
@stop

@section('content')

@if (session('errors'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (session()->has('success'))
<div class="alert alert-success">
    <button type="button" class="close close2" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>
        <li> {!! session()->get('success')!!}</li>
    </ul>
</div>
@endif
<div class="alert alert-success alert2" style="display: none">
    <button type="button" class="close1 close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>
<div class="alert alert-warning alert-danger2" style="display: none">
    <button type="button" class="close1 close">×</button>
    <span id="msg-danger"></span>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 pr-3">
                <div class="row float-right ">
                    @php $date = \Carbon\Carbon::now()->locale('es_VE') @endphp
                    <p><b>FECHA: </b> {{   $date->isoFormat('dddd D [de] MMMM [de] YYYY')}}
                        <b class="pl-5">HORA: </b> {{   $date->isoFormat('hh:mm A')}}
                    </p>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-header">
                    
                        <form class="form-group">
                        <div class="row">
                      @foreach($status as $item)
                        <div class="col-lg-3 col-md-3 col-xs-3">
                          <label class="text-danger tex" style="font-size:14px !important;" >({{  $item->code }}) </label>
                            <label style="color: #23298a; font-size:14px !important;">{{ $item->name }} </label>
                          </div>
                      @endforeach
                      </div>
                    <hr>

                            <div class="row">

                                <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">CLIENTE</label>
                                    <select class="form-control select2 select2-hidden-accessible"
                                        style="width: 100%; min-height: 70px !important;" tabindex="-1"
                                        aria-hidden="true" name="usuario" id="usuario">
                                    </select>
                                </div>

                                <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">WAREHOUSE</label>
                                    <input type="text" name="ware_house" id="ware_house"
                                        class="form-control form-control-sm  {{ $errors->has('Ware-House') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Nro. Warehouse" autofocus maxlength="40">
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">NRO. CONTAINER</label>
                                    <input type="text" name="container" id="container"
                                        class="form-control form-control-sm  {{ $errors->has('container') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Nro. Container" autofocus maxlength="40">
                                </div>


                                <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">TIPO DE
                                        ENVÍO</label>

                                    <select name="shippins_id" id="shippins_id"
                                        class="col-12 form-control form-control-sm  {{ $errors->has('shippins_id') ? 'is-invalid' : '' }} ">
                                        <option value="">Seleccione</option>
                                        <option value="1">MARITIMO</option>
                                        <option value="2">AEREO</option>
                                    </select>
                                </div>

                               



                            </div>
                            <div style="height:7px;"></div>
                            <div class="row">

                            <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">WH GENERAL</label>
                                    <input type="text" name="wh_general" id="wh_general"
                                        class="form-control form-control-sm  {{ $errors->has('wh_general') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="BUSCAR" autofocus maxlength="40">
                                </div>
                               
                                <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">STATUS</label>
                                    <select name="status" id="status"
                                        class="form-control form-control-sm  {{ $errors->has('status') ? 'is-invalid' : '' }} mr-1">
                                        <option value="">Seleccione</option>
                                        @foreach($status as $item)
                                        <option value="{{$item->id}}">{{$item->name}} </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-lg-3 col-md-3 col-xs-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">DESDE</label>
                                    <input type="date" name="date" id="date"
                                        class="form-control form-control-sm  {{ $errors->has('date') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Fecha de registro - Desde" autofocus maxlength="20">
                                </div>

                                <div class="col-lg-3 col-md-3 col-xs-3">
                                    <label class="float-left"
                                        style="font-size:15px !important; letter-spacing: -0.5px !important;">HASTA</label>
                                    <input type="date" name="date1" id="date1"
                                        class="form-control form-control-sm  {{ $errors->has('date1') ? 'is-invalid' : '' }} mr-1"
                                        value="" placeholder="Fecha de registro - Hasta" autofocus maxlength="20">
                                </div>
                            </div>


                            <div style="height:7px;"></div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                                <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">

                                            <button id="_btn_filter"
                                                class=" ml-1 btn btn-primary btn-sm col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                                style=" color: #fff;
          background-color: #23298a;
          border-color: #23298a;
          box-shadow: none; margin-top: 7px !important; margin-left:0px !important;"><i class="fas fa-search"></i>&nbsp;BUSCAR</button>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">

                                            <button id="_btn_refresh"
                                                class=" ml-1 btn btn-primary btn-sm  col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                                style=" color: #fff;
          background-color: #23298a;
          border-color: #23298a;
          box-shadow: none; margin-top: 7px !important; min-height: 31px; margin-left:0px !important;">

                                    <i class="fas fa-eraser"></i>&nbsp;LIMPIAR CAMPOS 
                                            </button>

                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                            </div>


                        </form>





                    </div>

                    

                    <div class="card-body">
                        <div class="row  float-right mr-2"> 
                            <button id="edit_lot" type="button" class=" ml-1 btn btn-primary btn-sm" style="color: #24298D;
    background-color: #FFB901;
    border-color: #FFB901;
    box-shadow: none;"> <strong>EDITAR LOTE</strong></button>
                        </div>
                        <div class="table-responsive">
                            <table
                                class="table table-sm table-striped  datatable table-responsive-md table-responsive-sm table-responsive-xs table-bordered"  style="letter-spacing: -0.5px !important;">
                                <thead class="h6">
                                    <tr>
                                        <th width="1%"></th>
                                        <th width="16%" style="background:#24298d; color:#ffb901;">
                                            CLIENTE</th>
                                        <th width="8%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            WAREHOUSE</th>

                                        <th width="5%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            FACTURA</th>
                                        <th width="8%" class="text-center" style="background:#24298d; color:#ffb901;">
                                        REEMP. </th>

                                        
                                        <th width="8%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            CAJAS</th>
                                        <th width="9%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            F. LLEGADA</th>
                                        <th width="6%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            PESO LB</th>
                                        <th width="6%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            FT<sup>3</sup></th>
                                        <th width="5%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            VOLUMEN</th>
                                        <th width="5%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            CON</th>
                                        <th width="6%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            STATUS</th>
                                        <th width="6%" class="text-center" style="background:#24298d; color:#ffb901;">
                                            ENVÍO</th>
                                        <th width="17%" class="text-center"
                                            style="background:#24298d; color:#ffb901; min-width: 100px !important">
                                            </strong>ACCIONES<strong></th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:14px !important;">
                                    <tr>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">

                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>

<!--modal asignar code-->
<section class="content">
    <div class="modal fade show" id="modal_preview" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="">
                    <button type="button" class="close pr-3 pt-2" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="min-height: 300px !important; max-height: 350px !important">
                    <img src="" border="0" width="300" class="img-fluid" align="center"
                        style="max-width: 300px !important; max-height: 350px !important" id="image_details" />
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade show" id="modal_shippings" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">AGREGAR ENVÍO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="needs-validation" id="_frm_shippings" method="post"
                        action="{{ route('admin.shippings.addShipping') }}">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <input type="hidden" name="id" id="id">

                        <div class="col-12">
                            <label>FECHA ESTIMADA DE LLEGADA</label>
                            <input type="text" class="form-control" name="date_delivery" required id="date_delivery"
                                readonly="" value="">
                        </div>

                        <div class="col-12 mt-2">
                            <label>NÚMERO DE CONTAINER</label>
                            <input type="text" class="form-control" name="nro_container" required id="nro_container">
                            <div class="invalid-feedback">
                                Ingrese un Número de container
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save_shippings" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade show" id="modal_update" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">EDITAR ENVÍO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="needs-validation" id="_frm_update" method="post" action="">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="id_edit" id="id_edit">

                        <div class="col-12">
                            <label>FECHA ESTIMADA DE LLEGADA</label>
                            <input type="text" class="form-control" name="date_delivery_edit" required
                                id="date_delivery_edit" readonly="" value="">
                        </div>

                        <div class="col-12 mt-2">
                            <label>NÚMERO DE CONTAINER</label>
                            <input type="text" class="form-control" name="nro_container_edit" required
                                id="nro_container_edit" value="">
                            <div class="invalid-feedback">
                                Ingrese un Número de container
                            </div>
                        </div>

                        <div class="col-12 mt-2">
                            <label>ESTATUS</label>
                            <select class="form-control" name="status_id" id="status_id">
                                <option value="">Seleccione</option>
                                @foreach($status as $statu)
                                @if($statu->id > 1)
                                <option value="{{ $statu->id }}">{{ $statu->name }}</option>
                                @endif
                                @endforeach
                            </select>

                            <div class="invalid-feedback">
                                Ingrese un estatus
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary update_shippings" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade show" id="modal_lot" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">EDITAR LOTE DE ENVIOS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">

                    <div class="col-12">
                        <label>ESTATUS</label>
                        <select name="status_id_lot" id="status_id_lot" class="form-control">
                            @foreach ($status as $sta)>
                            @if($sta->id > 1)
                            <option value="{{ $sta->id }}">{{ $sta->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save_lote" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- /.modal-status-change -->
    <div class="modal fade show" id="modal_change" aria-modal="true" role="dialog" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">CAMBIAR ESTATUS DE ENVÍO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id_paq" id="id_paq">
                    <input type="hidden" name="invoice_id" id="invoice_id">
                    <input type="hidden" name="packages" id="packages">


                    <div class="col-12">
                        <label>ESTATUS</label>
                        <select name="_status_id" id="_status_id" class="form-control">
                            @foreach ($status as $sta)>
                            @if($sta->id > 1)
                            <option value="{{ $sta->id }}">{{ $sta->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save_change" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!--  tracking -->
    <div class="modal fade show modal-md" id="modal_tracking" aria-modal="true" role="dialog"
        style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">SEGUIMIENTO DEL PAQUETE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-12">
                        <iframe class="embed-responsive" src="{{ route('shippings_invoices.iframeMap') }}" height="320"
                            width="auto" id="map_frame"></iframe>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</section>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap'
    rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>


<style type="text/css">
    .tracking {
        word-break: break-all;
    }

    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 4px;
        min-height: 30px;
    }


    .form-control {
        display: block;
        width: 100%;
        height: calc(1.95rem + 1px) !important;
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        box-shadow: inset 0 0 0 transparent;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    body {
        font-family: 'Montserrat', sans-serif !important;
    }

    .table td,
    .table th {
        padding: .40rem !important;
        vertical-align: middle !important;
        border-top: 1px solid #dee2e6;
        font-size: 15px;
    }

    div.dataTables_wrapper .dataTables_info {
        font-size: 15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination {
        font-size: 15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-green {
        border-top: solid 10px #38c53e !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-blue {
        border-top: solid 10px #f44336 !important;
    }

    hr {
        background: #e3e3e3;
    }

</style>
@stop
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })



    $(document).ready(function () {


        $(".alert2").css('display', 'none')
        $(".alert-danger2").css('display', 'none')

        $('.select2').select2();
        $('.select2-selection').css('min-height', '67px')

        $('.select2').select2({
            placeholder: 'Seleccionar Usuario',
            ajax: {
                url: "{{ route('users.usersAutocomplete') }}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name + ' ' + item.last_name + ' - ' + item.code,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });


        let arr = [];
        let users = [];
        let type = [];
        let _invoices = [];

        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 20,
            searching: false,
            paging: true,
            bLengthChange: false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            // scrollX: true,
            "order": [
                [0, "desc"]
            ],
            ajax: {
                url: "{{route('shippings_invoices.getFilterShippings') }}",
                data: function (d) {
                   
                        d.ware_house = $('#ware_house').val(),
                        d.nro_container = $('#container').val(),
                        d.status = $('#status').val(),
                        d.date = $('#date').val(),
                        d.date1 = $('#date1').val(),
                        d.usuario = $('#usuario').val(),
                        

                        d.shippins_id = $('#shippins_id').val()
                }
            },
            columns: [{
                   // name: 'id_items'
                    data: 'id_items',
                    'searchable': false,
                    orderable: false,
                    render: function (data, type, row) {
                        return row.id_items;
                    }
                },
                {
                    name: 'cliente',
                    data: 'cliente',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.name +' '+ row.last_name +'</span>';
                    }
                },
                {
                    name: 'ware_house',
                    data: 'ware_house',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.ware_house + '</span>';
                    }
                },
                {
                    name: 'invoice_id',
                    data: 'invoice_id',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.invoices_id + '</span>';
                    }
                },
                {
                    name: 'invoice_repackage',
                    data: 'invoice_repackage',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + (row.repackage == 1 ? 'SI' :
                            'NO') + '</span>';
                    }
                },
               /* {
                    data: 'insured',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.insured + '</span>';
                    }
                },*/
               
                {
                    name: 'nro_pieces',
                    data: 'nro_pieces',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.nro_pieces + '</span>';
                    }
                },
                {
                    name: 'created_date',
                    data: 'created_date',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.created_date + '</span>';
                    }
                },
                {
                    name: 'weight',
                    data: 'weight',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.weight + '</span>';
                    }
                },
                {
                    name: 'pie_cubico',
                    data: 'pie_cubico',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {

                        pie_cubico = 0;
                        if (row.pie_cubico == 0) {
                            pie_cubico = ((row.long * row.width * row.high) / 1728)

                        } else {
                            pie_cubico = row.pie_cubico
                        }

                        if (pie_cubico < 1)
                            pie_cubico = 1

                        return '<span class="tracking">' + pie_cubico + '</span>';
                    }
                },

                {
                    name: 'lb_volumen',
                    data: 'lb_volumen',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {

                        lb_volumen = 0;
                        if (row.invoice_id == 0) {
                            lb_volumen = ((row.long * row.width * row.high) / 166)

                        } else {
                            lb_volumen = row.lb_volumen
                        }

                        return '<span class="tracking">' + lb_volumen + '</span>';
                    }
                },

                {
                    name: 'nro_container',
                    data: 'nro_container',
                    sClass: 'text-center',
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.nro_container + '</span>';
                    }
                },
                {
                    name: 'status',
                    data: 'status',
                    sClass: 'text-center',
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.status_siglas + '</span>';
                    }
                },
                {
                    name: 'shippins_id',
                    data: 'shippins_id',
                    sClass: 'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return row.shippins_id == 2 ? 'AÉREO' : 'MARÍTIMO';
                    }
                },

                {
                    data: 'actions',
                    name: 'actions',
                    orderable: false,
                    serachable: false,
                    sClass: 'text-center'
                }
            ]
        });


        $('.close1').on('click', function (e) {
            $(this).parent().fadeToggle("slow");
        });

        //Cambiar estatus del paquete
        $('body').on('click', '.change', function (e) {
            //e.preventDefault();}
            _id = $(this).attr('data-id')
            _status_id = $(this).attr('status_id')
            _invoice_id = $(this).attr('invoice_id')
            _packages = $(this).attr('packages')

            $('#_status_id option[value="' + _status_id + '"]').attr("selected", true);

            $("#id_paq").val(_id)
            $("#invoice_id").val(_invoice_id)
            $("#packages").val(_packages)

            $("#modal_change").modal("show");
        });

        $('.save_change').on('click', function (e) {
            e.preventDefault();
            _url = "<?=url("/admin/shippings/changePaq")?>"

            $.ajax({
                url: _url,
                type: "POST",
                data: {
                    "status_id": $("#_status_id").val(),
                    "id_paq": $("#id_paq").val(),
                    "invoice_id": $("#invoice_id").val(),
                    "packages": $("#packages").val(),

                    "_token": "{{ csrf_token() }}"
                },
                dataType: 'json',

                success: function (data) {
                    if (data.success == 0) {
                        $("#msg-danger").html(data.message)
                        $(".alert-danger2").css('display', 'block')
                    } else {
                        $("#msg").html(data.message)
                        $(".alert2").css('display', 'block')
                        $("#modal_change").modal("hide");
                    }
                    dataTable.draw()
                },
                error: function (data) {

                }
            });

        });



        $('#_btn_filter').on('click', function (e) {
            e.preventDefault();
            arr = [];
            let _cont = 0
            let sw = 0
            arr[0] = $("#ware_house").val()
           
            arr[2] = $("#status").val()
            arr[7] = $("#container").val()

            if ($("#usuario").val() == null)
                arr[3] = '';
            else
                arr[3] = $("#usuario").val();

            arr[4] = $("#date").val()
            arr[5] = $("#date1").val()
            arr[6] = $("#shippins_id").val()

            for (let i in arr) {
                if (i < 8 && (arr[i] != "")) {
                    _cont++
                }

            }

            if (_cont > 1 && (arr[4] == "" && arr[5] == "")) {
                sw = 1
            } else if (_cont > 2 && (arr[4] != "" && arr[5] != "")) {
                sw = 1
            } else if (_cont > 1 && (arr[4] != "" && arr[5] == "")) {
                sw = 1
            } else if (_cont > 1 && (arr[4] == "" && arr[5] != "")) {
                sw = 1
            }

            if (sw == 1) {
                $.confirm({
                    title: '',
                    content: 'No puede seleccionar más de dos criterios',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });

                return false;
            }

            if (_cont == 0) {
                $.confirm({
                    title: '',
                    content: 'Debe seleccionar algún criterio',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            if (_cont == 1 && (arr[4] != "" && arr[5] == "")) {
                $.confirm({
                    title: '',
                    content: 'Indique un rango de fechas válido',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            if (_cont == 1 && (arr[4] == "" && arr[5] != "")) {
                $.confirm({
                    title: '',
                    content: 'Indique un rango de fechas válido',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            if (arr[4] != "" && arr[5] != "") {
                let date1 = new Date($("#date").val());
                let date2 = new Date($("#date1").val());

                if (date1 > date2) {
                    $.confirm({
                        title: '',
                        content: 'Indique un rango de fecha válido',
                        type: 'blue',
                        typeAnimated: true,

                        buttons: {
                            cancelar: function () {

                            }
                        }
                    });
                    return false;
                }
            }

            dataTable.draw()
        });


        function removeItemFromArr(arr, item) {
            var i = arr.indexOf(item);

            if (i !== -1) {
                arr.splice(i, 1);
            }
        }

        //Edit lote
        $('#edit_lot').on('click', function (e) {
            e.preventDefault();
            let sw = 0;
            arr = []
            _invoices = []

            $("input[name='items[]']").each(function (index) {
                if ($(this).is(':checked')) {
                    sw = 1;
                    _fat = $(this).attr('invoice_id')
                    arr.push($(this).val())
                    _invoices.push(_fat)
                }
            });

            if (sw == 0) {
                $.confirm({
                    title: '',
                    content: 'Debe seleccionar un envío',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            //Envio el ajax
            $("#modal_lot").modal("show");

        });

        $('.save_lote').on('click', function (e) {
            e.preventDefault();
            _url = "<?=url("/admin/shippings/changeLot")?>"

            $.ajax({
                url: _url,
                type: "POST",
                data: {
                    "status_id": $("#status_id_lot").val(),
                    "items": arr,
                    "invoices": _invoices,
                    "_token": "{{ csrf_token() }}"
                },
                dataType: 'json',

                success: function (data) {

                    if (data.success == 0) {
                        $("#msg-danger").html(data.message)
                        $(".alert-danger2").css('display', 'block')
                    } else {
                        $("#msg").html(data.message)
                        $(".alert2").css('display', 'block')
                        $("#modal_lot").modal("hide");
                    }
                    arr = [];
                    _invoices = []
                    dataTable.draw()
                },
                error: function (data) {

                }
            });

        });



        $('#_btn_refresh').on('click', function (e) {
            e.preventDefault();

            $("#date").val('')
            $("#date1").val('')
            $("#ware_house").val('')
            $("#tracking").val('')
            $("#usuario").val('').trigger('change')
            $("#status").val('').trigger('change')
            $("#shippins_id").val('').trigger('change')
            $("#container").val('').trigger('change')

            //$("#_btn_filter").trigger('click')
            dataTable.draw()

        });

        //Mostrar el mapa
        $('body').on('click', '.shippings', function (e) {
            e.preventDefault();
            _status_id = $(this).attr('status_id')
            latitud = 0
            longitud = 0

            _arr_status = @json($status);

            //Busco la lat y log del estatus
            for (l in _arr_status) {
                if (_arr_status[l].id == _status_id) {
                    longitud = _arr_status[l].longitude
                    latitud = _arr_status[l].latitude
                }
            }

            _base_url = "<?=url('admin/shippings_invoices/iframeMapFind')?>"
            _url = _base_url + '/' + longitud + '/' + latitud

            $('#map_frame').attr('src', _url);
            $("#modal_tracking").modal("show");
            $("#map").css('width', '600')
            $("#map_root").width(600)

        });

    });

</script>
@stop
