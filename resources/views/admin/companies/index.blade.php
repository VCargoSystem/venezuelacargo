@extends('adminlte::page')

@section('title', 'EMPRESAS')

@section('content_header')
    <h5><b style="color:#24298D; font-weight: 900;">EMPRESAS</b></h5>
@stop

@section('content')


@if (session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            <li> {!! session()->get('success')!!}</li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>


  <section class="content">

      <div class="container-fluid">
        <div class="row">

            <div class="col-12 mb-1 pr-3">
                <div class="row mb-1 float-right ">
                    <a href="{{ route('companies.create') }}" class="btn btn-sm btn-primary" style=" color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;"> <strong>AGREGAR</strong> </a>
                </div>
            </div>

          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
               <div class="card-header">
                  <form class="">
                    <div class="row">    
                      <div class="col-lg-4 col-md-4 col-xs-4">
                      <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">NOMBRE</label>
                          <input type="text" name="name" id="name"
                              class="form-control form-control-sm  {{ $errors->has('name') ? 'is-invalid' : '' }} mr-1"
                              value="" style="font-size:14px;" placeholder="Nombre de la empresa" autofocus maxlength="40">
                      </div>

                      <div class="col-lg-4 col-md-4 col-xs-4">
                      <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">DESCRIPCIÓN</label>
                          <input type="text" name="description" id="description"
                              class="form-control form-control-sm  {{ $errors->has('description') ? 'is-invalid' : '' }} mr-1"
                              value="" placeholder="Descripción" style="font-size:14px;" autofocus maxlength="40">
                      </div>

                      <div class="col-lg-4 col-md-4 col-xs-4">
                      <label class="float-left" style="font-size:15px !important; letter-spacing: -0.5px !important;">STATUS</label>
                          <select name="status" id="status" class="form-control" style="font-size:14px;">
                            <option value="">Seleccione</option>
                            <option value="ACTIVO">ACTIVO</option>
                            <option value="INACTIVO">INACTIVO</option>
                          </select>
                      </div>
                    </div>
                    <div style="height:7px;"></div>
                    <div class="row">
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                    <button id="_btn_filter"
                                        class=" ml-1 btn btn-primary btn-sm col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                        style=" color: #fff; background-color: #23298a; border-color: #23298a; box-shadow: none; margin-top: 7px !important; margin-left:0px !important;"><i class="fas fa-search"></i>&nbsp;BUSCAR</button>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                    <button id="_btn_refresh"
                                        class=" ml-1 btn btn-primary btn-sm  col-lg-12  col-sm-12 col-xs-12 col-md-12"
                                        style=" color: #fff; background-color: #23298a; border-color: #23298a; box-shadow: none; margin-top: 7px !important; margin-left:0px !important; min-height: 31px">
                                        <i class="fas fa-eraser"></i>&nbsp;LIMPIAR CAMPOS
                                    </button>
                                </div>
                                </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4"></div>
                            </div>
                    
                        </form>
                    </div>
              <div class="card-body">
                 <div class="table-responsive">
                    <table class="table table-striped table-responsive-lg table-responsive-md table-responsive-sm table-responsive-xs table-bordered datatable table-bordered" style="font-size:15px !important; letter-spacing: -0.5px !important;">
                    <thead>                  
                      <tr>
                        <th width="15%" style="background:#24298d; color:#ffb901;">NOMBRE</th>
                        <th width="15%" style="background:#24298d; color:#ffb901;">CÓDIGO</th>
                        <th width="15%" style="background:#24298d; color:#ffb901;">DESCRIPCIÓN</th>
                        <th width="15%" class="text-center" style="background:#24298d; color:#ffb901;">STATUS</th>
                        <th width="15%" class="text-center" style="background:#24298d; color:#ffb901;">ACCIÓN</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
    	                    
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
            
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>

<style type="text/css">
    
  .tracking {
    word-break: break-all;
  }

   .form-control {
        display: block;
        width: 100%;
        height: calc(1.95rem + 1px) !important;
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        box-shadow: inset 0 0 0 transparent;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    .table td, .table th {
        padding: .40rem !important;
        vertical-align: middle !important;
        border-top: 1px solid #dee2e6;
    }

    div.dataTables_wrapper .dataTables_info {
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    div.dataTables_wrapper .dataTables_paginate ul.pagination{
        font-size:15px !important;
        letter-spacing: -0.5px !important;
    }

    .jconfirm .jconfirm-box.jconfirm-type-blue {
    border-top: solid 10px #f44336 !important;
}

</style>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.css') }}">
<script src="{{ asset('js/plugins/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function() {

        // init datatable.
        var dataTable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 20,
            searching: false,
            paging: true,
            bLengthChange: false,
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: {
                url: "{{ route('companies.getFilterCompanies') }}",
                data: function (d) {
                  d.name = $('#name').val(),
                  d.description = $('#description').val(),
                  d.status = $('#status').val()
                }
            },
            columns: [
                  {
                    data: 'name',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.name + '</span>';
                    }
                  },
                  {
                    data: 'code',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.code + '</span>';
                    }
                  },
                  {
                    data: 'description',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.description + '</span>';
                    }
                  },
                  {
                    data: 'status',
                    sClass:'text-center',
                    'searchable': false,
                    render: function (data, type, row) {
                        return '<span class="tracking">' + row.status + '</span>';
                    }
                  },
                {data: 'actions', name: 'actions',orderable:false,serachable:false,sClass:'text-center'}
            ]
        });


        $('#_btn_filter').on('click', function (e) {
            e.preventDefault();
            arr = [];
            let _cont = 0
            let sw = 0
            arr[0] = $("#name").val()
            arr[1] = $("#description").val()
            arr[2] = $("#status").val()


            for (let i in arr) {
                if (i < 3 && (arr[i] != "")) {
                    _cont++
                }
            }

            if (_cont == 0) {
                $.confirm({
                    title: 'Filtro de busquedas!',
                    content: 'Debe seleccionar algun criterio',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        cancelar: function () {

                        }
                    }
                });
                return false;
            }

            dataTable.draw()
        });


        $('#_btn_refresh').on('click', function (e) {
            e.preventDefault();

            $("#name").val('')
            $("#description").val('')
            $("#status").val('').trigger('change')
            dataTable.draw()
        });

        //Delete user
        $('body').on('click', '.delete', function(e) {
          e.preventDefault();

          _id = $(this).attr("data-id");

          $.confirm({
                title: 'Eliminar Empresa!',
                content: 'Seguro desea eliminar el registro?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    delete: {
                        text: 'Eliminar',
                        btnClass: 'btn-red',
                        action: function(){
                          deleteCompanie(_id)
                        }
                    },
                    cancelar: function () {
                    }
                }
            });

        });

        function deleteCompanie(_id){

            $.ajax({
              url: _url = "<?=url("/admin/companies/")?>/"+_id,
              type: "DELETE",
              dataType: 'json',
              data: {
                "id": _id,
                "_token": "{{ csrf_token() }}"
              },
              success: function (data) {

                  if(data.success==1){
                    $("#msg").html(data.message)
                    $(".alert2").css('display', 'block')
                    $('#content').css('display','none')
                    dataTable.draw() 
                  }
              },
              error: function (data) {

              }
          });

        }
  });


    
</script>
@stop
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>
