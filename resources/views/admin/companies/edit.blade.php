@extends('adminlte::page')
@section('title', 'EDITAR EMPRESA')

@section('content_header')
    <h5><b style="color:#24298D; font-weight: 600;"> <strong>EDITAR EMPRESA</strong></b></h5>
@stop

@section('content')


@if (\session('errors'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>{!! session()->get('success')!!}
            <li>  </li>
        </ul>
    </div>
@endif
<div class="alert alert-success alert2" style="display: none">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span id="msg"></span>
</div>

  <section class="content">
      <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                   <div class="card">
                      <!-- /.card-header -->
                      <div class="card-body">
                               <form  method="post" id="_form_register" action="{{route('companies.update', $companie->id)  }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}

                                {{-- Name field --}}
                                <input type="hidden" name="id" value="{{ $companie->id }}">
                                <div class="row mb-1">
                                    <div class="col-12">
                                        <label>NOMBRE</label>
                                        <input type="text" name="name"  id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                               value="{{ $companie->name }}" placeholder="Nombre" autofocus>
                                  
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mb-1">
                                   <div class="col-12">
                                      <label>CÓDIGO</label>
                                        <input type="text" name="code" id="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}"
                                               value="{{ $companie->code }}" placeholder="Código" autofocus maxlength="20" readonly>
                                      
                                        @if($errors->has('code'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mb-1">
                                   <div class="col-12">
                                      <label>ESTATUS</label>
                                       <select name="status" class="form-control">

                                          <option value="1" @if($companie->status == 1) selected @endif>ACTIVO</option>
                                          <option value="0" @if($companie->status == 0) selected @endif>INACTIVO</option>
                                       </select>
                                      
                                        @if($errors->has('code'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="col-12">
                                        <label>DESCRIPCIÓN</label>
                                        <textarea name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                               value="" placeholder="Descripción" autofocus>{{ $companie->description }}</textarea>
                              
                                        @if($errors->has('description'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>


              {{-- Save button --}}
                    <div class="row mt-2 float-right">
                      <div class="col-sm-12">
                        <a href="{{ route('companies.index') }}" class="btn btn-default" id="btn_cancel" value="cancel">CANCELAR
                        </a>
                        <button type="submit" class="btn btn-primary" id="_btn_save" value="create" style="color: #fff;
    background-color: #23298a;
    border-color: #23298a;
    box-shadow: none;">GUARDAR
                        </button>
                      </div>
                    </div>

                </form>

              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
@stop

@section('js')

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

  });

</script>
@stop
<style type="text/css">
body{
        font-family: 'Montserrat', sans-serif !important;
    }
</style>
