@extends('adminlte::page')

@section('title', 'ESCRITORIO')

@section('content_header')
<h4><b style="color:#24298D; font-weight: 600;"> <strong>ESCRITORIO</strong> </b></h4>
@stop

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )

@if (config('adminlte.use_route_url', false))
@php( $login_url = $login_url ? route($login_url) : '' )
@php( $register_url = $register_url ? route($register_url) : '' )
@else
@php( $login_url = $login_url ? url($login_url) : '' )
@php( $register_url = $register_url ? url($register_url) : '' )
@endif

@section('content')

<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">El costo estimado de su envío es:</h5>
        
      </div>
      <div class="modal-body">
       <center> <h1 id="respuesta"></h1></center>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-secondary" id="button_cerrar" data-dismiss="modal" style="color: #24298d;
                              background-color: #ffb901;
                              border-color:#ffb901;
                              box-shadow: none; font-size:15px !important; font-weight: 900;">CERRAR</button>
      </div>
    </div>
  </div>
</div>
<section class="content">
    <div class="container-fluid">
        
        <!-- Info boxes -->
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon elevation-1" style="background:#24298d; color:white;">
                        <img src="{{ asset('vendor/adminlte/dist/img/user.svg') }}" class="" width="30" height="30">
                    </span>

                    <div class="info-box-content">
                        <span class="info-box-text" style="color:24298D; font-weight: 600;">CÓDIGOS POR ASIGNAR</span>
                        <a href="{{ route('admin.users.index') }}">
                            <span class="info-box-number h5">
                                {{$codes}}
                            </span>
                        </a>
                    </div>
                    <!-- /.info-box-content -->
                </div>

                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1">
                        <img src="{{ asset('vendor/adminlte/dist/img/bell.svg') }}" class="" width="30" height="30">
                    </span>

                    <div class="info-box-content">
                        <span class="info-box-text" style="color:24298D; font-weight: 600;">PREALERTAS</span>
                        <a href="{{ route('prealerts.index') }}">
                            <span class="info-box-number h5">
                                {{$prealerts}}
                            </span>
                        </a>
                    </div>

                    <!-- /.info-box-content -->

                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1">
                        <img src="{{ asset('vendor/adminlte/dist/img/almacen.svg') }}" class="" width="30" height="30">
                    </span>

                    <div class="info-box-content">
                        <span class="info-box-text" style="color:24298D; font-weight: 600;">ALMACÉN</span>
                        <a href="{{ route('packages.index') }}">
                            <span class="info-box-number h5">
                                {{$package}}
                            </span>
                        </a>
                    </div>

                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1">
                        <img src="{{ asset('vendor/adminlte/dist/img/instrucciones.svg') }}" class="" width="30"
                            height="30">
                    </span>

                    <div class="info-box-content">
                        <span class="info-box-text" style="color:24298D; font-weight: 600;">INSTRUCCIONES</span>
                        <a href="{{ route('admin.instrucctions.index') }}">
                            <span class="info-box-number h5">
                                {{$instrucctions}}
                            </span>
                        </a>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div><hr>

        <div class="card">

            <div class="card-body">
                <center>
                    <h5 class=""><b style="color:#24298D; font-weight: 900;">CALCULADORA</b></h5>
                </center>
                <hr>
                
                <div class="alert alert-danger error-calculator" role="alert" style="display:none;">
                        </div>
                
                <div>
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-6">
                            <center>
                                <h6 style="font-weight: 900;">TIPO DE ENVÍO</h6><br>

                                <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="option" id="option1" value="1"
                                        style="position: absolute; opacity: 0;" checked>
                                        <label class="form-check-label" for="option1" value="1"
                                        style="width:140px; height:140px; background:#24298d; border: solid 8px #24298d; border-radius: 15px;"><img
                                             id ="imagen-radio1" src="{{ asset('vendor/adminlte/dist/img/ship-light.svg') }}" height="100%"
                                            width="100%"></label>
                                            </div>  
                                <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="option" id="option2" value="2"
                                style="position: absolute; opacity: 0;">
                                        <label class="form-check-label" for="option2" value="2"
                                        style="width:140px; height:140px; background:white; border: solid 8px #24298d; border-radius: 15px;"><img
                                        id ="imagen-radio2" src="{{ asset('vendor/adminlte/dist/img/airplane-blue.svg') }}"
                                            height="100%" width="100%"></label>
                            </div>    
                                     

                                  </center>    

                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-6">
                        <center>
                        <h6 style="font-weight: 900; padding:7px;">SELECCIONA EL DESTINO</h6>
                                  <select class="form-control" name="destiner_id" id="destiner_id">
                                        <option value="">Seleccione</option>
                                        @foreach ($destiner as $sta)>
                                      
                                        <option value="{{ $sta->id }}">{{ $sta->name }}</option>
                                        
                                        @endforeach    
                                  </select><br>
                                  <h6 style="font-weight: 900; padding:7px;">MEDIDAS</h6>
                          <div class="row">
                            <div class="col-lg-3 col-md-3 col-xs-3">
                                <input type="text" name="width" id="width"
                                          class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                                           placeholder="ANCHO" autofocus maxlength="40">
                            </div>x
                            <div class="col-lg-3 col-md-3 col-xs-3"> 
                                <input type="text" name="lenght" id="lenght"
                                class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                                placeholder="LARGO" autofocus maxlength="40">
                              </div>x
                            <div class="col-lg-3 col-md-3 col-xs-3">
                                <input type="text" name="height" id="height"
                                  class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                                  placeholder="ALTO" autofocus maxlength="40">
                            </div>
                            
                            <div class="col-lg-2 col-md-2 col-xs-2">
                                <input type="text" name="weight" id="weight"
                                  class="form-control form-control-sm  {{ $errors->has('ware_house') ? 'is-invalid' : '' }} " value=""
                                  placeholder="PESO" autofocus maxlength="40">
                            </div>
                          </div><br>
                          <h6 style="font-weight: 900; padding:7px;">DESEA REEMPAQUE</h6>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="reemp" id="reemp1" value="1">
                                <label class="form-check-label" for="inlineRadio1">SI</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="reemp" id="reemp2" value="2" checked>
                                <label class="form-check-label" for="inlineRadio2">NO</label>
                              </div>

                        </div>
                        
                    </div>
                    </center>  
                    <hr>

                
                        <center><button class="btn btn-primary" id="_btn_calculator" style=" color: #24298d;
                              background-color: #ffb901;
                              border-color:#ffb901;
                              box-shadow: none; font-size:15px !important; font-weight: 900;">CALCULAR
                           </button></center>
            </div>
            
        </div>

        <div class="col-lg-4 col-md-4 col-xs-4">

        </div>
    </div>
   

</div>
    </div>
    </div>


    <!-- /.row -->
    </div>
    <!--/. container-fluid -->
</section>


@stop

@section('css')
<head>
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<style type="text/css">
    body {
        font-family: 'Montserrat', sans-serif !important;
    }

    .modal {
   width: 500px;
   height: 300px;
   position: absolute;
   left: 50%;
   top: 50%; 
   margin-left: -150px;
   margin-top: -150px;
}


</style>
@stop

@section('js')
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>

<script type="text/javascript">

$(document).ready(function() {
 

 // funcion para radio button y el style

 function checkRadio(){
    if($('#option1').prop('checked')){
    $('#option1').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option1]").attr({
        'style': 'width:140px; height:140px; background:#24298d; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio1").attr({
        'src': 'vendor/adminlte/dist/img/ship-light.svg',
        'height':'100%',
        'width':'100%'
    });
    }else{
        $('#option1').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option1]").attr({
        'style': 'width:140px; height:140px; background:white; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio1").attr({
        'src': 'vendor/adminlte/dist/img/ship-blue.svg',
        'height':'100%',
        'width':'100%'
    });
    }

    if($('#option2').prop('checked')){
    $('#option2').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option2]").attr({
        'style': 'width:140px; height:140px; background:#24298d; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio2").attr({
        'src': 'vendor/adminlte/dist/img/airplane-light.svg',
        'height':'100%',
        'width':'100%'
    });
    }else{
        $('#option2').attr({
        'style': 'position: absolute; opacity: 0;'
    });

    $("label[for=option2]").attr({
        'style': 'width:140px; height:140px; background:white; border: solid 8px #24298d; border-radius: 15px;'
    });

    $("#imagen-radio2").attr({
        'src': 'vendor/adminlte/dist/img/airplane-blue.svg',
        'height':'100%',
        'width':'100%'
    });
    }

 }
 $("input[name=option]").change(function() {
    checkRadio();
});
 



 $("#button_cerrar").on('click',function(){
         $('.modal').hide();
      });

  $("#_btn_calculator").on('click',function(){
      console.log($("input[name=option]:checked").val());
      console.log($("input[name=reemp]:checked").val());
        _url = "<?=url('/admin/calculator')?>"
        
        $.ajax({
        url: _url,
        type: "post",
        data: {
            "_token": "{{ csrf_token() }}",
            "width": $("#width").val(),
            "height": $("#height").val(),
            "lenght": $("#lenght").val(),
            "weight": $("#weight").val(),
            "trip_id": $("input[name=option]:checked").val(),
            "destiner_id": $("#destiner_id").val(),
            "reemp": $("input[name=reemp]:checked").val()
        },
        dataType: 'json',

        success: function (data) {
            if(data.error == 1){
                $('.error-calculator').html(data.message);
			    $('.error-calculator').fadeIn('fast');
                setTimeout(function(){ $('.error-calculator').fadeOut('fast'); }, 2000);
            }else{
                $('.modal').show();
                $('#respuesta').text('$'+data.success);
            }           
            
        },
       
        })
    });

  //create shippings
  

  
});

</script>
@stop




