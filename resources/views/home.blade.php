@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h5><b>Usuarios</b></h5>
@stop


@section('content')
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
@stop

@section('js')
    <script>
    </script>
@stop
