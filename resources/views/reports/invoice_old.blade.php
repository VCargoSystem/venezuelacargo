<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>Factura</title>

        <!-- Favicon -->
        <link rel="icon" href="./images/favicon.png" type="image/x-icon" />

        <!-- Invoice styling -->
        <style>
            body {
                font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                text-align: center;
                color: #777;
            }

            body h1 {
                font-weight: 300;
                margin-bottom: 0px;
                padding-bottom: 0px;
                color: #000;
            }

            body h3 {
                font-weight: 300;
                margin-top: 5px;
                margin-bottom: 10px;
                font-style: italic;
                color: #555;
            }

            body a {
                color: #06f;
            }

            .invoice-box {
                max-width: 800px;
                margin: auto;
                padding: 5px;
                /*border: 1px solid #eee;*/
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                line-height: 24px;
                font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                color: #555;
            }

            .invoice-box table {
                width: 100%;
                line-height: inherit;
                text-align: left;
                border-collapse: collapse;
                border-spacing: 1px;
            }

            .invoice-box table td {
                padding: 3px;
                vertical-align: top;
            }

            .invoice-box table tr td:nth-child(2) {
                text-align: right;
            }

            .invoice-box table tr.top table td {
                padding-bottom: 10px;
            }

            .invoice-box table tr.top table td.title {
                font-size: 45px;
                line-height: 45px;
                color: #333;
            }

            .invoice-box table tr.information table td {
                padding-bottom: 5px !important;
                padding-top: 4px !important;
                margin-top: 3px !important;
            }

            .invoice-box table tr.heading td {
                background: #eee;
                border-bottom: 1px solid #ddd;
                font-weight: bold;
            }

            .invoice-box table tr.details td {
                padding-bottom: 20px;
            }

            .invoice-box table tr.item td {
                border-bottom: 1px solid #eee;
            }

            .invoice-box table tr.item.last td {
                border-bottom: none;
            }

            .invoice-box table tr.total td:nth-child(2) {
                border-top: 2px solid #eee;
                font-weight: bold;
            }

            @media only screen and (max-width: 600px) {
                .invoice-box table tr.top table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }

                .invoice-box table tr.information table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }
            }

            .f12{
                font-size: 12px;
                line-height: 14px;
                font-style: bold;
            }
            .f18{
                font-size: 18px;
                line-height: 14px;
                font-style: bold;
            }
            .f14{
                font-size: 14px;
                line-height: 20px;
                font-style: bold;
            }

            .table-destina{
                text-align: left !important;
                background-color: #EAECEE;
                border: solid 1px;

            }
            .ulist{
                list-style: none;
            }
            .fblue{
                color: blue;
            }
            .fred{
                color: red;
            }
            .fgreen{
                color: green;
            }
        </style>
    </head>

    <body>
    
        <div class="invoice-box">
            <table border="0">
                <tr>
                    <td  width="30%" style="text-align: center;">
                        <img src="{{ asset('vendor/adminlte/dist/img/logo_reporte.svg') }}" width="150" height="120">
                    </td>
                     <td  width="80%" style="text-align: left; padding-top: 5px">
                        <img src="{{ asset('vendor/adminlte/dist/img/almacen.svg') }}" width="30" height="30" style="
                        padding-left: 20px; ">
                        <img src="{{ asset('vendor/adminlte/dist/img/package.svg') }}" width="30" height="30" style="
                        padding-left: 20px; padding-top: 5px">

                        <table border="1">
                            <tr>
                                <td rowspan="3"width="35%" style="font-size: 9; text-align: justify; line-height: 1.2;" class="table-destina"  >
                                    <b>Note:</b> You are hereby giving consent to inspect and/or screen this and all future cargo and/or shipments tendered by your company</td>
                                <td style="font-size: 11;">Invoice Number</td>
                                <td style="padding-left: 7px;"><b>{{ $invoice->id }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%" style="font-size: 11; text-align: right;">Invoice Date/Time</td>
                                 <td style="padding-left: 7px; text-align: left;"><b>{{ \Carbon\Carbon::parse($invoice->created_at)->format('d-m-Y') }} </b></td>
                            </tr>
                            <tr>
                                <td style="padding-left: 7px; text-align: right;">Received by</td>
                                <td></td>
                            </tr>
                
                        </table>
                    </td>
                </tr>
            </table>


            <!--tabla de datos-->

            <table border="1" style="padding-top: 5px;">
                <tr class="table-destina">
                    <td  colspan="2" width="50%" style="text-align: left;">
                        Shipper Information
                    </td>

                    <td  colspan="2" width="50%" style="text-align: left;">
                        Consignee Information
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: left; line-height: 1.2">
                        <b>VENEZUELA CARGO(7057)</b><br>
                        <b>0412-1812469</b><br>
                        <b>MIAMI FL VENEZUELA</b>
                    </td>
                </tr>

                <tr class="table-destina">
                    <td  colspan="4" style="text-align: center !important;">
                        Island Carrier and Supplier Information
                    </td>
                </tr>

                <tr>
                    <td width="20%" style="text-align: right !important;" class="table-destina">
                        Carrier Name
                    </td>
                    <td width="30%" style="text-align: left;">
                        <b>UPS</b>
                    </td>
                    <td width="20%"  style="text-align: right !important;" class="table-destina">
                        Driver License
                    </td>
                    <td width="30%" style="text-align: left;">
                        <b>0</b>
                    </td>
                </tr>

                <tr>
                    <td width="20%" style="text-align: right !important;" class="table-destina">
                        PRO Number
                    </td>
                    <td width="30%" style="text-align: left;">
                        <b>0</b>
                    </td>
                    <td width="20%"  style="text-align: right !important;" class="table-destina">
                        Supplier Name
                    </td>
                    <td width="30%" style="text-align: left;">
                        <b>0</b>
                    </td>
                </tr>

                <tr>
                    <td width="20%" style="text-align: right !important;" class="table-destina">
                        Tracking Number
                    </td>
                    <td width="30%" style="text-align: left;">
                        <b>0</b>
                    </td>
                    <td width="20%"  style="text-align: right !important;" class="table-destina">
                        Invoice Number
                    </td>
                    <td width="30%" style="text-align: left;">
                        <b></b>
                    </td>
                </tr>

                 <tr class="table-destina">
                    <td  colspan="2" width="50%" style="text-align: left;">
                        Notes
                    </td>

                    <td  colspan="2" width="50%" style="text-align: left;">
                        Applicable Charges
                    </td>
                </tr>

                <tr class="">
                    <td  colspan="2" width="50%" style="text-align: left;">
                        @if($invoice->shippings_id == 1 )
                            <b>MARITIME</b>
                        @else
                            AIR
                        @endif
                        <br><br>
                        <span style="font-size: 9">Apply for Insurance __ YES __NO </span>    <span style="font-size: 9;  padding-left: 10px !important;">Signature_____________</span>
                    </td>
                    <td  colspan="2" width="50%" style="text-align: left;">
                        
                    </td>


                </tr>

            </table>

            <!--Datos de paquetes-->
            {{--<table border="1" style="border-top: 0 !important;">
                 <tr class="table-destina" style="font-size: 11">
                    <td width="8%" style="text-align: left;">
                        Pcs
                    </td>
                    <td width="10%" style="text-align: center !important;">
                        Package
                    </td>
                     <td width="18%" style="text-align: center !important;">
                        Dimensions
                    </td>
                     <td width="38%" style="text-align: left;">
                        Description
                    </td>
                    <td width="15%" style="text-align: center !important;">
                        Weight
                    </td>
                    <td width="15%" style="text-align: center !important;">
                        Volume
                    </td>
                </tr>

                <tr class="table-destina" style="font-size: 11">
                    <td colspan="2" style="text-align: center !important;">
                        Locations
                    </td>
                    <td width="10%" style="text-align: center !important;">
                        Invoice Number
                    </td>
                     <td width="18%" style="text-align: left !important;">
                        Notes
                    </td>
                    <td  style="text-align: left;">
                        
                    </td>
                    <td width="15%" style="text-align: center !important; font-size: 9">
                        Volume Weight
                    </td>
                </tr>

                <tr class="table-destina" style="font-size: 11">
                    <td colspan="2" style="text-align: center !important;">
                        Quanty
                    </td>
                    <td width="10%" style="text-align: center !important;">
                        PO Number
                    </td>
                     <td width="18%" style="text-align: left !important;">
                        Part Number | Model | Serial Number
                    </td>
                    <td  style="text-align: left;">
                        
                    </td>
                    <td width="15%" style="text-align: center !important; font-size: 9">
                       
                    </td>
                </tr>

                <tr  style="font-size: 11; min-height: 150px !important;">
                    <td colspan="2" style="text-align: center !important;">
                        <b>{{ $inovice->nro_pieces }}<b>
                    </td>
                    <td width="10%" style="text-align: center !important;">
                        <b>0</b>
                    </td>
                     <td width="18%" style="text-align: left !important;">
                       <b>{{ $inovice->description }}</b>
                       <br><br><br><br>
                    </td>
                    <td  style="text-align: left;">
                        <b>{{ $inovice->weight }} LB</b>
                    </td>
                    <td width="15%" style="text-align: center !important; font-size: 11">
                        <b>{{ $inovice->lb_volume }} CTU</b>
                    </td>
                </tr>

            </table>
       
            <table border="1" style="border-top: none">
                <tr style="vertical-align: middle !important;">
                    <td rowspan="2" width="57%" style="line-height: 0.9em; font-size: 11; ">
                        <br>
                        <span  style="text-align: left !important; ">Received by</span>
                        <br>
                        <span>Signature _______________________</span>
                        <span ><b style="padding-left: 45px !important; color:blue; font-size: 14">TOTAL</b></span>
                    </td>
                    <td class="table-destina" style="font-size: 11; text-align: center !important;" width="12%">Pieces</td>
                    <td  class="table-destina" style="font-size: 11; text-align: center !important;" width="14%">Weight</td>
                    <td class="table-destina" style="font-size: 11; text-align: center !important;" width="14%">Volume</td>
                </tr>
                <tr>
                    <td style="font-size: 11; text-align: center !important;"><b>{{ $package->nro_pieces }}</b></td>
                    <td style="font-size: 11; text-align: center !important;"><b>{{ $package->weight }} LB</b></td>
                    <td style="font-size: 11; text-align: center !important;"><b>{{ $package->lb_volume }} CUFT</b></td>
                </tr>
            </table>--}}

            <table style="border: solid 1px;">
                <thead style="background-color: #EAECEE;">
                   <tr >
                        <td width="25%" style="border: 1px solid; text-align: center !important;">Total Piezas</td>
                        <td width="25%" style="border: 1px solid; text-align: center !important;">Total Peso</td>
                        <td width="25%" style="border: 1px solid;">Total Peso-Volumen</td>
                        <td width="25%" style="border: 1px solid; text-align: center !important;">
                        @if($invoice->shippings_id == 1)   
                            Total Pie 3
                        @else
                            Total Volumen
                        @endif
                        </td>
                    </tr>
                </thead>
                <tbody style="border-right: 1px solid; border-left: 1px solid;">
                    <tr>
                        <td style="text-align: center !important;" >{{ $invoice->total_pieces }}</td>
                        <td  style="text-align: center !important;" >{{ $invoice->total_pieces * $invoice->weight }} lbs/ft3
                        </td>
                        <td style="text-align: center !important;" >{{ $invoice->nro_pieces * $invoice->weight * $invoice->total_volume}} lbs/ft3</td>
                        <td style="text-align: center !important;" >
                            @if($invoice->shippings_id == 1)   
                                {{ $invoice->total_pie }}
                            @else
                                {{ $invoice->total_volume }}
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" width="50%" class="ulist" style="text-align: center !important;">
                            <li class="fblue f14">Usuario: {{ auth()->user()->first_name.' '.auth()->user()->last_name}} - <span class="fred">Zona: @if($invoice->shippings_id ==1) MARITIMO @else AEREO @endif</span></li>
                        </td>
                        <td  colspan="2"  width="50%" class="ulist" style="text-align: center !important;">
                        <li class="fred f14"><b>Servicio: @if($invoice->shippings_id ==1) Maritime @else Airpress @endif</b></li>
                        </td>
                    </tr>
                </tbody>

        </table>
             <table style="border: solid 1px;">
                        <thead style="background-color: #EAECEE;">
                            <tr>
                                <th width="7%"  style="border: 1px solid; text-align: center;">Long</th>
                                <th width="7%"  style="border: 1px solid; text-align: center;">Width</th>
                                <th width="7%"   style="border: 1px solid; text-align: center;">High</th>
                                <th width="7%"  style="border: 1px solid; text-align: center;">Units</th>
                                <th width="10%"  style="border: 1px solid; text-align: center;">Package</th>
                                <th width="10%"  style="border: 1px solid; text-align: center;">Weight</th>
                                <th width="50%"  style="border: 1px solid; text-align: center;">Description of Contens</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($invoice->details->count() > 0)

                            @foreach($invoice->details->where('description', '<>', 'Reempaque') as $item)
                                <tr style="border-bottom:hidden !important; text-align: center">
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->long }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->width }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->high }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->nro_box }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">Caja </td>
                                    <td style=" border-right: hidden !important; text-align: center">
                                        @if($invoice->shippings_id == 1)         
                                           {{ number_format($item->total_pie3 , 2) }}
                                        @else 
                                           @if($item->subtotal_lb > $item->subtotal_w)
                                               {{ $item->subtotal_lb }}
                                            @else 
                                               {{ $item->subtotal_w }}
                                            @endif

                                        @endif

                                       
                                    </td>
                                    <td style="text-align: left;">{{ $item->description }} </td>
                                </tr>
                            @endforeach

                            @if($invoice->details->where('description', '=', 'Reempaque')->count() > 0)
                                <tr>
                                    <td colspan="7" class="ulist" style="text-align: left; !important;">
                                        <li class=" f14">With Repackage</li>
                                    </td>
                                </tr>

                                @foreach($invoice->details->where('description', '=', 'Reempaque') as $item)
                                <tr style="border-bottom:hidden !important; text-align: center">
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->long }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->width }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->high }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">{{ $item->nro_pieces }} </td>
                                    <td style=" border-right: hidden !important; text-align: center">Caja </td>
                                    <td style=" border-right: hidden !important; text-align: center">
                                        @if($invoice->shippings_id == 1)         
                                           {{ number_format($item->total_pie3 , 2) }}
                                        @else 
                                           @if($item->subtotal_lb > $item->subtotal_w)
                                               {{ $item->subtotal_lb }}
                                            @else 
                                               {{ $item->subtotal_w }}
                                            @endif

                                        @endif

                                       
                                    </td>
                                    <td style="text-align: left;">{{ $item->description }} </td>
                                </tr>
                            @endforeach
                            @endif

                         @endif
                        </tbody>

                    </table>
          <table style="text-align: right;">
                <tr>
                    <td colspan="7">
                        Sub packages: {{ number_format($invoice->subtotal_package,  2)}}
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        Sub repackages: {{ number_format($invoice->subtotal_repackage, 2)}}
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        Sub aditional: {{ number_format($invoice->additional, 2)}}
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        Sub box: {{ number_format($invoice->amount_box, 2)}}
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                    </td>
                    <td colspan="3" style="border-top:solid 1px #000 !important;">
                        Total: {{ number_format($invoice->amount, 2)}}
                    </td>
                </tr>

            </table>
        </div>
    </body>
</html>
