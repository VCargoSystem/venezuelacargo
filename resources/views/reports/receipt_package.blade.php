<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>Recibo de paquete</title>

        <!-- Favicon -->
        <link rel="icon" href="./images/favicon.png" type="image/x-icon" />
        
        

        <!-- Invoice styling -->
        <style>
            body {
                font-family: 'Montserrat', sans-serif !important;
                text-align: center;
                color: #777;
            }

            body h1 {
                font-weight: 300;
                margin-bottom: 0px;
                padding-bottom: 0px;
                color: #000;
            }

            body h3 {
                font-weight: 300;
                margin-top: 5px;
                margin-bottom: 10px;
                font-style: italic;
                color: #555;
            }

            body a {
                color: #06f;
            }

            .invoice-box {
                max-width: 800px;
                margin: auto;
                padding: 5px;
                /*border: 1px solid #ccc;*/
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                line-height: 24px;
                font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                color: #555;
            }

            .invoice-box table {
                width: 100%;
                line-height: inherit;
                text-align: left;
                border-collapse: collapse;
                border-spacing: 1px;
            }

            .invoice-box table td {
                padding: 3px;
                vertical-align: top;
            }

            .invoice-box table tr td:nth-child(2) {
                text-align: right;
            }

            .invoice-box table tr.top table td {
                padding-bottom: 10px;
            }

            .invoice-box table tr.top table td.title {
                font-size: 45px;
                line-height: 45px;
                color: #333;
            }

            .invoice-box table tr.information table td {
                padding-bottom: 5px !important;
                padding-top: 4px !important;
                margin-top: 3px !important;
            }

            .invoice-box table tr.heading td {
                background: #eee;
                border-bottom: 1px solid #ddd;
                font-weight: bold;
            }

            .invoice-box table tr.details td {
                padding-bottom: 20px;
            }

            .invoice-box table tr.item td {
                border-bottom: 1px solid #eee;
            }

            .invoice-box table tr.item.last td {
                border-bottom: none;
            }

            .invoice-box table tr.total td:nth-child(2) {
                border-top: 2px solid #eee;
                font-weight: bold;
            }

            @media only screen and (max-width: 600px) {
                .invoice-box table tr.top table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }

                .invoice-box table tr.information table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }
            }

            .f12{
                font-size: 12px;
                line-height: 14px;
                font-style: bold;
            }
            .f18{
                font-size: 18px;
                line-height: 14px;
                font-style: bold;
            }
            .f14{
                font-size: 14px;
                line-height: 20px;
                font-style: bold;
            }

            .table-destina{
                text-align: left !important;
                background-color: #EAECEE;
                border: solid 1px;

            }
            .ulist{
                list-style: none;
            }
            .fblue{
                color: blue;
            }
            .fred{
                color: red;
            }
            .fgreen{
                color: green;
            }
            .fcolor{
                color:#23298a
            }


            #watermark {
                position: fixed;

                /** 
                    Establece una posición en la página para tu imagen
                    Esto debería centrarlo verticalmente
                **/
                bottom:   6cm;
                right:     -1.5cm;

                /** Cambiar las dimensiones de la imagen **/
                width:    12cm;
                height:   12cm;

                /** Tu marca de agua debe estar detrás de cada contenido **/
                z-index:  -1000;
            }
        </style>
    </head>

    <body>
    
        <div id="watermark">
        <img src="../public/vendor/adminlte/dist/img/marca-de-agua.png" height="100%" width="100%" />
        </div>

        <div class="invoice-box">
            <table border="0">
                <tr>
                    <td  width="30%" style="text-align: center;">
                        <img src="../public/vendor/adminlte/dist/img/logo_reporte.svg" width="130" height="76" style="padding-top:20px;">
                    </td>
                     <td  width="80%" style="text-align: left; padding-top: 5px">

                        <table style="border:1px solid #ccc !important;">
                            <tr >
                                <td style="font-size: 11; line-height: 0.5em; padding-left: 10px">
                                    <p>venezuelacargo.com</p>
                                    <p>8601 NW 72nd Miami, FL - 33166</p>
                                    <p>+ 58 (412) 181 2469</p>
                               
                                </td>
                                <td style="padding-left: 7px; font-size: 11;">
                                    <b class="fcolor">RECIBO - {{ $package->id }}</b>
                                    <br>
                                    {{ \Carbon\Carbon::parse($package->created_at)->format('d-m-Y') }} 
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
            </table>

            <!--tabla de datos usuario-->
            <table style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 5px;">     
                <tr>
                    <td  colspan="2"  style="text-align: left; padding-left: 10px;">
                        <b class="fcolor">COD - {{ $user->code }}</b>
                    </td>
                </tr>
                <tr style="font-size: 11; line-height: 0.7em;">
                    <td  colspan="2"  style="text-align: left; padding-left: 10px;">
                        {{  \Str::upper($user->name.' '.$user->last_name) }}
                    </td>
                </tr>

                <tr>
                    <td  colspan="2"  style="text-align: left; padding-left: 10px; font-size:13px;">
                        {{ \Str::upper($user->address[0]->country->name.', '. $user->address[0]->state->name.', '.$user->address[0]->city[0]->name.', '.$user->address[0]->address) }}
                    </td>
                </tr>
            </table>                  


            <!--tabla de trackings-->
            <table style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 5px;">  
                <thead>
                    <tr>
                      <th width="65%" class="fcolor" style="text-align: left; padding-left: 10px; font-size:13px;">TRACKING</th>
                      <th class="fcolor" style="text-align: left; padding-left: 10px; font-size:13px;">TRANSPORTISTA</th>
                    </tr>
                </thead>   
                <tr style="font-size: 11; line-height: 0.2em;">
                    <td style="text-align: left; padding-left: 10px;">
                        @foreach($package->trackings as $t)
                            <p>{{ \Str::upper($t->tracking) }}</p>
                        @endforeach
                    </td>
                    <td  colspan="2"  style="text-align: left; padding-left: 10px;">
                        @foreach($companies as $t)
                            <p>{{ \Str::upper($t) }}</p>
                        @endforeach
                    </td>
                </tr>
            </table>     
            
            <p class="fcolor" style="text-align: left; font-size:13px;"><b>INFORMACIÓN DEL PAQUETE</b></p>

            <!--tabla de datos del paquete-->
            <table style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 5px;">  
                <thead>
                    <tr>
                       <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="25%" class="fcolor">TOTAL PIEZAS</th>
                       <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="25%" class="fcolor">TOTAL PESO LB</th>
                        <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="25%" class="fcolor">TOTAL VOLUME LB</th>
                        <th width="25%" class="fcolor" style="text-align: center; font-size:13px;">TOTAL PIE CÚB</th>
                    </tr>
                </thead>   
                <tr style="font-size: 11; text-align: center;">
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ $package->nro_pieces }}</b>
                    </td>
                     <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ ($package->weight * $package->nro_pieces) }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ $package->lb_volume }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ $package->pie_cubico }}</b>
                    </td>
                </tr>
            </table>


            <!--tabla de datos recibos-->
            <table style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 8px;">  
                <thead>
                    <tr>
                       <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="10%">ALTO</th>
                        <th style="border-right:1px solid #ccc !important;; text-align: center; font-size:13px;" width="10%">ANCHO</th>
                        <th width="10%" style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;">LARGO</th>
                        <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="10%" >PESO LB</th>
                        <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="10%" >VOL LB</th>
                        <th width="10%" style="text-align: center; border-right:1px solid #ccc !important; font-size:13px;">FT<sup>3</sup></th>
                        <th width="30%" style="text-align: center; border-right:1px solid #ccc !important; font-size:13px;">DESCRIPCIÓN</th>
                    </tr>
                    </tr>
                </thead>   
                <tr style="font-size: 11; text-align: center;">
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ $package->high }}</b>
                    </td>
                     <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ ($package->width) }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ $package->long }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ number_format(($package->weight/$package->nro_pieces), 2, '.', '') }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b> 
                            {{ number_format(($package->lb_volume/$package->nro_pieces ), 2, '.', '') 
                            }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b> {{ number_format(($package->pie_cubico/$package->nro_pieces  ), 2, '.', '') 
                            }}</b>
                    </td>
                    <td style="text-align: left; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                        <b>{{ $package->description }}</b>
                        <br><br><br><br>
                    </td>

                </tr>
            </table>

            
        </div>
    </body>
</html>
