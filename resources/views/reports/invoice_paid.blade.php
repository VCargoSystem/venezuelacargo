<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>Factura de envios</title>

        <!-- Favicon -->
        <link rel="icon" href="./images/favicon.png" type="image/x-icon" />
        

        <!-- Invoice styling -->
        <style>
            body {
                font-family: 'Montserrat', sans-serif !important;
                text-align: center;
                color: #777;
            }

            body h1 {
                font-weight: 300;
                margin-bottom: 0px;
                padding-bottom: 0px;
                color: #000;
            }

            body h3 {
                font-weight: 300;
                margin-top: 5px;
                margin-bottom: 10px;
                font-style: italic;
                color: #555;
            }

            body a {
                color: #06f;
            }

            .invoice-box {
                max-width: 800px;
                margin: auto;
                padding: 2px;
                /*border: 1px solid #ccc;*/
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                line-height: 24px;
                font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                color: #555;
            }

            .invoice-box table {
                width: 100%;
                line-height: inherit;
                text-align: left;
                border-collapse: collapse;
                border-spacing: 1px;
            }

            .invoice-box table td {
                padding: 3px;
                vertical-align: top;
            }

            .invoice-box table tr td:nth-child(2) {
                text-align: right;
            }

            .invoice-box table tr.top table td {
                padding-bottom: 10px;
            }

            .invoice-box table tr.top table td.title {
                font-size: 45px;
                line-height: 45px;
                color: #333;
            }

            .invoice-box table tr.information table td {
                padding-bottom: 2px !important;
                padding-top: 2px !important;
                margin-top: 3px !important;
            }

            .invoice-box table tr.heading td {
                background: #eee;
                border-bottom: 1px solid #ddd;
                font-weight: bold;
            }

            .invoice-box table tr.details td {
                padding-bottom: 20px;
            }

            .invoice-box table tr.item td {
                border-bottom: 1px solid #eee;
            }

            .invoice-box table tr.item.last td {
                border-bottom: none;
            }

            .invoice-box table tr.total td:nth-child(2) {
                border-top: 2px solid #eee;
                font-weight: bold;
            }

            @media only screen and (max-width: 600px) {
                .invoice-box table tr.top table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }

                .invoice-box table tr.information table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }
            }

            .f12{
                font-size: 12px;
                line-height: 14px;
                font-style: bold;
            }
            .f18{
                font-size: 18px;
                line-height: 14px;
                font-style: bold;
            }
            .f14{
                font-size: 14px;
                line-height: 20px;
                font-style: bold;
            }

            .table-destina{
                text-align: left !important;
                background-color: #EAECEE;
                border: solid 1px;

            }
            .ulist{
                list-style: none;
            }
            .fblue{
                color: blue;
            }
            .fred{
                color: red;
            }
            .fgreen{
                color: green;
            }
            .fcolor{
                color:#23298a
            }

            #watermark {
                position: fixed;

                /** 
                    Establece una posición en la página para tu imagen
                    Esto debería centrarlo verticalmente
                **/
                bottom:   6cm;
                right:     -1.5cm;

                /** Cambiar las dimensiones de la imagen **/
                width:    12cm;
                height:   12cm;

                /** Tu marca de agua debe estar detrás de cada contenido **/
                z-index:  -1000;
            }

            #paid_mark {
                position: fixed;

                /** 
                    Establece una posición en la página para tu imagen
                    Esto debería centrarlo verticalmente
                **/
                bottom:   6cm;
                left:     -1.5cm;

                /** Cambiar las dimensiones de la imagen **/
                width:    12cm;
                height:   12cm;

                /** Tu marca de agua debe estar detrás de cada contenido **/
                z-index:  -1000;
            }

            footer{
                position: fixed;
                bottom:   3cm; 
            }
        </style>
    </head>

    <body>
    
        <div id="watermark">
            <img src="../public/vendor/adminlte/dist/img/marca-de-agua.png" height="100%" width="100%" /> 
        </div>

        <div id="paid_mark">
            <img src="../public/vendor/adminlte/dist/img/paid-invoice.png" height="100%" width="100%" /> 
        </div>

        <div class="invoice-box">
            <table border="0">
                <tr>
                    <td  width="30%" style="text-align: center;">
                        <img src="../public/vendor/adminlte/dist/img/logo_reporte.svg" width="130" height="76" style="padding-top:20px;">
                    </td>
                     <td  width="80%" style="text-align: left; padding-top: 5px">

                        <table style="border:1px solid #ccc !important;">
                            <tr >
                                <td style="font-size: 11; line-height: 0.6em; padding-left: 10px">
                                    <p>venezuelacargo.com</p>
                                    <p>8601 NW 72nd Miami, FL - 33166</p>
                                    <p>+ 58 (412) 181 2469</p>
                               
                                </td>
                                <td style="padding-left: 7px; padding-right: 7px; font-size: 11; line-height: 0.5em;">
                                    <p><b class="fcolor">FACTURA - {{ $invoice->id }}</b></p>
                                   
                                    <p>{{ \Carbon\Carbon::parse($invoice->created_at)->format('d-m-Y') }} </p>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
            </table>

            <!--tabla de datos usuario-->
            <table style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 5px;">     
                <tr>
                    <td  colspan="2"  style="text-align: left; padding-left: 10px;">
                        <b class="fcolor">COD - {{ $user->code }}</b>
                    </td>
                </tr>
                <tr style="font-size: 11; line-height: 0.7em;">
                    <td  colspan="2"  style="text-align: left; padding-left: 10px;">
                        {{  \Str::upper($user->name.' '.$user->last_name) }}
                    </td>
                </tr>

                <tr>
                    <td  colspan="2"  style="text-align: left; padding-left: 10px; font-size:13px;">
                        {{ \Str::upper($user->address[0]->country->name.', '. $user->address[0]->state->name.', '.$user->address[0]->city[0]->name.', '.$user->address[0]->address) }}
                    </td>
                </tr>
            </table>                  


            <!--tabla de trackings-->
            <table  style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 10px; line-height: 1.2em;">  
                <thead>
                    <tr>
                        <th width="33%" class="fcolor" style="text-align: center; padding-left: 10px; border-right: 1px solid #ccc !important; font-size:13px;">FECHA ESTIMADA DE LLEGADA</th>
                        <th width="33%" class="fcolor" style="text-align: center; padding-left: 10px; border-right: 1px solid #ccc !important; font-size:13px;">TIPO DE VIAJE</th>
                        <th width="33%" class="fcolor" style="ext-align: center; padding-left: 10px; border-right: 1px solid #ccc !important; font-size:13px;">NÚMERO DE CONTENEDOR</th>
                    </tr>
                </thead>   
                <tr style="font-size: 11; line-height: 1.2em;">
                    <td style="text-align: center; padding-left: 10px; border-right: 1px solid #ccc !important; border-top: 1px solid #ccc !important;">
                       {{ $invoice->details[0]->date_in }}
                    </td>
                    <td  style="text-align: center; padding-left: 10px; border-right: 1px solid #ccc !important; border-top: 1px solid #ccc !important;">
                        @if($invoice->shippings_id == 1 )
                            <b>MARITIMO</b>
                        @else
                            AEREO
                        @endif
                    </td>
                    <td  style="text-align: center; padding-left: 10px; border-right: 1px solid #ccc !important; border-top: 1px solid #ccc !important;">
                         {{ $invoice->nro_contenedor }}
                    </td>
                </tr>
            </table>     
            
            <p class="fcolor" style="text-align: left; font-size:13px;"><b>INFORMACIÓN DEL PAQUETE</b></p>

            <!--tabla de datos del paquete-->
            <table style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 5px;">  
                <thead>
                    <tr>
                       <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="25%" class="fcolor">TOTAL PIEZAS</th>
                       <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="25%" class="fcolor">TOTAL PESO LB</th>
                        <th style="border-right:1px solid #ccc !important; text-align: center; font-size:13px;" width="25%" class="fcolor">TOTAL VOLUME LB</th>
                        <th width="25%" class="fcolor" style="text-align: center;">TOTAL PIE CÚB</th>
                    </tr>
                </thead>   
                <tr style="font-size: 11; text-align: center;">
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">
                        <b>{{ $invoice->total_pieces }}</b>
                    </td>
                     <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">
                        <b>{{ ($invoice->total_w) }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">
                        <b>{{ number_format($invoice->total_volume,2) }}</b>
                    </td>
                    <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">
                        <b>{{ number_format( $invoice->total_pie,2) }}</b>
                    </td>
                </tr>
            </table>
        
            <!--tabla de datos del paquete-->
            <table style="border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; border-top: 1px solid #ccc !important; margin-right: 3px; margin-top: 8px;">  
                <thead>
                    <tr>
                       <th style="border-right:1px solid #ccc !important; text-align: center; text-align: center; font-size:13px;" width="25%">WH</th>
                       <th style="border-right:1px solid #ccc !important; text-align: center; text-align: center; font-size:13px;" width="25%">RECIBOS</th>
                        <th width="50%" style="text-align: center; border-right:1px solid #ccc !important; text-align: center; font-size:13px;">DESCRIPCIÓN</th>
                    </tr>
                </thead>  
                    <tr style="font-size: 9; text-align: center; line-height: 0.5em;">
                            <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">
                            <p style="Line-height:0.5cm">
                                @foreach($packages as $pak) 
                        
                                    @if($pak->package_id > 0)
                                        {{ $pak->ware_house }}<br>
                                    @endif
                                @endforeach
                                </p>
                            </td>
                            <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">
                            <p style="Line-height:0.5cm">
                                @foreach($packages as $pak)
                                    @if($pak->package_id > 0) 
                                        &nbsp;<br>
                                    @endif
                                @endforeach

                                @foreach($packages as $pak) 
                                    @if($pak->package_id == 0)
                                        @foreach($pak->wh as $w) 
                                            {{ $w }}<br>
                                        @endforeach
                                    @endif
                                @endforeach
                                </p>
                            </td>
                            <td style="text-align: left; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">
                            <p style="Line-height:0.5cm">
                                @foreach($packages as $pak) 
                                    @if($pak->package_id > 0)
                                        {{ $pak->description }}<br>
                                    @else
                                        @foreach($pak->descriptions as $w) 
                                            {{ $w }}<br>
                                        @endforeach
                                    @endif
                                @endforeach
                                </p>
                            </td>
                        
                    </tr>
                    <tr style="font-size: 11; text-align: center; line-height: 1.2em;">
                            <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                            </td>
                            <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">

                                <b>GASTOS EXTRAS</b>
                                
                            </td>
                            <td style="text-align: left; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">

                            </td>
                        
                    </tr>
                     <tr style="font-size: 11; text-align: center; line-height: 0.5em;">
                            <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">
                            </td>
                            <td style="text-align: center; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important; padding-top:0.5px;">

                                <p>{{ $invoice->additional }} $</p>
                                
                            </td>
                            <td style="text-align: left; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;">

                            </td>
                        
                    </tr>

            </table>



            
            <!--tabla totales-->
            <table border="0" style="padding-top: 5px;">  
                <thead>
                
                  <tr style="font-size: 11; text-align: center; line-height: 1em;">
                        <td width="50%">
                        </td>
                        <td width="50%" style="text-align: left; ">
                                <table>


                                    <tr>
                                        <td style="text-align: left; border-right:1px solid #ccc !important; border-top:1px solid #ccc !important;border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            <b class="fcolor">TOTAL PAQUETES</b>
                                        </td>
                                        <td style="text-align: right; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; border-top:1px solid #ccc !important; font-size:13px;">
                                            $ {{ $invoice->subtotal_package!=null? number_format($invoice->subtotal_package,2):0 }}
                                        </td>
                                    </tr>

                                     <tr>
                                        <td style="text-align: left; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            <b class="fcolor">TOTAL PAQUETES REEMP.</b>
                                        </td>
                                        <td style="text-align: right; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            $ {{ $invoice->subtotal_repackage != null ? number_format($invoice->subtotal_repackage,2) : 0}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            <b class="fcolor">TOTAL SEGURO</b>
                                        </td>
                                        <td style="text-align: right; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            $ {{ number_format($invoice->cost_insured,2) }}
                                        </td>
                                    </tr>
                    
                                @if($invoice->cost_repackage > 0)
                                    <tr>
                                        <td style="text-align: left; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            <b class="fcolor">COSTOS REEMPAQUE</b>
                                        </td>
                                        <td style="text-align: right; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">$ {{ $invoice->cost_repackage }}</td>
                                    </tr>
                                @endif

                                @if($invoice->additional > 0)
                                    <tr>
                                        <td style="text-align: left; border-right:1px solid #ccc !important; border-bottom:1px solid #ccc !important;border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            <b class="fcolor">COSTOS EXTRAS</b>
                                        </td>
                                        <td style="text-align: right; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; border-bottom:1px solid #ccc !important; font-size:13px;">$ {{   $invoice->additional }}</td>
                                    </tr>
                                @endif

                                 @if($invoice->amount_box > 0)
                                    <tr>
                                        <td style="text-align: left; border-right:1px solid #ccc !important; border-bottom:1px solid #ccc !important;border-left:1px solid #ccc !important; padding-left: 10px; font-size:13px;">
                                            <b class="fcolor">COSTOS CAJAS</b>
                                        </td>
                                        <td style="text-align: right; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; border-bottom:1px solid #ccc !important; font-size:13px;">$ {{   $invoice->amount_box }}</td>
                                    </tr>
                                @endif

                                </table>

                        </td>
                        
                    </tr>
            </table>

            <!--totales-->
             <table border="0" style="padding-top: 2px;">  
                <thead>
                
                  <tr style="font-size: 11; text-align: center; line-height: 1.2em;">
                        <td width="50%">
                        </td>
                        <td style="text-align: left; ">
                                <table style="background: #ccc">
                                    <tr>
                                        <td style="text-align: left; border-right:1px solid #ccc !important; border-bottom:1px solid #ccc !important;border-left:1px solid #ccc !important; padding-left: 10px;">
                                            <b class="fcolor">COSTO TOTAL</b>
                                        </td>
                                        <td style="text-align: right; border-right:1px solid #ccc !important; border-left:1px solid #ccc !important; padding-left: 10px; border-bottom:1px solid #ccc !important;">
                                            $ {{ number_format($invoice->amount,2)  }}
                                        </td>
                                    </tr>
                                </table>

                        </td>
                        
                    </tr>
            </table>
            <br><br><br>
            <br><br><br>
            <footer>
            <hr style="color: #ccc; line-height: 0.5em">
            <li class="fcolor" style="text-align: left; font-size: 9;  line-height: 1.2em; list-style-type: none;"><strong>CONDICIONES DE ENVÍO</strong></li>
            <li style="text-align: left; padding-left: 10px; font-size: 9; line-height: 1.2em">Su fecha estimada de entrega será considerada tan pronto su factura sea cancelada.</li>
            <li style="text-align: left; padding-left: 10px; color: red; font-size: 9; line-height: 1.2em">LA FECHA DE LLEGADA ES UNA ESTIMACIÓN DEL INGRESO DEL PAQUETE A VENEZUELA.</li>
            <li style="text-align: left; padding-left: 10px; font-size: 9; line-height: 1.2em">En caso de realizar el pago por transferencia en BS al cambio consultar la tasa actual del $.</li>
            <li style="text-align: left; padding-left: 10px; color: red; font-size: 9; line-height: 1.2em">Antes de registrar el pago de la factura consultar su monto actualizado.</li>
            <li style="text-align: left; padding-left: 10px; font-size: 9; line-height: 1.2em">CADA DIA DE RETRASO EN SU PAGO SE CONSIDERA UN DIA DE RETRASO EN SU ENTREGA.</li>
            </footer>
           

        </div>
    </body>
</html>
