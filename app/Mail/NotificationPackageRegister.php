<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Package;
use App\Models\ImagePackage;

class NotificationPackageRegister extends Mailable
{
    use Queueable, SerializesModels;
    public $package;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Package $package, ImagePackage $image)
    {
        $this->package = $package;
        $this->image = $image;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('notificación de registro de paquete')
            ->view('mails.notification_package_register')
            ->from('admin@gmail.com');
    }
}
