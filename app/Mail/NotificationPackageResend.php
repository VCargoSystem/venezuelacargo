<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Package;

class NotificationPackageResend extends Mailable
{
    use Queueable, SerializesModels;
    public $package;
    public $pdf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Package $package, $pdf)
    {
        $this->package = $package;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('notificación de registro de paquete')
            ->view('mails.notification_package_register')
            ->from('admin@gmail.com')
            ->attachData($this->pdf, 'recibo.pdf', ['mime' => 
                'application/pdf']);
    }
}
