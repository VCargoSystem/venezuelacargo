<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\Prealerts;
use App\Models\Companies;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use App\Mail\NotificationPackageResend;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class PackagesController extends Controller
{
   	public function index(Request $request)
    {
         if ($request->ajax()) {
            $data = Package::with('status')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->orderBy('date_delivery', 'desc')
                    ->select('packages.*', 'status.name as status', 'images_packages.path', 'images_packages.name as image',  'images_packages.id as image_id')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('packages.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a> <a href="#" class="btn btn-xs btn-primary delete" data-id="'.$row->id.'"><i class="fa fa-trash"></i> Eliminar</a>';
    
                            return $btn;
                    })
                   
                    ->rawColumns(['actions'])
                    ->make(true);
        }
       
        return view('admin.packages.index');
    }


    public function getFilterPackages(Request $request)
    {
        
        $packages = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->where('packages.status_id', '=', 1)
                    ->groupBy('packages.id')
                    ->orderBy('confirmed', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'lb_volume','images_packages.name as image', 'images_packages.url', 'confirmed as status', 'packages_trackings.tracking', DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code', 'nro_pieces', 'pie_cubico', 'packages.instrucctions', 'repackage',DB::raw("DATE_FORMAT(packages.created_at, '%d-%m-%Y') as date")])
                    ;

        if (!empty($request->get('ware_house'))) {
            $packages->where('packages.ware_house', 
                    'like', '%' . $request->get('ware_house') . '%'
                    );
        }

        if (!empty($request->get('tracking'))) {
                $packages->where('packages_trackings.tracking', 
                    'like', '%' . $request->get('tracking') . '%'
                    );
        }

        if (!empty($request->get('instrucctions'))) {
            $packages->where('packages.instrucctions', '=', $request->get('instrucctions'));
        }

        if (!empty($request->get('usuario'))) {
            $packages->where('users.id', '=',  $request->get('usuario')
            );
        }

        /*if (!empty($request->get('status'))) {
            $packages->where('packages.confirmed', 
                    '=', $request->get('status')
            );
        }*/

        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $packages->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }


        return Datatables::of($packages)
        			->addIndexColumn()
                    ->addColumn('actions', function($row){
                        $btn_i = ""; 

                        if($row->instrucctions == 0){
                           $btn_i = '<a href="#" data-toggle="tooltip" data-placement="top" title="Asignar Instrucción de envío" class="btn btn-xs btn-dark instrucction" data-id="'.$row->id.'"><i class="fas fa-plus"></i> </a>';
                        }
                           $btn = '<a href="'.route('packages.edit', $row->id).'"  class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit" style="padding:1px;"></i></a> 

                              
                                <a href="#" style="background-color:#f44336 !important; border: solid 1px #f44336;" class="btn btn-xs btn-dark delete" data-id="'.$row->id.'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Eliminar" style="padding:1px;"></i> </a>
                               
                                <a href="'.route('packages.receipt', $row->id).'" class="btn btn-xs btn-warning" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Ver recibo" target="_blank"><i class="fa fa-print" style="padding:1px;"></i> </a>
                                <a href="'.route('packages.resend', $row->id).'" class="btn btn-xs btn-success" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Reenviar recibo"><i class="fas fa-envelope-open-text" style="padding:1px;"></i> </a>';
                            
                           // $con = '<a href="#" class="btn btn-xs btn-dark ml-1 confirma" data-id="'.$row->id.'" instrucctions="'.$row->instrucctions.'" data-toggle="tooltip" data-placement="top" title="Pasar a instrucciones"><i class="fa fa-check-square" style="padding:1px;"></i></a>';

                           return $btn_i." ".$btn;
                    })
              
                   
                    ->rawColumns(['actions'])
        ->make(true);
    }

    public function create(){
        return view('admin.packages.create');
    }

    public function assing($id){
        $package = Package::where('id', $id)
                        ->select('packages.*')
                        ->first();

        if($package->instrucctions == 1){
            return response()->json(["success" => 0, "message" => "Ya tiene una instrucción asignada, por favor seleccione la opcion editar" ]);
        }else{
            return response()->json(["success" => 1, "message" => "Asignar Instrucción" ]);
        }
    }

    public function createInstructions(Request $request){

        $validator = \Validator::make($request->all(), [
           'hold' => 'required|min:0', 
           'repackage' => 'required|min:0',
           'shippins_id' => 'required|min:1'
       ],
       [
           'hold.required' => 'Campo Almacen requerido',
           'hold.min' => 'Valor minimo requerido (0)',
           'repackage.required' => 'Campo Reempaque requerido',
           'repackage.min' => 'Valor minimo requerido (0)',
           'shippins_id.required' => 'Campo tipo de envio requerido',
           'shippins_id.min' => 'Valor minimo requerido (1)'
       ]
       );

       if ($validator->fails()){
           return view('admin.packages.index')->withErrors($validator);
       }

     
       $package = Package::where('id', $request->input('id_package'))->first();


       $package->hold = $request->input('hold');
       $package->insured = ($request->input('insured') > 0 ? $request->input('insured') : 0);
       $package->shippins_id = $request->input('shippins_id');
       $package->repackage = $request->input('repackage');
      
       if($request->input('hold') == 1){
           $package->instrucctions = 1;
           $package->status_id = 1;
          // $package->confirmed = 'POR CONFIRMAR';
       }else{
           $package->instrucctions = 1;
           $package->status_id = 2;
           //$package->confirmed = 'CONFIRMADO';
       }

       $package->save();


       return view('admin.packages.index')->with('success', 'Instrucción asignada correctamente!');

   }

    public function store(Request $request){
      $storeData = $request->validate([
            'ware_house' => 'required|max:20|unique:packages',
            //'tracking' => 'required|max:20',
            'code' => 'required|max:20|exists:users',
            'description' => 'required|max:255',
            'width' => 'required|numeric|min:0.01',
            'high' => 'required|numeric|min:0.01',
            'weight' => 'required',
            'long' => 'required|numeric|min:0.01',
            'lb_volume' => 'required|numeric|min:0.01',
            'pie_cubico' => 'required|numeric|min:0.01',
            //'nro_box' => 'required|integer|min:1',
            'image_package' => 'required|mimes:jpg,bmp,png, jpeg,svg'
        ],
        [
            'ware_house.required' => 'Ware House requerido',
            'ware_house.max' => 'Máximo 20 caracteres',
            'ware_house.unique' => 'Existe el mismo Ware House para otro paquete',
            //'tracking.required' => 'Tracking requerido',
            //'tracking.max' => 'Máximo 20 caracteres',
            'code.required' => 'Código requerido',
            'code.max' => 'Máximo 20 caracteres',
            'code.exists' => 'No existe el codigo',
            'description.required' => 'Descripción requerida',
            'description.max' => 'Máximo 255 caracteres',
            'width.required' => 'Ancho requerido',
            'width.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'high.required' => 'Alto requerido',
            'high.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'long.required' => 'Largo requerido',
            'long.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'weight.required' => 'Peso requerido',
            'width.min' => 'Debe ser mayor a 0',
            /*'nro_box.required' => 'Número de cajas requerido',
            'nro_box.integer' => 'Debe ser numerico',
            'nro_box.min' => 'Debe ser mayor a 0',*/
            'lb_volume.required' => 'Volumen requerido',
            'lb_volume.numeric' => 'Debe ser numerico',
            'pie_cubico.required' => 'Pie cubico requerido',
            'pie_cubico.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'image_package.required' => 'Imagen requerida',
            'image_package.mimes' => 'Formato invalido'

        ]
        );


        if($request->input('tracking1')[0]=='' and  empty($request->input('tracking'))){
                return redirect()->route('packages.create')->withErrors('Tracking requerido')->withInput();;
        }

        $package = new Package();
        $package->ware_house = $request->input('ware_house');
        $package->nro_box = 1;
        $package->description = $request->input('description');
        $package->long = $request->input('long');
        $package->width = $request->input('width');
        $package->high = $request->input('high');
        $package->weight = $request->input('weight');
        $package->lb_volume = $request->input('lb_volume');
        $package->pie_cubico = $request->input('pie_cubico');
        $package->nro_pieces = $request->input('nro_pieces');
        $package->confirmed = 'POR CONFIRMAR';


        $user = User::where('code', $request->input('code'))
                    ->select('id', 'email')->first();
        $package->user_id = $user->id;
        $package->shippins_id = 1;
        $package->status_id = 1;
        $package->save(); //Save package

        if($package->id){
            //save trackings
            if($request->input('tracking1')[0] !=''){
                foreach ($request->input('tracking1') as $tracking) {
                    if(!empty($tracking)){
                        PackageTracking::create([
                            'package_id' => $package->id,
                            'tracking' => $tracking,
                            'status' => 1
                        ]);

                        //Actualizo la prealerta asociada
                        Prealerts::where('tracking', $tracking)->update(['status'=> 1]);
                    }
                }
            }else{
                PackageTracking::create([ 
                        'package_id' => $package->id,
                        'tracking' => $request->input('tracking'),
                        'status' => 1
                    ]);

                $pre = Prealerts::where('tracking', $request->input('tracking'))->update(['status'=> 1]);
            }
        }

        //save image
        if (!empty($request->image_package)){

                $image = $request->file('image_package');
                $name  = time().'.'.$image->getClientOriginalExtension();

                //$destinationPath = public_path('storage/packages');

                $destinationPath = public_path('img');

                $img = Image::make($image->path());
                $img->resize(600, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$name, 72);
                $url= url('img');


            $image = new ImagePackage();
            $image->path = $destinationPath.'/'.$name;
            $image->url =  $url.'/'.$name; 
            $image->name = $name;
            $image->status = 1;
            $image->package_id = $package->id;
            $image->save();
        }
        try {
            Mail::to($user->email)->send(new NotificationPackageRegister($package,$image));
            
        
            return redirect()->route('packages.index')->with('success', 'Registro creado correctamente!');

        } catch (Exception $ex) {
            // Debug via $ex->getMessage();
            return redirect()->route('packages.index')->with('success', 'Registro creado correctamente!');
        }
       
        

        
    }

    public function edit($id){

        $package = Package::with('status')
                        ->with('trackings')
                        ->with('user')
                        ->with('images')
                        ->where('id', $id)
                        ->select('packages.*')
                        ->first();

        if (empty($package)){
            return redirect()->route('packages.index')->withErrors('Paquete no existe');
        }

        return view('admin.packages.edit')->with('package', $package);
    }

    public function update(Request $request, $id){

        $data = $this->validator($request->all())->validate();

        if($request->input('tracking1')[0]==''){
                return redirect()->route('packages.edit')->withErrors('Tracking requerido');
        }

        $package = Package::find($id);
        $package->ware_house = $request->input('ware_house');
        $package->description = $request->input('description');
        $package->long = $request->input('long');
        $package->width = $request->input('width');
        $package->high = $request->input('high');
        $package->weight = $request->input('weight');
        $package->lb_volume = $request->input('lb_volume');
        $package->pie_cubico = $request->input('pie_cubico');
        $package->nro_pieces = $request->input('nro_pieces');
        $package->confirmed = 'POR CONFIRMAR';

        $user = User::where('code', $request->input('code'))
                    ->select('id')->first();
        $package->user_id = $user->id;
        $package->shippins_id = 1;
        $package->status_id = 1;
        $package->save(); //Save package


        if($request->input('tracking1')[0] !=''){
            PackageTracking::where('package_id', $package->id)
                            ->delete();
            $arr = array_unique(array_merge($request->input('tracking1'), $request->input('tracking2')));

            foreach ($arr as $tracking) {
                if(!empty($tracking)){
                    PackageTracking::create([
                        'package_id' => $package->id,
                        'tracking' => $tracking,
                        'status' => 1
                    ]);

                    Prealerts::where('tracking', $tracking)->update(['status'=> 1]);
                }
            }
        }

        //save image
        if (!empty($request->image_package)){
            
            $img = ImagePackage::where('package_id', $package->id)->first();

            if(!empty($img) && $img->path !=""){
                if (\File::exists($img->path)) {
                    //File::delete($image_path);
                    unlink($img->path);
                }

                $img->delete();
            }
           

            $image = $request->file('image_package');
            $name  = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('img');
            $img = Image::make($image->path());
            $img->resize(600, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$name, 72);
            $url= url('img');

            $image = new ImagePackage();
            $image->path =$destinationPath.'/'.$name; 
            $image->url = $url.'/'.$name;
            $image->name = $name;
            $image->status = 1;
            $image->package_id = $package->id;
            $image->save();

        }

        return redirect()->route('packages.index')
            ->withSuccess('Registro actualizado correctamente');
    }


    public function destroy($id){
        $package = Package::with('images')->find($id);

        if($package->confirmed =='POR CONFIRMAR'){
            if(!empty($package->images) && $package->images[0]->path !=''){
                foreach ($package->images as $img) {
 
                    if (\File::exists($img->path)) {
                        unlink($img->path);
                    }
                }
            }

            $package->delete();

            return response()->json([
                'success' => 1,
                'message' => 'Paquete eliminado correctamente'
            ]);
        }
      
    }

    public function confirmed(Request $request){
        $package = Package::find($request->input('id'));

        if($package->instrucctuons == 0){
            return redirect()->route('packages.index')
            ->withErrors('Error al guardar registro');
        }

        if($request->input('confirmed')=='POR CONFIRMAR'){
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 1;
        }else{
            $package->confirmed = 'CONFIRMADO';
            $package->status_id = 2;
        }

        $save  = $package->save();

        if($save)
            return redirect()->route('packages.index')
            ->withSuccess('Registro actualizado correctamente');
        else
            return redirect()->route('packages.index')
            ->withErrors('Error al guardar registro');
        
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'ware_house' => 'required|max:20|unique:packages,id,'.$id,
            //'tracking' => 'required|max:20',
            'code' => 'required|max:20|exists:users',
            'description' => 'required|max:255',
            'width' => 'required|numeric|min:0.01',
            'high' => 'required|numeric|min:0.01',
            'weight' => 'required',
            'long' => 'required|numeric|min:0.01',
            'lb_volume' => 'required|numeric|min:0.01',
            'pie_cubico' => 'required|numeric|min:0.01'
        ],
        [
            'ware_house.required' => 'Ware House requerido',
            'ware_house.max' => 'Máximo 20 caracteres',
            'ware_house.unique' => 'Existe el mismo Ware House para otro paquete',
            //'tracking.required' => 'Tracking requerido',
            //'tracking.max' => 'Máximo 20 caracteres',
            'code.required' => 'Código requerido',
            'code.max' => 'Máximo 20 caracteres',
            'code.exists' => 'No existe el codigo',
            'description.required' => 'Descripción requerida',
            'description.max' => 'Máximo 255 caracteres',
            'width.required' => 'Ancho requerido',
            'width.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'high.required' => 'Alto requerido',
            'high.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'long.required' => 'Largo requerido',
            'long.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'weight.required' => 'Peso requerido',
            'width.min' => 'Debe ser mayor a 0',
            'lb_volume.required' => 'Volumen requerido',
            'lb_volume.numeric' => 'Debe ser numerico',
            'pie_cubico.required' => 'Pie cubico requerido',
            'pie_cubico.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0'
        ]
        );
    }

    public function receipt($id){

        $package = Package::with('user')->where('id', $id)->first();
 
        $companies = [];

        foreach ($package->trackings as $t) {
            $pre_alerts = Prealerts::where('tracking', $t->tracking)->first();

            if(!empty($pre_alerts)){
                $com = Companies::where('id', $pre_alerts->company_id)->first();

                $companies[] = $com->name;
            }

        }

        $data = [
            "package" => $package,
            "user" =>  $package->user,
            "companies" => $companies
        ];

        //pdf de recibo
        $pdf = \PDF::loadView('reports.receipt_package', $data)->stream('archivo.pdf');

        return $pdf;
        
    }

     public function resend($id){

        $package = Package::with('user')->where('id', $id)->first();
 
        $companies = [];

        foreach ($package->trackings as $t) {
            $pre_alerts = Prealerts::where('tracking', $t->tracking)->first();

            if(!empty($pre_alerts)){
                $com = Companies::where('id', $pre_alerts->company_id)->first();

                $companies[] = $com->name;
            }

        }

        $data = [
            "package" => $package,
            "user" => $package->user,
            "companies" => $companies
        ];

        //pdf de recibo
        $pdf = \PDF::loadView('reports.receipt_package', $data);

        try {
            Mail::to($package->user->email)
            ->send(new NotificationPackageResend($package, $pdf->output()));

            return redirect()->route('packages.index')
            ->withSuccess('Recibo enviado por correo correctamente');

        } catch (Exception $e) {
            return redirect()->route('packages.index')
            ->withSuccess('Falló al enviar el recibo por correo');
        }
        
    }

    public function getUser($name){

        if(!\Request::segment(3)){
            return json_encode([
                "success" => 0,
                "message" =>"Debe ingresar el nombre o codigo"
            ]);
        }

        $user = User::where( DB::raw('CONCAT(users.name," ",users.last_name)'), 
                    'like', '%' . $name . '%'
            )
            ->orWhere('code', $name)
            ->first();

        if($user){
            return json_encode([
                "success" => 1,
                "user" => $user,
                "message" =>"Usuario encontrado"
            ]);
        }else{
            return json_encode([
                "success" => 0,
                "message" =>"Usuario no encontrado"
            ]);
        }

    }

    public function findWareHouse($ware_house){
        $package = Package::where('ware_house', $ware_house)->first();

        if(!empty($package)){
        
            return response()->json([
                'success' => 1,
                'message' => 'Existe el mismo Ware House para otro paquete'
            ]);
        }else{
             return response()->json([
                'success' => 0,
                'message' => ''
            ]);
        }

    }
}
