<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Address;
use App\Models\Prealerts;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\RegionApi;
use App\Models\TarifaRegion;

class UserController extends Controller
{

    public function show($id){
         $users =User::leftJoin('address', 'address.users_id', 'users.id')
                ->leftJoin('states', 'states.id', 'address.states_id')
                ->select([DB::raw('distinct(users.id)'), 'users.dni', 'users.username', 'users.name', 'last_name', 'email', 'code', 'phone', 'users.status', 'states.name as state'])
                ->where('users.id', $id)
                ->first();

            $states = States::all();

            return view('admin.users.show')->with('states', $states)->with('user', $users);

    }

   	public function index(Request $request)
    {
        $users =User::leftJoin('address', 'address.users_id', 'users.id')
                ->leftJoin('states', 'states.id', 'address.states_id')
                ->select([DB::raw('distinct(users.id)'), 'dni', 'users.username', 'users.name', 'last_name', 'email', 'code', 'phone', 'users.status', 'states.name as state'])->get();

         if ($request->ajax()) {
            $users = User::leftJoin('address', 'address.users_id', 'users.id')
                ->leftJoin('states', 'states.id', 'address.states_id')
                ->select([DB::raw('distinct(users.id)'), 'dni', 'users.username', 'users.name', 'last_name', 'email', 'code', 'phone', 'users.status', 'states.name as state'])->get();


            return Datatables::of($users)
                    ->addIndexColumn()
                    ->addColumn('codes', function($row){
        				if(empty($row->code)){
                           $btn = '<button class="btn btn-xs btn-success assing-code" data-id="'.$row->id.'"><i class="fa fa-edit"></i> Asignar</button>';
    
                            return $btn;
                        }else{
                        	return $row->code;
                        }

                    })
                    ->addColumn('status', function($row){
                        if($row->status){
                            $class = 'btn-info';
                            $name = 'Activo';
                            $icon = 'fas fa-user-plus';
                        }else{
                            $class = 'btn-danger';
                            $name = 'Inactivo';
                            $icon = 'fas fa-user-times';
                        }

                        $btn = '<button class="btn btn-xs '.$class.' assing-code" data-id="'.$row->id.'"><i class="'.$icon.'"></i>'.$name.'</button>';
    
                        return $btn;
                        
                    })
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.users.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a> <a href="#" class="btn btn-xs btn-primary delete" data-id="'.$row->id.'"><i class="fa fa-trash"></i> Eliminar</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['codes','actions'])
                    ->make(true);   
        } 

        $states = States::all();

        return view('admin.users.users')->with('states', $states)->with('users', $users);
    }


    public function getFilterUsers(Request $request)
    {

        $users = User::leftJoin('address', 'address.users_id', 'users.id')
                ->leftJoin('states', 'states.id', 'address.states_id')
                ->select([DB::raw('distinct(users.id)'), 'dni', 'users.username', 'users.name', 'last_name', 'email', 'code', 'phone', 'users.status', 'states.name as state',DB::raw('DATE_FORMAT(users.created_at, "%d-%m-%Y") as created_date'),]);

        //Filters 
        if (!empty($request->get('usuario'))) {
            $users->where('users.id', '=',  $request->get('usuario')
            );
        }

        if (!empty($request->get('state_id'))) {
            $users->where('address.states_id', '=',  $request->get('state_id')
            );
        }

        if (!empty($request->get('city_id'))) {
            $users->Where('address.cities_id','=', $request->get('city_id'));
        }

        if ($request->get('status_user') != "") {
            $users->where('users.status','=', $request->get('status_user'));
        }

    
        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $users->where(DB::raw("DATE_FORMAT(users.created_at, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(users.created_at, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }

        return Datatables::of($users)
        			->addIndexColumn()
        			->addColumn('code', function($row){
        				if(empty($row->code)){
                           $btn = '<button href="'.route('admin.users.edit', $row->id).'" style="background:#FBC02D; border-color:#FBC02D; color:#24298d; font-size:12px; font-weight:600 !important; letter-spacing: 1px !important;" class="btn btn-xs btn-success assing-code" data-id="'.$row->id.'"> ASIGNAR</button>';
    
                            return $btn;
                        }else{
                        	return $row->code;
                        }

                    })
                    ->addColumn('status', function($row){
                        if($row->status){
                            $class = 'btn-success';
                            $name = 'ACTIVO';
                            $icon = '';
                        }else{
                            $class = 'btn-danger';
                            $name = 'INACTIVO';
                            $icon = '';
                        }


                        $btn = '<button class="btn btn-xs '.$class.' edit-staus" data-id="'.$row->id.'" data-status="'.$row->status.'" style="font-size:12px; font-weight:600 !important; letter-spacing: 1px !important;">'.$name.'</button>';
    
                        return $btn;
                    })
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('users.show', $row->id).'"  class="btn btn-xs btn-primary mr-1"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" style="padding:3px;" title="Ver detalle"></i><a href="'.route('admin.users.edit', $row->id).'"  class="btn btn-xs btn-dark"><i class="fa fa-edit" style="padding:3px;" data-toggle="tooltip" data-placement="top" title="Editar"></i></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['code', 'status', 'actions'])
                    /*->filterColumn('name', function ($query, $keyword) {
					        $keywords = trim($keyword);
					        $query->whereRaw("CONCAT(name, last_name) like ?", ["%{$keywords}%"]);
					     })*/
        ->make(true);

    }

    public function codeUpdate(Request $request){

    	$user = User::find($request->input('user_id'));

 		$validator = \Validator::make($request->all(), [
            'code' => ['required', 'string', 'max:15', 'unique:users,code,'.$user->id,]
        ], 
        ['code.required' => 'Codigo requerido',
    	 'code.max' => 'Maximo 15 caracteres',
         'code.unique' => 'Codigo asignado a otro usuario']);


 		if ($validator->fails()){
	        return redirect()->route('admin.users.index')->withErrors($validator);
	    }

	    $user->code = $request->input('code');
        $user->save();

        //Enviar email con el codigo
        $details = [ 
                    'username' => $user->name,
                    'code' => $user->code,
                    'url' => route('login')
                ];

        $send=false;
        if(Mail::to($user->email)->send(new NotificationCode($details)))
        {
            $send = true;
        }

        return response()->json([
			    'success' => 1,
                'notification' => true,
			    'message' => 'Codigo asignado correctamente'
			]);
    }

    public function edit($id){
        $user = User::find($id);

        if (empty($user)){
            return redirect()->route('admini.users.index')->withErrors('Usuario no existe');
        }

        $country = Countries::where('iso3166a2', 'VEN')->first();

        return view('admin.users.edit')->with('user', $user)->with('country', $country);
    }

    public function update(Request $request, User $user){
    
        $this->validator($request->all())->validate();
        $data = $request->all();

        $user->dni = $data['dni'];
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];


        if(!empty($data['password'])){
            $new_password = Hash::make($data['password']);

            if (!Hash::check($new_password , $user->password)) {
                $user->password = $new_password;
            }
        }

        $user->save();

        //asigno rol cliente
        //$user->assignRole('cliente');

        //Guardo direccion
        if(!empty($data['address'])){
            Address::where('users_id', $user->id)->delete();

            $direcction = new Address();
            $direcction->countries_id = $data['country_id'];
            $direcction->states_id = $data['state_id'];
            $direcction->cities_id = $data['city_id'];
            $direcction->address = $data['address'];
            $direcction->address1 = $data['address1'];
            $direcction->latitude = $data['latitude'];
            $direcction->longitude = $data['longitude'];

            $direcction->users_id = $user->id;
            $direcction->save();
        }

        return redirect()->route('admin.users.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $user = User::find($id);
        $user->status = 0;
        $user->save();

        return response()->json([
                'success' => 1,
                'message' => 'Usuario eliminado correctamente'
        ]);
      
    }

    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;

        return view('admin.users.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;
        
        return view('admin.users.map',compact('long', 'lati'));
    }

    public function findCities($id){
        $cities = Cities::where('state_id', $id)->select('id', 'name')->get();

        return response()->json([
            'cities' =>  $cities
        ]);
    }

    public function countNotifications(){

        $users = User::whereNull('code')->count();
        $prealerts_count = Prealerts::where('status',0)->count();

        return response()->json([
            'success' => 1,
            'users_count' => $users,
            'prealerts_count' => $prealerts_count
        ]);
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'dni' => ['required', 'string', 'max:10'],
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'.$id],
            'phone' => ['required', 'string', 'max:14'],
            //'password' => ['string', 'min:4', 'confirmed'],
            'state_id' => ['required', 'numeric'],
            'city_id' => ['required', 'numeric'],
            'address' => ['required', 'string', 'max:255']
        ], [
            'dni.required' => 'Cedula requerida',
            'dni.max' => 'Maximo 10 caracteres',
            'name.required' => 'Nombre requerido',
            'name.max' => 'Maximo 255 caracteres',
            'last_name.required' => 'Apellido requerido',
            'last_name.max' => 'Maximo 255 caracteres',
            'email.required' => 'Email requerido',
            'email.email' => 'Ingrese un email valido',
            'phone.required' => 'Teléfono requerido',
            'phone.max' => 'Maximo 14 caracteres',
            //'password.min' => 'Minimo 4 caracteres',
            //'password.confirmed' => 'La clave no coincide',
            'address.required' => 'Dirección requerida',
            'address.max' => 'Maximo 255 caracteres',

        ]);
    }

    protected function validator_calculator(array $data)
    {

        return Validator::make($data, [
            'width' => ['required'],
            'height' => ['required'],
            'lenght' => ['required'],
            'weight' => ['required'],
            'trip_id' => ['required', 'numeric'],
            //'password' => ['string', 'min:4', 'confirmed'],
            'destiner_id' => ['required', 'numeric'],
            'reemp' => ['required', 'numeric'],
        ], [
            'width.required' => 'Ancho requerido',
            'height.required' => 'Alto requerido',
            'lenght.required' => 'Largo requerido',
            'height.required' => 'Peso requerido',
            'trip_id.required' => 'Tipo de envío requerido',
            'destiner_id.required' => 'Destino requerido',
            'reemp.required' => 'Opción de reempaque requerido'
        ]);
    }

    public function selectSearch(Request $request)
    {
        $users = [];

        if($request->has('q')){
            $search = $request->q;
            $users =User::join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                ->select("id", "name", "last_name", "code")
                    ->where(DB::raw('CONCAT(users.name," ",users.last_name)'), 'LIKE', "%$search%")
                    ->whereNotNull('code')
                    ->where('role_id', 2)
                    ->get();
        }
        return response()->json($users);
    }


    public function changeStatus(Request $request)
    {
        $user = User::find($request->input('id'));

        if(empty($user->code)){
            return response()->json(["success" => 0, "message" => "El usuario no tiene codigo asignado, no se le puede cambiar el estatus"]);
        }

        $user->status = $request->input('status');
        $user->save();

        return response()->json(["success" => 1, "message" => "Estatus del usuario modificado correctamente"]);
    }


    /**RUTAS DE LA CALCULADOR WEB */
    public function calculator(Request $request){

        if ( $request->input('width') == '' || $request->input('height') == '' || $request->input('lenght') == '' || $request->input('weight') == '' || $request->input('destiner_id') < 1){
            return response()->json(["error"=> 1, "success" =>[], "message" => "Falta datos para calcular el costo del envío"]);
	    }else{

            if ( $request->input('width') == 0 || $request->input('height') == 0 || $request->input('lenght') == 0 || $request->input('weight') == 0 || $request->input('destiner_id') < 1){
                return response()->json(["error"=> 1, "success" =>[], "message" => "Las medidas deben ser mayores a 0"]);
            }else{

            $totail_pie3 = ($request->input('width') * $request->input('height') * $request->input('lenght'))/ 1728; 

            $volumetric_lb = ($request->input('width') * $request->input('height') * $request->input('lenght'))/166;
    
            $peso = $request->input('weight');
    
    
            $trip_id = $request->input('trip_id');
            $destiner = $request->input('destiner_id');
    
            $reempaque = $request->input('reemp');
    
            $cost_destiner = TarifaRegion::select('tarifas_region.id as id','tarifas_region.cost as cost_destiner')
            ->where('tarifas_region.region_id',$destiner)
            ->where('tarifas_region.trip_id',$trip_id)
            ->get();
    
    
            // calculo del costo estimado
    
            if($trip_id == 1){
                // el envio es maritimo
                if($totail_pie3<1){
                    // costo minimo de libra vol
                    $subtotal = $cost_destiner[0]->cost_destiner;
                }else{
                    $subtotal = $totail_pie3*$cost_destiner[0]->cost_destiner;
                }
            }else{
                // el envio es aereo
                if($volumetric_lb > $peso){
                    $subtotal = $volumetric_lb*$cost_destiner[0]->cost_destiner;
                }else{
                    $subtotal = $peso*$cost_destiner[0]->cost_destiner;
                }
            }
    
            if($reempaque == 1){
                //tiene reempaque
    
                $total = $subtotal+15;
            }else{
                $total = $subtotal;
            }
            $resultado = round($total, 2);
            return response()->json(["error"=> 0, "success" =>$resultado, "message" => "Estatus del usuario modificado correctamente"]);  
        }
     }

        
    }
}
