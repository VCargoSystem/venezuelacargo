<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\RateShippings;
use App\Models\Box;
use App\Models\Currency;
use App\Models\PackagesInvoiceAdmin;
use App\Models\Prealerts;
use App\Models\Companies;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use App\Mail\NotificationInvoiceResend;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class ShippingsInvoicesController extends Controller
{
	public function index(Request $request)
	{
		$status = Status::all();

		return view('admin.shippings_invoices.index')->with('status', $status);
	}

	public function getFilterShippings(Request $request)
	{

		



		$reempaques = InvoiceDetails::where('package_id', 0)
						->select('packages')->get();
		$arr = [];


		foreach ($reempaques as $key => $item) {
			$d = explode(",", $item->packages);

			for ($i = 0; $i < count($d) ; $i++) {
				$arr[] = $d[$i];
			}
		}

		$arr = array_unique($arr);

        $packages = InvoiceDetails::select(['invoice_details.id as repackage_id', 'invoice_details.invoices_id', 'invoice_details.high', 'invoice_details.width',
        'invoice_details.long', 'invoice_details.weight', 'packages.pie_cubico','packages.lb_volume as lb_volumen', 'invoice_details.ware_house', 'invoices.nro_contenedor as nro_container',
        'invoice_details.status_id', 'packages.shippins_id', 'packages.id as package_id', 'packages.description', 'shippins.name as shippins_name',DB::raw('DATE_FORMAT(invoice_details.date_in, "%d-%m-%Y") as created_date'),
         DB::raw('DATE_FORMAT(packages.date_delivery, "%d-%m-%Y") as date_delivery'), 'packages.repackage' ,'users.name as name', 'users.last_name as last_name', 'invoice_details.packages', 'status.code as status_siglas', 'status.latitude', 'status.longitude', 'invoice_details.nro_pieces', 'packages.invoice_status as facturado',
		 'users.id as user_id', 'users.code as cliente']) 
        ->join('invoices','invoices.id','invoice_details.invoices_id')
        ->join('packages','packages.user_id','invoices.client_id')
        ->join('status','status.id','invoice_details.status_id')
        ->join('shippins','shippins.id','packages.shippins_id')
		->join('users','users.id','packages.user_id')
        ->where('packages.invoice_status', 1)
        ->where('packages.instrucctions', 1)
        ->where('invoice_details.status_id','>=', 2)
			->groupBy('invoice_details.invoices_id')
			->orderBy('invoice_details.invoices_id','DESC');

          

        if (!empty($request->get('ware_house'))) {
            $packages->where('invoice_details.ware_house', 
                    'like', '%' . $request->get('ware_house') . '%'
           	);
        }

        if (!empty($request->get('tracking'))) {
            $packages->where('tracking', 
                    'like', '%' . $request->get('tracking') . '%'
           	);
        }

		if (!empty($request->get('nro_container'))) {
            $packages->where('invoices.nro_contenedor', $request->get('nro_container')
           	);
        }


        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $packages->where(DB::raw("DATE_FORMAT(created_date, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(created_date, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }

        if (!empty($request->get('usuario'))) {
            $packages->where('client_id', $request->get('usuario'));
        }

        if (!empty($request->get('status'))) {

            $packages->where('invoice_details.status_id', $request->get('status'));
               
        }

		if (!empty($request->get('shippins_id'))) {

	           $packages->where('packages.shippins_id', $request->get('shippins_id')
	           );
		}

	
		return Datatables::of($packages)
					->addIndexColumn()
					->setRowAttr([
						    'style' => function($row){
						        return $row->packages ==0 ? 'background-color: white' : 'background-color: white';
						    }
					])
					->addColumn('id_items', static function ($row) {

						$invoice_id = 0;
			
						//if($row->facturado == 1){
							$invoice_id = $row->invoice_id;
						//}
							return '<input type="checkbox" name="items[]"
							value="'.$row->repackage_id.'" data-user="'.$row->user_id.'" data-type="'.$row->shippins_id.'"  data-facturado="'.$row->facturado.'" invoice_id="'.$invoice_id.'"
								packages="'.$row->packages.'"/>';
						//}
					})->rawColumns(['select_orders'])
					->addColumn('actions', function($row){

						$btn_print='';
						$btn_show ='';
						$btn_send ='';
						
						$invoice_id = $row->invoices_id;
						//if($row->facturado > 0){
							$btn_print='<a href="'.route('admin.printInvoiceShippinngs',$invoice_id).'" class="btn btn-xs btn-warning" data-id="'.$row->repackage_id.'" data-toggle="tooltip" data-placement="top" title="Ver factura" target="_blank"><i class="fa fa-print" style="padding:3px !important;"></i> </a>';

							$btn_send ='<a href="'.route('admin.sendInvoiceShippings', $invoice_id).'" class="btn btn-xs btn-success " data-id="'.$row->repackage_id.'" data-toggle="tooltip" data-placement="top" title="Reenviar factura por correo"><i class="fas fa-envelope-open-text" style="padding:3px !important;"></i> </a>';

							
						//}
						
						

						$btn_show ='<a href="'.route('admin.shippings_invoices.show', $invoice_id).'"  class="btn btn-xs btn-primary view " data-toggle="tooltip" data-placement="top" title="Ver detalle" data-id="'.$row->id.'" status_id="'.$row->status_id.'"
								facturado="'.$row->facturado.'" invoice_id="'.$invoice_id.'"
								packages="'.$row->packages.'"><i class="fa fa-search" style="padding:3px !important;"></i></a>
							<a href=""  class="btn btn-xs btn-dark shippings " data-toggle="tooltip" data-placement="top" title="Ver detalle" latitude="'.$row->latitude.'" longitude="'.$row->longitude.'" status_id="'.$row->status_id.'"><i class="fas fa-shipping-fast" style="padding:3px !important;"></i></a>
							<a href="#" class="btn btn-xs btn-dark change" data-id="'.$row->repackage_id.'" status_id="'.$row->status_id.'"
								facturado="'.$row->facturado.'" invoice_id="'.$invoice_id.'"
								packages="'.$row->packages.'"
								"data-toggle="tooltip" data-placement="top" title="Cambiar estado del paquete"><i class="fas fa-check-square" style="padding:3px !important;"></i> </a>';
						
						
						$btn =  $btn_show." ".$btn_print." ".$btn_send;

						return $btn;
					})
			  
				   
					->rawColumns(['id_items','actions'])
		->make(true);
	}


	public function edit($id){
		$package = Package::with('trackings')
						->where('id', $id)
						->select('*')
						->first();

		if($package->date_delivery != "" && $package->nro_container != ""){
			return response()->json(["success" => 1, 'package' => $package]);
		}else{
			return response()->json(["success" => 0, "message" => "No posee datos de envio, debe agregarlos" ]);
		}
	}

	public function update(Request $request){

		$validator = \Validator::make($request->all(), [
			'nro_container_edit' => 'required|min:1',
			'status_id' => 'required|min:1'
		],
		[
			'nro_container_edit.required' => 'Campo número de container requerido',
			'nro_container_edit.min' => 'Valor minimo requerido (1)',
			'status_id.required' => 'Campo estatus requerido',
			'status_id.min' => 'Valor minimo requerido (1)'
		]
		);
		

		if ($validator->fails()){
			return redirect()->route('clients.shippings_invoices.index')->withErrors($validator);
		}

		$package = Package::where('id', $request->input('id_edit'))->first();

	
		$package->nro_container = $request->input('nro_container_edit');
		$package->status_id = $request->input('status_id');

		$package->save();

		return redirect()->route('admin.shippings_invoices.index')->with('success', 'Envio agregado correctamente!');

	}


	public function destroy($id){
  
	}
   
	public function receipt($id){

		$package = Package::with('user')->where('id', $id)->first();
 
		$companies = [];

        foreach ($package->trackings as $t) {
            $pre_alerts = Prealerts::where('tracking', $t->tracking)->first();

            if(!empty($pre_alerts)){
                $com = Companies::where('id', $pre_alerts->company_id)->first();

                $companies[] = $com->name;
            }

        }

        $data = [
            "package" => $package,
            "user" => auth()->user(),
            "companies" => $companies
        ];

		//pdf de recibo
		$pdf = \PDF::loadView('reports.receipt_package', $data)->stream('archivo.pdf');

		return $pdf;
		
	}

	public function show($id_invoices_details)
    {
		$invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id',$id_invoices_details)->first();

                    $details = $invoice->details;
                    $packages = [];
                    $dollar = Currency::first();
            
            
                    foreach ($details as $d) {
                        //Si es si reempaque
                        if($d->package_id > 0){
                            
                            $images = [];
                            
                            $imagesList = ImagePackage::where('package_id', $d->package_id)->select('url')->first();
                        	$images[] = $imagesList->url;

							$d->images = $images; 
							$packages[] = $d; 
            
                        }else{
            
                            $id_paq = explode(",", $d->packages);
                            $wh = [];
                            $descriptions = [];
                            $images = [];
                    
                            foreach ($id_paq as $p) {
                                
                                
                                $pak = Package::join('images_packages','images_packages.package_id','packages.id')->where('packages.id', $p)->select('packages.*','images_packages.url')->first();
                                $wh[] =  $pak->ware_house;
                                $descriptions [] = $pak->description;
                                $images[] = $pak->url;
                            }
            
                            $d->wh = $wh;
                            $d->descriptions = $descriptions;
                            $d->images = $images;

                            $packages[] = $d;
                        }
            
                    }

                    $user = $invoice->user;
            
                    if(!empty($invoice)){ 
            
                        $data = [
                            "invoice" => $invoice,
                            "user" => $invoice->user,
                            "packages" => $packages,
                            "dollar_value" => $dollar
                        ];
                 }

       

        return view('admin.shippings_invoices.view',compact('invoice','user','packages'));
    }

	public function assing($id){
		$package = Package::where('id', $id)
						->select('packages.*')
						->first();

		if($package->date_delivery != "" && $package->nro_container){
			return response()->json(["success" => 0, "message" => "Ya tiene un envío asignado, por favor seleccione la opcion editar" ]);
		}else{
			return response()->json(["success" => 1, "message" => "Agregar envío", "shippins_id" => $package->shippins_id]);
		}
	}

	public function changeLot(Request $request)
	{
		if(empty($request->input('items'))){
			return response()->json(["success" => 0, "message" => "Debe seleccionar algun item" ]);
		}

		$ids = array_unique($request->input('invoices'));
		
		$details = InvoiceDetails::whereIn('invoices_id', $ids)
			->select('id', 'package_id', 'packages', 'status_id')
			->get();

		//Recorro las facturas para actualizar sus paquetes
		foreach ($details as $d) {

			$d->status_id = $request->input('status_id');
			$d->save();

			//Si es un paquete normal
			if($d->package_id > 0){
				$package = Package::where('id', $d->package_id)->first();

				$package->status_id = $request->input('status_id');
				$package->save();

			}else{
				//si es reeempaque

				$arr = explode(",", $d->packages);

				foreach ($arr as $id) {

					$package = Package::where('id', $id)->first();

					if(!empty($package)){
						$package->status_id = $request->input('status_id');
						$package->save();
					}
				}
			}
		}

	   
		return response()->json(["success" => 1, "message" => "Registros actualizados correctamente","array" => $details ]);
	}

	public function changePackage(Request $request)
	{
		if(empty($request->input('id_paq'))){
			return response()->json(["success" => 0, "message" => "Debe seleccionar algun item" ]);
		}


		$package = Package::where('id', $request->input('id_paq'))->first();

		//Si ya fue facturado y tiene reempaques
		if($request->input('packages') ==0){

			if(!empty($package)){
				$package->status_id = $request->input('status_id');
				$package->save();
			}
		}else{

			//Actualizo invoice_details
			$detail = InvoiceDetails::where('invoices_id', $request->input('invoice_id'))
				->where('package_id', 0)
				->first();


			$detail->status_id = $request->input('status_id');
			$detail->save();

			//Actualizo paquetes
			$arr = explode(",", $request->input('packages'));

			foreach ($arr as $id) {

				$package = Package::where('id', $id)->first();

				if(!empty($package)){
					$package->status_id = $request->input('status_id');
					$package->save();
				}
			}
		}
		
	   
		return response()->json(["success" => 1, "message" => "Registro actualizado correctamente" ]);
	}


	public function position($id)
    {
      	$status = Status::where('id', $id)->first();

      	if(!empty($status)){
      		return response()->json(["longitude" => $status->longitude, "latitude" => $status->latitude]);

      	}else{
      		return response()->json(["longitude" => 0, longitude, "latitude" => 0]);

      	}
    }


	public function  printInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];
		$dollar = Currency::first();


        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){ 

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
				"dollar_value" => $dollar
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data)->stream('invoice.pdf');

            return $pdf;
        }

    }

	 public function  sendInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];
		$dollar = Currency::first();

        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
				"dollar_value" => $dollar
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data);

            try {
                Mail::to($invoice->user->email)
                ->send(new NotificationInvoiceResend($invoice, $pdf->output()));
            
                return redirect()->route('admin.invoices.index')
                ->withSuccess('Factura reenviada por correo correctamente');

            } catch (Exception $ex) {
                // Debug via $ex->getMessage();
                  return redirect()->route('admin.invoices.index')
                ->withErrors('Falló al reenviar la factura por correo');
            }
        }

    }

	public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('admin.shippings_invoices.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;
        
        return view('admin.shippings_invoices.map',compact('long', 'lati'));
    }
}
