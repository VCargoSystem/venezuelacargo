<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\Payments;
use App\Models\Currency;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationInvoiceResend;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

class InvoiceController extends Controller
{
   	public function index(Request $request)
    {
        $status = Status::all();

        return view('admin.invoices.index')->with('status', $status);
    }

    public function getFilterInvoices(Request $request)
    {
  
        $invoices = Invoice::join('users', 'users.id', 'invoices.client_id')
            //->where('invoices.status', '>', 1)
            ->select([
                'invoices.id',
                'client_id', 
                'shippings_id',
                 DB::raw('round(amount, 2) as amount'),
                 DB::raw('round(amount_ves) as amount_ves'),
                'nro_contenedor',
                DB::raw('CASE
                            WHEN invoices.status = 1 THEN "POR PAGAR"
                            WHEN invoices.status = 2 THEN "PENDIENTE"
                            ELSE "PAGADA"
                        END as status_name'),
                DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") as created_date'),
                'users.name',
                'users.last_name',
                DB::raw('concat(users.name," ",users.last_name) as cliente'),
                DB::raw('DATEDIFF(NOW(),   DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as days_delay'), 
                DB::raw('DATEDIFF(NOW(),   DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as expiration_days',
                'invoices.status as status_invoice')
            ]);
  
       
        if (!empty($request->get('usuario'))) {
            $invoices->where( 'invoices.client_id', $request->get('usuario'));
        }

        if (!empty($request->get('id'))) {
            $invoices->where('invoices.id', 
                    '=', $request->get('id')
            );
        }

        if (!empty($request->get('status'))) {
            $invoices->where('invoices.status', 
                    '=', $request->get('status')
            );
        }

        if (!empty($request->get('shippings_id'))) {
            $invoices->where('invoices.shippings_id', 
                    '=', $request->get('shippings_id')
            );
        }


        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $invoices->where('invoices.created_at', 
                    '>=', $request->get('date'))
                ->where('invoices.created_at', 
                    '<=', $request->get('date1'));
        }

        $invoices->get();

        return Datatables::of($invoices)
        			->addIndexColumn()
                    ->addColumn('actions', function($row){
                        if(($row->status_name) == "PAGADA"){
                            $btn = '<a href="'.route('admin.invoices.changeStatus', $row->id).'" class="btn btn-xs btn-dark change mr-1" data-id="'.$row->id.'" target="blank" data-toggle="tooltip" data-placement="top" title="Cambiar estatus"><i class="fa fa-check" style="padding:3px !important;"></i>
                                </a>';

                            $btn_print='<a href="'.route('admin.invoices.printInvoice.paid', $row->id).'" class="btn btn-xs btn-warning mr-1" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Ver factura" target="_blank"><i class="fa fa-print" style="padding:3px !important;"></i></a>';

                            $btn_send ='<a href="'.route('admin.invoices.sendInvoice', $row->id).'" class="btn btn-xs btn-success " data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Reenviar factura por correo"><i class="fas fa-envelope-open-text" style="padding:3px !important;"></i> </a>';

                            $btn_view = '<a href="'.route('admin.invoices.edit', $row->id).'"  class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-search" style="padding:3px;"></i></a>'; 
                        }else if(($row->status_name) == "PENDIENTE"){
                            $btn = '<a href="'.route('admin.invoices.changeStatus', $row->id).'" class="btn btn-xs btn-dark change mr-1" data-id="'.$row->id.'" target="blank" data-toggle="tooltip" data-placement="top" title="Cambiar estatus"><i class="fa fa-check" style="padding:3px !important;"></i>
                                </a>';

                            $btn_print='<a href="'.route('admin.printInvoiceShippinngs', $row->id).'" class="btn btn-xs btn-warning mr-1" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Ver factura" target="_blank"><i class="fa fa-print" style="padding:3px !important;"></i></a>';

                            $btn_send ='<a href="'.route('admin.invoices.sendInvoice', $row->id).'" class="btn btn-xs btn-success " data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Reenviar factura por correo"><i class="fas fa-envelope-open-text" style="padding:3px !important;"></i> </a>';
                            $btn_view = '<a href="'.route('admin.invoices.edit', $row->id).'"  class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-search" style="padding:3px;"></i></a>'; 
                            
                        }else{
                            $btn = '<a href="'.route('admin.invoices.changeStatus', $row->id).'" class="btn btn-xs btn-dark change mr-1" data-id="'.$row->id.'" target="blank" data-toggle="tooltip" data-placement="top" title="Cambiar estatus"><i class="fa fa-check" style="padding:3px !important;"></i>
                                </a>';

                            $btn_print='<a href="'.route('admin.printInvoiceShippinngs', $row->id).'" class="btn btn-xs btn-warning mr-1" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Ver factura" target="_blank"><i class="fa fa-print" style="padding:3px !important;"></i></a>';

                            $btn_send ='<a href="'.route('admin.invoices.sendInvoice', $row->id).'" class="btn btn-xs btn-success " data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Reenviar factura por correo"><i class="fas fa-envelope-open-text" style="padding:3px !important;"></i> </a>';

                            $btn_view = '';
                        }
                        return $btn."".$btn_print."".$btn_send."".$btn_view;
                    }) 
              
                   
                    ->rawColumns(['actions'])
        ->make(true);
    }


    public function edit($id){

        $invoice = Payments::with('user')
                        ->where('invoice_id', $id)
                        ->select('payments.*')
                        ->first();

        /*if (empty($invoice)){
            return redirect()->route('invoices.index')->withErrors('No existe la factura');
        }*/

        return view('admin.invoices.view')->with('invoice', $invoice);
    }

    public function  printInvoicePaid($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id) 
                    ->first();

        $details = $invoice->details;
        $packages = [];
        $dollar = Currency::first();

        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
                "dollar_value" => $dollar
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice_paid', $data)->stream('invoice.pdf');

            return $pdf;
        }

    }

    public function  printInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];
        $dollar = Currency::first();

        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
                "dollar_value" => $dollar 
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoiceSendEmail', $data)->stream('invoice.pdf');
            $this->sendInvoice($invoice->id);

            return $pdf;
        }

    }

    public function  sendInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];
        $dollar = Currency::first();

        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
                "dollar_value" => $dollar 
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data);

            try {
                Mail::to($invoice->user->email)
                ->send(new NotificationInvoiceResend($invoice, $pdf->output()));
            
                return redirect()->route('admin.invoices.index')
                ->withSuccess('Factura reenviada por correo correctamente');

            } catch (Exception $ex) {
                // Debug via $ex->getMessage();
                  return redirect()->route('admin.invoices.index')
                ->withErrors('Falló al reenviar la factura por correo');
            }
        }

    }

    public function  changeStatus(Request $request)
    {

        $invoice = Invoice::where('id', $request->input('id_edit'))->first();

        $invoice->status = $request->input('status');
        $invoice->save();

        return redirect()->route('admin.invoices.index')->with('success', 'Estatus actualizado correctamente!');

    }
  

    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('admin.shippings.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;

        return view('admin.shippings.map',compact('long', 'lati'));
    }

}
