<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RateShippings;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class RateShippingsController extends Controller
{
   	public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = RateShippings::orderBy('regions', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.rate_shippings.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['status','actions'])
                    ->make(true);
        }
       
        return view('admin.rate_shippings.index');
    }


    public function getFilterRateShippings()
    {
            $data = RateShippings::orderBy('id', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.rate_shippings.edit', $row->id).'" class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar tasa"><i class="fa fa-edit" style="padding:3px;"></i> </a>';
    
                            return $btn;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
       
    }

    public function create(){
        return view('admin.rate_shippings.create');
    }

    public function store(Request $request){
        $storeData = $request->validate([
            'cost' => 'required|numeric',
        ],
        [
            'cost.required' => 'Valor de la Tasa requerido',
            'cost.numeric' => 'Debe ser númerico'
        ]
        );

        $currency = RateShippings::create($storeData);

        return redirect()->route('admin.rate_shippings.index')->with('success', 'Registro actualizado correctamente!');
    }

    public function edit($id){

        $rate_shippings = RateShippings::findOrFail($id);

        if (empty($rate_shippings)){
            return redirect()->route('admin.rate_shippings.index')->withErrors('No existe el registro');
        }

        return view('admin.rate_shippings.edit')->with('rate_shippings', $rate_shippings);
    }

    public function update(Request $request, $id){

        $data = $this->validator($request->all())->validate();
        RateShippings::whereId($id)->update($data);

        return redirect()->route('admin.rate_shippings.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $user = Companies::find($id);
        $user->status = 0;
        $user->save();

        return response()->json([
            'success' => 1,
            'message' => 'Empresa eliminado correctamente'
        ]);
      
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'cost' => 'required|numeric'
        ],
        [
            'cost.required' => 'Valor de la tasa requerido',
            'cost.numeric' => 'Debe ser un valor numerico'
        ]
        );
    }

}
