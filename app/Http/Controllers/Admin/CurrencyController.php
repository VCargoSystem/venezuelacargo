<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Currency;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class CurrencyController extends Controller
{
   	public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Currency::orderBy('name', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.currency.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['status','actions'])
                    ->make(true);
        }
       
        return view('admin.currency.index');
    }


    public function getFilterCurrency()
    {
            $data = Currency::orderBy('name', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.currency.edit', $row->id).'" class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar tasa"><i class="fa fa-edit" style="padding:3px;"></i> </a>';
    
                            return $btn;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
       
    }

    public function create(){
        return view('admin.currency.create');
    }

    public function store(Request $request){
        $storeData = $request->validate([
            'value_bs' => 'required|numeric',
        ],
        [
            'name.required' => 'Valor de la Tasa requerido',
            'name.numeric' => 'Debe ser númerico'
        ]
        );

        $currency = Currency::create($storeData);

        return redirect()->route('currency.index')->with('success', 'Registro actualizado correctamente!');
    }

    function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
      
        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }
    
        $resultado = floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );

       return $resultado;
    }
    

    public function edit($id){

        $currency = Currency::findOrFail($id);

        if (empty($currency)){
            return redirect()->route('currency.index')->withErrors('No existe el registro');
        }

        return view('admin.currency.edit')->with('currency', $currency);
    }

    public function update(Request $request, $id){
         
        $numeric =[
            'id'=>$request['id'],
            'value_bs'=>number_format((float)$this->tofloat($request['value_bs']), 2, '.', '')
        ];

        

        $data = $this->validator($numeric)->validate();
        Currency::whereId($id)->update($numeric);

        return redirect()->route('admin.currency.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $user = Companies::find($id); 
        $user->status = 0;
        $user->save();

        return response()->json([
            'success' => 1,
            'message' => 'Empresa eliminado correctamente'
        ]);
      
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'value_bs' => 'required|numeric'
        ],
        [
            'value_bs.required' => 'Valor de la tasa requerido',
            'value_bs.numeric' => 'Debe ser un valor numerico'
        ]
        );
    }

}
