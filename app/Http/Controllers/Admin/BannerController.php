<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Banner;
use DataTables;
Use Session;
Use Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;

class BannerController extends Controller
{
   	public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Banner::orderBy('id', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.banner.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['status','actions'])
                    ->make(true);
        }
       
        return view('admin.banner.index');
    }


    public function getFilterBox()
    {
            $data = Banner::orderBy('id', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.banner.edit', $row->id).'" class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar imagen"><i class="fa fa-edit" style="padding:3px;"></i> </a>
                               <a href="#" class="btn btn-xs btn-danger delete" data-toggle="tooltip" data-placement="top" title="Eliminar imagen" data-id="'.$row->id.'"><i class="fa fa-trash" style="padding:3px;"></i> </a>';
    
                            return $btn;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
       
    }

    public function create(){
        return view('admin.banner.create');
    }

    public function store(Request $request){
        $storeData = $request->validate([
            'description' => 'string|required',
            'image' => 'required|mimes:jpg,bmp,png, jpeg,svg'
        ],
        [
            'description.required' => 'Descripción de la imagen requerida',
            'image.required' => 'Imagen requerida'
        ]
        );


        $banner = new Banner();

        $banner->description = $request->description;
        $banner->status = 1;

        if (!empty($request->file('image'))){

                $image = $request->file('image');
                $name  = $image->getClientOriginalName();

                //$destinationPath = public_path('storage/captures');
                $destinationPath = public_path('img/banner');

                if(!File::isDirectory($destinationPath)){
                    File::makeDirectory($destinationPath, 0777, true, true);
                }

                $img = Image::make($image->path());
                $img->resize(600, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$name, 72);
                $url= url('img/banner');

            $banner->url =  $url.'/'.$name;
            $banner->path =  $destinationPath.'/'.$name;

        }

        $banner->save();

        return redirect()->route('admin.banner.index')->with('success', 'Registro creado correctamente!');
    }

    public function edit($id){

        $banner = Banner::findOrFail($id);

        if (empty($banner)){
            return redirect()->route('admin.banner.index')->withErrors('No existe el registro');
        }

        return view('admin.banner.edit')->with('banner', $banner);
    }

    public function update(Request $request, $id){

        $data = $this->validator($request->all())->validate();

        $banner = Banner::find($id);

        if(!empty($banner)){

            $banner->description = $request->description;

            if (!empty($request->file('image'))){

                //Borro la imagen
                if($banner->path !=""){
                    if (\File::exists($banner->path)) {
                        unlink($banner->path);
                    }
                }

                $image = $request->file('image');
                $name  = $image->getClientOriginalName();

                //$destinationPath = public_path('storage/captures');
                $destinationPath = public_path('img/banner');

                if(!File::isDirectory($destinationPath)){
                    File::makeDirectory($destinationPath, 0777, true, true);
                }

                $img = Image::make($image->path());
                $img->resize(600, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$name, 72);
                $url= url('img/banner');

                $banner->url =  $url.'/'.$name;
                $banner->path =  $destinationPath.'/'.$name;

            }
           
        }

        $banner->save();

        return redirect()->route('admin.banner.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $banner = Banner::find($id);

        if($banner->path !=""){
            if (\File::exists($banner->path)) {
                unlink($banner->path);
            }
        }

        $banner->delete();

        return response()->json([
            'success' => 1,
            'message' => 'Imagen eliminada correctamente'
        ]);
      
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'description' => 'string|required',
            //'image' => 'required|mimes:jpg,bmp,png, jpeg,svg'
        ],
        [
            'description.required' => 'Nombre de la caja requerido',
            //'image.required' => 'Imagen requerida'
        ]
        );
    }

}
