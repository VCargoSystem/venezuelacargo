<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\RateShippings;
use App\Models\Box;
use App\Models\Currency;
use App\Models\PackagesInvoiceAdmin;

use App\Models\Prealerts;
use App\Models\Companies;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use App\Mail\NotificationInvoiceResend;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class ShippingsController extends Controller
{
	public function index(Request $request)
	{
		$status = Status::all();

		return view('admin.shippings.index')->with('status', $status);
	}

	public function getFilterShippings(Request $request)
	{

		$packages = Package::join('users', 'users.id', 'packages.user_id') 
			->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
			->join('shippins', 'shippins.id', 'packages.shippins_id')
			->join('status', 'status.id', 'packages.status_id')
			->where('packages.status_id', '>=', 2)
			->where('packages.confirmed', '=', 'CONFIRMADO')
			->where('packages.instrucctions', 1)
			->where('packages.invoice_status', 0)
			->groupBy('packages.id')
			->select([
						DB::raw('DISTINCT(packages.id) as id'),
		                'ware_house', 
		                'nro_box', 
		                'packages_trackings.tracking', 
		                'shippins.name as shippins',  
		                'shippins.id as shippins_id', 
		                'nro_container', 
		                //'packages.created_at as created_date', 
		                DB::raw('IF( date_delivery,  date_delivery, "No Asignada") as date_delivery'), 
		                'status_id', 
		                'invoice_status', 
		                'status.latitude', 
		                'status.longitude', 
		                'repackage',
		                'status.code as status',
		                'high',
		                'width',
		                'long',
		                'packages.lb_volume',
		                'weight',
		                'pie_cubico',
		            	'nro_pieces',
		            	'packages.invoice_status  AS facturado',
		            	DB::raw('(insured*0.1) as insured'),
		            	DB::raw('concat(users.name," ",users.last_name) as cliente'),
		            	'users.code', 
		            	'description',
		            	'users.id as user_id',
						DB::raw("DATE_FORMAT(packages.created_at, '%d-%m-%Y') as date")]);



        if (!empty($request->get('ware_house'))) {
            $packages->where('ware_house', 
                    'like', '%' . $request->get('ware_house') . '%'
           	);
        }

        if (!empty($request->get('tracking'))) {
            $packages->where('tracking', 
                    'like', '%' . $request->get('tracking') . '%'
           	);
        }

        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $packages->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }


        if (!empty($request->get('usuario'))) {
            $packages->where('user_id', $request->get('usuario'));
        }

        if (!empty($request->get('status'))) {

            $packages->where('status_id', $request->get('status'));
               
        }

		if (!empty($request->get('shippins_id'))) {
			          $packages->where('shippins_id', 
			                   '=', $request->get('shippins_id')
		            );
		        }

		return Datatables::of($packages)
					->addIndexColumn()
				
					->addColumn('id_items', static function ($row) {

						$invoice_id = 0;
			
						if($row->facturado ==1){
							$invoice_id = $row->invoice_id;
						}
							return '<input type="checkbox" name="items[]"
							value="'.$row->id.'" data-user="'.$row->user_id.'" data-type="'.$row->shippins_id.'"  data-facturado="'.$row->facturado.'" 
								/>';
						//}
					})->rawColumns(['select_orders'])
					->addColumn('actions', function($row){

						$btn_print='';
						$btn_show ='';
						$btn_send ='';
						$invoice_id=0;
						
						$btn_show ='<a href="'.route('admin.shippings.show', $row->id).'"  class="btn btn-xs btn-primary view " data-toggle="tooltip" data-placement="top" title="Ver detalle" data-id="'.$row->id.'" status_id="'.$row->status_id.'"
								facturado="'.$row->facturado.'" invoice_id="0"
								packages="'.$row->packages.'"><i class="fa fa-search" style="padding:3px !important;"></i></a>
							<a href=""  class="btn btn-xs btn-dark shippings " data-toggle="tooltip" data-placement="top" title="seguimiento" latitude="'.$row->latitude.'" longitude="'.$row->longitude.'" status_id="'.$row->status_id.'"><i class="fas fa-shipping-fast" style="padding:3px !important;"></i></a>';
						$btn_edit = '<a href="'.route('admin.shippings.edit', $row->id).'"  class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit" style="padding:3px;"></i></a>';
						$btn_delete = '<a href="#" style="background-color:#f44336 !important; border: solid 1px #f44336;" class="btn btn-xs btn-dark delete" data-id="'.$row->id.'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Eliminar" style="padding:3px;"></i> </a>';
						
						
						$btn =  $btn_show." ".$btn_print ." ".$btn_edit ." ".$btn_delete ." "; 

						return $btn;
					})
			  
				   
					->rawColumns(['id_items','actions'])
		->make(true);
	}

	public function create(){
		return view('clients.shippings.create');
	}

	public function store(Request $request){
	}

	public function addShipping(Request $request){

		$validator = \Validator::make($request->all(), [
			'nro_container' => 'required|min:0'
		],
		[
			'nro_container.required' => 'Campo número de container requerido',
			'nro_container.min' => 'Valor minimo requerido (0)'
		]
		);

		if ($validator->fails()){
			return redirect()->route('admin.shippings.index')->withErrors($validator);
		}

		$package = Package::where('id', $request->input('id'))->first();

		$package->date_delivery =  \Carbon\Carbon::parse($request->input('date_delivery'))->format('Y-m-d');
	
		$package->nro_container = $request->input('nro_container');

		$package->save();

		return redirect()->route('admin.shippings.index')->with('success', 'Envio agregado correctamente!');
	 }


   
	public function edit($id){
		$package = Package::with('trackings')
		->join('users', 'users.id', 'packages.user_id')
		->where('packages.id', $id)
		->select('packages.*', 'users.code', 'users.name', 'users.last_name')
		->first();

		$images = ImagePackage::where('package_id', $package->id)->first();

	  
			return view('admin.shippings.edit',compact('package', 'images'));
		
	}

	public function update(Request $request, $id){


        $package = Package::find($id);
        $package->ware_house = $request->input('ware_house');
        $package->description = $request->input('description');
        $package->long = $request->input('long');
        $package->width = $request->input('width');
        $package->high = $request->input('high');
        $package->weight = $request->input('weight');
        $package->lb_volume = $request->input('lb_volume');
        $package->pie_cubico = $request->input('pie_cubico');
        $package->nro_pieces = $request->input('nro_pieces');
        $package->confirmed = 'CONFIRMADO';

        $user = User::where('code', $request->input('code'))
                    ->select('id')->first();
        $package->user_id = $user->id;
        $package->hold = $request->input('hold');;
		$package->insured = $request->input('insured');;
		$package->repackage = $request->input('repackage');;
		$package->shippins_id = $request->input('shippins_id');;
        $package->save(); //Save package



        //save image
        if (!empty($request->image_package)){
            
            $img = ImagePackage::where('package_id', $package->id)->first();

            if(!empty($img) && $img->path !=""){
                if (\File::exists($img->path)) {
                    //File::delete($image_path);
                    unlink($img->path);
                }

                $img->delete();
            }
           

            $image = $request->file('image_package');
            $name  = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('img');
            $img = Image::make($image->path());
            $img->resize(600, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$name, 72);
            $url= url('img');

            $image = new ImagePackage();
            $image->path =$destinationPath.'/'.$name; 
            $image->url = $url.'/'.$name;
            $image->name = $name;
            $image->status = 1;
            $image->package_id = $package->id;
            $image->save();

        }

        return redirect()->route('admin.shippings.index')
            ->withSuccess('Registro actualizado correctamente');

	}


	public function destroy($id){
  
	}
   
	public function receipt($id){

		$package = Package::with('user')->where('id', $id)->first();
 
		$companies = [];

        foreach ($package->trackings as $t) {
            $pre_alerts = Prealerts::where('tracking', $t->tracking)->first();

            if(!empty($pre_alerts)){
                $com = Companies::where('id', $pre_alerts->company_id)->first();

                $companies[] = $com->name;
            }

        }

        $data = [
            "package" => $package,
            "user" => auth()->user(),
            "companies" => $companies
        ];

		//pdf de recibo
		$pdf = \PDF::loadView('reports.receipt_package', $data)->stream('archivo.pdf');

		return $pdf;
		
	}

	public function show($id)
    {
        $package = Package::with('trackings')
        	->join('users', 'users.id', 'packages.user_id')
            ->where('packages.id', $id)
            ->select('packages.*', 'users.code', 'users.name', 'users.last_name')
            ->first();

        $images = ImagePackage::where('package_id', $package->id)->first();
    

        if($package->lb_volumen == 0){
            $package->lb_volumen = (($package->long * $package->width * $package->high)/ 166); 
            $package->lb_volumen = number_format($package->lb_volumen ,2);
        }

        return view('admin.shippings.view',compact('package', 'images'));
    }

	public function assing($id){
		$package = Package::where('id', $id)
						->select('packages.*')
						->first();

		if($package->date_delivery != "" && $package->nro_container){
			return response()->json(["success" => 0, "message" => "Ya tiene un envío asignado, por favor seleccione la opcion editar" ]);
		}else{
			return response()->json(["success" => 1, "message" => "Agregar envío", "shippins_id" => $package->shippins_id]);
		}
	}

	public function changeLot(Request $request)
	{
		if(empty($request->input('items'))){
			return response()->json(["success" => 0, "message" => "Debe seleccionar algun item" ]);
		}

		$ids = array_unique($request->input('items'));
		
		$details = InvoiceDetails::whereIn('id', $ids)
			->select('id', 'package_id', 'packages', 'status_id')
			->get();

		//Recorro las facturas para actualizar sus paquetes
		foreach ($details as $d) {

			$d->status_id = $request->input('status_id');
			$d->save();

			//Si es un paquete normal
			if($d->package_id > 0){
				$package = Package::where('id', $d->package_id)->first();

				$package->status_id = $request->input('status_id');
				$package->save();

			}else{
				//si es reeempaque

				$arr = explode(",", $d->packages);

				foreach ($arr as $id) {

					$package = Package::where('id', $id)->first();

					if(!empty($package)){
						$package->status_id = $request->input('status_id');
						$package->save();
					}
				}
			}
		}

	   
		return response()->json(["success" => 1, "message" => "Registros actualizados correctamente", "success" => $details]);
	}

	public function changePackage(Request $request)
	{
		if(empty($request->input('id_paq'))){
			return response()->json(["success" => 0, "message" => "Debe seleccionar algun item" ]);
		}

		$details = InvoiceDetails::where('invoices_id', $request->input('invoice_id'))
			->select('id', 'package_id', 'packages', 'status_id')
			->get();


		//Recorro las facturas para actualizar sus paquetes
		foreach ($details as $d) {

			$d->status_id = $request->input('status_id');
			$d->save();

			//Si es un paquete normal
			if($d->package_id > 0){
				$package = Package::where('id', $d->package_id)->first();

				$package->status_id = $request->input('status_id');
				$package->save();

			}else{
				//si es reeempaque
				$d->status_id = $request->input('status_id');
				$d->save();
				$arr = explode(",", $d->packages);

				foreach ($arr as $id) {

					$package = Package::where('id', $id)->first();
					
					if(!empty($package)){
						$package->status_id = $request->input('status_id');
						$package->save();
						
				}
			}
			
		}
		
	}
	   
	return response()->json(["success" => 1, "message" => "Estatus actualizado correctamente"]);
			
	}

	public function invoice(Request $request)
	{
		$type;
		$ware_house ="";

		if(empty($request->input('items_invoice'))){
			return redirect()->route('admin.shippings.index')->withErrors("Debe seleccionar algun paquete");
		}

		$items = explode(",", $request->input('items_invoice'));

		$pack = Package::where('id', $items[0])->first();

		$user = User::with('address')->where('id',  $pack->user_id)->first();

		$packages = Package::whereIn('id', $items)
				->where('repackage', 0)
				->get();
		$count_p =  Package::whereIn('id', $items)
				->where('repackage', 0)
				->count();

		$repackages = Package::whereIn('id', $items)
				->where('repackage', 1)
				->get();

		$count_rp = Package::whereIn('id', $items)
				->where('repackage', 1)
				->count();

		$total_packages = Package::whereIn('id', $items)
				->get();

		$invoice='';

		if($count_p > 0){
			$type = $packages[0]->shippins_id;
			$invoice = $this->invoiceCalculate($packages, $user);

		}

		if($count_rp > 0){
			$type = (int) $repackages[0]->shippins_id;
			$ware_house = $repackages[0]->ware_house; 
		}

		$date_aprox = $this->setDateDelivery($type);

		$box = Box::all();
		$dollar = Currency::first();


		return  view('admin.shippings.invoice')->with('packages',  $packages)
					->with('user', $user)->with('invoice', $invoice)->with('repackages', $repackages)->with('type', $type)->with('ware_house', $ware_house)->with('count_rp', $count_rp)->with('count_p', $count_p)
					->with('date_aprox', $date_aprox)
					->with('box', $box)
					->with('dollar', $dollar->value_bs);
	}

	public function invoiceCalculate($packages, $user)
	{
		$type = $packages[0]->shippins_id;  //1-Mar  2-Air
		$invoice = new \stdClass();
		$total_pie3 = 0;
		$subtotal = 0;
		$volumetric_lb = 0; //libras volumetricas
		$subtotal_lb = 0;
		$subtotal_w = 0;
		$items_invoice = [];
		$total_pieces = 0;

		$cost_repackage = 0;
		$regions = 0;
		$rate = 1;
		$subtotal_secure=0;

		//Cost shippins for cities/regions
		$city = States::where('id', $user->address[0]->states_id)->first();

		if(!$city){
			$rate = RateShippings::where('regions', 'Otros estados')
					->where('trip_id', $type)
					->select('cost')->first()->cost;
		}else{

			$rate = RateShippings::where('regions', 
                    'like', '%' . $city->name . '%'
            )->where('trip_id', $type)
            ->first();

           if(empty($rate)){
           		$rate = RateShippings::where('regions', 'Otros estados')
					->where('trip_id', $type)
					->select('cost')->first()->cost;
           }else{
           		$rate = $rate->cost;
           }

		}


		foreach ($packages as $item) {
			//$totail_pie3 = 0;
			//$volumetric_lb = 0;

			//Calculates maritime
			if( $type == 1){
				//pie cubico
			$totail_pie3 = $item->pie_cubico;
			if($item->pie_cubico < 1)
			$totail_pie3 = 1;

			$totail_pie3 = number_format($item->pie_cubico, 2);

			//volumen
			$volumetric_lb = $item->lb_volume;

			if($volumetric_lb < 1)
				$volumetric_lb = 1;

			$volumetric_lb = number_format($volumetric_lb, 2);

			//if package have secured
			if($item->insured > 0){
				$subtotal_secure += ($item->insured * 0.10); 
			}

			$subtotal += $totail_pie3; 
			$subtotal_w += $item->weight;
			$total_pie3 += $totail_pie3;

			$item->pie = $totail_pie3;
			$total_pieces = ($total_pieces + $item->nro_pieces);
			$subtotal_lb += $volumetric_lb;

				

				

			
			}else{
				//libra volumetrica aereo
				$volumetric_lb = $item->lb_volume;

				if($volumetric_lb < 1)
					$volumetric_lb = 1;

				$volumetric_lb = number_format($volumetric_lb, 2);

				$totail_pie3 = $item->pie_cubico;

				if($totail_pie3 < 1)
					$totail_pie3 = 1;

				$totail_pie3 = number_format($totail_pie3, 2);

				if($item->insured > 0){
					$subtotal_secure += ($item->insured * 0.10);

				}

				$subtotal_lb += $volumetric_lb;
				//peso
				$subtotal_w += $item->weight;
				$total_pieces = ($total_pieces + $item->nro_pieces);
				$total_pie3 = ($total_pie3 + $totail_pie3);
				$item->pie = $totail_pie3;
			}


			$item->volumetric_lb = $volumetric_lb;
			$item->subtotal_lb = $subtotal_lb;
			$item->subtotal_w = $subtotal_w;
			$item->subtotal_pie = $total_pie3;
			$item->cost_secure = ($item->insured * 0.10);
			$item->subtotal_secure = $subtotal_secure;

			$items_invoice[] = $item;

		}//fin for

		if($subtotal_lb < 3.33){
			$subtotal_lb = 3.33;	
		}else{
			$subtotal_lb = $subtotal_lb;
		}

		if($type == 2) //aereo
		{ 
			$may = 0;
			if($subtotal_lb > $subtotal_w){
				$subtotal +=  $subtotal_lb ;

				$may = $subtotal_lb;
			}else{
				$subtotal += $subtotal_w;

				$may = $subtotal_w;
			}

		}
		$subtotal = number_format($subtotal, 2);

		$invoice->subtotal =  ($subtotal * $rate);
		$invoice->subtotal_lb =  $subtotal_lb;
		$invoice->subtotal_w = $subtotal_w;
		$invoice->total_pieces = $total_pieces;
		$invoice->type = $type;
		$invoice->items_invoice = $items_invoice;
		$invoice->total_pie3 = $total_pie3;
		$invoice->cost_repackage = $cost_repackage;
		$invoice->rate = $rate;
		$invoice->subtotal_secure = $subtotal_secure;


		return $invoice;
	}

	public function repackage(Request $request)
	{

		$type = $request->type;  //1-Mar  2-Air
		$invoice_repackage = new \stdClass();
		$total_pie3 = 0;
		$subtotal = 0;
		$volumetric_lb = 0; //libras volumetricas
		$subtotal_lb = 0;
		$subtotal_w = 0;
		$insured = 0;
		$vol_pie = 0;
		$total_pieces = 0;
		$rate = 1;
		$subtotal_secure = 0;

		//Cost shippins for cities/regions
		$city = $request->city;


		if(!$city){
			$rate = RateShippings::where('regions', 'Otros estados')
				->where('trip_id', $type)
				->select('cost')->first()->cost;
		}else{
			$rate = RateShippings::where('regions', 
                    'like', '%' . $city. '%'
            )->where('trip_id', $type)
            ->first();

           if(empty($rate)){
           		$rate = RateShippings::where('regions', 'Otros estados')
					->where('trip_id', $type)
					->select('cost')->first()->cost;
           }else{
           		$rate = $rate->cost;
           }
		}


		//recorro los paquetes para sumar el seguro
		foreach ($request->items as $id) {
			$paq = Package::find($id);

			$total_pieces += $paq->nro_pieces;

			if($paq->insured > 0){
				$subtotal_secure += ($paq->insured * 0.10);
			}
		}


		$cost_repackage = 0;

		//Calculates maritime
		if( $type == 1){
				//pie cubico
			$totail_pie3 = ($request->long * $request->width * $request->high)/ 1728; 

			$totail_pie3 = number_format($totail_pie3, 2);

			if($totail_pie3 < 1)
				$totail_pie3 = 1;

			$volumetric_lb = ($request->long * $request->width * $request->high)/166; 

			if($volumetric_lb < 1)
				$volumetric_lb = 1;

			$subtotal_lb += $volumetric_lb;
			$subtotal_lb = number_format($subtotal_lb, 2);

	
			$subtotal +=  $totail_pie3; 

			$vol_pie = $totail_pie3;

			$total_pie3 = ($total_pie3 + $totail_pie3);
			
			//peso
			$subtotal_w += $request->weight;

		}else{
			//libra volumetrica aereo
			$totail_pie3 = ($request->long * $request->width * $request->high)/ 1728; 

			if($totail_pie3 < 1)
				$totail_pie3 = 1;


			$totail_pie3 = number_format($totail_pie3, 2);
			
			$volumetric_lb = ($request->long * $request->width * $request->high)/166; 

			if($volumetric_lb < 1)
				$volumetric_lb = 1;


			$subtotal_lb += $volumetric_lb;
			$subtotal_lb = number_format($subtotal_lb, 2);

			$total_pie3 = ($total_pie3 + $totail_pie3);

			//peso
			$subtotal_w += $request->weight;
		}

		if($subtotal_lb < 3.33){
			$subtotal_lb = 3.33;	
		}else{
			$subtotal_lb = $subtotal_lb;
		}


		if($type == 2) //aereo
		{ 
			if($subtotal_lb > $subtotal_w){
				
				$subtotal +=  $subtotal_lb;

			}else{
			
				$subtotal +=  $subtotal_w;
			}

			$vol_pie = $subtotal_lb;

		}

		//Costo de reempaque
		if(count($request->items) < 15){
			$cost_repackage = 15;
		}else{
			$cost_repackage = count($request->items);
		}


		$subtotal = ($subtotal * $rate);
		$subtotal = number_format($subtotal, 2);

		$pack = implode(",", $request->items);

		$invoice_repackage->subtotal =  $subtotal;
		$invoice_repackage->type = $type;
		$invoice_repackage->items = $request->items;
		$invoice_repackage->high = $request->high;
		$invoice_repackage->long = $request->long;
		$invoice_repackage->weight = $request->weight;
		$invoice_repackage->width = $request->width;
		$invoice_repackage->insured = $subtotal_secure;
		$invoice_repackage->ware_house = $request->ware_house;
		$invoice_repackage->volume = number_format($vol_pie, 2);
		$invoice_repackage->packages = $pack;
		$invoice_repackage->subtotal_lb =  $subtotal_lb;
		$invoice_repackage->subtotal_w = $subtotal_w;
		$invoice_repackage->total_pieces = $request->nro_pieces;
		$invoice_repackage->total_pie3 = $total_pie3;
		$invoice_repackage->rate = $rate;
		$invoice_repackage->cost_repackage =  $cost_repackage;
		$invoice_repackage->subtotal_secure =  $subtotal_secure;

		return response()->json(["success" => 1, "data" => $invoice_repackage]);

	}

	public function position($id)
    {
      	$status = Status::where('id', $id)->first();

      	if($status->id){
      		return response()->json(["success" => 1, "longitude" => $status->longitude, "latitude" => $status->latitude]);

      	}else{
      		return response()->json(["success" => 0, "longitude" => 0, longitude, "latitude" => 0]);

      	}
    }


    public function storeInvoice(Request $request)
	{
		$subtotal_lb  = 0;
		$subtotal_w   = 0;
		$total_pieces = 0;
		$total_pie3   = 0;
		$arr = [];
		$paq = 0;
		$repaq = 0;
		$dollar = Currency::first()->value_bs;


		DB::beginTransaction();

		try {

	 		$invoice = new Invoice(); 
			$invoice->id = $request->input('factura_id');
			$invoice->client_id = $request->input('client_id');
			$invoice->shippings_id = $request->input('shippings');
			$invoice->count_box = $request->input('box_count');
			$invoice->cost_box = $request->input('cost_box');
			$invoice->amount_box = $request->input('total_box');
			$invoice->cost_repackage = $request->input('total_repackage');
			$invoice->additional = $request->input('total_additional');
			$invoice->amount = $request->input('amount');
			$invoice->amount_ves = ($request->input('amount') *$dollar)/100;
			$invoice->nro_contenedor = $request->input('nro_container');
			$invoice->cost_insured = $request->input('subtotal_secure');

			$invoice->status = 1;

			

			$invoice->save();

			//Hay items sin reempaque
			if($request->input('items_packages')){

				

				foreach ($request->input('items_packages') as $item) {
					$details = new InvoiceDetails();
					$paq = 1;

					$item = (object)$item;
					$details->invoices_id = $request->input('factura_id');
					$details->package_id = $item->id;
					$details->high = $item->high;
					$details->weight = $item->weight;
					$details->long = $item->long;
					$details->width = $item->width;
					$details->volume = $item->lb_volume;
					$details->nro_box = $item->nro_box;
					$details->nro_pieces = $item->nro_pieces;
					$details->description = $item->description;
					$details->insured = $item->insured;
					$details->status = $invoice->status;
					$details->total_pie3 = ($details->total_pie3+$item->pie);
					$details->subtotal_w = $item->subtotal_w;
					$details->subtotal_lb = $item->subtotal_lb;
					$details->ware_house = $item->ware_house;
					$details->status_id = 2;
					$details->nro_container = $request->input('nro_container');
					$details->date_in = $request->input('date_delivery');


					$details->save();

					$total_pieces +=  $item->nro_pieces; 

					Package::where('id', $item->id)->update(['invoice_status'=> 1, 'status_id'=> 2]);
				}
			}


 			$arr =  json_decode($request->input('items_repackages'));

 			if($request->input('items_repackages')){

				$details = new InvoiceDetails();
				$repaq = 1;

				$total_pieces += $arr->total_pieces;

				$container = '';
				$date_in = '';
				$item_p = explode(",", $arr->packages);

				foreach ($item_p as $id) {
					$pa = Package::where('id', $id)->first();
				          
					$container = $request->input('nro_container');
					$date_in = $pa->date_delivery;

					Package::where('id', $id)->update([
						'invoice_status'=> 1, 
						'status_id'=> 4,
						'nro_container' => $request->input('nro_container'),
						'date_delivery' => $request->input('date_delivery')

					]);
				}

				InvoiceDetails::create([
					"invoices_id" => $request->input('factura_id'),
					"package_id" => 0,
					"high" => $arr->high,
					"weight" => $arr->weight,
					"long" => $arr->long,
					"width" => $arr->width,
					"description" => "Reempaque",
					"insured" => $arr->insured,
					"status" => $invoice->status,
					"packages" => $arr->packages,
					"total_pie3" => $arr->total_pie3,
					"subtotal_w" => $arr->subtotal_w,
					"subtotal_lb" => $arr->subtotal_lb,
					"nro_pieces" => $arr->total_pieces,
					"ware_house" => $arr->ware_house,
					"nro_cointaner" => $request->input('nro_cointaner'),
					"date_in" => $request->input('date_delivery'),
					"status_id" => 4
				]);


			}
			

			$inv = $request->input('invoice');
			$item = (object) $inv;

			//Actualizo los totales
			if($paq == 1 && $repaq  == 1){
				$invoice->total_volume = $item->subtotal_lb + $arr->subtotal_lb;
				$invoice->total_pie = $item->total_pie3 + $arr->total_pie3;
				$invoice->total_w = $item->subtotal_w + $arr->subtotal_w;
				$invoice->subtotal_package = $item->subtotal;
				$invoice->subtotal_repackage = $arr->subtotal;
				$invoice->total_pieces = $total_pieces;

				$invoice->save();

			}else if($paq == 1 && $repaq  == 0){
				$invoice->total_volume = $item->subtotal_lb;
				$invoice->total_pie = $item->total_pie3;
				$invoice->total_w = $item->subtotal_w;
				$invoice->subtotal_package = $item->subtotal;
				$invoice->total_pieces = $total_pieces;

				$invoice->save();

			}else if($paq == 0 && $repaq  == 1){
				$invoice->total_volume = $arr->subtotal_lb;
				$invoice->total_pie = $arr->total_pie3;
				$invoice->total_w = $arr->subtotal_w;
				$invoice->subtotal_repackage = $arr->subtotal;
				$invoice->total_pieces = $total_pieces;

				$invoice->save();
			}

		    DB::commit();

		    return response()->json(["success" => 1, "message" => "Factura creada correctamente", "id" => $invoice->id]);

		} catch (\Exception $e) {
		    DB::rollback();
		    throw $e;
		}

	}
	
	public function  printInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];
		$dollar = Currency::first();


        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d; 

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
				"dollar_value" => $dollar
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data)->stream('invoice.pdf');
			

            return $pdf;
        }

    }

	 public function  sendInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];
		$dollar = Currency::first();

        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
				"dollar_value" => $dollar 
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data);

            try {
                Mail::to($invoice->user->email)
                ->send(new NotificationInvoiceResend($invoice, $pdf->output()));
            
                return redirect()->route('admin.invoices.index')
                ->withSuccess('Factura reenviada por correo correctamente');

            } catch (Exception $ex) {
                // Debug via $ex->getMessage();
                  return redirect()->route('admin.invoices.index')
                ->withErrors('Falló al reenviar la factura por correo');
            }
        }

    }

	public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('admin.shippings.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;
        
        return view('admin.shippings.map',compact('long', 'lati'));
    }

    public function setDateDelivery($type){
		$today = \Carbon\Carbon::now()->format('d-m-Y');
		$date = \Carbon\Carbon::now();
		$day= \Carbon\Carbon::now()->dayOfWeek;

		//Si es aereo
		if($type == 2){
			//Dia habil
			if($day >= 0 && $day <= 2){
				$date->addDays(3);

			}else if($day > 2 && $day < 6){
				//dia es mie-vie
				$date->addDays(5);
			}

		}else{


			if($day == 0){
				$date->addDays(19);
			}elseif($day >= 1 && $day <= 5){
				$date->addDays(21);
			}else{
				$date->addDays(20);
			}
		}

		return $date->format('Y-m-d');
	}



}
