<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Address;
use App\Models\Companies;
use App\Models\Prealerts;

use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;



class CompanieController extends Controller
{
   	public function index(Request $request)
    {
         if ($request->ajax()) {
            $data = User::orderBy('name', 'asc')
                    ->select('id', 'name', 'code','email', 'description','status', 'status as status_id')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                       if($row->status == 1){
                            $class = 'badge badge-primary';
                            $value = 'Activo';
                        }else{
                            $class = 'badge badge-danger';
                            $value = 'Inactivo';
                        }
                        return '<span class="'.$class.'">'.$value.'</span>';
                    })
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('users.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a> <a href="#" class="btn btn-xs btn-primary delete" data-id="'.$row->id.'"><i class="fa fa-trash"></i> Eliminar</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['status','actions'])
                    ->make(true);
        }
       
        return view('admin.companies.index');
    }


    public function getFilterCompanies(Request $request)
    {
        $companies = Companies::select(['id', 'name',  'code', 'description', 'status']);

        if (!empty($request->get('name'))) {
                $companies->where('companies.name', 
                    'like', '%' . $request->get('name') . '%'
                    );
        }

        if (!empty($request->get('description'))) {
                $companies->where('companies.description', 
                    'like', '%' . $request->get('description') . '%'
                    );
        }

        if (!empty($request->get('status'))) {

            if($request->get('status') == 'ACTIVO'){
                $companies->where('status', '=',1);
            } else if($request->get('status') == 'INACTIVO'){
                $companies->where('status', '=',0);
            }

        }

        return Datatables::of($companies)
        			->addIndexColumn()
                    ->addColumn('status', function($row){
                        if($row->status == 1){
                            $class = 'badge badge-success';
                            $value = 'ACTIVO';
                        }else{
                            $class = 'badge badge-danger';
                            $value = 'INACTIVO';
                        }
                        return '<span class="'.$class.'" style="font-size:12px; font-weight:600 !important; letter-spacing: 1px !important; padding: .125rem .25rem; line-height: 1.5; border-radius: 3px;">'.$value.'</span>';
                    })
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('companies.edit', $row->id).'"  class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit" style="padding:3px;"></i> </a> 
                                <a href="#" class="btn btn-xs btn-dark delete" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash" style="padding:3px;"></i> </a>';
    
                            return $btn;
                    })
                    ->rawColumns(['status', 'actions'])
        ->make(true);
    }

    public function create(){
        return view('admin.companies.create');
    }

    public function store(Request $request){
      $storeData = $request->validate([
            'name' => 'required|max:255'
            ],
            [
                'name.required' => 'Nombre requerido',
                'name.max' => 'Máximo 255 caracteres'
            ]
        );

        $last = Companies::orderBy('id', 'desc')->first();

        $storeData['code'] = $last->id+1;
        $storeData['description'] = $request->description;
        $storeData['status'] = 1;

        $companie = Companies::create($storeData);

        return redirect()->route('companies.index')->with('success', 'Registro creado correctamente!');
    }

    public function edit($id){

        $companie = Companies::findOrFail($id);

        if (empty($companie)){
            return redirect()->route('companies.index')->withErrors('Empresa no existe');
        }

        return view('admin.companies.edit')->with('companie', $companie);
    }

    public function update(Request $request, $id){

        $data = $this->validator($request->all())->validate();
        Companies::whereId($id)->update($data);

        return redirect()->route('companies.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $comp = Companies::find($id);

        if (Prealerts::where('company_id', '=', $comp->id)->exists()) {
            $comp->status = 0;
            $comp->save();
        }else{
            Companies::where('id', $id)->delete();
        }

        return response()->json([
            'success' => 1,
            'message' => 'Empresa eliminado correctamente'
        ]);
      
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'name' => 'required|max:255',
            'code' => 'required',
            'description' => 'required|max:255',
            'status' => 'required',
        ],
        [
            'name.required' => 'Nombre requerido',
            'name.max' => 'Máximo 255 caracteres',
            'code.required' => 'Código requerido',
            'description.required' => 'Descripción requerida',
            'description.max' => 'Máximo 255 caracteres',
            'status' => 'Estatus requerido'
        ]
        );
    }

}
