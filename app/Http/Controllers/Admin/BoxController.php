<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Box;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class BoxController extends Controller
{
   	public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Box::orderBy('name', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.box.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['status','actions'])
                    ->make(true);
        }
       
        return view('admin.box.index');
    }


    public function getFilterBox()
    {
            $data = Box::orderBy('name', 'asc')
                    ->select('*')->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('admin.box.edit', $row->id).'" class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar tasa"><i class="fa fa-edit" style="padding:3px;"></i> </a>';
    
                            return $btn;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
       
    }

    public function create(){
        return view('admin.box.create');
    }

    public function store(Request $request){
        $storeData = $request->validate([
            'name' => 'string|required',
            'cost' => 'required|numeric',
        ],
        [
            'name.required' => 'Nombre de la caja requerido',
            'cost.required' => 'Costo de la caja requerido',
            'cost.numeric' => 'Debe ser númerico'
        ]
        );

        $currency = Box::create($storeData);

        return redirect()->route('admin.box.index')->with('success', 'Registro creado correctamente!');
    }

    public function edit($id){

        $box = Box::findOrFail($id);

        if (empty($box)){
            return redirect()->route('admin.box.index')->withErrors('No existe el registro');
        }

        return view('admin.box.edit')->with('box', $box);
    }

    public function update(Request $request, $id){

        $data = $this->validator($request->all())->validate();
        Box::whereId($id)->update($data);

        return redirect()->route('admin.box.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $user = Companies::find($id);
        $user->status = 0;
        $user->save();

        return response()->json([
            'success' => 1,
            'message' => 'Empresa eliminado correctamente'
        ]);
      
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'name' => 'string|required',
            'cost' => 'required|numeric',
        ],
        [
            'name.required' => 'Nombre de la caja requerido',
            'cost.required' => 'Costo de la caja requerido',
            'cost.numeric' => 'Debe ser númerico'
        ]
        );
    }

}
