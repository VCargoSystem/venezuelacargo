<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Prealerts;
use App\Models\Companies;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Mail\NotificationPackageRegister;
use Intervention\Image\ImageManagerStatic as Image;
use Barryvdh\DomPDF\Facade as PDF;

class PrealertsController extends Controller
{
    public function show(){

    }

   	public function index(Request $request)
    {
         if ($request->ajax()) {
            $data = Prealerts::with('user')
                        ->with('company')
                        ->where('status','=',0)
                        ->orderBy('created_at', 'desc')
                    ->select('pre_alerts.*')->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('username', function (Prealerts $prealerts) {
                        return $prealerts->user->map(function($user) {
                                return $user->username;
                            });
                    })
                   ->addColumn('company', function (Prealerts $prealerts) {
                        return $prealerts->company->map(function($company) {
                                return $company->name;
                            });
                    })
                    ->addColumn('status', function($row){
                        if($row->status){
                            $class = 'badge badge-primary';
                            $vale = 'Confirmada';
                        }else{
                            $class = 'badge badge-danger';
                            $vale = 'Pendiente';
                        }
                        return '<span class="'.$class.'">'.$value.'</span>';
                    })
                    ->addColumn('actions', function($row){
                       
                           $btn = '<a href="'.route('prealerts.edit', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> </a> <a href="#" class="btn btn-xs btn-primary delete" data-id="'.$row->id.'"><i class="fa fa-trash"></i> </a>';
    
                            return $btn;
                    })
                    ->rawColumns(['username', 'company', 'status','actions'])
                    ->make(true);
        }
       
        return view('admin.prealerts.index');
    }


    public function getFilterPrealerts(Request $request)
    {
        $prealerts =Prealerts::join('users','users.id', 'pre_alerts.user_id')
                            ->join('companies', 'companies.id', 'pre_alerts.company_id')
                            ->where('pre_alerts.status','=',0)
                            ->orderBy('pre_alerts.created_at', 'desc')
                        ->select(['pre_alerts.id','pre_alerts.code','pre_alerts.tracking',DB::raw("DATE_FORMAT(pre_alerts.date_in, '%d-%m-%Y') as date_in"),'pre_alerts.status','pre_alerts.user_id','pre_alerts.company_id',DB::raw("DATE_FORMAT(pre_alerts.created_at, '%d-%m-%Y') as created_at"), DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code as code_user', 'users.username','companies.name']);

        //Filters 
        if (!empty($request->get('usuario'))) {
            $prealerts->where('users.id', '=',  $request->get('usuario')
            );
        }
                        
        if (!empty($request->get('code'))) {
            $prealerts->where('users.code', 
                    'like', '%' . $request->get('code') . '%'
                    );
        }

        if (!empty($request->get('tracking'))) {
                $prealerts->where('tracking', 
                    'like', '%' . $request->get('tracking') . '%'
                    );
        }

        if (!empty($request->get('username'))) {
            $prealerts->where('users.username', 
                    'like', '%' . $request->get('code') . '%'
            );
        }

        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $prealerts->where(DB::raw("DATE_FORMAT(pre_alerts.date_in, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(pre_alerts.date_in, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }

        return Datatables::of($prealerts)
        			->addIndexColumn()
                    ->addColumn('username', function (Prealerts $prealerts) {
                        return $prealerts->user->username; 
                    })
                   ->addColumn('company', function (Prealerts $prealerts) {
                        return $prealerts->company->name;
                    })
                    ->addColumn('status', function($row){
                        if($row->status){
                            $class = 'badge badge-primary';
                            $value = 'Confirmada';
                        }else{
                            $class = 'badge badge-danger';
                            $value = 'Pendiente';
                        }
                        return '<span class="'.$class.'">'.$value.'</span>';
                    })
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('prealerts.addPackage', $row->id).'"  class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Crear Paquete - '.$row->tracking.'"><i class="fa fa-plus" style="padding:3px;"></i> 
                               </a> <a href="#" class="btn btn-xs btn-dark delete data-placement="top" title="Eliminar Prealerta" data-id="'.$row->id.'"><i class="fa fa-trash" style="padding:3px;"></i></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['username', 'company','status', 'actions'])
        ->make(true);
    }

    public function getCompaniesQuery(Request $request){
        $data = [];  

        if($request->has('q')){
            $search = $request->q;
            $data =Companies::select("id","name")
                    ->where('name','LIKE',"%$search%")
                    ->get();
        }

        return response()->json($data);
    }

    public function getCompanies(){
        $companies = Companies::where('status', 1)->get();
        $data = [];

        foreach ($companies as $key => $com) {
            $data[] = ['id'=>$com->id, 'text'=>$com->name];
        }
        return response()->json([
            $data
        ]);

    }

    public function create(){
        $companies = Companies::where('status',1)->get();
        $users = User::join('model_has_roles', 'model_id', 'users.id')
                    ->join('roles', 'roles.id', 'model_has_roles.role_id')
            ->where('model_type','App\Models\User')
            ->where('roles.name', 'cliente')
            ->where('status', 1)
            ->select('users.*')->get();

        return view('admin.prealerts.create')->with('companies', $companies)->with('users', $users);
    }

    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'tracking' => 'required|max:30',
            'date_in' => 'required|date',
            'company_id' => 'required|numeric',
        ],
        [
            'tracking.required' => 'Tracking requerido',
            'tracking.max' => 'Máximo 30 caracteres',
            'date_in.required' => 'Fecha requerida',
            'date_in.date' => 'Ingrese una fecha',
            'company_id.required' => 'Empresa requerida',
            'company_id.numeric' => 'Ingrese una empresea valida',
        ]
        );

        if ($validator->fails()){
            return redirect()->route('prealerts.create')->withErrors($validator)->withInput();
        }

        if(Prealerts::where('tracking', '=', $request->input('tracking'))->exists()){
               return redirect()->route('prealerts.create')
            ->withErrors('Existe una prealerta con el mismo tracking');
        }

        $prealerts = new Prealerts();
        $prealerts->code = $this->generateCode();
        $prealerts->tracking = $request->input('tracking');
        $prealerts->date_in = $request->input('date_in');
        $prealerts->company_id = $request->input('company_id');
        $prealerts->status = 0;
        $prealerts->user_id = $request->input('user_id');
        //$prealerts->user_id = auth()->user()->id;

        $prealerts->save();

        return redirect()->route('prealerts.index')->with('success', 'Registro creado correctamente!');
    }

    public function edit($id){

        $prealerts = Prealerts::findOrFail($id);
        $companies = Companies::where('status', 1)->get();
        $users = User::join('model_has_roles', 'model_id', 'users.id')
                    ->join('roles', 'roles.id', 'model_has_roles.role_id')
            ->where('model_type','App\Models\User')
            ->where('roles.name', 'cliente')
            ->where('status', 1)
            ->select('users.*')->get();

        if (empty($prealerts)){
            return redirect()->route('prealerts.index')->withErrors('La prealerta no existe');
        }

        return view('admin.prealerts.edit')->with('prealerts', $prealerts)->with('companies', $companies)->with('users', $users);
    }

    public function storeCompany(Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:50',
            //'description' => 'required|max:255'
        ],
        [
            'name.required' => 'Nombre requerido',
            'name.max' => 'Máximo 50 caracteres',
            //'description.required' => 'Descripción requerida',
            //'description.max' => 'Máximo 255 caracteres'
        ]
        );

        if ($validator->fails()){
            return response()->json([
                "success" => 0,
                "error" => $validator
            ]);
        }

        $com = new Companies();

        $last = Companies::orderBy('id', 'desc')->first();

        $com->code = 'EMP-'.$last->id+1;
        $com->name = $request->input('name');
        $com->description = $request->input('description');
        $com->status = 1;
        $com->save();

        $companies = Companies::where('status', 1)->get();

        return response()->json([
                "success" => 1,
                "company_id" => $com->id,
                "companies" => $companies,
        ]);

    }

    public function update(Request $request, $id){

        $data = $this->validator($request->all())->validate();

        if(Prealerts::where('tracking', '=', $request->input('tracking'))
            ->where('id', '<>', $id)->exists()){
               return redirect()->route('admin.prealerts.index')
            ->withErrors('Existe una prealerta con el mismo tracking');
        }


        Prealerts::whereId($id)->update($data);

        return redirect()->route('admin.prealerts.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $user = Prealerts::whereId($id)->delete();

        return response()->json([
            'success' => 1,
            'message' => 'Prealerta eliminada correctamente'
        ]);
      
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'tracking' => 'required|max:30|unique:pre_alerts,tracking,'.$id,
            'date_in' => 'required|date',
            'company_id' => 'required|numeric',
            //'user_id' => 'required|numeric',
        ],
        [
            'tracking.required' => 'Tracking requerido',
            'tracking.max' => 'Máximo 30 caracteres',
            'tracking.unique' => 'Tracking existente para otra prealerta',
            'date_in.required' => 'Fecha requerida',
            'date_in.date' => 'Fecha incorrecta',
            //'user_id.required' => 'Usuario requerido' 
        ]
        );
    }


    protected function generateCode()
    {
        $last = Prealerts::orderBy('id', 'desc')->select('id')->first();

        //Si ya existe datos
        if(!empty($last)){
            $code = 'PRE-'.($last->id+1);
        }else {
            $code = 'PRE-1';
        }

        return $code;
    }

    public function addPackage($id){
        $prealerts = Prealerts::with('user')
                        ->with('company')
                        ->where('status','=',0)
                        ->where('id', $id)
                    ->select('pre_alerts.*')->first();
 
        return view('admin.packages.create2')->with('prealerts', $prealerts);
    }

    public function storePackage(Request $request){
      $storeData = $request->validate([
            'ware_house' => 'required|max:20|unique:packages',
            //'tracking' => 'required|max:20',
            'code' => 'required|max:20|exists:users',
            'description' => 'required|max:255',
            'width' => 'required|numeric|min:0.01',
            'high' => 'required|numeric|min:0.01',
            'weight' => 'required',
            'long' => 'required|numeric|min:0.01',
            'lb_volume' => 'required|numeric|min:0.01',
            'pie_cubico' => 'required|numeric|min:0.01',
            //'nro_box' => 'required|integer|min:1',
            'image_package' => 'required|mimes:jpg,bmp,png, jpeg,svg'
        ],
        [
            'ware_house.required' => 'Ware House requerido',
            'ware_house.max' => 'Máximo 20 caracteres',
            'ware_house.unique' => 'Existe el mismo Ware House para otro paquete',
            //'tracking.required' => 'Tracking requerido',
            //'tracking.max' => 'Máximo 20 caracteres',
            'code.required' => 'Código requerido',
            'code.max' => 'Máximo 20 caracteres',
            'code.exists' => 'No existe el codigo',
            'description.required' => 'Descripción requerida',
            'description.max' => 'Máximo 255 caracteres',
            'width.required' => 'Ancho requerido',
            'width.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'high.required' => 'Alto requerido',
            'high.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'long.required' => 'Largo requerido',
            'long.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'weight.required' => 'Peso requerido',
            'width.min' => 'Debe ser mayor a 0',
            //'nro_box.required' => 'Número de cajas requerido',
            //'nro_box.integer' => 'Debe ser numerico',
            //'nro_box.min' => 'Debe ser mayor a 0',
            'lb_volume.required' => 'Volumen requerido',
            'lb_volume.numeric' => 'Debe ser numerico',
            'pie_cubico.required' => 'Pie cubico requerido',
            'pie_cubico.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'image_package.required' => 'Imagen requerida',
            'image_package.mimes' => 'Formato invalido'

        ]
        );



        if($request->input('tracking1')[0]=='' and  empty($request->input('tracking'))){
                return redirect()->route('prealerts.create')->withErrors('Tracking requerido')->withInput();;
        }

        $package = new Package();
        $package->ware_house = $request->input('ware_house');
        $package->nro_box = 1;
        $package->description = $request->input('description');
        $package->long = $request->input('long');
        $package->width = $request->input('width');
        $package->high = $request->input('high');
        $package->weight = $request->input('weight');
        $package->lb_volume = $request->input('lb_volume');
        $package->nro_pieces = $request->input('nro_pieces');
        $package->pie_cubico = $request->input('pie_cubico');

        $user = User::where('code', $request->input('code'))
                    ->select('id', 'email')->first();
        $package->user_id = $user->id;
        $package->shippins_id = 1;
        $package->status_id = 1;
        $package->save(); //Save package

        if($package->id){
            //save trackings
            if($request->input('tracking1')[0] !=''){
                foreach ($request->input('tracking1') as $tracking) {
                    if(!empty($tracking)){
                        PackageTracking::create([
                            'package_id' => $package->id,
                            'tracking' => $tracking,
                            'status' => 1
                        ]);

                        //Actualizo la prealerta asociada
                        Prealerts::where('tracking', $tracking)->update(['status'=> 1]);
                    }
                }
            }else{
                PackageTracking::create([ 
                        'package_id' => $package->id,
                        'tracking' => $request->input('tracking'),
                        'status' => 1
                    ]);

                $pre = Prealerts::where('tracking', $request->input('tracking'))->update(['status'=> 1]);
            }
        }

        //save image
        if (!empty($request->image_package)){

                $image = $request->file('image_package');
                $name  = time().'.'.$image->getClientOriginalExtension();

                //$destinationPath = public_path('storage/packages');

                $destinationPath = public_path('img');

                $img = Image::make($image->path());
                $img->resize(600, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$name, 72);
                $url= url('img');


            $image = new ImagePackage();
            $image->path = $destinationPath.'/'.$name;
            $image->url =  $url.'/'.$name; 
            $image->name = $name;
            $image->status = 1;
            $image->package_id = $package->id;
            $image->save();
        }

        //send email of register package
        $send=false;
        if(Mail::to($user->email)->send(new NotificationPackageRegister($package)))
        {
            $send = true;
        }

        return redirect()->route('prealerts.index')->with('success', 'Registro creado correctamente!');
    }
}
