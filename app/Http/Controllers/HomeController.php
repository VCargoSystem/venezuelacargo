<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Package;
use App\Models\Prealerts;
use Illuminate\Support\Facades\DB;
use App\Models\RegionApi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check() && auth()->user()->hasRole('admin')){

            $codes = User::whereNull('code')->count();

            $package = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->where('packages.status_id', '=', 1)
                    //->groupBy('packages.id')
                    ->orderBy('confirmed', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'volume','images_packages.name as image', 'images_packages.url', 'confirmed as status', 'packages_trackings.tracking', DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code', 'nro_pieces'])
                    ->count();

            $prealerts =Prealerts::join('users','users.id', 'pre_alerts.user_id')
                            ->with('company')
                            ->where('pre_alerts.status','=',0)
                            ->orderBy('pre_alerts.created_at', 'desc')
                        ->select('pre_alerts.*', 'users.id as id_user', 'users.name', 'users.last_name')->count();

            $instrucctions = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->join('shippins', 'shippins.id', 'packages.shippins_id')
                    ->where('packages.instrucctions', 1)
                    ->where('packages.status_id', '=', 2)
                    ->where('packages.invoice_status', '=', 0)
                    ->where('packages.confirmed', '=', 'POR CONFIRMAR')
                    ->groupBy('packages.id')
                    ->orderBy('packages.instrucctions', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'images_packages.name as image', 'confirmed as status', 'packages_trackings.tracking', 'hold','repackage', 'insured', 'shippins.name as shippins', 'shippins.id as shippins_id', 'status.latitude', 'status.longitude'])
                    ->count();

            $destiner = RegionApi::select('regions_api.id as id','regions_api.region_name as name')
            ->get();

            $resultado = 0;

            return view('admin.dashboard', compact('package', 'prealerts', 'instrucctions', 'codes','destiner','resultado'));


            return view('admin.dashboard');

        }else if(Auth::check() && auth()->user()->hasRole('cliente')){

            $code = User::select('code')
            ->where('id', auth()->user()->id)
            ->get();

            $package = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->where('packages.user_id', auth()->user()->id)
                    ->where('packages.status_id', '=', 1)
                    ->groupBy('packages.id')
                    ->orderBy('date_delivery', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'images_packages.name as image', 'confirmed as status', 'packages_trackings.tracking', DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code', 'status.latitude', 'status.longitude'])->count()
                    ;

            $prealerts =Prealerts::join('users','users.id', 'pre_alerts.user_id')
                            ->with('company')
                            ->where('pre_alerts.status','=',0)
                            ->where('user_id','=',auth()->user()->id)
                            ->orderBy('pre_alerts.created_at', 'desc')
                        ->select('pre_alerts.*', 'users.id as id_user', 'users.name', 'users.last_name')->count();

            $instrucctions = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->join('shippins', 'shippins.id', 'packages.shippins_id')
                    ->where('packages.user_id', auth()->user()->id)
                    ->where('packages.confirmed', '=', 'POR CONFIRMAR')
                    ->groupBy('packages.id')
                    ->orderBy('packages.instrucctions', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'images_packages.name as image', 'confirmed as status', 'packages_trackings.tracking', 'hold','repackage', 'insured', 'shippins.name as shippins', 'shippins.id as shippins_id', 'status.latitude', 'status.longitude'])->count()
                    ;

                    $destiner = RegionApi::select('regions_api.id as id','regions_api.region_name as name')
                    ->get();
        
                    $resultado = 0;

            return view('clients.dashboard', compact('package', 'prealerts', 'instrucctions','destiner','resultado', 'code'));

        }else{
            return redirect('/login');
        }
    }

}
