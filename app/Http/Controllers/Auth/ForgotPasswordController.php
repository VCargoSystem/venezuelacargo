<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request; 
use DB; 
use Carbon\Carbon; 
use App\Models\User; 
use Mail; 
use Hash;
use Illuminate\Support\Str;
use App\Mail\NotificationReset;

class ForgotPasswordController extends Controller
{
    //use SendsPasswordResetEmails;

    public function showForgetPasswordForm()
   {
         return view('auth.passwords.email');
    }

    public function submitForgetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $token = Str::random(64);

        User::where('email', $request->email)->update([
           'remember_token' => $token,
         ]);

        //Enviar email con el codigo
        $details = [ 
                   'url' =>route('reset.password.get', $token)
                ];

       $send=false;
        if(Mail::to($request->email)->send(new NotificationReset($details)))
        {
            $send = true;
        }
        
        return redirect()->route('forget.email_success')->with('message', 'Hemos enviado su enlace de restablecimiento de contraseña por correo electrónico!');
   }

    public function submitEmailSuccess(){
       return view('auth.passwords.email_success');
    }

    public function showResetPasswordForm($token) { 
      return view('auth.passwords.reset', ['token' => $token]);
    }
  
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function submitResetPasswordForm(Request $request)
      {
          $request->validate([
              'email' => 'required|email|exists:users',
              'password' => 'required|string|min:4|confirmed',
              'password_confirmation' => 'required'
          ]);
  

          $updatePassword = User::where([
                               'email' => $request->email, 
                                'remember_token' => $request->token
                              ])
                              ->first();
 
          if(!$updatePassword){
              return back()->withInput()->with('error', 'Token invavalido!');
         }
  
          $user = User::where('email', $request->email)
                      ->update(['password' => Hash::make($request->password)]);
 
          return redirect('login')->with('message', 'Su password ha sido cambiado!');
      }
}
