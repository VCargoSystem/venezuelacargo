<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Cities;
use App\Models\States;
use App\Models\Address;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
Use Session;
Use Redirect;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;
 
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'dni' => ['required', 'string', 'max:10'],
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:14'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
            'state_id' => ['required', 'numeric'],
            'city_id' => ['required', 'numeric'],
            'address' => ['required', 'string', 'max:255'],
            'longitude' => ['required', 'string'],
            'latitude' => ['required', 'string']
        ], [
            'dni.required' => 'Cedula requerida',
            'dni.max' => 'Maximo 10 caracteres',
            'name.required' => 'Nombre requerido',
            'name.max' => 'Maximo 255 caracteres',
            'username.required' => 'Nombre requerido',
            'username.max' => 'Maximo 255 caracteres',
            'last_name.required' => 'Apellido requerido',
            'last_name.max' => 'Maximo 255 caracteres',
            'email.required' => 'Email requerido',
            'email.email' => 'Ingrese un email valido',
            'phone.required' => 'Teléfono requerido',
            'phone.max' => 'Maximo 14 caracteres',
            'password.required' => 'Clave requerida',
            'password.min' => 'Minimo 4 caracteres',
            'password.confirmed' => 'La clave no coincide',
            'address.required' => 'Dirección requerida',
            'address.max' => 'Maximo 255 caracteres',
            'longitude.required' => 'Longitud requerido',
            'latitude.required' => 'Latitud requerido'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->dni = $data['dni'];
        $user->username = $data['username'];
        $user->name = $data['name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->password = Hash::make($data['password']);
        $user->status = 0;


        $user->save();

        if($user->id > 0){
            //asigno rol cliente
            $user->assignRole('cliente');

            //Guardo direccion
            $direccion = new Address();
            $direccion->countries_id = $data['country_id'];
            $direccion->states_id = $data['state_id'];
            $direccion->cities_id = $data['city_id'];
            $direccion->address = $data['address'];
            $direccion->address1 = $data['address1'];
            $direccion->users_id = $user->id;
            $direccion->latitude = $data['latitude'];
            $direccion->longitude = $data['longitude'];
            $direccion->save();

        }

        return $user;
    }


    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('auth.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;
        
        return view('auth.map',compact('long', 'lati'));
    }

    public function findCities($id){
        $cities = Cities::where('state_id', $id)->select('id', 'name')->get();

        return response()->json([
            'cities' =>  $cities
        ]);
    }

    public function registerSuccess()
    {
        return view('auth.register_success');
    }
}
