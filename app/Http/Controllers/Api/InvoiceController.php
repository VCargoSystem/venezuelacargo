<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\Invoice;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

class InvoiceController extends Controller
{
    /**
    * Get list of prealerts
    */
    public function index(Request $request)
    {
     
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        //facturas
        $invoices = Invoice::where('invoices.client_id',  $user->id)
                    ->whereIn('invoices.status',  [3])
                    ->orderBy('invoices.created_at', 'desc')
                    ->select([
                        'invoices.id', 
                        DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") AS created_date'), 
                        DB::raw('ROUND(amount, 2) as amount'), 
                        DB::raw('ROUND(amount, 2) as amount_ves'), 
                        'invoices.status as status_id', 
                        DB::raw('CASE
                            WHEN invoices.status = 1 THEN "POR PAGAR"
                            WHEN invoices.status = 2 THEN "PENDIENTE"
                            ELSE "PAGADA"
                        END as status'),
                        'invoices.status as estatus_id',
                        DB::raw('DATEDIFF(NOW(), DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as days_delay'),
                        DB::raw('DATEDIFF(NOW(),   DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as expiration_days')])
                    ->get()
                    ->map(function ($invoices) {
                        $invoices['link_print'] = route('printInvoice', $invoices->id);

                        if($invoices->days_delay <= 0){
                            $invoices->days_delay = 0;
                        }
                        
                        if($invoices->expiration_days >= 0){ 
                            $invoices->expiration_days = 0;
                        }else{
                            $invoices->expiration_days *= (-1);
                        }

                      return $invoices;
                    });


        if(count($invoices) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen facturas"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $invoices
            ]
        );

    }


    public function searchInvoice(Request $request, $date1, $date2)
    {

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        //facturas
        $invoices = Invoice::join('users', 'users.id', 'invoices.client_id')
                    ->where('invoices.client_id',  $user->id)
                    ->whereIn('invoices.status',  [3])
                    ->whereRaw("DATE_FORMAT(invoices.created_at, '%Y-%m-%d') >='".$date1."' and DATE_FORMAT(invoices.created_at, '%Y-%m-%d') <='".$date2."'")

                    ->orderBy('invoices.created_at', 'desc')
                    ->select(['invoices.id', DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") AS created_date'), DB::raw('ROUND(amount, 2) as amount'), DB::raw('ROUND(amount, 2) as amount_ves'), 
                        DB::raw('CASE
                            WHEN invoices.status = 1 THEN "POR PAGAR"
                            WHEN invoices.status = 2 THEN "PENDIENTE"
                            ELSE "PAGADA"
                        END as status'),
                        'invoices.status as estatus_id',
                         DB::raw('DATEDIFF(NOW(),   DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as days_delay'), DB::raw('DATEDIFF(NOW(),   DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as expiration_days')])
                    ->get()
                    ->map(function ($invoices) {
                        $invoices['link_print'] = route('printInvoice', $invoices->id);

                        if($invoices->days_delay <= 0){
                            $invoices->days_delay = 0;
                        }
                        
                        if($invoices->expiration_days >= 0){
                            $invoices->expiration_days = 0;
                        }else{
                            $invoices->expiration_days *= (-1);
                        }

                      return $invoices;
                    });


        if(count($invoices) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen facturas"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $invoices
            ]
        );

    }
    public function invoicesPending(Request $request)
    {
     
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        //facturas
        $invoices = Invoice::join('users', 'users.id', 'invoices.client_id')
                    ->where('invoices.client_id',  $user->id)
                    ->whereIn('invoices.status',  [1,2])
                    ->orderBy('invoices.created_at', 'desc')
                    ->select(['invoices.id', DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") AS created_date'), DB::raw('ROUND(amount, 2) as amount'), DB::raw('ROUND(amount, 2) as amount_ves'), 
                        DB::raw('CASE
                            WHEN invoices.status = 1 THEN "POR PAGAR"
                            WHEN invoices.status = 2 THEN "PENDIENTE"
                            ELSE "PAGADA"
                        END as status'),
                        'invoices.status as status_id', DB::raw('DATEDIFF(NOW(), invoices.created_at) as days_delay')])
                    ->get()
                    ->map(function ($invoices) {
                      $invoices['link_print'] = route('printInvoice', $invoices->id);
                      return $invoices;
                    });


        if(count($invoices) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen facturas"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $invoices
            ]
        );

    }

    public function show($id, Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }
 
        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user->id)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        //facturas
        $invoices = Invoice::join('users', 'users.id', 'invoices.client_id')
                    ->where('invoices.client_id',  $user->id)
                    ->where('invoices.id',  $id)
                    ->orderBy('invoices.created_at', 'desc')
                    ->select(['invoices.id', DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") AS created_date'), DB::raw('ROUND(amount, 2) as amount'), DB::raw('ROUND(amount, 2) as amount_ves'), DB::raw('IF(invoices.status=1, "POR PAGAR", "PAGADA") as status'),'invoices.status as status_id', DB::raw('DATEDIFF(NOW(), invoices.created_at) as days_delay')])
                    ->get()
                    ->map(function ($invoices) {
                      $invoices['link_print'] = route('printInvoice', $invoices->id);
                      return $invoices;
                    });


        if(empty($invoices->id)){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "Factura no encontrada"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data" => $invoices
            ]
        );
    }

    public function  printInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];

        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data)->stream('invoice.pdf');

            return $pdf;

    }
    }




    public function userValid($request){

        if(!$request->bearerToken()){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $c = User::where('api_token', $request->bearerToken())->select('id')->count();

        if($c == 0){
            return 0;
        }else{
            $u = User::where('api_token', $request->bearerToken())->select('id')->first();

            return $u->id;
        }

    }

}
