<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Address;
use App\Models\Prealerts;
use App\Models\AccessTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Companies;
use Illuminate\Support\Facades\DB;


class PrealertsController extends Controller
{
    /**
    * Get list of prealerts
    */
    public function index(Request $request)
    {

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $prealerts = Prealerts::join('users', 'users.id', 'pre_alerts.user_id')
                    ->join('companies', 'companies.id', 'pre_alerts.company_id')
                        ->where('pre_alerts.status','=',0)
                        ->where('pre_alerts.user_id','=', $user->id)
                        ->orderBy('pre_alerts.created_at', 'desc')
                    ->select('pre_alerts.id','pre_alerts.code', 'pre_alerts.tracking', 'pre_alerts.date_in', DB::raw('IF(pre_alerts.status=1, "CONFIRMADA", "PENDIENTE") as status'), 'pre_alerts.user_id', 'pre_alerts.company_id', 'companies.name as currier')->get();


        if(empty($prealerts)){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen prealertas"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $prealerts
            ]
        );

    }

    public function show($id, Request $request)
    {
        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $prealerts = Prealerts::join('users', 'users.id', 'pre_alerts.user_id')
                    ->join('companies', 'companies.id', 'pre_alerts.company_id')
                        ->where('pre_alerts.status','=',0)
                        ->where('pre_alerts.user_id','=', $user->id)
                        ->where('pre_alerts.id', $id)
                        ->orderBy('pre_alerts.created_at', 'desc')
                    ->select('pre_alerts.id','pre_alerts.code', 'pre_alerts.tracking', 'pre_alerts.date_in', DB::raw('IF(pre_alerts.status=1, "CONFIRMADA", "PENDIENTE") as status'), 'pre_alerts.user_id', 'pre_alerts.company_id', 'companies.name as currier')->get();

        if(count($prealerts) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=>  "Prealert no encontrado"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data" => $prealerts
            ]
        );

    }

    public function getCompanies(Request $request){

        if(empty($token)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro api token"
            ]);
        }

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $companies = Companies::where('status', 1)->get();
        $data = [];

        foreach ($companies as $key => $com) {
            $data[] = ['id'=>$com->id, 'text'=>$com->name];
        }

        return response()->json([
            "success" => true,
            "code" => 200,
            "data" => $data
        ]);
    }


    /*Create new prealerts*/
    public function store(Request $request)
    {

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $validator = \Validator::make($request->all(), [
            'tracking' => 'required|max:40|unique:pre_alerts,tracking',
            'date_in' => 'required|date',
            'company_id' => 'required|numeric',
        ],
        [
            'tracking.required' => 'Tracking requerido',
            'tracking.max' => 'Máximo 50 caracteres',
            'date_in.required' => 'Fecha requerida',
            'date_in.date' => 'Ingrese una fecha',
            'company_id.required' => 'Empresa requerida',
            'company_id.numeric' => 'Ingrese una empresea valida',
        ]
        );

        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=>  $validator->messages()
                    ]);
        }


        $user_id = $this->userValid($request);

        if(!$user_id){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

    
        $prealerts = new Prealerts();
        $prealerts->code = $this->generateCode();
        $prealerts->tracking = $request->input('tracking');
        $prealerts->date_in = $request->input('date_in');
        $prealerts->company_id = $request->input('company_id');
        $prealerts->status = 0;
        $prealerts->user_id = $user_id;

        $prealerts->save();

        return response()->json(
                    ["success" => true,
                     "code"=> 200,
                     "message"=>  "Prealerta registrada correctamente",
                     "data" => $prealerts
                    ]);
    }

    public function update(Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user_id = $this->userValid($request);

        if(!$user_id){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $id = $request->input('id');
        $data = $request->all();

        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'tracking' => 'required|max:50|unique:pre_alerts,tracking,'.$id,
            'date_in' => 'required|date',
            'company_id' => 'required|numeric',
        ],
        [
            'tracking.required' => 'Tracking requerido',
            'tracking.max' => 'Máximo 50 caracteres',
            'tracking.unique' => 'Tracking existente para otra prealerta',
            'date_in.required' => 'Fecha requerida',
            'date_in.date' => 'Ingrese una fecha',
            'company_id.required' => 'Empresa requerida',
            'company_id.numeric' => 'Ingrese una empresea valida',
        ]
        );
     
        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=>  $validator->messages()
                    ]);
        }

        Prealerts::whereId($id)->update($data);

        return response()->json(
                    ["success" => true,
                     "code"=> 200,
                     "message"=>  "Prealerta actualizada correctamente"
                    ]);
    }

    public function destroy(Request $request, $id){

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        Prealerts::whereId($id)->delete();

        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'Prealerta eliminada correctamente'
        ]);
      
    }

    public function userValid($request){

        $c = User::where('api_token', $request->bearerToken())->select('id')->count();

        if($c == 0){
            return 0;
        }else{ 
            $u = User::where('api_token', $request->bearerToken())->select('id')->first();

            return $u->id;
        }

    }

    protected function generateCode()
    {
        $last = Prealerts::orderBy('id', 'desc')->select('id')->first();

        //Si ya existe datos
        if(!empty($last)){
            $code = 'PRE-'.($last->id+1);
        }else {
            $code = 'PRE-1';
        }

        return $code;
    }
}
