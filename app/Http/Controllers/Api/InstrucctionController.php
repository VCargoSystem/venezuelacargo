<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

class InstrucctionController extends Controller
{
    /**
    * Get list of prealerts
    */
    public function index(Request $request)
    {
     
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        //instrcciones en almacen, ya confirmadas, listas para instruccion
        $instrucctions = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->join('shippins', 'shippins.id', 'packages.shippins_id')
                    ->where('packages.user_id',  $user->id)
                    ->where('packages.instrucctions', 1)
                    ->where('packages.confirmed', 'POR CONFIRMAR')
                    ->where('packages.hold', 0)
                    ->where('packages.status_id', '=', 2)
                    ->groupBy('packages.id')
                    ->orderBy('packages.instrucctions', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'packages_trackings.tracking', 'confirmed as status', 'packages.description',  'hold','repackage', 'insured', 'shippins.name as shippins', 'shippins.id as shippins_id', DB::raw('DATE_FORMAT(packages.created_at, "%d-%m-%Y") AS created_date'), DB::raw('DATE_FORMAT(packages.updated_at, "%d-%m-%Y") AS updated_date'), 'packages.status_id', 'instrucctions'])->get();

        if(count($instrucctions) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen instrucciones"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $instrucctions
            ]
        );

    }

    public function show($id, Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }
 
        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user->id)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }


        $instrucctions = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->join('shippins', 'shippins.id', 'packages.shippins_id')
                    ->where('packages.instrucctions', '=', 1)
                    ->where('packages.id', $id)
                    ->where('packages.hold', 0)
                    ->where('packages.status_id', '=', 2)
                    ->groupBy('packages.id')
                    ->orderBy('packages.instrucctions', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'packages_trackings.tracking', 'confirmed as status', 'packages.description',  'hold','repackage', 'insured', 'shippins.name as shippins', 'shippins.id as shippins_id', DB::raw('DATE_FORMAT(packages.created_at, "%d-%m-%Y") AS created_date'), DB::raw('DATE_FORMAT(packages.updated_at, "%d-%m-%Y") AS updated_date'), 'packages.status_id', 'instrucctions'])->first();


        if(empty($instrucctions)){
            return response()->json(
                    ["success"=>true,
                     "code"=> 200,
                     "message"=>  "Instrucción no encontrada"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data" => $instrucctions
            ]
        );
    }

    /*Create new prealerts*/
    public function store(Request $request)
    {

        if(!$request->bearerToken()){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user_id = $this->userValid($request);

        if(!$user_id){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $validator = \Validator::make($request->all(), [
            'id' => 'required|numeric', 
            'hold' => 'required|min:0',
            'insured' => 'required|min:0',
            'repackage' => 'required|min:0',
            'shippins_id' => 'required|min:1'
        ],
        [
            'id.required' => 'Campo Id requerido',
            'hold.numeric' => 'Valor debe ser un número entero',
            'hold.required' => 'Campo Almacen requerido',
            'hold.min' => 'Valor minimo requerido (0)',
            'insured.required' => 'Campo Seguro requerido',
            'insured.min' => 'Valor minimo requerido (0)',
            'repackage.required' => 'Campo Reempaque requerido',
            'repackage.min' => 'Valor minimo requerido (0)',
            'shippins_id.required' => 'Campo tipo de envio requerido',
            'shippins_id.min' => 'Valor minimo requerido (1)'
        ]
        );

        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=>  $validator->messages()
                    ]);
        }
    
      
        //Verified date 
       /*if($this->validDate($request->input('repackage'), 'insert') == 0){
            $message = [
                'Hasta el día miércoles hasta las 4:00pm se puede confirmar una instrucción, si solo si, el envío es con Reempaque',
                'Si es Sin Reempaque y no ha confirmado su instrucción, puede
                colocar la instrucción el día jueves para las salidas del viernes'
            ];

            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=> $message
                    ]);
        }*/
      
        $package = Package::where('id', $request->input('id'))->first();


        if(!$package){
            return response()->json(
                [
                    "error" => true,
                    "code" => 200,
                    "message" => "No existe el paquete"
                ]
            );
        }

        if($package->instrucctions == 1){
            return response()->json(
                [
                    "error" => true,
                    "code" => 200,
                    "message" => "El paquete ya posee instrucciones"
                ]
            );
        }

        $package->hold = $request->input('hold');
        $package->insured = $request->input('insured');
        $package->shippins_id = $request->input('shippins_id');
        $package->repackage = $request->input('repackage');

        if($request->input('hold') == 1){
            $package->instrucctions = 1;
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 1;
        }else{
            $package->instrucctions = 1;
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 2;
        }

        $package->save();

        return response()->json(
                    ["success" => true,
                     "code"=> 200,
                     "message"=>  "Instrucción registrada correctamente",
                     "data" => $package
                    ]);
    }

    public function update(Request $request)
    {
        if(!$request->bearerToken()){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user_id = $this->userValid($request);

        if(!$user_id){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $validator = \Validator::make($request->all(), [
            'id' => 'required|numeric',
            'hold' => 'required|min:0',
            'insured' => 'required|min:0',
            'repackage' => 'required|min:0',
            'shippins_id' => 'required|min:1'
        ],
        [
            'id.required' => 'Campo Id requerido',
            'hold.numeric' => 'Valor debe ser un número entero',
            'hold.required' => 'Campo Almacen requerido',
            'hold.min' => 'Valor minimo requerido (0)',
            'insured.required' => 'Campo Seguro requerido',
            'insured.min' => 'Valor minimo requerido (0)',
            'repackage.required' => 'Campo Reempaque requerido',
            'repackage.min' => 'Valor minimo requerido (0)',
            'shippins_id.required' => 'Campo tipo de envio requerido',
            'shippins_id.min' => 'Valor minimo requerido (1)'
        ]
        );

        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=>  $validator->messages()
                    ]);
        }

       /*if($this->validDate($request->input('repackage'), 'edit') == 0){
            $message = [
                'La instrucción no se puede modificar (bloqueada) hasta el dia sabado, que se habilita nuevamente',
            ];

            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=> $message
                    ]);
        }*/

        $package = Package::where('id', $request->input('id'))->first();

        if(!$package){
            return response()->json(
                [
                    "error" => true,
                    "code" => 200,
                    "message" => "No se existe el paquete"
                ]
            );
        }

        if($package->instrucctions == 0){
            return response()->json(
                [
                    "error" => true,
                    "code" => 200,
                    "message" => "El paquete no posee instrucciones"
                ]
            );
        }

        $package->hold = $request->input('hold');
        $package->insured = $request->input('insured');
        $package->shippins_id = $request->input('shippins_id');
        $package->repackage = $request->input('repackage');
        
        if($request->input('hold') == 1){
            $package->instrucctions = 1;
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 1;
        }else{
            $package->instrucctions = 1;
            $package->confirmed = 'CONFIRMADO';
            $package->status_id = 2;
        }

        $package->save();

        return response()->json(
                    ["success" => true,
                     "code"=> 200,
                     "message"=>  "Instrucción actualizada correctamente",
                     "data" => $package
                    ]);
    }

    public function userValid($request){

        if(!$request->bearerToken()){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $c = User::where('api_token', $request->bearerToken())->select('id')->count();

        if($c == 0){
            return 0;
        }else{
            $u = User::where('api_token', $request->bearerToken())->select('id')->first();

            return $u->id;
        }

    }

   
    public function validDate($repackage, $op){
        // 0  es domingo, traigo el dia de la semana 
        // de la fecha de hoy
        $today = \Carbon\Carbon::now()->locale('es_VE')->dayOfWeek;
        $hour = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('h');
        $min = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('m');

        if($op == 'insert'){
            if($repackage == 1){
                if(($today <= 3 && $today >= 1) &&  ($hour < 16 &  $min < 1)){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                if(($today <= 4 && $today >= 1) &&  ($hour < 23 &  $min < 59)){
                    return 1;
                }else{
                    return 0;
                }
            }

        }else{
            if($today == 4 or  $today == 5 or $today==0){
                return 0;
            }else{
                 if($repackage == 1){
                    if(($today <= 3 && $today >= 1) &&  ($hour < 16 &  $min < 1)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }else{
                        if(($today <= 4 && $today >= 1) &&  ($hour < 23 &  $min < 59)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }
            }
        }
    }
}
