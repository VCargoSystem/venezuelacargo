<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegionApi extends Model
{
    protected $table = 'regions_api';
    public $timestamps = true;
}
