<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\RateShippings;
use App\Models\PackageTracking;
use App\Models\Notification;
use App\Models\RegionApi;
use App\Models\TarifaRegion;


use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class ShippingsController extends Controller
{
    public function index(Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $ids = [];
        $packages_id;
        $ids_pak;
        $repa = InvoiceDetails::join('invoices', 'invoices.id', 'invoice_details.invoices_id')
            ->where('package_id', '=',0)
            ->where('client_id', '=', $user->id)
            ->select([DB::raw('distinct(invoice_details.id)'),'invoice_details.packages'])->get();

        if(!empty($repa)){
            foreach ($repa as $item) {
               $items = explode(",", $item->packages);

               foreach ($items as $value) {
                    array_push($ids, $value);
               }
             
            }
        }


        $packages = InvoiceDetails::select(['invoice_details.id as id', 'invoice_details.invoices_id', 'invoice_details.high', 'invoice_details.width',
        'invoice_details.long', 'invoice_details.weight', 'invoice_details.subtotal_lb','invoice_details.total_pie3', 'invoice_details.ware_house', 'invoices.nro_contenedor as currier',
        'invoice_details.status_id','status.name as status', 'packages.shippins_id', 'packages.id', 'packages.description', 'shippins.name as shippins',DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") as date_delivery'),
         DB::raw('DATE_FORMAT(packages.date_delivery, "%d-%m-%Y") as date_delivery'), 'status.code as status_siglas', 'packages_trackings.tracking', 'status.latitude as latitud', 'status.longitude as longitud', 'invoice_details.nro_pieces', 'packages.invoice_status as facturado']) 
        ->join('invoices','invoices.id','invoice_details.invoices_id')
        ->join('packages','packages.user_id','invoices.client_id')
        ->join('status','status.id','invoice_details.status_id')
        ->join('shippins','shippins.id','packages.shippins_id')
        ->join('packages_trackings','packages_trackings.package_id','packages.id')
        ->where('client_id', $user->id)
        ->where('packages.invoice_status', 1)
        ->where('packages.instrucctions', 1)
        ->where('packages.status_id','>=', 2)
        ->groupBy('invoice_details.invoices_id')
        ->get();
        
        

        if(count($packages) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen paquetes de envio"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $packages
            ]
        );

    }

    public function show($id, Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }
 
        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user->id)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $ids = [];
        $packages_id;
        $ids_pak;
        


        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id',$id)->first();

                    $details = $invoice->details;
                    $packages = [];
                    
            
            
                    foreach ($details as $d) {
                        //Si es si reempaque
                        if($d->package_id > 0){
                            
                            $images = [];
                            
                            $imagesList = ImagePackage::where('package_id', $d->package_id)->select('url')->first();
                        	$images[] = $imagesList->url;

							$d->images = $images; 
							$packages[] = $d; 
            
                        }else{
            
                            $id_paq = explode(",", $d->packages);
                            $wh = [];
                            $descriptions = [];
                            $images = [];
                    
                            foreach ($id_paq as $p) {
                                
                                
                                $pak = Package::join('images_packages','images_packages.package_id','packages.id')->where('packages.id', $p)->select('packages.*','images_packages.url')->first();
                                $wh[] =  $pak->ware_house;
                                $descriptions [] = $pak->description;
                                $images[] = $pak->url;
                            }
            
                            $d->wh = $wh;
                            $d->descriptions = $descriptions;
                            $d->images = $images;

                            $packages[] = $d;
                        }
            
                    }

                    $user = $invoice->user;
            
                    if(!empty($invoice)){ 
            
                        $data = [
                            "invoice" => $invoice
                            //"packages" => $packages
                        ];
                         }

        if(empty($data)){
            return response()->json(
                    ["success"=>true,
                     "code"=> 200,
                     "message"=>  "Envio no encontrado"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data" => $data
            ]
        );
    }

    public function tracking(Request $request, $tracking)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $packages = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->join('shippins', 'shippins.id', 'packages.shippins_id')
                    //->where('packages.confirmed', '<>', 'HOLD')
                    ->where('packages.status_id', '>=',1)
                    ->where('packages.user_id',  $user->id)
                    ->where(function ($q) use ($tracking) {
                        $q->orWhere('tracking', $tracking)
                        ->orWhere('ware_house', $tracking);
                    })
                    ->groupBy('packages.id')
                    ->orderBy('packages.instrucctions', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'description', 'images_packages.path', 'images_packages.name as image', 'packages_trackings.tracking', 'shippins.name as shippins', 'shippins.id as shippins_id', 'packages.nro_container', 'packages.date_delivery', 'status.name as status', 'users.id as user_id', 'invoice_status', 'status.code', 'status.longitude', 'status.latitude'])->get()
                    ;

        if(count($packages) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existe este pquete"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $packages
            ]
        );

    }

    public function addShipping(Request $request){

        $validator = \Validator::make($request->all(), [
            'nro_container' => 'required|min:0'
        ],
        [
            'nro_container.required' => 'Campo número de container requerido',
            'nro_container.min' => 'Valor minimo requerido (0)'
        ]
        );

        if ($validator->fails()){
            return redirect()->route('admin.shippings.index')->withErrors($validator);
        }

        $package = Package::where('id', $request->input('id'))->first();

        $package->date_delivery =  \Carbon\Carbon::parse($request->input('date_delivery'))->format('Y-m-d');
    
        $package->nro_container = $request->input('nro_container');

        $package->save();

        return redirect()->route('admin.shippings.index')->with('success', 'Envio agregado correctamente!');
     }


   
    public function edit($id){
        $package = Package::where('id', $id)
                        ->select('packages.*')
                        ->first();

      
        if($package->date_delivery != "" && $package->nro_container != ""){
            return response()->json(["success" => 1, 'package' => $package]);
        }else{
            return response()->json(["success" => 0, "message" => "No posee datos de envio, debe agregarlos" ]);
        }
    }

    public function update(Request $request){

        $validator = \Validator::make($request->all(), [
            'nro_container_edit' => 'required|min:1',
            'status_id' => 'required|min:1'
        ],
        [
            'nro_container_edit.required' => 'Campo número de container requerido',
            'nro_container_edit.min' => 'Valor minimo requerido (1)',
            'status_id.required' => 'Campo estatus requerido',
            'status_id.min' => 'Valor minimo requerido (1)'
        ]
        );
        

        if ($validator->fails()){
            return redirect()->route('clients.shippings.index')->withErrors($validator);
        }

        $package = Package::where('id', $request->input('id_edit'))->first();

    
        $package->nro_container = $request->input('nro_container_edit');
        $package->status_id = $request->input('status_id');

        $package->save();

        return redirect()->route('admin.shippings.index')->with('success', 'Envio agregado correctamente!');

    }


    public function destroy($id){
  
    }
   
    public function receipt($id){

        $package = Package::with('user')->where('id', $id)->first();
 
        $data = [
            "package" => $package,
            "user" => auth()->user()
        ];

        //pdf de recibo
        $pdf = \PDF::loadView('reports.receipt_package', $data)->stream('archivo.pdf');

        return $pdf;
        
    }

    public function assing($id){
        $package = Package::where('id', $id)
                        ->select('packages.*')
                        ->first();

        if($package->date_delivery != "" && $package->nro_container){
            return response()->json(["success" => 0, "message" => "Ya tiene un envío asignado, por favor seleccione la opcion editar" ]);
        }else{
            return response()->json(["success" => 1, "message" => "Agregar envío", "shippins_id" => $package->shippins_id]);
        }
    }

    public function changeLot(Request $request)
    {
        if(empty($request->input('items'))){
            return response()->json(["success" => 0, "message" => "Debe seleccionar algun item" ]);
        }

        foreach ($request->input('items') as $item) {

            $package = Package::where('id', $item)->first();

            if(!empty($pack)){
                $package->status_id = $request->input('status_id');
                $package->save();
            }
            
        }
       
        return response()->json(["success" => 1, "message" => "Registros actualizados correctamente" ]);
    }

    public function invoice(Request $request)
    {
        $type;
        $ware_house ="";

        if(empty($request->input('items_invoice'))){
            return redirect()->route('admin.shippings.index')->withErrors("Debe seleccionar algun paquete");
        }



        $items = explode(",", $request->input('items_invoice'));

        $pack = Package::where('id', $items[0])->first();

        $user = User::with('address')->where('id',  $pack->user_id)->first();

        $packages = Package::whereIn('id', $items)
                ->where('repackage', 0)
                ->get();
        $count_p =  Package::whereIn('id', $items)
                ->where('repackage', 0)
                ->count();

        $repackages = Package::whereIn('id', $items)
                ->where('repackage', 1)
                ->get();

        $count_rp = Package::whereIn('id', $items)
                ->where('repackage', 1)
                ->count();

        $total_packages = Package::whereIn('id', $items)
                ->get();

        $invoice='';

        if($count_p > 0){

            $type = $packages[0]->shippins_id;
            $ware_house = $packages[0]->trackings[0]->tracking;

            $invoice = $this->invoiceCalculate($packages);

        }else if($count_rp > 0){
            $type = (int) $repackages[0]->shippins_id;
            $ware_house = $repackages[0]->trackings[0]->tracking;
        }

        


        return  view('admin.shippings.invoice')->with('packages',  $packages)
                    ->with('user', $user)->with('invoice', $invoice)->with('repackages', $repackages)->with('type', $type)->with('ware_house', $ware_house)->with('count_rp', $count_rp)->with('count_p', $count_p);
    }

    public function invoiceCalculate($packages)
    {
        $type = $packages[0]->shippins_id;  //1-Mar  2-Air
        $invoice = new \stdClass();
        $total_pie3 = 0;
        $subtotal = 0;
        $rate = 1000; //Consultar tabla para valor
        $volumetric_lb = 0; //libras volumetricas
        $subtotal_lb = 0;
        $subtotal_w = 0;
        $items_invoice = [];
        $total_pieces = 0;
        $total_pie3 = 0;

        foreach ($packages as $item) {
            $totail_pie3 = 0;
            $volumetric_lb = 0;

            //Calculates maritime
            if( $type == 1){
                //pie cubico
                $totail_pie3 = (($item->long * $item->width * $item->high)/ 1728) * $item->nro_pieces; 

                if($item->insured){
                    $secure = ($totail_pie3 * $rate) *0.10;
                    $subtotal +=  ($totail_pie3 * $rate) + $secure; //subtotal x tarifa
                }else{
                    $subtotal +=  ($totail_pie3 * $rate);
                }

                $item->pie = $totail_pie3;
                $total_pieces = ($total_pieces + $item->nro_pieces);
                $total_pie3 = ($total_pie3 + $totail_pie3);
                $item->subtotal_lb = 0;
                $item->subtotal_w = 0;
            
            }else{
                //libra volumetrica aereo
                $volumetric_lb = (($item->long * $item->width * $item->high)/ 166) *$item->nro_pieces; 

                if($item->insured){

                    $subtotal_lb += $volumetric_lb + ($volumetric_lb * 0.10);

                    //peso
                    $subtotal_w += ($item->weight * $item->nro_pieces) + (($item->weight * $item->nro_pieces) * 0.10);

                }else{
                    $subtotal_lb += $volumetric_lb;
                    $subtotal_w += ($item->weight * $item->nro_pieces);
                }

                $total_pieces = ($total_pieces + $item->nro_pieces);

            }
            
            $item->volumetric_lb = $volumetric_lb;
            $item->subtotal_lb = $subtotal_lb;
            $item->subtotal_w = $subtotal_w;

            $items_invoice[] = $item;

        }//fin for

        if( $type == 2) //aereo
        { 
            if($subtotal_lb > $subtotal_w){
                $subtotal +=  ($subtotal_lb * $rate);
            }else{
                $subtotal +=  ($subtotal_w * $rate);
            }
        }

        $invoice->subtotal =  $subtotal;
        $invoice->subtotal_lb =  $subtotal_lb;
        $invoice->subtotal_w = $subtotal_w;
        $invoice->total_pieces = $total_pieces;
        $invoice->type = $type;
        $invoice->items_invoice = $items_invoice;
        $invoice->total_pie3 = $total_pie3;

        return $invoice;
    }

    public function repackage(Request $request)
    {
        $type = $request->input('type');  //1-Mar  2-Air
        $invoice_repackage = new \stdClass();
        $total_pie3 = 0;
        $subtotal = 0;
        $rate = 1000; //Consultar tabla para valor
        $volumetric_lb = 0; //libras volumetricas
        $subtotal_lb = 0;
        $subtotal_w = 0;
        $insured = 0;
        $vol_pie = 0;
        $total_pieces = 0;
        
        //Reempaque 
        foreach ($request->input('items') as $id) {
            $paq = Package::find($id);

            if($paq->insured){
                $insured = 1;
            }
        
            //Calculates maritime
            if( $type == 1){
                //pie cubico
                $totail_pie3 = ($request->input('long') * $request->input('width') * $request->input('high'))/ 1728; 

                if($insured){
                    $subtotal += ($totail_pie3 * $rate) + ($totail_pie3 * $rate) * 0.10;
                }else{
                    $subtotal +=  ($totail_pie3 * $rate); //subtotal x tarifa
                }

                $vol_pie = $totail_pie3;
                $total_pieces = ($total_pieces + $paq->nro_pieces);
                $total_pie3 = ($total_pie3 + $totail_pie3);
            
            }else{
                //libra volumetrica aereo
                $volumetric_lb = ($request->input('long') * $request->input('width') * $request->input('high'))/166; 

                $subtotal_lb += $volumetric_lb;

                //peso
                $subtotal_w += $item->weight;
                $total_pieces = ($total_pieces + $paq->nro_pieces);
            }

            if($type == 2) //aereo
            { 
                if($subtotal_lb > $subtotal_w){
                    if($insured){
                        $subtotal +=  ($subtotal_lb * $rate) + ($subtotal_lb * $rate) * 0.10;
                    }else{
                        $subtotal +=  ($subtotal_lb * $rate);
                    }

                }else{
                    if($insured){
                        $subtotal +=  ($subtotal_w * $rate) + ($subtotal_w * $rate) * 0.10;
                    }else{
                        $subtotal +=  ($subtotal_w * $rate);
                    }


                }

                $vol_pie = $subtotal_lb;

            }


        }//Fin for 

        $pack = implode(",", $request->input('items'));

        $invoice_repackage->subtotal =  $subtotal;
        $invoice_repackage->type = $type;
        $invoice_repackage->items = $request->input('items');
        $invoice_repackage->high = $request->input('high');
        $invoice_repackage->long = $request->input('long');
        $invoice_repackage->weight = $request->input('weight');
        $invoice_repackage->width = $request->input('width');
        $invoice_repackage->insured = $insured;
        $invoice_repackage->ware_house = $request->input('ware_house');
        $invoice_repackage->volume = number_format($vol_pie, 2);
        $invoice_repackage->packages = $pack;
        $invoice_repackage->subtotal_lb =  $subtotal_lb;
        $invoice_repackage->subtotal_w = $subtotal_w;
        $invoice_repackage->total_pieces = $total_pieces;
        $invoice_repackage->total_pie3 = $total_pie3;

        return response()->json(["success" => 1, "data" => $invoice_repackage]);

    }

    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('admin.shippings.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;
        
        return view('admin.shippings.map',compact('long', 'lati'));
    }

    public function storeInvoice(Request $request)
    {
        $subtotal_lb  = 0;
        $subtotal_w   = 0;
        $total_pieces = 0;
        $total_pie3   = 0;
        $arr = [];
        $paq = 0;
        $repaq = 0;

        DB::beginTransaction();

        try {

            $invoice = new Invoice();
            $invoice->client_id = $request->input('client_id');
            $invoice->shippings_id = $request->input('shippings');
            $invoice->count_box = $request->input('box_count');
            $invoice->cost_box = $request->input('cost_box');
            $invoice->amount_box = $request->input('total_box');
            $invoice->cost_repackage = $request->input('total_repackage');
            $invoice->additional = $request->input('total_additional');
            $invoice->amount = $request->input('amount');
            $invoice->amount_ves = $request->input('amount');

            $invoice->status = 1;

            $invoice->save();

            //Hay items sin reempaque
            if($request->input('items_packages')){

                $details = new InvoiceDetails();
                $paq = 1;

                foreach ($request->input('items_packages') as $item) {
                    
                    $item = (object)$item;
                    $details->invoices_id = $invoice->id;
                    $details->package_id = $item->id;
                    $details->high = $item->high;
                    $details->weight = $item->weight;
                    $details->long = $item->long;
                    $details->width = $item->width;
                    $details->volume = $item->volume;
                    $details->nro_box = $item->nro_box;
                    $details->nro_pieces = $item->nro_pieces;
                    $details->description = $item->description;
                    $details->insured = $item->insured;
                    $details->status = $invoice->status;
                    $details->total_pie3 = $item->pie;
                    $details->subtotal_w = $item->subtotal_w;
                    $details->subtotal_lb = $item->subtotal_lb;

                    $details->save();

                    $total_pieces = $total_pieces + $item->nro_pieces; 

                    Package::where('id', $item->id)->update(['invoice_status'=> 1]);
                }
            }
 

            if($request->input('items_repackages')){

                $details = new InvoiceDetails();
                $repaq = 1;
                $arr = json_decode($request->input('items_repackages'));

                $total_pieces = $total_pieces + $arr->total_pieces;

                $nro_pieces = Package::whereIn('id', [$arr->packages])->sum('nro_pieces');

                InvoiceDetails::create([
                    "invoices_id" => $invoice->id,
                    "package_id" => 0,
                    "high" => $arr->high,
                    "weight" => $arr->weight,
                    "long" => $arr->long,
                    "width" => $arr->width,
                    //"volume" => $arr->volume,
                    "description" => "Reempaque",
                    "insured" => $arr->insured,
                    "status" => $invoice->status,
                    "packages" => $arr->packages,
                    "total_pie3" => $arr->total_pie3,
                    "subtotal_w" => $arr->subtotal_w,
                    "subtotal_lb" => $arr->subtotal_w,
                    "nro_pieces" => $nro_pieces
                ]);
        
                Package::whereIn('id', [$arr->packages])->update(['invoice_status'=> 1]);
            }


            $inv = $request->input('invoice');
            $item = (object) $inv;

            //Actualizo los totales
            if($paq == 1 && $repaq  == 1){
                $invoice->total_volume = $item->subtotal_lb + $arr->subtotal_lb;
                $invoice->total_pie = $item->total_pie3 + $arr->total_pie3;
                $invoice->total_w = $item->subtotal_w + $arr->subtotal_w;
                $invoice->subtotal_package = $item->subtotal;
                $invoice->subtotal_repackage = $arr->subtotal;
                $invoice->total_pieces = $total_pieces;

                $invoice->save();

            }else if($paq == 1 && $repaq  == 0){
                $invoice->total_volume = $item->subtotal_lb;
                $invoice->total_pie = $item->total_pie3;
                $invoice->total_w = $item->subtotal_w;
                $invoice->subtotal_package = $item->subtotal;
                $invoice->total_pieces = $total_pieces;

                $invoice->save();

            }else if($paq == 0 && $repaq  == 1){
                $invoice->total_volume = $arr->subtotal_lb;
                $invoice->total_pie = $arr->total_pie3;
                $invoice->total_w = $arr->subtotal_w;
                $invoice->subtotal_repackage = $arr->subtotal;
                $invoice->total_pieces = $total_pieces;

                $invoice->save();
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
            

        return response()->json(["success" => 1, "message" => "Factura creada correctamente", "id" => $invoice->id]);

    }


    public function  printInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)->first();

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data)->stream('invoice.pdf');

            return $pdf;
        }

    }

   //listar destinos de envio
    public function getDestinerCalculator(){

        $destiner = RegionApi::select('regions_api.id as id','regions_api.region_name as name')
        ->get();
        
        return $destiner;  
    }

    //listar destinos de envio
    public function getNotifications(){

        $notifications = Notification::select('notifications.id as id','notifications.title as title','notifications.description as description','notifications.date as date')
        ->get();

        if($notifications->isEmpty()){
            return response()->json(["code" => 400,"data" => [],"success" => true ]);
        }else{
            return response()->json(["code" => 200,"data" => $notifications,"success" => true]);
        }
    
    }

    //Calcular costo estimado de viaje
    public function costTripCalculator(Request $request){

        $totail_pie3 = ($request->input('width') * $request->input('height') * $request->input('lenght'))/ 1728; 

        $volumetric_lb = ($request->input('width') * $request->input('height') * $request->input('lenght'))/166;

        $peso = $request->input('weight');


        $trip_id = $request->input('trip_id');
        $destiner = $request->input('destiner_id');

        $reempaque = $request->input('reemp');

        $cost_destiner = TarifaRegion::select('tarifas_region.id as id','tarifas_region.cost as cost_destiner')
        ->where('tarifas_region.region_id',$destiner)
        ->where('tarifas_region.trip_id',$trip_id)
        ->get();


        // calculo del costo estimado

        if($trip_id == 1){
            // el envio es maritimo
            if($totail_pie3<1){
                // costo minimo de libra vol
                $subtotal = $cost_destiner[0]->cost_destiner;
            }else{
                $subtotal = $totail_pie3*$cost_destiner[0]->cost_destiner;
            }
        }else{
            // el envio es aereo
            if($volumetric_lb > $peso){
                $subtotal = $volumetric_lb*$cost_destiner[0]->cost_destiner;
            }else{
                $subtotal = $peso*$cost_destiner[0]->cost_destiner;
            }
        }

        if($reempaque == 1){
            //tiene reempaque

            $total = $subtotal+15;
        }else{
            $total = $subtotal;
        }
        $total_redondeado = round($total, 2);
        return response()->json(["cost" => 200, "message" => "El costo estimado es: $".$total_redondeado],200);
    }
}
