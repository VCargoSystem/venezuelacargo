<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Banner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class BannerController extends Controller
{
    /**
    * Get list of prealerts
    */
    public function index(Request $request)
    {

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $banner = Banner::get();


        if(empty($banner)){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen prealertas"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $banner
            ]
        );

    }

    public function show($id, Request $request)
    {
        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $banner = Banner::where('id',$id)->firts();

        if(count($banner) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "Banner no encontrado"
                    ]);
        }

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data" => $banner
            ]
        );

    }


    public function userValid($request){

        $c = User::where('api_token', $request->bearerToken())->select('id')->count();

        if($c == 0){
            return 0;
        }else{ 
            $u = User::where('api_token', $request->bearerToken())->select('id')->first();

            return $u->id;
        }

    }

}
