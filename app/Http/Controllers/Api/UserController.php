<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Address;
use App\Models\Countries;
use App\Models\Cities;
use App\Models\States;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationAdminRegisterUser;

class UserController extends Controller
{
    /**
    * Obtener el objeto User como json
    */
    public function find($id)
    {
        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $user = $this->findUser($id);

        if(empty($user)){
            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=>  "Cliente no encontrado"
                    ]);
        }

        return response()->json($user);
    }

    public function show(Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('*')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No existe el usuario"
                ]
            );
        }

        $u = new \stdClass();

        $u->name = $user->name;
        $u->last_name = $user->last_name;
        $u->username = $user->username;
        $u->email = $user->email;
        $addr = [];

        foreach ($user->address as $address) {
            $a = new \stdClass();
            $a->countries_id  = $address->countries_id;
            $a->states_id  = $address->states_id;
            $a->cities_id  = $address->cities_id;
            $a->address  = $address->address;
            $a->address1  = $address->address1;
            $a->address1  = $address->address1;

            $addr[]  = $a;
        }

        $u->address = $addr;

        return response()->json(
            [
                "success" => true,
                "user" => $u
            ]
        );
    }
    /*Create new client*/
    public function register(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data,[
            'dni' => ['required', 'string', 'max:10'],
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'phone' => ['required', 'string', 'max:14'],
            'password' => ['required', 'string', 'min:4'],
            'country' => ['required', 'string'],
            'state' => ['required', 'string'],
            'city' => ['required', 'string'],
            'address' => ['required', 'string', 'max:255'],
            //'address1' => ['required', 'string', 'max:255'],
            'longitude' => ['required', 'string'],
            'latitude' => ['required', 'string']
        ], [
            'dni.required' => 'Cedula requerida',
            'dni.max' => 'Maximo 10 caracteres',
            'name.required' => 'Nombre requerido',
            'name.max' => 'Maximo 255 caracteres',
            'username.required' => 'Nombre requerido',
            'username.max' => 'Maximo 255 caracteres',
            'last_name.required' => 'Apellido requerido',
            'last_name.max' => 'Maximo 255 caracteres',
            'email.required' => 'Email requerido',
            'email.email' => 'Ingrese un email valido',
            'phone.required' => 'Teléfono requerido',
            'phone.max' => 'Maximo 14 caracteres',
            'password.required' => 'Clave requerida',
            'password.min' => 'Minimo 4 caracteres',
            'country.required' => 'Páis requerido',
            'state.required' => 'Estado requerido',
            'city.required' => 'Ciudad requerido',
            'address.required' => 'Dirección requerida',
            'address.max' => 'Maximo 255 caracteres',
            //'address1.required' => 'Dirección requerida',
            //'address1.max' => 'Maximo 255 caracteres',
            'longitude.required' => 'Longitud requerido',
            'longitude.string' => 'Campo deber ser string',
            'latitude.required' => 'Longitud requerido',
            'latitude.string' => 'Campo deber ser string'
          
        ]);

        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=>  $validator->messages()
                    ]);
        }

        $exits = User::where('email', $data['email'])->first();
        $exitsU = User::where('username', $data['username'])->first();

        if($exits or $exitsU){
            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=>  "Email o Nombre de usuario existentes"
                    ]);
        }

        DB::beginTransaction();

        try {
            
            $user = new User();
            $user->dni = $data['dni'];
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->last_name = $data['last_name'];
            $user->email = $data['email'];
            $user->phone = $data['phone'];
            $user->password = Hash::make($data['password']);

            $user->save();
            //asigno rol cliente
            $user->assignRole('cliente');

            //Guardo direccion
            $address = new Address();
            $address->countries_id = 95;//$data['countries_id'];

            $state = States::where('name', $data['state'])->first();

            //Address 
            $arr = explode(",", $data['address']);
            $count = count($arr)-1;

            //previo ciudad
            $c = Cities::where('name', 'like', '%'.$arr[($count-2)].'%')
                ->where('state_id',  $state->id)
                ->first();

            $city = 1;
            if(!empty($c)){
                $city = $c->id;
            }else{
                //busco la capital del estado, (hacer la busqueda del api)

                $c = Cities::where('capital', 1)
                ->where('state_id',  $state->id)
                ->first();

                $city = $c->id;
            }

            $address->states_id = $state->id;
            $address->cities_id = $c->id;
            $address->address = $data['address'];
            $address->address1 = $data['address'];
            $address->latitude = $data['latitude'];
            $address->longitude = $data['longitude'];
            $address->users_id = $user->id;
            $address->save();

            DB::commit();

              //Enviar email al administrador
                $details = [ 
                            'cedula' => $user->dni,
                            'username' => $user->username,
                            'client' => $user->name.' '.$user->last_name,
                            'url' => route('login')
                        ];

                $admin = User::join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                        ->where('role_id', 1)->get();

                foreach ($admin as $key => $adm) {
                    Mail::to($adm->email)->send(new NotificationAdminRegisterUser($details));
                }


            return response()->json([
                "success" => true,
                'message' => 'Usuario creado',
                'user' => $this->findUser($user->id)
            ], 200);

        } catch (Exception $e) {
            
            DB::rollback();

            return response()->json([
                "success" => true,
                'message' => 'Error al crear usuario'
            ], 201);
        }
    }

    public function edit(Request $request){
        
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Usuario invalido,no tiene permiso para acceder a este recurso"
                ]
            );

        }else{

            $u = new \stdClass();

            $u->dni = $user->dni;
            $u->name = $user->name;
            $u->last_name = $user->last_name;
            $u->username = $user->username;
            $u->email = $user->email;
            $u->phone = $user->phone;

            return response()->json(
                [
                    "success" => true,
                    "user" => $u
                ]
            );

        }



    }

    public function update(Request $request)
    {

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('*')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $data = $request->all();
        $validator = Validator::make($data,[
            'dni' => ['required', 'string', 'max:10'],
            'username' => ['required', 'string', 'max:255', 'unique:users,username,'.$user->id],
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'.$user->id],
            'phone' => ['required', 'string', 'max:14']
        ], [
            'dni.required' => 'Cedula requerida',
            'dni.max' => 'Maximo 10 caracteres',
            'name.required' => 'Nombre requerido',
            'name.max' => 'Maximo 255 caracteres',
            'username.required' => 'Usuario requerido',
            'username.max' => 'Maximo 255 caracteres',
            'last_name.required' => 'Apellido requerido',
            'last_name.max' => 'Maximo 255 caracteres',
            'email.required' => 'Email requerido',
            'email.email' => 'Ingrese un email valido',
            'phone.required' => 'Teléfono requerido',
            'phone.max' => 'Maximo 14 caracteres'
        ]);

        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=>  $validator->messages()
                    ]);
        }

        DB::beginTransaction();

        try {

            $user->dni = $data['dni'];
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->last_name = $data['last_name'];
            $user->email = $data['email'];
            $user->phone = $data['phone'];

            $user->save();

            DB::commit();

            return response()->json([
                "success" => true,
                'message' => 'Usuario actualizado'
            ], 201);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                "error" => true,
                'message' => 'Error de base de datos'
            ], 400);
        }
    }


    public function findUser($id){
        $user = User::with('address')
                    ->join('model_has_roles', 'model_id', 'users.id')
                    ->where('model_has_roles.role_id', 2)
                    ->where('users.id', $id)
                    ->select('users.*')
                    ->first();  

        return $user;
    }

    public function destroy($id){
        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $user = $this->findUser($id);

        if(empty($user)){
            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=>  "Cliente no encontrado"
                    ]);
        }

        $user->status = 0;
        $user->save();

        return response()->json(
                    ["success"=>true,
                     "message"=>  "Cliente eliminado correctamente"
                    ]);

    }

    public function changePassword(Request $request){
        
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

       if (\Hash::check($request->input('oldPassword'), $user->password)){

            $data = $request->all();
            $validator = Validator::make($data,[
                'password' => ['required', 'string', 'min:4']
            ], [
                'password.required' => 'Clave requerida',
                'password.min' => 'Minimo 4 caracteres'
            ]);

            if($validator->fails()){
                return response()->json(
                        ["error"=>true,
                         "code"=> 404,
                         "message"=>  $validator->messages()
                        ]);
            }

            $user->password =  Hash::make($request->input('password'));
            $user->save();

            return response()->json(
                    ["success"=>true,
                     "message"=>  "Contraseña actualizada correctamente"
                    ]);

        }else{

            return response()->json(
                    ["error"=>true,
                     "message"=>  "La clave anterior no coincide con el usuario"
                    ]);

        }

    }


    public function states(Request $request){
        
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $states = States::where('country_id', 95)
                    ->get();

        return response()->json(
                [
                    "success" => true,
                    "states"  => $states,
                ]
        );
    }


     public function cities(Request $request, $id){
        
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $cities = cities::where('state_id', $id)
                    ->get();

        return response()->json(
                [
                    "success" => true,
                    "states"  => $cities,
                ]
        );
    }

    public function updateAddress(Request $request)
    {

        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())
                    ->select('*')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $data = $request->all();
        $validator = Validator::make($data,[
            'state_id' => ['required', 'numeric'],
            'city_id' => ['required', 'numeric'],
            'address' => ['required', 'string', 'max:255'],
        ], [
            'state_id.required' => 'Estado requerido',
            'city_id.required' => 'Ciudad requerido',
            'address.required' => 'Dirección requerida',
            'address.max' => 'Maximo 255 caracteres',
        ]);

        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=>  $validator->messages()
                    ]);
        }

        DB::beginTransaction();

        try {

            $addr = Address::where('users_id', $user->id)->first();
            $state = States::find($request->input('state_id'));
            $cities = Cities::find($request->input('city_id'));


            $address = 'Venezuela '.$state->name.", ".$cities->name.", ".$request->input('address');

            //Hago una peticion al api para traerme la log y lat
            $response = Http::get('https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates', [
                'SingleLine' => $address,
                'f' => 'json',
                "city" => $cities->name,
                "category"=> "Address",
                "sourceCountry"=>"VEN",
                "Region"=> $state->name,
                'outFields' => 'Loc_name',
                'outSR' =>  array("wkid" => 102100, "latestWkid"=> 3857)
            ]);

            $lat = 0;
            $log = 0;
            $address_new ='';

            if($response->status() == 200){
                $result = $response->json();

                $log = $result['candidates'][0]['location']['x'];
                $lat = $result['candidates'][0]['location']['y'];
                $address_new = $result['candidates'][0]['address'];

            }

            $address1 = 'Venezuela '.$state->name.", ".$cities->name.", ".$address_new;

            $addr->states_id = $data['state_id'];
            $addr->cities_id = $data['city_id'];
            $addr->address = $address_new;
            $addr->address1 = $address1;
            $addr->longitude = $log;
            $addr->latitude = $lat;

            $addr->save();

            DB::commit();

            return response()->json([
                "success" => true,
                'address' => $addr->address,
                'message' => 'Dirección actualizada'
            ], 201);

        } catch (\Exception $e) {
            DB::rollback();

            print_r($e->getMessage());

            return response()->json([
                "error" => true,
                'message' => 'Error de base de datos'
            ], 400);
        }
    }
}
