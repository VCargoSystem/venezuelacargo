<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TarifaRegion extends Model
{
    protected $table = 'tarifas_region';
    public $timestamps = true;
}