<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

class PackagesController extends Controller
{
    /**
    * Get list of prealerts
    */
    public function index(Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();


        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        $packages = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->where('packages.user_id',  $user->id )
                    ->where('packages.status_id', '=', 1)
                    ->groupBy('packages.id')
                    ->orderBy('date_delivery', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path',  'images_packages.url', 'images_packages.name as image', 'confirmed as status', 'packages_trackings.tracking', DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code', 'status.latitude', 'status.longitude', 'packages.created_at', 'packages.updated_at', 'packages.date_delivery', 'packages.shippins_id', 'nro_pieces', 'status.id as status_id', 'instrucctions'])->get();


        if(count($packages) == 0){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen paquetes"
                    ]);
        }


        $data = [];
        // Query the users table
        $map = $packages->map(function($items){
            $data['id'] = $items->id;
            $data['ware_house'] = $items->ware_house;
            $data['weight'] = $items->weight;
            $data['long'] = $items->long;
            $data['high'] = $items->high;
            $data['width'] = $items->width;
            $data['nro_box'] = $items->nro_box;
            $data['description'] = $items->description;
            $data['path'] = $items->path;
            $data['url'] = $items->url;
            $data['image'] = $items->image;
            $data['status'] = $items->status;
            $data['status_id'] = (int) $items->status_id;
            $data['tracking'] = $items->tracking;
            $data['cliente'] = $items->cliente;
            $data['code'] = $items->code;
            $data['latitude'] = $items->latitude;
            $data['longitude'] = $items->longitude;
            $data['instrucctions'] = (int) $items->instrucctions;
            $data['created_at'] =  \Carbon\Carbon::parse($items->created_at)->format('d-m-Y');
            $data['updated_at'] =  \Carbon\Carbon::parse($items->updated_at)->format('d-m-Y');
            $data['date_delivery'] = $items->date_delivery;

            $volumen_lb = (($items->long * $items->width * $items->high)/ 166) *$items->nro_pieces;
            $data['volumen_lb'] =  number_format($volumen_lb, 2, '.', ''); 

            $pie3 = (($items->long * $items->width * $items->high)/ 1728) * $items->nro_pieces; 
            $data['pie3'] =  number_format($pie3, 2, '.', '');

            $data['nro_pieces'] = $items->nro_pieces;
            $data['shippins_id'] = $items->shippins_id;
           
           return $data;
        });

        return response()->json(
            [
                "success" => true,
                "code" => 200,
                "data"=>  $map
            ]
        );

    }

    public function show($id, Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $user = User::where('api_token', $request->bearerToken())->select('id')->first();

        if(empty($user)){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "No tiene permiso para acceder a este recurso"
                ]
            );
        }

        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }

        $packages = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->where('packages.id',  $id )
                    ->where('packages.status_id', '=', 1)
                    ->groupBy('packages.id')
                    ->orderBy('date_delivery', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'images_packages.url' ,'images_packages.name as image', 'confirmed as status', 'packages_trackings.tracking', DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code', 'status.latitude', 'status.longitude', DB::raw('DATE_FORMAT(packages.created_at, "%d-%m-%Y") AS date_in'), DB::raw('DATE_FORMAT(packages.updated_at, "%d-%m-%Y") AS updated_date'), 'packages.date_delivery','packages.shippins_id', 'nro_pieces', 'status.id as status_id', 'instrucctions'])->get()[0];


        if(empty($packages)){
            return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  "No existen paquetes"
                    ]);
        }

        $packages->pie3 = (($packages->long * $packages->width * $packages->high)/ 1728) * $packages->nro_pieces; 
        $packages->pie3 =  number_format($packages->pie3, 2, '.', '');


        $packages->volumen_lb = (($packages->long * $packages->width * $packages->high)/ 166) *$packages->nro_pieces; 
        $packages->volumen_lb =  number_format($packages->volumen_lb, 2, '.', '');

        return response()->json(
            $packages
            , 200
        );
    }



    /*Create new packages*/
    public function store(Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $validator = \Validator::make($request->all(), [
            'ware_house' => 'required|max:20',
            'code' => 'required|max:20|exists:users',
            'description' => 'required|max:255',
            'width' => 'required|numeric|min:0.01',
            'high' => 'required|numeric|min:0.01',
            'weight' => 'required|numeric|min:0.01',
            'long' => 'required|numeric|min:0.01',
            'volume' => 'required|numeric|min:0.01',
            'nro_box' => 'required|integer|min:1',
            'image_package' => 'required|mimes:jpg,bmp,png, jpeg,svg'
        ],
        [
            'ware_house.required' => 'Ware House requerido',
            'ware_house.max' => 'Máximo 20 caracteres',
            'code.required' => 'Código requerido',
            'code.max' => 'Máximo 20 caracteres',
            'code.exists' => 'No existe el codigo',
            'description.required' => 'Descripción requerida',
            'description.max' => 'Máximo 255 caracteres',
            'width.required' => 'Ancho requerido',
            'width.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'high.required' => 'Alto requerido',
            'high.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'long.required' => 'Largo requerido',
            'long.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'weight.required' => 'Peso requerido',
            'weight.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'nro_box.required' => 'Número de cajas requerido',
            'nro_box.integer' => 'Debe ser numerico',
            'nro_box.min' => 'Debe ser mayor a 0',
            'volume.required' => 'Volumen requerido',
            'volume.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'image_package.required' => 'Imagen requerida',
            'image_package.mimes' => 'Formato invalido'
        ]
        );


        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=>  $validator->messages()
                    ]);
        }

        $user_id = $this->userValid($request);

        if(!$user_id){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Falta parametro api_token"
                ]
            );
        }

        if($request->input('tracking')[0]==''){
                return response()->json(
                    ["error"=>true,
                     "code"=> 200,
                     "message"=>  'Tracking requerido'
                    ]);
        }

        $package = new Package();
        $package->ware_house = $request->input('ware_house');
        $package->nro_box = $request->input('nro_box');
        $package->description = $request->input('description');
        $package->long = $request->input('long');
        $package->width = $request->input('width');
        $package->high = $request->input('high');
        $package->weight = $request->input('weight');
        $package->lb_volume = $request->input('lb_volume');
        $package->pie_cubico = $request->input('pie_cubico');
        $package->nro_pieces = $request->input('nro_pieces');

        $user = User::where('code', $request->input('code'))
                    ->select('id', 'email')->first();
        $package->user_id = $user->id;
        $package->status_id = 1;
        $package->save(); //Save package

        if($package->id){
            //save trackings
            if($request->input('tracking')[0] !=''){
                foreach ($request->input('tracking') as $tracking) {
                    if(!empty($tracking)){
                        PackageTracking::create([
                            'package_id' => $package->id,
                            'tracking' => $tracking,
                            'status' => 1
                        ]);
                           //Actualizo la prealerta asociada
                        Prealerts::where('tracking', $tracking)->update(['status'=> 1]);
                    }
                }
        }else{
                PackageTracking::create([ 
                        'package_id' => $package->id,
                        'tracking' => $request->input('tracking'),
                        'status' => 1
                    ]);

                Prealerts::where('tracking', $request->input('tracking'))->update(['status'=> 1]);
            }
        }

        //save image
        if (!empty($request->image_package)){

                $image = $request->file('image_package');
                $name  = time().'.'.$image->getClientOriginalExtension();

                //$destinationPath = public_path('storage/packages');

                $destinationPath = public_path('img');

                $img = Image::make($image->path());
                $img->resize(600, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$name, 72);
                $url= url('img');


            $image = new ImagePackage();
            $image->path = $destinationPath.'/'.$name;
            $image->url =  $url.'/'.$name; 
            $image->name = $name;
            $image->status = 1;
            $image->package_id = $package->id;
            $image->save();
        }


        return response()->json(
                    ["success" => true,
                     "code"=> 200,
                     "message"=>  "Paquete registrado correctamente",
                     "data" => $package
                    ]);
    }

    public function update(Request $request)
    {
        if(empty($request->bearerToken())){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $validator =\Validator::make($request->all(), [
            'ware_house' => 'required|max:20',
            'code' => 'required|max:20|exists:users',
            'description' => 'required|max:255',
            'width' => 'required|numeric|min:0.01',
            'high' => 'required|numeric|min:0.01',
            'weight' => 'required|numeric|min:0.01',
            'long' => 'required|numeric|min:0.01',
            'volume' => 'required|numeric|min:0.01',
            'nro_box' => 'required|integer|min:1',
            'image_package' => 'required|mimes:jpg,bmp,png, jpeg,svg'
        ],
        [
            'ware_house.required' => 'Ware House requerido',
            'ware_house.max' => 'Máximo 20 caracteres',
            'code.required' => 'Código requerido',
            'code.max' => 'Máximo 20 caracteres',
            'code.exists' => 'No existe el codigo',
            'description.required' => 'Descripción requerida',
            'description.max' => 'Máximo 255 caracteres',
            'width.required' => 'Ancho requerido',
            'width.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'high.required' => 'Alto requerido',
            'high.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'long.required' => 'Largo requerido',
            'long.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'weight.required' => 'Peso requerido',
            'weight.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'nro_box.required' => 'Número de cajas requerido',
            'nro_box.integer' => 'Debe ser numerico',
            'nro_box.min' => 'Debe ser mayor a 0',
            'volume.required' => 'Volumen requerido',
            'volume.numeric' => 'Debe ser numerico',
            'width.min' => 'Debe ser mayor a 0',
            'image_package.required' => 'Imagen requerida',
            'image_package.mimes' => 'Formato invalido'

        ]
        );

        if ($validator->fails()){
            return response()->json(
                    ["error"=>true,
                     "code"=> 400,
                     "message"=>  $validator->messages()
                    ]);
        }
        $user_id = $this->userValid($request);

        if(!$user_id){
            return response()->json(
                [
                    "error" => true,
                    "code" => 404,
                    "message" => "Falta parametro api_token"
                ]
            );
        }
        
        $id = $request->input('id');

        if(empty($id)){
            response()->json([
                    "error" => true,
                    "message" => "Falta parametro id"
            ]);
        }


        if($this->validDate($request->input('repackage'), 'edit') == 0){
            $message = [
                'La instrucción no se puede modificar (bloqueada) hasta el dia sabado, que se habilita nuevamente',
            ];

            return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=> $message
                    ]);
        }

        if($request->input('tracking')[0]==''){
                return response()->json(
                    ["error"=>true,
                     "code"=> 404,
                     "message"=> 'Tracking requerido'
                    ]);
        }

        $package = Package::find($id);
        $package->ware_house = $request->input('ware_house');
        $package->nro_box = $request->input('nro_box');
        $package->description = $request->input('description');
        $package->long = $request->input('long');
        $package->width = $request->input('width');
        $package->high = $request->input('high');
        $package->weight = $request->input('weight');
        $package->lb_volume = $request->input('lb_volume');
        $package->pie_cubico = $request->input('pie_cubico');
        $package->nro_pieces = $request->input('nro_pieces');

        $user = User::where('code', $request->input('code'))
                    ->select('id')->first();
        $package->user_id = $user->id;
        $package->status_id = $request->input('status_id');
        $package->save(); //Save package
 

        if($request->input('tracking')[0] !=''){
            PackageTracking::where('package_id', $package->id)
                            ->delete();
            $arr = array_unique($request->input('tracking'));

            foreach ($arr as $tracking) {
                if(!empty($tracking)){
                    PackageTracking::create([
                        'package_id' => $package->id,
                        'tracking' => $tracking,
                        'status' => 1
                    ]);

                    Prealerts::where('tracking', $tracking)->update(['status'=> 1]);
                }
            }
        }

        //save image
        if (!empty($request->image_package)){
            
            $img = ImagePackage::where('package_id', $package->id)->first();

            if(!empty($img) && $img->path !=""){
                if (\File::exists($img->path)) {
                    //File::delete($image_path);
                    unlink($img->path);
                }

                $img->delete();
            }
           

            $image = $request->file('image_package');
            $name  = time().'.'.$image->getClientOriginalExtension();

                $destinationPath = public_path('img');
            $img = Image::make($image->path());
            $img->resize(600, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$name, 72);
            $url= url('img');

            $image = new ImagePackage();
            $image->path =$destinationPath.'/'.$name; 
            $image->url = $url.'/'.$name;
            $image->name = $name;
            $image->status = 1;
            $image->package_id = $package->id;
            $image->save();
        }

        return response()->json(
                    ["success" => true,
                     "code"=> 200,
                     "message"=>  "Paquete actualizado correctamente",
                     "data" => $prealerts
                    ]);
    }


    public function userValid($request){

        $c = User::where('api_token', $request->bearerToken())->select('id')->count();

        if($c == 0){
            return 0;
        }else{
            $u = User::where('api_token', $request->bearerToken())->select('id')->first();

            return $u->id;
        }

    }

    public function validDate($repackage, $op){
        // 0  es domingo, traigo el dia de la semana 
        // de la fecha de hoy
        $today = \Carbon\Carbon::now()->locale('es_VE')->dayOfWeek;
        $hour = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('h');
        $min = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('m');

        if($op == 'insert'){
            if($repackage == 1){
                if($today <= 3 &&  ($hour < 16 &  min < 1)){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                if($today <= 4 &&  ($hour < 23 &  min < 59)){
                    return 1;
                }else{
                    return 0;
                }
            }

        }else{
            if($today == 4 or  $today == 5){
                return 0;
            }else{
                 if($repackage == 1){
                    if($today <= 3 &&  ($hour < 16 &  min < 1)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }else{
                        if($today <= 4 &&  ($hour < 23 &  min < 59)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }
            }
        }
    }
}
