<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
   
    /**
     * Inicio de sesión y creación de token
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required|string'
        ], [
        	'username.required' => 'Campo usuario/email requerido',
            'password.required' => 'Campo password requerido'
        ]);

        // check validation
        $dat=[];

        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL ) 
        ? 'email' 
        : 'username';

        $request->merge([
            $login_type => $request->input('username')
        ]);

        if ($validator->fails()) {
            $response = [
                'error' => true,
                'code' => 400,
                'message' => $validator->messages()
            ];
            return response()->json($response, 400);
        }

   
        $credentials = request([$login_type, 'password']);


        if (!Auth::attempt($credentials)){
            return response()->json([
            	'error' => true,
                'code' => 400,
                'message' => 'Usuario o contraseña incorrecta'
            ], 401);
        }

        if(Auth::user()->status == 0)
                return response()->json([
                        'error' => true,
                        'code' => 400,
                        'message' => 'Usuario inactivo'
                    ], 401);

        $user = $request->user();
        /*$tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();*/

        $user->api_token = Str::random(80);
        $user->save();

        return response()->json([
        	'success' => true,
        	'id' => $user->id,
        	'email' => $user->email,
            'api_token' => $user->api_token,
            'token_type' => 'Bearer'
            
        ], 200);
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout()
    {
        Auth::user()->token = NULL;

        return response()->json([
        	'success' => true,
            'message' => 'Sesion cerrada correctamente'
        ], 200);
    }

    public function isValid($token)
    {
        if(!$token){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Campo token vacio"
                ]
            );
        }

        $c = User::where('api_token', $token)->select('id')->count();

        if($c == 0){
            return response()->json(
                [
                    "error" => true,
                    "code" => 400,
                    "message" => "Token invalido"
                ]
            );
        }else{
            return response()->json(
                [
                    "success" => true,
                    "code" => 200,
                    "message" => "Token válido"
                ]
            );
        }

    }
}
