<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\PackagesInvoice;
use App\Models\Currency;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class ShippingsClientController extends Controller
{
   	public function index(Request $request)
    {
        $status = Status::all();

        return view('clients.shippings.index')->with('status', $status);
    }

    public function getFilterShippings(Request $request)
    {

        //facturados sin reempaque
        $packages = InvoiceDetails::select(['invoice_details.id as repackage_id', 'invoice_details.invoices_id', 'invoice_details.high', 'invoice_details.width',
        'invoice_details.long', 'invoice_details.weight', 'invoice_details.subtotal_lb','invoice_details.total_pie3', 'invoice_details.ware_house', 'invoices.nro_contenedor',
        'invoice_details.status_id', 'packages.shippins_id', 'packages.id', 'packages.description', 'shippins.name as shippins_name',DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") as created_date'),
         DB::raw('DATE_FORMAT(packages.date_delivery, "%d-%m-%Y") as date_delivery'), 'status.code as status_siglas', 'packages_trackings.tracking', 'status.latitude', 'status.longitude', 'invoice_details.nro_pieces', 'packages.invoice_status as facturado']) 
        ->join('invoices','invoices.id','invoice_details.invoices_id')
        ->join('packages','packages.user_id','invoices.client_id')
        ->join('status','status.id','invoice_details.status_id')
        ->join('shippins','shippins.id','packages.shippins_id')
        ->join('packages_trackings','packages_trackings.package_id','packages.id')
        ->where('client_id', auth()->user()->id)
        ->where('packages.invoice_status', 1)
        ->where('packages.instrucctions', 1)
        ->where('packages.status_id','>=', 2)
        ->groupBy('invoice_details.invoices_id');
        //->get();
        


        if (!empty($request->get('usuario'))) {
            $packages->where('client_id', $request->get('usuario'));
        }       

        if (!empty($request->get('ware_house'))) {
              $packages->where('invoice_details.ware_house', 
                    'like', '%' . $request->get('ware_house') . '%'
                    );
        }

        if (!empty($request->get('tracking'))) {
                $packages->where('packages_trackings.tracking', 
                    'like', '%' . $request->get('tracking') . '%'
                    );
        }

        if (!empty($request->get('status'))) {

            $packages->where('invoice_details.status_id', 
                    '=', $request->get('status')
            );
        }

        if (!empty($request->get('shippins_id'))) {
                        $packages->where('packages.shippins_id', 
                                '=', $request->get('shippins_id')
                        );
                    }

        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $packages->where(DB::raw("DATE_FORMAT(packages_invoices.created_date, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(packages_invoices.created_date, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }


        return Datatables::of($packages)
        			->addIndexColumn()
                    ->addColumn('actions', function($row){
                        $btn_print='<a href="'.route('clients.shippings.printInvoice',$row->invoices_id).'" class="btn btn-xs btn-warning" data-id="'.$row->repackage_id.'" data-toggle="tooltip" data-placement="top" title="Ver factura" target="_blank"><i class="fa fa-print" style="padding:3px !important;"></i> </a>';

                        $btn = '<a href="'.route('clients.shippings.show', $row->invoices_id).'" class="btn btn-xs btn-primary details" data-id="'.$row->repackage_id.'"  data-latitude="'.$row->latitude.'" data-longitude="'.$row->longitude.'" data-toggle="tooltip" data-placement="top" title="Ver detalle"><i style="padding:3px;" class="fa fa-search"></i>
                            </a>
                            <a href="#" class="btn btn-xs btn-dark shipping" data-id="'.$row->repackage_id.'"  data-latitude="'.$row->latitude.'" data-longitude="'.$row->longitude.'" data-toggle="tooltip" data-placement="top" title="Ver tracking"><i style="padding:3px;" class="fas fa-shipping-fast"></i>
                                </a>';
    
                            return $btn." ".$btn_print;
                    })
                          
                   
                    ->rawColumns(['actions'])
        ->make(true);
    }

    public function  printInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)->first();

                    $details = $invoice->details;
                    $packages = [];
                    $dollar = Currency::first();
            
            
                    foreach ($details as $d) {
                        //Si es si reempaque
                        if($d->package_id > 0){
                            $packages[] = $d;
            
                        }else{
            
                            $id_paq = explode(",", $d->packages);
                            $wh = [];
                            $descriptions = [];
            
                            foreach ($id_paq as $p) {
                                $pak = Package::where('id', $p)->select('*')->first();
                                $wh[] =  $pak->ware_house;
                                $descriptions [] = $pak->description;
                            }
            
                            $d->wh = $wh;
                            $d->descriptions = $descriptions;
                            $packages[] = $d;
                        }
            
                    }
            
                    if(!empty($invoice)){ 
            
                        $data = [
                            "invoice" => $invoice,
                            "user" => $invoice->user,
                            "packages" => $packages,
                            "dollar_value" => $dollar
                        ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data)->stream('invoice.pdf');

            return $pdf;
        }

    }
  

    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('clients.shippings.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;

        return view('clients.shippings.map',compact('long', 'lati'));
    }

    public function show($id_invoices_details)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id',$id_invoices_details)->first();

                    $details = $invoice->details;
                    $packages = [];
                    $dollar = Currency::first();
            
            
                    foreach ($details as $d) {
                        //Si es si reempaque
                        if($d->package_id > 0){
                            
                            $images = [];
                            
                            $imagesList = ImagePackage::where('package_id', $d->package_id)->select('url')->first();
                        	$images[] = $imagesList->url;

							$d->images = $images; 
							$packages[] = $d; 
            
                        }else{
            
                            $id_paq = explode(",", $d->packages);
                            $wh = [];
                            $descriptions = [];
                            $images = [];
                    
                            foreach ($id_paq as $p) {
                                
                                
                                $pak = Package::join('images_packages','images_packages.package_id','packages.id')->where('packages.id', $p)->select('packages.*','images_packages.url')->first();
                                $wh[] =  $pak->ware_house;
                                $descriptions [] = $pak->description;
                                $images[] = $pak->url;
                            }
            
                            $d->wh = $wh;
                            $d->descriptions = $descriptions;
                            $d->images = $images;

                            $packages[] = $d;
                        }
            
                    }

                    $user = $invoice->user;
            
                    if(!empty($invoice)){ 
            
                        $data = [
                            "invoice" => $invoice,
                            "user" => $invoice->user,
                            "packages" => $packages,
                            "dollar_value" => $dollar
                        ];
                 }
                     
    

        /*if($package->lb_volumen == 0){
            $package->lb_volumen = (($package->long * $package->width * $package->high)/ 166) *$package->nro_pieces; 
            $package->lb_volumen = number_format($package->lb_volumen ,2);
        }*/

        return view('clients.shippings.view',compact('invoice','user','packages'));
    }
}
