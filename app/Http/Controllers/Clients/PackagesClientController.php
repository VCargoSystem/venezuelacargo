<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\Prealerts;
use App\Models\PackageTracking;
use App\Models\Companies;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\RegionApi;
use App\Models\TarifaRegion;


class PackagesClientController extends Controller
{
   	public function index(Request $request)
    {
         if ($request->ajax()) {
            $data = Package::with('status')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->where('packages.user_id', auth()->user()->id)
                    ->where('packages.insured', 0)
                    ->where('packages.repackage', 0)
                    ->where('packages.status_id', '=', 1)
                    ->groupBy('packages.id')
                    ->orderBy('date_delivery', 'desc')
                    ->select('packages.*', 'status.name as status', 'images_packages.path', 'images_packages.name as image',  'images_packages.id as image_id')->get();


            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.route('clients.packages.view', $row->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Editar</a> <a href="#" class="btn btn-xs btn-primary delete" data-id="'.$row->id.'"><i class="fa fa-trash"></i> Eliminar</a>';
    
                            return $btn;
                    })
                   
                    ->rawColumns(['actions'])
                    ->make(true);
        }
       
        return view('clients.packages.index');
    }


    public function getFilterPackages(Request $request)
    {
        $packages = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->where('packages.user_id', auth()->user()->id)
                    ->where('packages.status_id', '=', 1)
                    ->groupBy('packages.id')
                    ->orderBy('date_delivery', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'images_packages.name as image', 'confirmed as status', 'packages_trackings.tracking', DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code', 'status.latitude', 'status.longitude', 'packages.nro_pieces', 'instrucctions', 'lb_volume', 'pie_cubico'])
                    ;



        if (!empty($request->get('ware_house'))) {
                $packages->where('packages.ware_house', 
                    'like', '%' . $request->get('ware_house') . '%'
                    );
        }

        if (!empty($request->get('tracking'))) {
                $packages->where('packages_trackings.tracking', 
                    'like', '%' . $request->get('tracking') . '%'
                    );
        }

        if (!empty($request->get('status'))) {
            $packages->where('packages.confirmed', $request->get('status'));
        }


        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $packages->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }

        if (!empty($request->get('instrucctions'))) {
            $packages->where('packages.instrucctions', '=', $request->get('instrucctions'));
        }
 
        return Datatables::of($packages)
        			->addIndexColumn()
                    ->addColumn('actions', function($row){
                        $btn_i = ""; 

                        if($row->instrucctions == 0){
                           $btn_i = '<a href="#" data-toggle="tooltip" data-placement="top" title="Asignar Instrucción de envío" class="btn btn-xs btn-dark instrucction " data-id="'.$row->id.'"><i class="fas fa-plus"></i> </a>';
                        }
                                

                            $btn = '<a href="'.route('clients.packages.view', $row->id).'"  class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="Ver detalle"><i class="fa fa-search"></i></a>
                                <a href="'.route('clients.packages.receipt', $row->id).'" class="btn btn-xs btn-warning" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Ver recibo" target="_blank"><i class="fa fa-print"></i> </a>';

                            return $btn_i." ".$btn;
                    })
              
                   
                    ->rawColumns(['actions'])
        ->make(true);
    }

    public function create(){
        return view('clients.packages.create');
    }

    /*Crear instruccion*/
    public function store(Request $request){

         $validator = \Validator::make($request->all(), [
            'hold' => 'required|min:0', 
            'repackage' => 'required|min:0',
            'shippins_id' => 'required|min:1'
        ],
        [
            'hold.required' => 'Campo Almacen requerido',
            'hold.min' => 'Valor minimo requerido (0)',
            'repackage.required' => 'Campo Reempaque requerido',
            'repackage.min' => 'Valor minimo requerido (0)',
            'shippins_id.required' => 'Campo tipo de envio requerido',
            'shippins_id.min' => 'Valor minimo requerido (1)'
        ]
        );

        if ($validator->fails()){
            return redirect()->route('clients.packages.index')->withErrors($validator);
        }

      
        $package = Package::where('id', $request->input('id_package'))->first();


        $package->hold = $request->input('hold');
        $package->insured = ($request->input('insured') > 0 ? $request->input('insured') : 0);
        $package->shippins_id = $request->input('shippins_id');
        $package->repackage = $request->input('repackage');
       
        if($request->input('hold') == 1){
            $package->instrucctions = 1;
            $package->status_id = 1;
           // $package->confirmed = 'POR CONFIRMAR';
        }else{
            $package->instrucctions = 1;
            $package->status_id = 2;
            //$package->confirmed = 'CONFIRMADO';
        }

        $package->save();


        return redirect()->route('clients.packages.index')->with('success', 'Instrucción asignada correctamente!');

    }

    public function edit($id){
        $package = Package::where('id', $id)
                        ->select('packages.*')
                        ->first();

         //Verified date 
        if($package->instrucctions == 1){
            return response()->json(["success" => 1, 'package' => $package]);
        }else{
            return response()->json(["success" => 0, "message" => "El paquete no posee instrucción, debe agregar una" ]);
        }
    }

     public function update(Request $request){

        $validator = \Validator::make($request->all(), [
            'hold' => 'required|min:0',
            'repackage' => 'required|min:0',
            'shippins_id' => 'required|min:1'
        ],
        [
            'hold.required' => 'Campo Almacen requerido',
            'hold.min' => 'Valor minimo requerido (0)',
            'repackage.required' => 'Campo Reempaque requerido',
            'repackage.min' => 'Valor minimo requerido (0)',
            'shippins_id.required' => 'Campo tipo de envio requerido',
            'shippins_id.min' => 'Valor minimo requerido (1)'
        ]
        );

        if ($validator->fails()){
            return redirect()->route('clients.packages.index')->withErrors($validator);
        }

       /*if($this->validDate($request->input('repackage'), 'edit') == 0){
            $message = [
                'La instrucción no se puede modificar (bloqueada) hasta el dia sabado, que se habilita nuevamente',
            ];

            return redirect()->route('clients.packages.index')->withErrors($message);
        }*/

        $package = Package::where('id', $request->input('id_edit'))->first();

        $package->hold = $request->input('hold');
        $package->insured = ($request->input('insured') == 1 ? $request->input('insured') : 0);
        $package->shippins_id = $request->input('shippins_id');
        $package->repackage = $request->input('repackage');

        if($request->input('hold') == 1){
            $package->instrucctions = 1;
            $package->status_id = 1;
            //$package->confirmed = 'POR CONFIRMAR';
        }else{
            $package->instrucctions = 1;
            $package->status_id = 1;
            //$package->confirmed = 'CONFIRMADO';
        }

        $package->save();

        return redirect()->route('clients.packages.index')->with('success', 'Instrucción modificada correctamente!');
     }

    public function destroy($id){
  
    }
   
    public function receipt($id){

        $package = Package::with('user')->where('id', $id)->first();
 
        $companies = [];



        foreach ($package->trackings as $t) {
            $pre_alerts = Prealerts::where('tracking', $t->tracking)->first();

        

            if(!empty($pre_alerts)){
          
                $com = Companies::where('id', $pre_alerts->company_id)->first();

                $companies[] = $com->name;
            }

        }

        $data = [
            "package" => $package,
            "user" =>  $package->user,
            "user" => auth()->user(),
            "companies" => $companies
        ];

        //pdf de recibo
        $pdf = \PDF::loadView('reports.receipt_package', $data)->stream('archivo.pdf');

        return $pdf;
        
    }

    public function view($id){
        $package = Package::with('trackings')
                        ->with('images')
                        ->with('status')
                        ->with('user')
                        ->where('id', $id)
                        ->select('packages.*')
                        ->first();

        $package->pie3 = (($package->long * $package->width * $package->high)/ 1728) * $package->nro_pieces; 

        if($package->pie3 < 1)
            $package->pie3 = 1;

        $package->pie3 =  number_format($package->pie3, 2, '.', '');


        $package->volumen_lb = (($package->long * $package->width * $package->high)/ 166) *$package->nro_pieces; 

        if($package->volumen_lb < 1)
            $package->volumen_lb = 1;
        
        $package->volumen_lb =  number_format($package->volumen_lb, 2, '.', '');

        return view('clients.packages.view')->with('package', $package);
    }

    public function assing($id){
        $package = Package::where('id', $id)
                        ->select('packages.*')
                        ->first();

        if($package->instrucctions == 1){
            return response()->json(["success" => 0, "message" => "Ya tiene una instrucción asignada, por favor seleccione la opcion editar" ]);
        }else{
            return response()->json(["success" => 1, "message" => "Asignar Instrucción" ]);
        }
    }

     public function validDate($repackage, $op){
        // 0  es domingo, traigo el dia de la semana 
        // de la fecha de hoy
        $today = \Carbon\Carbon::now()->locale('es_VE')->dayOfWeek;
        $hour = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('h');
        $min = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('m');

        if($op == 'insert'){
            if($repackage == 1){
                if($today <= 3 &&  ($hour < 16 &  $min < 1)){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                if($today <= 4 &&  ($hour < 23 &  $min < 59)){
                    return 1;
                }else{
                    return 0;
                }
            }

        }else{
            if($today == 4 or  $today == 5){
                return 0;
            }else{
                 if($repackage == 1){
                    if($today <= 3 &&  ($hour < 16 &  $min < 1)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }else{
                        if($today <= 4 &&  ($hour < 23 &  $min < 59)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }
            }
        }
    }

    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('clients.packages.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;
        
        return view('clients.packages.map',compact('long', 'lati'));
    }

    /**RUTAS DE LA CALCULADOR WEB */
    public function calculatorClient(Request $request){

        if ( $request->input('width') == '' || $request->input('height') == '' || $request->input('lenght') == '' || $request->input('weight') == '' || $request->input('destiner_id') < 1){
            return response()->json(["error"=> 1, "success" =>[], "message" => "Falta datos para calcular el costo del envío"]);
	    }else{

            if ( $request->input('width') == 0 || $request->input('height') == 0 || $request->input('lenght') == 0 || $request->input('weight') == 0 || $request->input('destiner_id') < 1){
                return response()->json(["error"=> 1, "success" =>[], "message" => "Las medidas deben ser mayores a 0"]);
            }else{

            $totail_pie3 = ($request->input('width') * $request->input('height') * $request->input('lenght'))/ 1728; 

            $volumetric_lb = ($request->input('width') * $request->input('height') * $request->input('lenght'))/166;
    
            $peso = $request->input('weight');
    
    
            $trip_id = $request->input('trip_id');
            $destiner = $request->input('destiner_id');
    
            $reempaque = $request->input('reemp');
    
            $cost_destiner = TarifaRegion::select('tarifas_region.id as id','tarifas_region.cost as cost_destiner')
            ->where('tarifas_region.region_id',$destiner)
            ->where('tarifas_region.trip_id',$trip_id)
            ->get();
    
    
            // calculo del costo estimado
    
            if($trip_id == 1){
                // el envio es maritimo
                if($totail_pie3<1){
                    // costo minimo de libra vol
                    $subtotal = $cost_destiner[0]->cost_destiner;
                }else{
                    $subtotal = $totail_pie3*$cost_destiner[0]->cost_destiner;
                }
            }else{
                // el envio es aereo
                if($volumetric_lb > $peso){
                    $subtotal = $volumetric_lb*$cost_destiner[0]->cost_destiner;
                }else{
                    $subtotal = $peso*$cost_destiner[0]->cost_destiner;
                }
            }
    
            if($reempaque == 1){
                //tiene reempaque
    
                $total = $subtotal+10;
            }else{
                $total = $subtotal;
            }
            $resultado = round($total, 2);
            return response()->json(["error"=> 0, "success" =>$resultado, "message" => "Estatus del usuario modificado correctamente"]);  
        }
     }

        
    }

}
