<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Prealerts;
use App\Models\Companies;
use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\DB;

class PrealertsClientController extends Controller
{
    public function show(){

    }

   	public function index(Request $request)
    {
      
         if ($request->ajax()) {
            $data = Prealerts::with('user')
                        ->with('company')
                        ->where('pre_alerts.status','=',0)
                        ->where('pre_alerts.user_id','=',auth()->user()->id)
                        ->orderBy('created_at', 'desc')
                    ->select('pre_alerts.*', 'users.id as id_user', 'users.name', 'users.last_name')->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('username', function (Prealerts $prealerts) {
                        return $prealerts->user->map(function($user) {
                                return $user->username;
                            });
                    })
                   ->addColumn('company', function (Prealerts $prealerts) {
                        return $prealerts->company->map(function($company) {
                                return $company->name;
                            });
                    })
                    ->addColumn('status', function($row){
                        if($row->status){
                            $class = 'badge badge-primary';
                            $vale = 'Confirmada';
                        }else{
                            $class = 'badge badge-danger';
                            $vale = 'Pendiente';
                        }
                        return '<span class="'.$class.'">'.$value.'</span>';
                    })
                    ->addColumn('actions', function($row){
                           $btn = '<a href="'.url('/cliente/prealerts/'.$row->id.'/edit').'" class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a> <a href="#" class="btn btn-xs btn-dark delete" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['username', 'company', 'status','actions'])
                    ->make(true);
        }
       
        return view('clients.prealerts.index');
    }


    public function getFilterPrealerts(Request $request)
    {

        $prealerts =Prealerts::join('users','users.id', 'pre_alerts.user_id')
                            ->join('companies', 'companies.id', 'pre_alerts.company_id')
                            ->where('pre_alerts.status','=',0)
                            ->where('user_id','=',auth()->user()->id)
                            ->orderBy('pre_alerts.created_at', 'desc')
                        ->select(['pre_alerts.*', DB::raw("CONCAT(users.name,' ', users.last_name) as cliente"), 'users.code as code_user', 'users.username','companies.name']);


        if (!empty($request->get('tracking'))) {
                $prealerts->where('tracking', 
                    'like', '%' . $request->get('tracking') . '%'
                    );
        }

        if (!empty($request->get('usuario'))) {
            $prealerts->where('users.id',  $request->get('usuario'));
        }

        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $prealerts->where(DB::raw("DATE_FORMAT(pre_alerts.date_in, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(pre_alerts.date_in, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }

        return Datatables::of($prealerts)
        			->addIndexColumn()
                    ->addColumn('username', function (Prealerts $prealerts) {
                        return $prealerts->user->username; 
                    })
                   ->addColumn('company', function (Prealerts $prealerts) {
                        return $prealerts->company->name;
                    })
                    ->addColumn('status', function($row){
                        if($row->status){
                            $class = 'badge badge-primary';
                            $value = 'Confirmada';
                        }else{
                            $class = 'badge badge-danger';
                            $value = 'Pendiente';
                        }
                        return '<span class="'.$class.'">'.$value.'</span>';
                    })
                    ->addColumn('actions', function($row){
                            $btn = '<a href="'.url('/cliente/prealerts/'.$row->id.'/edit').'" class="btn btn-xs btn-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a> <a href="#" class="btn btn-xs btn-dark delete" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['username', 'company','status', 'actions'])
        ->make(true);
    }

    public function getCompaniesQuery(Request $request){
        $data = [];  

        if($request->has('q')){
            $search = $request->q;
            $data =Companies::select("id","name")
                    ->where('name','LIKE',"%$search%")
                    ->get();
        }

        return response()->json($data);
    }

    public function getCompanies(){
        $companies = Companies::where('status', 1)->get();
        $data = [];

        foreach ($companies as $key => $com) {
            $data[] = ['id'=>$com->id, 'text'=>$com->name];
        }
        return response()->json([
            $data
        ]);

    }

    public function create(){
        $companies = Companies::where('status',1)->get();
        return view('clients.prealerts.create')->with('companies', $companies);
    }

    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'tracking' => 'required|max:40',
            'date_in' => 'required|date',
            'company_id' => 'required|numeric',
        ],
        [
            'tracking.required' => 'Tracking requerido',
            'tracking.max' => 'Máximo 40 caracteres',
            'date_in.required' => 'Fecha requerida',
            'date_in.date' => 'Ingrese una fecha',
            'company_id.required' => 'Empresa requerida',
            'company_id.numeric' => 'Ingrese una empresea valida',
        ]
        );

        if ($validator->fails()){
            return redirect()->route('clients.prealerts.create')->withErrors($validator);
        }

        if(Prealerts::where('tracking', '=', $request->input('tracking'))->exists()){
               return redirect()->route('clients.prealerts.create')
            ->withErrors('Existe una prealerta con el mismo tracking');
        }

        $prealerts = new Prealerts();
        $prealerts->code = $this->generateCode();
        $prealerts->tracking = $request->input('tracking');
        $prealerts->date_in = $request->input('date_in');
        $prealerts->company_id = $request->input('company_id');
        $prealerts->status = 0;
        $prealerts->user_id = auth()->user()->id;

        $prealerts->save();

        return redirect()->route('clients.prealerts.index')->with('success', 'Registro creado correctamente!');
    }

    public function edit($id){
        $prealerts = Prealerts::findOrFail($id);
        $companies = Companies::where('status', 1)->get();

        if (empty($prealerts)){
            return redirect('/cliente/prealerts/index')->withErrors('La prealerta no existe');
        }

        return view('clients.prealerts.edit')->with('prealerts', $prealerts)->with('companies', $companies);
    }

    public function storeCompany(Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:50'
            //'description' => 'required|max:255'
        ],
        [
            'name.required' => 'Nombre requerido',
            'name.max' => 'Máximo 50 caracteres'
            //'description.required' => 'Descripción requerida',
            //'description.max' => 'Máximo 255 caracteres'
        ]
        );

        if ($validator->fails()){
            return response()->json([
                "success" => 0,
                "error" => $validator
            ]);
        }

        $com = new Companies();
        
        $last = Companies::orderBy('id', 'desc')->first();

        $com->code = $last->id+1;
        $com->name = $request->input('name');
        $com->description = $request->input('description');
        $com->status = 1;
        $com->save();

        $companies = Companies::where('status', 1)->get();

        return response()->json([
                "success" => 1,
                "company_id" => $com->id,
                "companies" => $companies,
        ]);

    }

    public function update(Request $request, $id){
        $data = $this->validator($request->all())->validate();

        if(Prealerts::where('tracking', '=', $request->input('tracking'))
            ->where('id', '<>', $id)->exists()){
               return redirect()->route('clients.prealerts.index')
            ->withErrors('Existe una prealerta con el mismo tracking');
        }

        Prealerts::whereId($id)->update($data);

        return redirect()->route('clients.prealerts.index')
            ->withSuccess('Registro actualizado correctamente');
    }

    public function destroy($id){
        $user = Prealerts::whereId($id)->delete();

        return response()->json([
            'success' => 1,
            'message' => 'Prealerta eliminada correctamente'
        ]);
      
    }

    protected function validator(array $data)
    {
        $id = $data['id'];

        return Validator::make($data, [
            'code' => 'required|max:20|unique:pre_alerts,code,'.$id,
            'tracking' => 'required|max:40|unique:pre_alerts,code,'.$id,
            'date_in' => 'required|date',
            'company_id' => 'required|numeric',
            //'status' => 'required|numeric',
        ],
        [
            'code.required' => 'Código requerido',
            'code.max' => 'Máximo 20 caracteres',
            'code.unique' => 'Codigo existente para otra prealerta',
            'tracking.required' => 'Código requerido',
            'tracking.max' => 'Máximo 40 caracteres',
            'tracking.unique' => 'Tracking existente para otra prealerta',
            'date_in.required' => 'Fecha requerida',
            'date_in.date' => 'Fecha incorrecta',
            //'status.required' => 'Estatus requerido' 
        ]
        );
    }

    protected function generateCode()
    {
        $last = Prealerts::orderBy('id', 'desc')->select('id')->first();

        //Si ya existe datos
        if(!empty($last)){
            $code = 'PRE-'.($last->id+1);
        }else {
            $code = 'PRE-1';
        }

        return $code;
    }
}
