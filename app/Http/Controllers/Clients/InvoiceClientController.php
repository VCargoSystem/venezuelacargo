<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\Bank;
use App\Models\Payments;
use App\Models\Currency;

use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;


class InvoiceClientController extends Controller
{
   	public function index(Request $request)
    {
        $status = Status::all();

        return view('clients.invoices.index')->with('status', $status);
    }

    public function getFilterInvoices(Request $request)
    {
        $invoices = Invoice::join('users', 'users.id', 'invoices.client_id')
            ->where('client_id', '=', auth()->user()->id)
            ->where('invoices.status', '>=', 1)
            ->select([
                'invoices.id',
                'client_id', 
                'shippings_id',
                 DB::raw('round(amount, 2) as amount'),
                 DB::raw('round(amount_ves) as amount_ves'),
                'nro_contenedor',
                DB::raw('CASE
                            WHEN invoices.status = 1 THEN "POR PAGAR"
                            WHEN invoices.status = 2 THEN "PENDIENTE"
                            ELSE "PAGADA"
                        END as status_name'),
                DB::raw('DATE_FORMAT(invoices.created_at, "%d-%m-%Y") as created_date'),
                'users.name',
                'users.last_name',
                DB::raw('concat(users.name," ",users.last_name) as cliente'),
                DB::raw('DATEDIFF(NOW(),   DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as days_delay'), 
                DB::raw('DATEDIFF(NOW(),   DATE_ADD(invoices.created_at,INTERVAL invoices.days_slack DAY)) as expiration_days'),
                'invoices.status'
            ]);
  
       
       
        if (!empty($request->get('code'))) {
            $invoices->where('users.code', 
                    'like', '%' . $request->get('code') . '%'
            );
        }

        if (!empty($request->get('status'))) {
            
            $invoices->where('invoices.status', 
                    '=', $request->get('status')
            );
        }

       if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $invoices->where(DB::raw("DATE_FORMAT(invoices.created_at, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(invoices.created_at, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }

        $invoices->get();

        return Datatables::of($invoices)
        			->addIndexColumn() 
                  
                    ->addColumn('actions', function($row){
                    if(($row->status_name) == "PAGADA"){
                        $btn_pay = "";

                        if($row->status != 3){
                            $btn_pay = '<a href="'.route('clients.invoices.payment', $row->id).'" class="btn btn-xs btn-success" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Pagar factura"><i class="far fa-credit-card"></i>
                                </a>';
                        }

                        $btn = '<a href="'.route('clients.invoices.printInvoice.paid', $row->id).'" class="btn btn-xs btn-warning" data-id="'.$row->id.'" target="blank" data-toggle="tooltip" data-placement="top" title="Ver factura"><i class="fa fa-print"></i>
                                </a>';
    
                        }else{
                            $btn_pay = "";

                        if($row->status != 3){
                            $btn_pay = '<a href="'.route('clients.invoices.payment', $row->id).'" class="btn btn-xs btn-success" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Pagar factura"><i class="far fa-credit-card"></i>
                                </a>';
                        }

                        $btn = '<a href="'.route('clients.invoices.printInvoice', $row->id).'" class="btn btn-xs btn-warning" data-id="'.$row->id.'" target="blank" data-toggle="tooltip" data-placement="top" title="Ver factura"><i class="fa fa-print"></i>
                                </a>';

                        }
                            return $btn. " ". $btn_pay;
                    })
              
                   
                    ->rawColumns(['delay','actions'])
        ->make(true);
        
    }

    public function  printInvoicePaid($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                    ->first();

        $details = $invoice->details;
        $packages = [];
        $dollar = Currency::first();

        foreach ($details as $d) {
            //Si es si reempaque
            if($d->package_id > 0){
                $packages[] = $d;

            }else{

                $id_paq = explode(",", $d->packages);
                $wh = [];
                $descriptions = [];

                foreach ($id_paq as $p) {
                    $pak = Package::where('id', $p)->select('*')->first();
                    $wh[] =  $pak->ware_house;
                    $descriptions [] = $pak->description;
                }

                $d->wh = $wh;
                $d->descriptions = $descriptions;
                $packages[] = $d;
            }

        }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
                "dollar_value" => $dollar
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice2', $data)->stream('invoice.pdf');

            return $pdf;
        }

    }


    public function  printInvoice($id)
    {
        $invoice = Invoice::with('details')
                    ->with('user')
                    ->where('id', $id)
                                       ->first();
                    
                            $details = $invoice->details;
                            $packages = [];
                            $dollar = Currency::first();
                    
                            foreach ($details as $d) {
                                //Si es si reempaque
                                if($d->package_id > 0){
                                    $packages[] = $d;
                    
                                }else{
                    
                                    $id_paq = explode(",", $d->packages);
                                    $wh = [];
                                    $descriptions = [];
                    
                                    foreach ($id_paq as $p) {
                                        $pak = Package::where('id', $p)->select('*')->first();
                                        $wh[] =  $pak->ware_house;
                                        $descriptions [] = $pak->description;
                                    }
                    
                                    $d->wh = $wh;
                                    $d->descriptions = $descriptions;
                                    $packages[] = $d;
                                }
                    
                            }

        if(!empty($invoice)){

            $data = [
                "invoice" => $invoice,
                "user" => $invoice->user,
                "packages" => $packages,
                "dollar_value" => $dollar
            ];

            //pdf de recibo
            $pdf = \PDF::loadView('reports.invoice', $data)->stream('invoice.pdf');

            return $pdf;
        }

    }
  

    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('clients.shippings.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;

        return view('clients.shippings.map',compact('long', 'lati'));
    }

    public function paymentInvoice($id){

        $invoice = Invoice::with('details')->find($id);
        $user = $invoice->user;
        $type = $invoice->shippings_id;
        $packages = $invoice->details->where('package_id', '>', 0)->all();
        $repackages = $invoice->details->where('package_id', '=', 0)->all();
    

        $payment = Payments::where('invoice_id', $invoice->id)->first();

        if(!empty($payment)){

            if($payment->status == 1){
        
                return redirect()->route('clients.invoices.index')->withErrors('La factura, ya posee un pago en espera de aprobación');   
            }
        }

        return view('clients.invoices.payment_invoice',compact('invoice', 'user', 'type', 'packages', 'repackages'));
    }


    public function paymentTransfer($id){

        $invoice = Invoice::find($id);

        $user = $invoice->user;
        $type = $invoice->shippings_id;

        $banks = Bank::all();

        return view('clients.invoices.transfer',compact('invoice', 'user', 'type', 'banks'));
    }
    
    public function transferStore(Request $request){

        $validator = \Validator::make($request->all(), [
            'bank' => 'required',
            'reference' => 'required',
            'capture' => 'required|mimes:jpg,bmp,png, jpeg,svg'        ],
        [
            'bank.required' => 'Campo banco requerido',
            'reference.required' => 'Campo referencia requerido',
            'capture.required' => 'Campo banco requerido',
            'capture.required' => 'Imagen requerida',
            'capture.mimes' => 'Formato invalido'
        ]
        );

        if ($validator->fails()){
            return redirect()->route('clients.invoices.paymentTransfer', $request->input('id'))->withErrors($validator);
        }

        $invoice = Invoice::find($request->input('id'));

        $bank = Bank::find($request->input('bank'));

        $payment = new Payments();

        $payment->invoice_id = $invoice->id;
        $payment->client_id = $invoice->client_id;
        $payment->amount = $invoice->amount;
        $payment->descriptions = 'Pago por transferencia / pago móvil,  '.$bank->code.' - '.$bank->name;
        $payment->status = 1;
        $payment->reference = $request->input('reference');
        $payment->bank = $request->input('bank');

        //save image
   
        if (!empty($request->file('capture'))){

                $image = $request->file('capture');
                $name  = $request->input('reference').'.'.$image->getClientOriginalExtension();

                //$destinationPath = public_path('storage/captures');
                $destinationPath = public_path('img/captures');

                if(!File::isDirectory($destinationPath)){
                    File::makeDirectory($destinationPath, 0777, true, true);
                }

                $img = Image::make($image->path());
                $img->resize(600, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$name, 72);
                $url= url('img/captures');


            //$paymen->capture_path = $destinationPath.'/'.$name;
            $payment->capture =  $url.'/'.$name;
        }

        $payment->save();

        $invoice->status = 2;
        $invoice->save();

        return redirect()->route('clients.invoices.index')->with('success', 'Pago creado correctamente. Será revisado por la administración, para su validación');
    }

}
