<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\ImagePackage;
use App\Models\Status;
use App\Models\Package;
use App\Models\PackageTracking;
use App\Models\Prealerts;
use App\Models\Companies;

use DataTables;
Use Session;
Use Redirect;
use App\Mail\NotificationCode;
use App\Mail\NotificationPackageRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class InstrucctionClientController extends Controller
{
   	public function index(Request $request)
    {
        return view('clients.instrucctions.index');
    }


    public function getFilterPackages(Request $request)
    {
        $packages = Package::join('status', 'status.id', 'packages.status_id')
                    ->leftjoin('images_packages', 'images_packages.package_id', 'packages.id')
                    ->join('packages_trackings', 'packages_trackings.package_id', 'packages.id')
                    ->join('users', 'users.id', 'packages.user_id')
                    ->join('shippins', 'shippins.id', 'packages.shippins_id')
                    ->where('packages.user_id', auth()->user()->id)
                    ->where('packages.instrucctions', 1)
                    ->where('packages.hold', 0)
                    ->where('packages.status_id', '=', 2)
                    ->where('packages.confirmed', '=', 'POR CONFIRMAR')
                    ->where('packages.invoice_status', '=', 0)

                    ->groupBy('packages.id')
                    ->orderBy('packages.instrucctions', 'desc')
                    ->select([DB::raw('distinct(packages.id)'), 'ware_house', 'weight','long', 'width', 'high', 'nro_box', 'description', 'images_packages.path', 'images_packages.name as image', 'confirmed as status', 'packages_trackings.tracking', 'hold','repackage', 'insured', 'shippins.name as shippins', 'shippins.id as shippins_id', 'status.latitude', 'status.longitude'])
                    ;


        if (!empty($request->get('ware_house'))) {
                $packages->where('packages.ware_house', 
                    'like', '%' . $request->get('ware_house') . '%'
                    );
        }

        if (!empty($request->get('tracking'))) {
                $packages->where('packages_trackings.tracking', 
                    'like', '%' . $request->get('tracking') . '%'
                    );
        }

       
       


        if (!empty($request->get('date1')) && !empty($request->get('date'))) {
            $packages->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '>=', $request->get('date'))
                ->where(DB::raw("DATE_FORMAT(packages.created_at, '%Y-%m-%d')"), 
                    '<=', $request->get('date1'));
        }

        return Datatables::of($packages)
        			->addIndexColumn()
                    ->addColumn('actions', function($row){
                        $btn_ins = "";

                        if($row->instrucctions){
                            $btn_ins =' 
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Asignar Instrucción de envío" class="btn btn-xs btn-dark instrucction"data-id="'.$row->id.'"><i class="fas fa-plus"></i> </a>
                                </a>';
                        }


                        $btn = '<a href="#" data-toggle="tooltip" data-placement="top" title="Editar Instrucción de envío" class="btn btn-xs btn-dark edit"data-id="'.$row->id.'"><i class="fas fa-edit"></i> 
                                </a>
                                <a href="#" class="btn btn-xs btn-dark shipping" data-id="'.$row->id.'"  data-latitude="'.$row->latitude.'" data-longitude="'.$row->longitude.'" data-toggle="tooltip" data-placement="top" title="Ver tracking"><i class="fas fa-shipping-fast"></i> </a>
                                </a>
                                <a href="'.route('clients.packages.receipt', $row->id).'" class="btn btn-xs btn-warning" data-id="'.$row->id.'" data-toggle="tooltip" data-placement="top" title="Ver recibo" target="_blank"><i class="fa fa-print"></i> </a>
                                <a href="'.route('clients.instrucctions.view', $row->id).'"  class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="Ver detalle"><i class="fa fa-search"></i></a>';
    
                            return $btn." ".$btn_ins;
                    })
              
                   
                    ->rawColumns(['actions'])
        ->make(true);
    }

    public function create(){
        return view('clients.instrucctions.create');
    }

    public function store(Request $request){

         $validator = \Validator::make($request->all(), [
            'hold' => 'required|min:0',
            'repackage' => 'required|min:0',
            'shippins_id' => 'required|min:1'
        ],
        [
            'hold.required' => 'Campo Almacen requerido',
            'hold.min' => 'Valor minimo requerido (0)',
            'repackage.required' => 'Campo Reempaque requerido',
            'repackage.min' => 'Valor minimo requerido (0)',
            'shippins_id.required' => 'Campo tipo de envio requerido',
            'shippins_id.min' => 'Valor minimo requerido (1)'
        ]
        );

        if ($validator->fails()){
            return redirect()->route('clients.instrucctions.index')->withErrors($validator);
        }

        //Verified date 
       /* if($this->validDate($request->input('repackage'), 'insert') == 0){
            $message = [
                'Hasta el día miércoles hasta las 4:00pm se puede confirmar una instrucción, si solo si, el envío es con Reempaque',
                'Si es Sin Reempaque y no ha confirmado su instrucción, puede
                colocar la instrucción el día jueves para las salidas del viernes'
            ];

            return redirect()->route('clients.instrucctions.index')->withErrors($message);
        }*/
      
        $package = Package::where('id', $request->input('id'))->first();

        $package->hold = $request->input('hold');
        $package->insured = ($request->input('insured') == 1 ? $request->input('insured') : 0) ;
        $package->shippins_id = $request->input('shippins_id');
        $package->repackage = $request->input('repackage');
        $package->instrucctions = 1;

        if($request->input('hold') == 1){
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 1;
        }else{
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 2;
        }

        $package->save();

        return redirect()->route('clients.instrucctions.index')->with('success', 'Instrucción asignada correctamente!');

    }

    public function edit($id){
        $package = Package::where('id', $id)
                        ->select('packages.*')
                        ->first();

         //Verified date 
        if($package->instrucctions == 1){
            return response()->json(["success" => 1, 'package' => $package]);
        }else{
            return response()->json(["success" => 0, "message" => "El paquete no posee instrucción, debe agregar una" ]);
        }
    }

     public function update(Request $request){

        $validator = \Validator::make($request->all(), [
            '_hold' => 'required|min:0',
            '_insured' => 'required|min:0',
            '_repackage' => 'required|min:0',
            '_shippins_id' => 'required|min:1'
        ],
        [
            '_hold.required' => 'Campo Almacen requerido',
            '_insured.required' => 'Campo Seguro requerido',
            '_insured.min' => 'Valor minimo requerido (0)',
            '_repackage.required' => 'Campo Reempaque requerido',
            '_repackage.min' => 'Valor minimo requerido (0)',
            '_shippins_id.required' => 'Campo tipo de envio requerido',
            '_shippins_id.min' => 'Valor minimo requerido (1)'
        ]
        );

        if ($validator->fails()){
            return redirect()->route('clients.instrucctions.index')->withErrors($validator);
        }
        

       /*if($this->validDate($request->input('repackage'), 'edit') == 0){
            $message = [
                'La instrucción no se puede modificar (bloqueada) hasta el dia sabado, que se habilita nuevamente',
            ];

            return redirect()->route('clients.instrucctions.index')->withErrors($message);
        }*/

        $package = Package::where('id', $request->input('id_edit'))->first();

        $package->hold = $request->input('_hold');
        $package->insured = $request->input('_insured');
        $package->shippins_id = $request->input('_shippins_id');
        $package->repackage = $request->input('_repackage');
        $package->instrucctions = 1;
        
        if($request->input('_hold') == 1){
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 1;
        }else{
            $package->confirmed = 'POR CONFIRMAR';
            $package->status_id = 2;
        }

        $package->save();

        return redirect()->route('clients.instrucctions.index')->with('success', 'Instrucción modificada correctamente!');
     }

    public function destroy($id){
  
    }
   
    public function receipt($id){

        $package = Package::with('user')->where('id', $id)->first();

        $companies = [];

        foreach ($package->trackings as $t) {
            $pre_alerts = Prealerts::where('tracking', $t->tracking)->first();

            if(!empty($pre_alerts)){
                $com = Companies::where('id', $pre_alerts->company_id)->first();

                $companies[] = $com->name;
            }

        }

        $data = [
            "package" => $package,
            "user" =>  $package->user,
            "user" => auth()->user()
        ];

        //pdf de recibo
        $pdf = \PDF::loadView('reports.receipt_package', $data)->stream('archivo.pdf');

        return $pdf;
        
    }

    public function view($id){
        $package = Package::with('trackings')
                        ->with('images')
                        ->with('status')
                        ->with('user')
                        ->where('id', $id)
                        ->select('packages.*')
                        ->first();

        return view('clients.instrucctions.view')->with('package', $package);
    }

    public function assing($id){
        $package = Package::where('id', $id)
                        ->select('packages.*')
                        ->first();

        if($package->instrucctions == 1){
            return response()->json(["success" => 0, "message" => "Ya tiene una instrucción asignada, por favor seleccione la opcion editar" ]);
        }else{
            return response()->json(["success" => 1, "message" => "Asignar Instrucción" ]);
        }
    }

    public function validDate($repackage, $op){
        // 0  es domingo, traigo el dia de la semana 
        // de la fecha de hoy
        $today = \Carbon\Carbon::now()->locale('es_VE')->dayOfWeek;
        $hour = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('h');
        $min = \Carbon\Carbon::now()->locale('es_VE')->isoFormat('m');


        if($op == 'insert'){
            if($repackage == 1){
                if($today <= 3 &&  ($hour < 16 &  $min < 1)){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                if($today <= 4 &&  ($hour < 23 &  $min < 59)){
                    return 1;
                }else{
                    return 0;
                }
            }

        }else{
            if($today == 4 or  $today == 5){
                return 0;
            }else{
                 if($repackage == 1){
                    if($today <= 3 &&  ($hour < 16 &  $min < 1)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }else{
                        if($today <= 4 &&  ($hour < 23 &  $min < 59)){
                            return 1;
                        }else{
                            return 0;
                        }
                    }
            }
        }
    }

    public function iframeMap()
    {
        $lati = 10.23535;
        $long = -67.59113;
        return view('clients.packages.map',compact('lati', 'long'));
    }

    public function iframeMapFind($log, $lat)
    {
        $lati = $lat;
        $long = $log;
        
        return view('clients.packages.map',compact('long', 'lati'));
    }

    public function selectSearch(Request $request)
    {
        $users = [];

        if($request->has('q')){
            $search = $request->q;
            $users =User::join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                ->select("id", "name", "last_name", "code")
                    ->where(DB::raw('CONCAT(users.name," ",users.last_name)'), 'LIKE', "%$search%")
                    ->whereNotNull('code')
                    ->where('role_id', 2)
                    ->get();
        }
        return response()->json($users);
    }
}
