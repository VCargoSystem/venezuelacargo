<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni' => ['required', 'string', 'max:10'],
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email'],
            'phone' => ['required', 'string', 'max:14'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
            'state_id' => ['required', 'numeric'],
            'city_id' => ['required', 'numeric'],
            'address' => ['required', 'string', 'max:255']
        ];
    }

    public function messages(){
         return [
            'dni.required' => 'Cedula requerida',
            'dni.max' => 'Maximo 10 caracteres',
            'name.required' => 'Nombre requerido',
            'name.max' => 'Maximo 255 caracteres',
            'last_name.required' => 'Apellido requerido',
            'last_name.max' => 'Maximo 255 caracteres',
            'email.required' => 'Email requerido',
            'email.email' => 'Ingrese un email valido',
            'phone.required' => 'Teléfono requerido',
            'phone.max' => 'Maximo 14 caracteres',
            'password.required' => 'Clave requerida',
            'password.min' => 'Minimo 4 caracteres',
            'password.confirmed' => 'La clave no coincide',
            'address.required' => 'Dirección requerida',
            'address.max' => 'Maximo 255 caracteres'
        ];
    }

    public function response(array $errors)
    {
        $transformed = [];

        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field' => $field,
                'message' => $message[0]
            ];
        }

        return response()->json([
            'errors' => $transformed
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
