<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
    protected $table = 'countries';
    public $timestamps = false;

    public function states() {
        return $this->hasMany(States::class, 'country_id');
    }
}
