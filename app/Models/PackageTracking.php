<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageTracking extends Model
{
    protected $table = 'packages_trackings';
    public $timestamps = true;
    protected $fillable = ['id', 'tracking', 'status', 'package_id'];

    public function package() {
        return $this->belongsTo(Package::class);
    }

    public function pre_alerts() {
        return $this->belongsTo(Prealerts::class, 'pre_alerts.tracking', 'tracking');
    }

}
