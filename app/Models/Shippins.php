<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shippins extends Model
{
    protected $table = 'shippins';
    public $timestamps = false;
    
    public function package() {
        return $this->hasMany(Package::class, 'package_id');
    }


}
