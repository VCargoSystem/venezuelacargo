<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    protected $table = 'bank';
    public $timestamps = false;

    protected $fillable = ['id', 'code', 'name'];
}
