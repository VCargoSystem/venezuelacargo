<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImagePackage extends Model
{
    protected $table = 'images_packages';
    public $timestamps = true;

    protected $fillable =['path','url','name','status','package_id','updated_at','created_at'];

    public function packages() {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }
}
