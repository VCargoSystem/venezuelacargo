<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{

	//protected $dates = ['deleted_at'];
    protected $table = 'address';
	//protected $hidden = ['created_at', 'updated_at'];

    public function user() {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function state() {
        return $this->belongsTo(States::class, 'states_id');
    }

    public function country() {
        return $this->belongsTo(Countries::class, 'countries_id');
    }

    public function city() {
        return $this->hasMany(Cities::class,  'id', 'cities_id');
    }
}
