<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    protected $table = 'packages';
    public $timestamps = true;

    protected $fillable = [
        'ware_house',
        'nro_container',
        'nro_box',
        'description',
        'date_delivery',
        'long',
        'width',
        'high',
        'weight',
        'lb_volume',
        'user_id',
        'status_id',
        'pie_cubico',
        'instrucctions'

    ];

    public function status() {
        return $this->hasMany(Status::class, 'id','status_id');
    }

    public function images() {
        return $this->hasMany(ImagePackage::class, 'package_id');
    }

    public function trackings() {
        return $this->hasMany(PackageTracking::class,  'package_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function shippins() {
        return $this->belongsTo(Shippins::class, 'id');
    }

}
