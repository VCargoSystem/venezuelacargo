<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prealerts extends Model
{
    protected $table = 'pre_alerts';
    public $timestamps = true;

    protected $fillable = [
    	'id',
        'code',
        'tracking',
        'date_in',
        'date_in',
        'company_id',
        'status'
    ];
    
    public function company() {
        return $this->belongsTo(Companies::class, 'company_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
