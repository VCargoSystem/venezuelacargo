<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceDetails extends Model
{
    protected $table = 'invoice_details';
    public $timestamps = false;
    
    
    protected $fillable = ['invoices_id', 'package_id', 'high', 'width', 'long', 'weight', 'volume', 'nro_box', 'description', 'insured', 'status', 'nro_pieces', 'subtotal_lb', 'subtotal_w', 'total_pie3','packages', 'ware_house', 'nro_contenedor', 'date_in', 'status_id'];
    	
    
    public function invoice() {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }

}
