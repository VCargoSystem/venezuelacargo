<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    protected $table = 'status';
    public $timestamps = false;

    public function package() {
        return $this->belongsTo(Package::class, 'status_id', 'id');
    }
}
