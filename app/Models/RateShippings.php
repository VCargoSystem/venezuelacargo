<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RateShippings extends Model
{
    protected $table = 'rate_shippings';
    public $timestamps = true;
    protected $fillable = ['id', 'regions', 'cost', 'created_at', 'updated_at', 'trip_id'];
}
