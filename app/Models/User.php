<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;


class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function address() {
        return $this->hasMany(Address::class, 'users_id', 'id');
    }

    public function adminlte_image(){
        return 'https://picsum.photos/300/300';
    }

    public function prealerts() {
        return $this->hasMany(Prealerts::class, 'user_id', 'id');
    }

    public function packages() {
        return $this->hasMany(Package::class, 'user_id', 'id');
    }

   public function payments() {
        return $this->hasMany(Payments::class, 'client_id', 'id');
    }
}
