<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cities extends Model
{

	//protected $dates = ['deleted_at'];
    protected $table = 'cities';
    public $timestamps = false;


    public function state() {
        return $this->belongsTo(States::class, 'state_id');
    }
}
