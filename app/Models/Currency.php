<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{

	//protected $dates = ['deleted_at'];
    protected $table = 'currency';
    public $timestamps = false;

}
