<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessTokens extends Model
{
    protected $table = 'oauth_access_tokens';
    public $timestamps = false;

    public function user() {
        return $this->belongsTo(Package::class, 'user_id', 'id');
    }
}
