<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payments extends Model
{
    protected $table = 'payments';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }

}
