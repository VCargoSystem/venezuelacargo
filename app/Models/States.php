<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class States extends Model
{
    protected $table = 'states';
    public $timestamps = false;

    public function country() {
        return $this->belongsTo(Countries::class, 'country_id');
    }

    public function cities() {
        return $this->hasMany(Cities::class, 'state_id');
    }


}
