<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Box extends Model
{
    protected $table = 'box';
    public $timestamps = true;

    protected $fillable = ['id', 'name', 'cost', 'status', 'created_at', 'updated_at'];
}
