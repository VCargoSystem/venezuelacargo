<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companies extends Model
{
    protected $table = 'companies';
    public $timestamps = true;

    protected $fillable = [
    	'id',
        'name',
        'code',
        'description',
        'status'
    ];
    

    public function prealerts() {
        return $this->hasMany(Prealerts::class, 'company_id', 'id');
    }
}
