<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    protected $table = 'invoices';
    public $timestamps = true;
      
    public function user() {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function details() {
        return $this->hasMany(InvoiceDetails::class, 'invoices_id');
    }

}
