<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    protected $table = 'banner';
    public $timestamps = true;

    protected $fillable = ['id', 'url', 'path', 'description', 'status', 'created_at', 'updated_at'];
}
