<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CompanieController;
use App\Http\Controllers\Admin\PrealertsController;
use App\Http\Controllers\Admin\PackagesController;
use App\Http\Controllers\Admin\InstrucctionController;
use App\Http\Controllers\Admin\ShippingsController;
use App\Http\Controllers\Admin\ShippingsInvoicesController;
use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\Admin\RateShippingsController;
use App\Http\Controllers\Admin\BoxController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\StatusController;
use App\Http\Controllers\Clients\PrealertsClientController;
use App\Http\Controllers\Clients\PackagesClientController;
use App\Http\Controllers\Clients\InstrucctionClientController;
use App\Http\Controllers\Clients\ShippingsClientController;
use App\Http\Controllers\Clients\InvoiceClientController;

App::setLocale('es');

Route::get('/', [HomeController::class, 'index']
);

Auth::routes();

/*get de estados, ciudades y map*/
Route::get('users/iframeMap', [RegisterController::class, 'iframeMap']
)->name('users.iframeMap');
Route::get('/findCities/{state_id}', [RegisterController::class, 'findCities'])->name('users.findCities');
Route::get('/users/iframeMapFind/{log}/{lat}', [RegisterController::class, 'iframeMapFind'])->name('users.iframeMapFind');
Route::get('/users/registerSuccess', [RegisterController::class, 'registerSuccess'])->name('users.register_success');


Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');

Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password'); 
Route::get('email-success', [ForgotPasswordController::class, 'submitEmailSuccess'])->name('forget.email_success'); 
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/redirects', [HomeController::class, 'index'])->name('redirects');

//rutas accesibles solo para clientes
Route::group(['middleware' => ['role:cliente'], 'prefix' => 'cliente'], function () {


	Route::get('/ajax-autocomplete-search', [InstrucctionClientController::class, 'selectSearch'])->name('clients.users.usersAutocomplete');
	
   //prealerts
	Route::get('/prealerts/filters', [PrealertsClientController::class, 'getFilterPrealerts'])->name('clients.prealerts.getFilterPrealerts');
	Route::get('/prealerts/companies', [PrealertsClientController::class, 'getCompanies'])->name('prealerts.getCompanies');
	Route::get('/prealerts/companiesq', [PrealertsClientController::class, 'getCompaniesQuery'])->name('prealerts.getCompaniesQuery');
	Route::post('/prealerts/storeCompany', [PrealertsClientController::class, 'storeCompany'])->name('clients.prealerts.storeCompany');

	
	Route::resource('prealerts', 'Clients\PrealertsClientController')
	->names([
		    'index' => 'clients.prealerts.index',
		    'create'=> 'clients.prealerts.create',
		    'store' => 'clients.prealerts.store',
		    'edit' => 'clients.prealerts.edit',
		    'update' => 'clients.prealerts.update',
		    'destroy' => 'clients.prealerts.destroy'
		]);

	

	Route::delete('/prealerts/delete/{id}', [PrealertsClientController::class, 'destroy'])->name('clients.prealerts.delete');
	

	//Package
	Route::get('/iframeMap', [PackagesClientController::class, 'iframeMap']
		)->name('packages.iframeMap');
	Route::get('/packages/iframeMapFind/{log}/{lat}', [PackagesClientController::class, 'iframeMapFind'])->name('packages.iframeMapFind');
	Route::get('/packages/filters', [PackagesClientController::class, 'getFilterPackages'])->name('clients.packages.getFilterPackages');
	Route::post('/packages/confirmed', [PackagesClientController::class, 'confirmed'])->name('clients.packages.confirmed');
	Route::get('/packages/receipt/{id}', [PackagesClientController::class, 'receipt'])->name('clients.packages.receipt');
	Route::get('/packages/view/{id}', [PackagesClientController::class, 'view'])->name('clients.packages.view');
	Route::get('/packages/assing/{id}', [PackagesClientController::class, 'assing'])->name('clients.packages.assign');

	Route::resource('packages', 'Clients\PackagesClientController')->names([
		    'index' => 'clients.packages.index',
		    'create'=> 'clients.packages.create',
		    'store' => 'clients.packages.store',
		    'edit' => 'clients.packages.edit',
		    'update' => 'clients.packages.update',
		    'destroy' => 'clients.packages.destroy'
		]);

	//Instrucctions
	Route::get('/instrucctions/filters', [InstrucctionClientController::class, 'getFilterPackages'])->name('clients.instrucctions.getFilterPackages');
	Route::get('/instrucctions/receipt/{id}', [InstrucctionClientController::class, 'receipt'])->name('clients.instrucctions.receipt');
	Route::get('/instrucctions/view/{id}', [InstrucctionClientController::class, 'view'])->name('clients.instrucctions.view');
	Route::get('/instrucctions/assing/{id}', [InstrucctionClientController::class, 'assing'])->name('clients.instrucctions.assign');
	Route::get('/instrucctions/iframeMapFind/{log}/{lat}', [InstrucctionClientController::class, 'iframeMapFind'])->name('instrucctions.iframeMapFind');
	Route::resource('instrucctions', 'Clients\InstrucctionClientController')->names([
		    'index'   => 'clients.instrucctions.index',
		    'create'  => 'clients.instrucctions.create',
		    'store'   => 'clients.instrucctions.store',
		    'edit'    => 'clients.instrucctions.edit',
		    'update'  => 'clients.instrucctions.update',
		    'destroy' => 'clients.instrucctions.destroy'
		]);

	//Shippings 
	Route::get('/iframeMap/shippings', [ShippingsClientController::class, 'iframeMap']
		)->name('shippings.iframeMap');
	Route::get('/iframeMapFind/shippings/{log}/{lat}', [ShippingsClientController::class, 'iframeMapFind'])->name('shippings.iframeMapFind');
	Route::get('/shippings/filters', [ShippingsClientController::class, 'getFilterShippings'])->name('clients.shippins.getFilterShippings');
	Route::get('/shippings/assing/{id}', [ShippingsClientController::class, 'assing'])->name('clients.shippings.assign');
	Route::post('/shippings/add', [ShippingsClientController::class, 'addShipping'])->name('clients.shippings.addShipping');
	Route::get('/shippings/receipt/{id}', [ShippingsClientController::class, 'receipt'])->name('clients.shippings.receipt');
	Route::get('/printInvoice/{id}', [ShippingsClientController::class, 'printInvoice'])->name('clients.shippings.printInvoice');


	Route::resource('shippings', 'Clients\ShippingsClientController')->names([
		    'index'   => 'clients.shippings.index',
		    'show' 	  => 'clients.shippings.show',
		    'create'  => 'clients.shippings.create',
		    'store'   => 'clients.shippings.store',
		    'edit'    => 'clients.shippings.edit',
		    'update'  => 'clients.shippings.update',
		    'destroy' => 'clients.shippings.destroy',

		]);
	
	Route::post('/calculatorClient', [PackagesClientController::class, 'calculatorClient'])->name('calculatorClient');

	//Invoices
	Route::get('/invoices/filters', [InvoiceClientController::class, 'getFilterInvoices'])->name('clients.invoices.getFilterInvoices');

	Route::get('/printInvoice/invoices/{id}', [InvoiceClientController::class, 'printInvoice'])->name('clients.invoices.printInvoice');

	Route::get('/printInvoice/invoices/paid/{id}', [InvoiceClientController::class, 'printInvoicePaid'])->name('clients.invoices.printInvoice.paid');

	Route::get('/payment/{id}', [InvoiceClientController::class, 'paymentInvoice'])->name('clients.invoices.payment');

	Route::get('/transfer/{id}', [InvoiceClientController::class, 'paymentTransfer'])->name('clients.invoices.paymentTransfer');

	Route::post('transferStore', [InvoiceClientController::class, 'transferStore'])->name('clients.invoices.transferStore');

	Route::resource('invoices', 'Clients\InvoiceClientController')->names([
		    'index'   => 'clients.invoices.index',
		    'create'  => 'clients.invoices.create',
		    'store'   => 'clients.invoices.store',
		    'edit'    => 'clients.invoices.edit',
		    'update'  => 'clients.invoices.update',
		    'destroy' => 'clients.invoices.destroy'
		]);
});


//rutas accesibles solo para admin
Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin'], function () {

	//Generales
	Route::get('/iframeMap', [UserController::class, 'iframeMap']
		)->name('iframeMap');
	Route::get('/findCities/{state_id}', [UserController::class, 'findCities'])->name('users.findCities');
	Route::get('/users/iframeMapFind/{log}/{lat}', [UserController::class, 'iframeMapFind'])->name('users.iframeMapFind');
	Route::get('/countNotifications', [UserController::class, 'countNotifications']
		)->name('admin.countNotifications');

	Route::get('/ajax-autocomplete-search', [UserController::class, 'selectSearch'])->name('users.usersAutocomplete');
	//users
	Route::get('/users/filters', [UserController::class, 'getFilterUsers'])->name('users.getFilterUsers');
	Route::post('/users/code', [UserController::class, 'codeUpdate'])->name('users.codeUpdate');
	Route::get('/users/edit/{id}', [UserController::class, 'edit'])->name('users.edit');

	Route::resource('users', 'Admin\UserController')->names([
	    'index'   => 'admin.users.index',
	    'create'  => 'admin.users.create',
	    'store'   => 'admin.users.store',
	    'edit'    => 'admin.users.edit',
	    'update'  => 'admin.users.update',
	    'destroy' => 'admin.users.destroy',

	]);

	Route::post('/calculator', [UserController::class, 'calculator'])->name('calculator');
	

	Route::get('/iframeMap/user', [UserController::class, 'iframeMap']
		)->name('users.iframeMap');

	Route::post('/users/changeStatus', [UserController::class, 'changeStatus'])->name('users.changeStatus');
	//companies
	Route::get('/companies/filters', [CompanieController::class, 'getFilterCompanies'])->name('companies.getFilterCompanies');
	Route::resource('companies', 'Admin\CompanieController');

	//prealerts
	Route::get('/prealerts/filters', [PrealertsController::class, 'getFilterPrealerts'])->name('prealerts.getFilterPrealerts');
	Route::get('/prealerts/companies', [PrealertsController::class, 'getCompanies'])->name('prealerts.getCompanies');
	Route::get('/prealerts/companiesq', [PrealertsController::class, 'getCompaniesQuery'])->name('prealerts.getCompaniesQuery');
	Route::post('/prealerts/storeCompany', [PrealertsController::class, 'storeCompany'])->name('prealerts.storeCompany');
	Route::resource('prealerts', 'Admin\PrealertsController');
	Route::get('/prealerts/addPackage/{id}', [PrealertsController::class, 'addPackage'])->name('prealerts.addPackage');
	Route::post('/prealerts/storePackage', [PrealertsController::class, 'storePackage'])->name('prealerts.storePackage');


	//packages
	Route::get('/packages/filters', [PackagesController::class, 'getFilterPackages'])->name('packages.getFilterPackages');
	Route::post('/packages/confirmed', [PackagesController::class, 'confirmed'])->name('packages.confirmed');
	Route::get('/packages/receipt/{id}', [PackagesController::class, 'receipt'])->name('packages.receipt');
	Route::get('/user/{name}', [PackagesController::class, 'getUser'])->name('packages.getUser');
	Route::get('/packages/resend/{id}', [PackagesController::class, 'resend'])->name('packages.resend');
	Route::get('/packages/findWareHouse/{ware_house}', [PackagesController::class, 'findWareHouse'])->name('packages.findWareHouse');

	Route::resource('packages', 'Admin\PackagesController');

	//Instrucctions
	Route::get('/instrucctions/filters', [InstrucctionController::class, 'getFilterInstrucctions'])->name('admin.instrucctions.getFilterInstrucctions');
	Route::get('/instrucctions/receipt/{id}', [InstrucctionController::class, 'receipt'])->name('admin.instrucctions.receipt');
	Route::get('/instrucctions/view/{id}', [InstrucctionController::class, 'view'])->name('admin.instrucctions.view');
	Route::get('/instrucctions/assing/{id}', [InstrucctionController::class, 'assing'])->name('admin.instrucctions.assign');
	Route::get('/instrucctions/iframeMapFind/{log}/{lat}', [InstrucctionController::class, 'iframeMapFind'])->name('instrucctions.iframeMapFind');

	Route::resource('instrucctions', 'Admin\InstrucctionController')->names([
		    'index'   => 'admin.instrucctions.index',
		    'create'  => 'admin.instrucctions.create',
		    'store'   => 'admin.instrucctions.store',
		    'edit'    => 'admin.instrucctions.edit',
		    'update'  => 'admin.instrucctions.update',
		    'destroy' => 'admin.instrucctions.destroy'
		]);

	Route::get('/instrucctions/confirmed/{id}', [InstrucctionController::class, 'confirm'])->name('instrucctions.confirmed');

	//Shippings 
	Route::get('/shippings/filters', [ShippingsController::class, 'getFilterShippings'])->name('admin.shippins.getFilterShippings');
	Route::get('/shippings/assing/{id}', [ShippingsController::class, 'assing'])->name('admin.shippings.assign');
	Route::post('/shippings/add', [ShippingsController::class, 'addShipping'])->name('admin.shippings.addShipping');
	Route::get('/shippings/receipt/{id}', [ShippingsController::class, 'receipt'])->name('admin.shippings.receipt');
	Route::post('/shippings/changeLot', [ShippingsController::class, 'changeLot'])->name('admin.shippings.changeLot');
	Route::get('/shippings/iframeMapFind/{log}/{lat}', [ShippingsController::class, 'iframeMapFind'])->name('shippings.iframeMapFind');
	Route::get('/iframeMap', [ShippingsController::class, 'iframeMap']
		)->name('shippings.iframeMap');
	Route::get('/position/{id}', [ShippingsController::class, 'position']
		)->name('shippings.position');

	Route::post('/invoice/shippings', [ShippingsController::class, 'invoice'])->name('admin.shippings.invoice');

	Route::get('/shippings/repackage', [ShippingsController::class, 'repackage'])->name('admin.shippings.repackage');
	Route::post('/shippings/changePaq', [ShippingsController::class, 'changePackage'])->name('admin.shippings.changePackage');

	Route::post('/invoice/storeInvoice', [ShippingsController::class, 'storeInvoice'])->name('admin.shippings.storeInvoice');

	Route::get('printInvoiceShippinngs/{id}', [ShippingsController::class, 'printInvoice'])->name('admin.printInvoiceShippinngs');
	Route::get('sendInvoiceShippinngs/{id}', [ShippingsController::class, 'sendInvoice'])->name('admin.sendInvoiceShippings');

	Route::resource('shippings', 'Admin\ShippingsController')->names([
		    'index'   => 'admin.shippings.index',
		    'show' 	  => 'admin.shippings.show',
		    'edit'    => 'admin.shippings.edit',
		    'update'  => 'admin.shippings.update',
		    'destroy' => 'admin.shippings.destroy',
		]);


	//Envios facturados
	Route::get('/shippings_invoices/filters', [ShippingsInvoicesController::class, 'getFilterShippings'])->name('shippings_invoices.getFilterShippings');
	Route::get('/shippings_invoices/iframeMapFind/{log}/{lat}', [ShippingsInvoicesController::class, 'iframeMapFind'])->name('shippings_invoices.iframeMapFind');
	Route::get('/InvoiceiframeMap', [ShippingsInvoicesController::class, 'iframeMap']
		)->name('shippings_invoices.iframeMap');

	Route::get('/position_shippings/{id}', [ShippingsInvoicesController::class, 'position']
		)->name('shippings_invoices.position');

	Route::resource('shippings_invoices', 'Admin\ShippingsInvoicesController')->names([
		    'index'   => 'admin.shippings_invoices.index',
		    'show' 	  => 'admin.shippings_invoices.show',
		    'create'  => 'admin.shippings_invoices.create',
		    'store'   => 'admin.shippings_invoices.store',
		    'edit'    => 'admin.shippings_invoices.edit',
		    'update'  => 'admin.shippings_invoices.update',
		    'destroy' => 'admin.shippings_invoices.destroy',
		]);
	
	//Invoices
	Route::get('/invoices/filters', [InvoiceController::class, 'getFilterInvoices'])->name('admin.invoices.getFilterInvoices');
	Route::get('/printInvoice/{id}', [InvoiceController::class, 'printInvoice'])->name('admin.invoices.printInvoice');
	Route::get('/printInvoice/paid/{id}', [InvoiceController::class, 'printInvoicePaid'])->name('admin.invoices.printInvoice.paid');
	Route::patch('/invoices/changeStatus', [InvoiceController::class, 'changeStatus'])->name('admin.invoices.changeStatus');
	Route::get('/sendInvoice/{id}', [InvoiceController::class, 'sendInvoice'])->name('admin.invoices.sendInvoice');

	Route::resource('invoices', 'Admin\InvoiceController')->names([
		    'index'   => 'admin.invoices.index',
		    'create'  => 'admin.invoices.create',
		    'store'   => 'admin.invoices.store',
		    'edit'    => 'admin.invoices.edit',
		    'update'  => 'admin.invoices.update',
		    'destroy' => 'admin.invoices.destroy'
		]);
	
	Route::get('/currency/filters', [CurrencyController::class, 'getFilterCurrency'])->name('admin.currency.getFilterCurrency');

	Route::resource('currency', 'Admin\CurrencyController')->names([
		    'index'   => 'admin.currency.index',
		    'create'  => 'admin.currency.create',
		    'store'   => 'admin.currency.store',
		    'edit'    => 'admin.currency.edit',
		    'update'  => 'admin.currency.update',
		    'destroy' => 'admin.currency.destroy'
		]);

	Route::get('/rate_shippings/filters', [RateShippingsController::class, 'getFilterRateShippings'])->name('admin.rate_shippings.getFilterRateShippings');

	Route::resource('rate_shippings', 'Admin\RateShippingsController')->names([
		    'index'   => 'admin.rate_shippings.index',
		    'edit'    => 'admin.rate_shippings.edit',
		    'update'  => 'admin.rate_shippings.update',
		]);

	Route::get('/box/filters', [BoxController::class, 'getFilterBox'])->name('admin.box.getFilterBox');

	Route::resource('box', 'Admin\BoxController')->names([
		    'index'   => 'admin.box.index',
		    'create'  => 'admin.box.create',
		    'store'   => 'admin.box.store',
		    'edit'    => 'admin.box.edit',
		    'update'  => 'admin.box.update',
		]);

	//status
	Route::get('/status/filters', [StatusController::class, 'getFilterBox'])->name('admin.status.getFilterBox');
	Route::get('/iframeMap', [StatusController::class, 'iframeMap']
		)->name('status.iframeMap');
	Route::get('/status/iframeMapFind/{log}/{lat}', [StatusController::class, 'iframeMapFind'])->name('status.iframeMapFind');

	Route::resource('status', 'Admin\StatusController')->names([
		    'index'   => 'admin.status.index',
		    'create'  => 'admin.status.create',
		    'store'   => 'admin.status.store',
		    'edit'    => 'admin.status.edit',
		    'update'  => 'admin.status.update',
		]);

	Route::get('/banner/filters', [BannerController::class, 'getFilterBox'])->name('admin.banner.getFilterBox');

	Route::resource('banner', 'Admin\BannerController')->names([
		    'index'   => 'admin.banner.index',
		    'create'  => 'admin.banner.create',
		    'store'   => 'admin.banner.store',
		    'edit'    => 'admin.banner.edit',
		    'update'  => 'admin.banner.update',
		    'destroy'  => 'admin.banner.destroy'
		]);
});