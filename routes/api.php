<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\PrealertsController;
use App\Http\Controllers\Api\InstrucctionController;
use App\Http\Controllers\Api\PackagesController;
use App\Http\Controllers\Api\ShippingsController;
use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\BannerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1'
], function () {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', 'Api\UserController@register');
    Route::get('isValid/{token}', [AuthController::class, 'isValid']);
    Route::get('states', [UserController::class, 'states']);
    Route::get('cities/{estate_id}', [UserController::class, 'cities']);

    
    Route::group([
      'middleware' => ['api', 'cors']
    ], function() {
        Route::get('logout', 'AuthController@logout');

        Route::get('clients/{id}', [UserController::class, 'find']);
        Route::patch('clients/update', [UserController::class, 'update']);
        Route::delete('clients/delete/{id}', [UserController::class, 'destroy']);
        Route::get('logout', [AuthController::class, 'logout']);

        //Prealerts
        Route::get('prealerts', [PrealertsController::class, 'index']);
        Route::get('prealerts/{id}', [PrealertsController::class, 'show']);
        Route::get('/companies/prealerts', [PrealertsController::class, 'getCompanies']);

        Route::post('/prealerts/store', [PrealertsController::class, 'store']);
        Route::patch('/prealerts/update', [PrealertsController::class, 'update']);
        Route::delete('/prealerts/delete/{id}', [PrealertsController::class, 'destroy']);


        //Instrucctions
        Route::get('instrucctions', [InstrucctionController::class, 'index']);
        Route::get('instrucctions/{id}', [InstrucctionController::class, 'show']);
        Route::post('/instrucctions/store', [InstrucctionController::class, 'store']);
        Route::patch('/instrucctions/update', [InstrucctionController::class, 'update']);


        //Packages
        Route::get('packages', [PackagesController::class, 'index']);
        Route::get('packages/{id}', [PackagesController::class, 'show']);
        Route::post('/packages/store', [PackagesController::class, 'store']);
        Route::patch('/packages/update', [PackagesController::class, 'update']);

        //Shippings
         //Packages
        Route::get('shippings', [ShippingsController::class, 'index']);
        Route::get('shippings/{id}', [ShippingsController::class, 'show']);
        Route::get('tracking/{tracking}', [ShippingsController::class, 'tracking']);
        Route::post('/shippings/store', [ShippingsController::class, 'store']);
        Route::patch('/shippings/update', [ShippingsController::class, 'update']);
        Route::get('destiner', [ShippingsController::class, 'getDestinerCalculator']);
        Route::post('shipCost', [ShippingsController::class, 'costTripCalculator']);
        //Invoice
        Route::get('invoices', [InvoiceController::class, 'index']);
        Route::get('invoicesPending', [InvoiceController::class, 'invoicesPending']);
        Route::get('invoices/{id}', [InvoiceController::class, 'show']);
        Route::get('/printInvoice/{id}', [InvoiceController::class, 'printInvoice'])->name('printInvoice');
        Route::get('/searchInvoice/{date1}/{date2}', [InvoiceController::class, 'searchInvoice'])->name('admin.invoices.searchInvoice');

        //Users
        Route::post('changePassword', 'Api\UserController@changePassword');
        Route::patch('user/update', 'Api\UserController@update');
        Route::get('user/edit', 'Api\UserController@edit');
        Route::get('user/show', 'Api\UserController@show');
        Route::patch('user/updateAddress', 'Api\UserController@updateAddress');

        //Banner
        Route::get('banner', 'Api\BannerController@index');
        Route::get('banner/{id}', 'Api\BannerController@show');

    });
});