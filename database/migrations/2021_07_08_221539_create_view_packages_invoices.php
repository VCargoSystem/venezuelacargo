<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateViewPackagesInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement("
          CREATE VIEW packages_invoices AS
          (
           (
            select distinct(invoice_details.id), 
            invoice_details.ware_house, 
            invoices.count_box as nro_box, 
            (SELECT tracking FROM packages_trackings WHERE packages_trackings.package_id IN (invoice_details.packages) LIMIT 1) AS tracking,

            shippins.name as shippins, 
            shippins.id as shippins_id, 
            invoices.nro_contenedor as nro_container, 
            invoices.created_at as created_date, 
            DATE_FORMAT(invoice_details.date_in, '%d-%m-%Y')  as date_delivery, 
            status.code as status, status.id as status_id, 
            invoices.status as invoice_status, 
            status.latitude, status.longitude, 
            1 AS invoice_repackage,
            invoice_details.high,
            invoice_details.width,
            invoice_details.long,
            invoice_details.weight,
            invoice_details.subtotal_lb,
            invoice_details.subtotal_w,
            invoice_details.total_pie3,
            invoice_details.packages,
            invoices.client_id as client_id,
            invoices.id as invoice_id,
            invoice_details.nro_pieces as nro_pieces,
            1 AS facturado,
            'REEMPACADO' AS description,
            invoice_details.insured

            from invoices
            inner join invoice_details on invoice_details.invoices_id = invoices.id 
            inner join users on users.id = invoices.client_id 
            inner join shippins on shippins.id = invoices.shippings_id 
            inner join status on status.id = invoice_details.status_id 
            where status.id > 2
            AND  invoice_details.package_id = 0 
            group by invoice_details.id
            order by invoice_details.id DESC)

            UNION 
            (
            SELECT 
            distinct(packages.id), 
            packages.ware_house, 
            packages.nro_box, 
            packages_trackings.tracking, 
            shippins.name as shippins, 
            shippins.id as shippins_id, 
            packages.nro_container, 
            packages.created_at as created_date, 
            DATE_FORMAT(packages.date_delivery, '%d-%m-%Y')  as date_delivery, 
            status.code as status, 
            status.id as status_id, 
            invoice_status as invoice_status, 
            status.latitude, 
            status.longitude, 
            0 AS invoice_repackage,
            packages.high,
            packages.width,
            packages.long,
            packages.weight,
            packages.lb_volume,
            packages.weight,
            packages.pie_cubico,
            0 as packages,
            packages.user_id as client_id,
            invoice_details.invoices_id as invoice_id,
            packages.nro_pieces as nro_pieces,
            packages.invoice_status AS facturado,
            packages.description,
            packages.insured

            from packages 
            inner join invoice_details on invoice_details.package_id = packages.id 
            inner join status on status.id = packages.status_id 
            inner join packages_trackings on packages_trackings.package_id = packages.id 
            inner join users on users.id = packages.user_id 
            inner join shippins on shippins.id = packages.shippins_id 
            where packages.status_id > 2

             and packages.invoice_status = 1 
             group by packages.id 
             order by packages.instrucctions DESC)
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::statement('DROP VIEW IF EXISTS packages_invoices');
    }
}
