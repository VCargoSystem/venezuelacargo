<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages_trackings', function (Blueprint $table) {
            $table->id();
            $table->string('tracking');
            $table->unsignedBigInteger('package_id');
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('package_id')->references('id')->on('packages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages_trackings');
    }
}
