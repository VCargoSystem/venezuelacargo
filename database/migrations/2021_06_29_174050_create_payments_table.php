<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invoice_id');
            $table->unsignedBigInteger('client_id');
            $table->double('amount');
            $table->string('descriptions')->nullable();
            $table->boolean('status')->default(1);
            $table->string('reference')->nullable();
            $table->string('capture')->nullable();
            $table->integer('bank')->default(0);
            $table->string('paypal_id')->nullable();
            $table->string('paypal_user')->nullable();
            $table->string('paypal_details')->nullable();
            $table->string('paypal_status')->nullable();

            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('invoice_id')->references('id')->on('invoices')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
