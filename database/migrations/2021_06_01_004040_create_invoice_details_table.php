<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invoices_id');
            $table->integer('package_id');
            $table->integer('high');
            $table->double('width');
            $table->double('long');
            $table->double('weight');
            $table->double('volume');
            $table->double('nro_box');
            $table->string('description');
            $table->integer('nro_pieces');
            $table->double('insured');
            $table->double('subtotal_lb');
            $table->double('subtotal_w');
            $table->double('total_pie3');
            $table->boolean('status')->default(0);
            $table->string('packages');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
