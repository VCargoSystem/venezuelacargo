<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('shippings_id');
            $table->integer('count_box');
            $table->double('cost_box');
            $table->double('amount_box');
            $table->double('cost_repackage');
            $table->double('additional');
            $table->double('cost_insured');
            $table->double('amount');
            $table->decimal('amount_ves', 30,2);
            $table->double('total_volume');
            $table->double('total_w');
            $table->double('subtotal_package');
            $table->double('subtotal_repackage');
            $table->string('nro_contenedor');
            $table->integer('total_pieces');
            $table->integer('total_pie');
            $table->boolean('status')->default(0);

            $table->timestamps();

            $table->foreign('shippings_id')->references('id')->on('shippins')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('client_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
