<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('countries_id');
            $table->unsignedBigInteger('states_id');
            $table->unsignedBigInteger('cities_id');
            $table->string('address');
            $table->string('address1');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('status')->default(1);
            $table->timestamps();


            $table->foreign('states_id')->references('id')->on('states')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('cities_id')->references('id')->on('cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('countries_id')->references('id')->on('countries')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('users_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
    }
}
