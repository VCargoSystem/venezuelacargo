<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('ware_house', 50);
            $table->string('nro_container', 50)->nullable();
            $table->integer('nro_box');
            $table->longText('description');
            $table->date('date_delivery')->nullable();
            $table->decimal('long', $precision = 8, $scale = 2)->default(0);
            $table->decimal('width', $precision = 8, $scale = 2)->default(0);
            $table->decimal('high', $precision = 8, $scale = 2)->default(0);
            $table->decimal('weight', $precision = 8, $scale = 2)->default(0);
            $table->decimal('lb_volume', $precision = 8, $scale = 2)->default(0);
            $table->decimal('pie_cubico', $precision = 8, $scale = 2)->default(0);
            $table->integer('nro_pieces')->default(1);

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('shippins_id');
            $table->boolean('repackage')->default(0);
            $table->double('insured')->default(0);
            $table->boolean('hold')->default(0);
            $table->boolean('instrucctions')->default(0);
            $table->string('confirmed')->default('POR CONFIRMAR');
            $table->unsignedBigInteger('shippins_approved')->default(0);
            $table->integer('invoice_status')->default(0);
            $table->timestamps();

            $table->foreign('shippins_id')->references('id')->on('shippins')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('status_id')->references('id')->on('status')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
