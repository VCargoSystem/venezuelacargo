<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('images_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('path');
            $table->string('url');
            $table->string('name');
            $table->boolean('status')->default(1);
            $table->unsignedBigInteger('package_id');
            $table->timestamps();

            $table->foreign('package_id')->references('id')->on('packages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_packages');
    }
}
