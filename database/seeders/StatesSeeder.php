<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Countries;
use App\Models\States;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Por defaul Venezuela
        $country = Countries::where('id', 95)->first();

        States::create( [
			'id'=>1,
			'iso3166a2' => 'VE-X',
			'name'=>'Amazonas',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>2,
			'iso3166a2' => 'VE-B',
			'name'=>'Anzoátegui',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>3,
			'iso3166a2' => 'VE-C',
			'name'=>'Apure',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>4,
			'iso3166a2' => 'VE-D',
			'name'=>'Aragua',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>5,
			'iso3166a2' => 'VE-E',
			'name'=>'Barinas',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>6,
			'iso3166a2' => 'VE-F',
			'name'=>'Bolívar',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>7,
			'iso3166a2' => 'VE-G',
			'name'=>'Carabobo',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>8,
			'iso3166a2' => 'VE-H',
			'name'=>'Cojedes',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>9,
			'iso3166a2' => 'VE-Y',
			'name'=>'Delta Amacuro',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>10,
			'iso3166a2' => 'VE-I',
			'name'=>'Falcón',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>11,
			'iso3166a2' => 'VE-J',
			'name'=>'Guárico',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>12,
			'iso3166a2' => 'VE-K',
			'name'=>'Lara',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>13,
			'iso3166a2' => 'VE-L',
			'name'=>'Mérida',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>14,
			'iso3166a2' => 'VE-M',
			'name'=>'Miranda',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>15,
			'iso3166a2' => 'VE-N',
			'name'=>'Monagas',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>16,
			'iso3166a2' => 'VE-O',
			'name'=>'Nueva Esparta',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>17,
			'iso3166a2' => 'VE-O',
			'name'=>'Portuguesa',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>18,
			'iso3166a2' => 'VE-R',
			'name'=>'Sucre',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>19,
			'iso3166a2' => 'VE-S',
			'name'=>'Táchira',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>20,
			'iso3166a2' => 'VE-T',
			'name'=>'Trujillo',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>21,
			'iso3166a2' => 'VE-W',
			'name'=>'La Guaira',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>22,
			'iso3166a2' => 'VE-U',
			'name'=>'Yaracuy',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>23,
			'iso3166a2' => 'VE-V',
			'name'=>'Zulia',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>24,
			'iso3166a2' => 'VE-A',
			'name'=>'Distrito Capital',
			'country_id' => $country->id
		] );

		States::create( [
			'id'=>25,
			'iso3166a2' => 'VE-Z',
			'name'=>'Dependencias Federales',
			'country_id' => $country->id
		] );
		
    }
}
