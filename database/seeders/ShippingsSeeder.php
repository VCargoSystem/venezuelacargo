<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Shippins;


class ShippingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//creo los tipos de envio
        Shippins::create([
        	'id' => 1,
            'name' => 'MARITIMO',
            'status' => 1
        ]);

        Shippins::create([
            'id' => 2,
            'name' => 'AEREO',
            'status' => 1
        ]);
    }
}
