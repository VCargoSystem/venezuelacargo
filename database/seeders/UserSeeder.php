<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//creo un usuario admin
        $user = User::create([
        	'dni' => 1111111,
            'name' => 'CARLOS',
            'username' => 'ADMINISTRADOR',
            'last_name' => 'ADMINISTRADOR',
            'email' => 'vencargsystem@gmail.com',
            'password' => bcrypt('vzlaCargoSystem2021!'),
            'phone' => '04121812469'
        ]);

        // Le asignamos el rol de admin
        $user->assignRole('admin'); 
    }
}
