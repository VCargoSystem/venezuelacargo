<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Countries;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Countries::create( [
		'id'=>144,
		'name'=>'Afganistán'
		] );


					
		Countries::create( [
		'id'=>114,
		'name'=>'Albania'
		] );


					
		Countries::create( [
		'id'=>18,
		'name'=>'Alemania'
		] );


					
		Countries::create( [
		'id'=>98,
		'name'=>'Algeria'
		] );


					
		Countries::create( [
		'id'=>145,
		'name'=>'Andorra'
		] );


					
		Countries::create( [
		'id'=>119,
		'name'=>'Angola'
		] );


					
		Countries::create( [
		'id'=>4,
		'name'=>'Anguilla'
		] );


					
		Countries::create( [
		'id'=>147,
		'name'=>'Antigua y Barbuda'
		] );


					
		Countries::create( [
		'id'=>207,
		'name'=>'Antillas Holandesas'
		] );


					
		Countries::create( [
		'id'=>91,
		'name'=>'Arabia Saudita'
		] );


					
		Countries::create( [
		'id'=>5,
		'name'=>'Argentina'
		] );


					
		Countries::create( [
		'id'=>6,
		'name'=>'Armenia'
		] );


					
		Countries::create( [
		'id'=>142,
		'name'=>'Aruba'
		] );


					
		Countries::create( [
		'id'=>1,
		'name'=>'Australia'
		] );


					
		Countries::create( [
		'id'=>2,
		'name'=>'Austria'
		] );


					
		Countries::create( [
		'id'=>3,
		'name'=>'Azerbaiyán'
		] );


					
		Countries::create( [
		'id'=>80,
		'name'=>'Bahamas'
		] );


					
		Countries::create( [
		'id'=>127,
		'name'=>'Bahrein'
		] );


					
		Countries::create( [
		'id'=>149,
		'name'=>'Bangladesh'
		] );


					
		Countries::create( [
		'id'=>128,
		'name'=>'Barbados'
		] );


					
		Countries::create( [
		'id'=>9,
		'name'=>'Bélgica'
		] );


					
		Countries::create( [
		'id'=>8,
		'name'=>'Belice'
		] );


					
		Countries::create( [
		'id'=>151,
		'name'=>'Benín'
		] );


					
		Countries::create( [
		'id'=>10,
		'name'=>'Bermudas'
		] );


					
		Countries::create( [
		'id'=>7,
		'name'=>'Bielorrusia'
		] );


					
		Countries::create( [
		'id'=>123,
		'name'=>'Bolivia'
		] );


					
		Countries::create( [
		'id'=>79,
		'name'=>'Bosnia y Herzegovina'
		] );


					
		Countries::create( [
		'id'=>100,
		'name'=>'Botsuana'
		] );


					
		Countries::create( [
		'id'=>12,
		'name'=>'Brasil'
		] );


					
		Countries::create( [
		'id'=>155,
		'name'=>'Brunéi'
		] );


					
		Countries::create( [
		'id'=>11,
		'name'=>'Bulgaria'
		] );


					
		Countries::create( [
		'id'=>156,
		'name'=>'Burkina Faso'
		] );


					
		Countries::create( [
		'id'=>157,
		'name'=>'Burundi'
		] );


					
		Countries::create( [
		'id'=>152,
		'name'=>'Bután'
		] );


					
		Countries::create( [
		'id'=>159,
		'name'=>'Cabo Verde'
		] );


					
		Countries::create( [
		'id'=>158,
		'name'=>'Camboya'
		] );


					
		Countries::create( [
		'id'=>31,
		'name'=>'Camerún'
		] );


					
		Countries::create( [
		'id'=>32,
		'name'=>'Canadá'
		] );


					
		Countries::create( [
		'id'=>130,
		'name'=>'Chad'
		] );


					
		Countries::create( [
		'id'=>81,
		'name'=>'Chile'
		] );


					
		Countries::create( [
		'id'=>35,
		'name'=>'China'
		] );


					
		Countries::create( [
		'id'=>33,
		'name'=>'Chipre'
		] );


					
		Countries::create( [
		'id'=>82,
		'name'=>'Colombia'
		] );


					
		Countries::create( [
		'id'=>164,
		'name'=>'Comores'
		] );


					
		Countries::create( [
		'id'=>112,
		'name'=>'Congo (Brazzaville)'
		] );


					
		Countries::create( [
		'id'=>165,
		'name'=>'Congo (Kinshasa)'
		] );


					
		Countries::create( [
		'id'=>166,
		'name'=>'Cook, Islas'
		] );


					
		Countries::create( [
		'id'=>84,
		'name'=>'Corea del Norte'
		] );


					
		Countries::create( [
		'id'=>69,
		'name'=>'Corea del Sur'
		] );


					
		Countries::create( [
		'id'=>168,
		'name'=>'Costa de Marfil'
		] );


					
		Countries::create( [
		'id'=>36,
		'name'=>'Costa Rica'
		] );


					
		Countries::create( [
		'id'=>71,
		'name'=>'Croacia'
		] );


					
		Countries::create( [
		'id'=>113,
		'name'=>'Cuba'
		] );


					
		Countries::create( [
		'id'=>22,
		'name'=>'Dinamarca'
		] );


					
		Countries::create( [
		'id'=>169,
		'name'=>'Djibouti, Yibuti'
		] );


					
		Countries::create( [
		'id'=>103,
		'name'=>'Ecuador'
		] );


					
		Countries::create( [
		'id'=>23,
		'name'=>'Egipto'
		] );


					
		Countries::create( [
		'id'=>51,
		'name'=>'El Salvador'
		] );


					
		Countries::create( [
		'id'=>93,
		'name'=>'Emiratos Árabes Unidos'
		] );


					
		Countries::create( [
		'id'=>173,
		'name'=>'Eritrea'
		] );


					
		Countries::create( [
		'id'=>52,
		'name'=>'Eslovaquia'
		] );


					
		Countries::create( [
		'id'=>53,
		'name'=>'Eslovenia'
		] );


					
		Countries::create( [
		'id'=>28,
		'name'=>'España'
		] );


					
		Countries::create( [
		'id'=>55,
		'name'=>'Estados Unidos'
		] );


					
		Countries::create( [
		'id'=>68,
		'name'=>'Estonia'
		] );


					
		Countries::create( [
		'id'=>121,
		'name'=>'Etiopía'
		] );


					
		Countries::create( [
		'id'=>175,
		'name'=>'Feroe, Islas'
		] );


					
		Countries::create( [
		'id'=>90,
		'name'=>'Filipinas'
		] );


					
		Countries::create( [
		'id'=>63,
		'name'=>'Finlandia'
		] );


					
		Countries::create( [
		'id'=>176,
		'name'=>'Fiyi'
		] );


					
		Countries::create( [
		'id'=>64,
		'name'=>'Francia'
		] );


					
		Countries::create( [
		'id'=>180,
		'name'=>'Gabón'
		] );


					
		Countries::create( [
		'id'=>181,
		'name'=>'Gambia'
		] );


					
		Countries::create( [
		'id'=>21,
		'name'=>'Georgia'
		] );


					
		Countries::create( [
		'id'=>105,
		'name'=>'Ghana'
		] );


					
		Countries::create( [
		'id'=>143,
		'name'=>'Gibraltar'
		] );


					
		Countries::create( [
		'id'=>184,
		'name'=>'Granada'
		] );


					
		Countries::create( [
		'id'=>20,
		'name'=>'Grecia'
		] );


					
		Countries::create( [
		'id'=>94,
		'name'=>'Groenlandia'
		] );


					
		Countries::create( [
		'id'=>17,
		'name'=>'Guadalupe'
		] );


					
		Countries::create( [
		'id'=>185,
		'name'=>'Guatemala'
		] );


					
		Countries::create( [
		'id'=>186,
		'name'=>'Guernsey'
		] );


					
		Countries::create( [
		'id'=>187,
		'name'=>'Guinea'
		] );


					
		Countries::create( [
		'id'=>172,
		'name'=>'Guinea Ecuatorial'
		] );


					
		Countries::create( [
		'id'=>188,
		'name'=>'Guinea-Bissau'
		] );


					
		Countries::create( [
		'id'=>189,
		'name'=>'Guyana'
		] );


					
		Countries::create( [
		'id'=>16,
		'name'=>'Haiti'
		] );


					
		Countries::create( [
		'id'=>137,
		'name'=>'Honduras'
		] );


					
		Countries::create( [
		'id'=>73,
		'name'=>'Hong Kong'
		] );


					
		Countries::create( [
		'id'=>14,
		'name'=>'Hungría'
		] );


					
		Countries::create( [
		'id'=>25,
		'name'=>'India'
		] );


					
		Countries::create( [
		'id'=>74,
		'name'=>'Indonesia'
		] );


					
		Countries::create( [
		'id'=>140,
		'name'=>'Irak'
		] );


					
		Countries::create( [
		'id'=>26,
		'name'=>'Irán'
		] );


					
		Countries::create( [
		'id'=>27,
		'name'=>'Irlanda'
		] );


					
		Countries::create( [
		'id'=>215,
		'name'=>'Isla Pitcairn'
		] );


					
		Countries::create( [
		'id'=>83,
		'name'=>'Islandia'
		] );


					
		Countries::create( [
		'id'=>228,
		'name'=>'Islas Salomón'
		] );


					
		Countries::create( [
		'id'=>58,
		'name'=>'Islas Turcas y Caicos'
		] );


					
		Countries::create( [
		'id'=>154,
		'name'=>'Islas Virgenes Británicas'
		] );


					
		Countries::create( [
		'id'=>24,
		'name'=>'Israel'
		] );


					
		Countries::create( [
		'id'=>29,
		'name'=>'Italia'
		] );


					
		Countries::create( [
		'id'=>132,
		'name'=>'Jamaica'
		] );


					
		Countries::create( [
		'id'=>70,
		'name'=>'Japón'
		] );


					
		Countries::create( [
		'id'=>193,
		'name'=>'Jersey'
		] );


					
		Countries::create( [
		'id'=>75,
		'name'=>'Jordania'
		] );


					
		Countries::create( [
		'id'=>30,
		'name'=>'Kazajstán'
		] );


					
		Countries::create( [
		'id'=>97,
		'name'=>'Kenia'
		] );


					
		Countries::create( [
		'id'=>34,
		'name'=>'Kirguistán'
		] );


					
		Countries::create( [
		'id'=>195,
		'name'=>'Kiribati'
		] );


					
		Countries::create( [
		'id'=>37,
		'name'=>'Kuwait'
		] );


					
		Countries::create( [
		'id'=>196,
		'name'=>'Laos'
		] );


					
		Countries::create( [
		'id'=>197,
		'name'=>'Lesotho'
		] );


					
		Countries::create( [
		'id'=>38,
		'name'=>'Letonia'
		] );


					
		Countries::create( [
		'id'=>99,
		'name'=>'Líbano'
		] );


					
		Countries::create( [
		'id'=>198,
		'name'=>'Liberia'
		] );


					
		Countries::create( [
		'id'=>39,
		'name'=>'Libia'
		] );


					
		Countries::create( [
		'id'=>126,
		'name'=>'Liechtenstein'
		] );


					
		Countries::create( [
		'id'=>40,
		'name'=>'Lituania'
		] );


					
		Countries::create( [
		'id'=>41,
		'name'=>'Luxemburgo'
		] );


					
		Countries::create( [
		'id'=>85,
		'name'=>'Macedonia'
		] );


					
		Countries::create( [
		'id'=>134,
		'name'=>'Madagascar'
		] );


					
		Countries::create( [
		'id'=>76,
		'name'=>'Malasia'
		] );


					
		Countries::create( [
		'id'=>125,
		'name'=>'Malawi'
		] );


					
		Countries::create( [
		'id'=>200,
		'name'=>'Maldivas'
		] );


					
		Countries::create( [
		'id'=>133,
		'name'=>'Malí'
		] );


					
		Countries::create( [
		'id'=>86,
		'name'=>'Malta'
		] );


					
		Countries::create( [
		'id'=>131,
		'name'=>'Man, Isla de'
		] );


					
		Countries::create( [
		'id'=>104,
		'name'=>'Marruecos'
		] );


					
		Countries::create( [
		'id'=>201,
		'name'=>'Martinica'
		] );


					
		Countries::create( [
		'id'=>202,
		'name'=>'Mauricio'
		] );


					
		Countries::create( [
		'id'=>108,
		'name'=>'Mauritania'
		] );


					
		Countries::create( [
		'id'=>42,
		'name'=>'México'
		] );


					
		Countries::create( [
		'id'=>43,
		'name'=>'Moldavia'
		] );


					
		Countries::create( [
		'id'=>44,
		'name'=>'Mónaco'
		] );


					
		Countries::create( [
		'id'=>139,
		'name'=>'Mongolia'
		] );


					
		Countries::create( [
		'id'=>117,
		'name'=>'Mozambique'
		] );


					
		Countries::create( [
		'id'=>205,
		'name'=>'Myanmar'
		] );


					
		Countries::create( [
		'id'=>102,
		'name'=>'Namibia'
		] );


					
		Countries::create( [
		'id'=>206,
		'name'=>'Nauru'
		] );


					
		Countries::create( [
		'id'=>107,
		'name'=>'Nepal'
		] );


					
		Countries::create( [
		'id'=>209,
		'name'=>'Nicaragua'
		] );


					
		Countries::create( [
		'id'=>210,
		'name'=>'Níger'
		] );


					
		Countries::create( [
		'id'=>115,
		'name'=>'Nigeria'
		] );


					
		Countries::create( [
		'id'=>212,
		'name'=>'Norfolk Island'
		] );


					
		Countries::create( [
		'id'=>46,
		'name'=>'Noruega'
		] );


					
		Countries::create( [
		'id'=>208,
		'name'=>'Nueva Caledonia'
		] );


					
		Countries::create( [
		'id'=>45,
		'name'=>'Nueva Zelanda'
		] );


					
		Countries::create( [
		'id'=>213,
		'name'=>'Omán'
		] );


					
		Countries::create( [
		'id'=>19,
		'name'=>'Países Bajos, Holanda'
		] );


					
		Countries::create( [
		'id'=>87,
		'name'=>'Pakistán'
		] );


					
		Countries::create( [
		'id'=>124,
		'name'=>'Panamá'
		] );


					
		Countries::create( [
		'id'=>88,
		'name'=>'Papúa-Nueva Guinea'
		] );


					
		Countries::create( [
		'id'=>110,
		'name'=>'Paraguay'
		] );


					
		Countries::create( [
		'id'=>89,
		'name'=>'Perú'
		] );


					
		Countries::create( [
		'id'=>178,
		'name'=>'Polinesia Francesa'
		] );


					
		Countries::create( [
		'id'=>47,
		'name'=>'Polonia'
		] );


					
		Countries::create( [
		'id'=>48,
		'name'=>'Portugal'
		] );


					
		Countries::create( [
		'id'=>246,
		'name'=>'Puerto Rico'
		] );


					
		Countries::create( [
		'id'=>216,
		'name'=>'Qatar'
		] );


					
		Countries::create( [
		'id'=>13,
		'name'=>'Reino Unido'
		] );


					
		Countries::create( [
		'id'=>65,
		'name'=>'República Checa'
		] );


					
		Countries::create( [
		'id'=>138,
		'name'=>'República Dominicana'
		] );


					
		Countries::create( [
		'id'=>49,
		'name'=>'Reunión'
		] );


					
		Countries::create( [
		'id'=>217,
		'name'=>'Ruanda'
		] );


					
		Countries::create( [
		'id'=>72,
		'name'=>'Rumanía'
		] );


					
		Countries::create( [
		'id'=>50,
		'name'=>'Rusia'
		] );


					
		Countries::create( [
		'id'=>242,
		'name'=>'Sáhara Occidental'
		] );


					
		Countries::create( [
		'id'=>223,
		'name'=>'Samoa'
		] );


					
		Countries::create( [
		'id'=>219,
		'name'=>'San Cristobal y Nevis'
		] );


					
		Countries::create( [
		'id'=>224,
		'name'=>'San Marino'
		] );


					
		Countries::create( [
		'id'=>221,
		'name'=>'San Pedro y Miquelón'
		] );


					
		Countries::create( [
		'id'=>225,
		'name'=>'San Tomé y Príncipe'
		] );


					
		Countries::create( [
		'id'=>222,
		'name'=>'San Vincente y Granadinas'
		] );


					
		Countries::create( [
		'id'=>218,
		'name'=>'Santa Elena'
		] );


					
		Countries::create( [
		'id'=>220,
		'name'=>'Santa Lucía'
		] );


					
		Countries::create( [
		'id'=>135,
		'name'=>'Senegal'
		] );


					
		Countries::create( [
		'id'=>226,
		'name'=>'Serbia y Montenegro'
		] );


					
		Countries::create( [
		'id'=>109,
		'name'=>'Seychelles'
		] );


					
		Countries::create( [
		'id'=>227,
		'name'=>'Sierra Leona'
		] );


					
		Countries::create( [
		'id'=>77,
		'name'=>'Singapur'
		] );


					
		Countries::create( [
		'id'=>106,
		'name'=>'Siria'
		] );


					
		Countries::create( [
		'id'=>229,
		'name'=>'Somalia'
		] );


					
		Countries::create( [
		'id'=>120,
		'name'=>'Sri Lanka'
		] );


					
		Countries::create( [
		'id'=>141,
		'name'=>'Sudáfrica'
		] );


					
		Countries::create( [
		'id'=>232,
		'name'=>'Sudán'
		] );


					
		Countries::create( [
		'id'=>67,
		'name'=>'Suecia'
		] );


					
		Countries::create( [
		'id'=>66,
		'name'=>'Suiza'
		] );


					
		Countries::create( [
		'id'=>54,
		'name'=>'Surinam'
		] );


					
		Countries::create( [
		'id'=>234,
		'name'=>'Swazilandia'
		] );


					
		Countries::create( [
		'id'=>56,
		'name'=>'Tadjikistan'
		] );


					
		Countries::create( [
		'id'=>92,
		'name'=>'Tailandia'
		] );


					
		Countries::create( [
		'id'=>78,
		'name'=>'Taiwan'
		] );


					
		Countries::create( [
		'id'=>101,
		'name'=>'Tanzania'
		] );


					
		Countries::create( [
		'id'=>171,
		'name'=>'Timor Oriental'
		] );


					
		Countries::create( [
		'id'=>136,
		'name'=>'Togo'
		] );


					
		Countries::create( [
		'id'=>235,
		'name'=>'Tokelau'
		] );


					
		Countries::create( [
		'id'=>236,
		'name'=>'Tonga'
		] );


					
		Countries::create( [
		'id'=>237,
		'name'=>'Trinidad y Tobago'
		] );


					
		Countries::create( [
		'id'=>122,
		'name'=>'Túnez'
		] );


					
		Countries::create( [
		'id'=>57,
		'name'=>'Turkmenistan'
		] );


					
		Countries::create( [
		'id'=>59,
		'name'=>'Turquía'
		] );


					
		Countries::create( [
		'id'=>239,
		'name'=>'Tuvalu'
		] );


					
		Countries::create( [
		'id'=>62,
		'name'=>'Ucrania'
		] );


					
		Countries::create( [
		'id'=>60,
		'name'=>'Uganda'
		] );


					
		Countries::create( [
		'id'=>111,
		'name'=>'Uruguay'
		] );


					
		Countries::create( [
		'id'=>61,
		'name'=>'Uzbekistán'
		] );


					
		Countries::create( [
		'id'=>240,
		'name'=>'Vanuatu'
		] );

		Countries::create( [
			'id'=>95,
			'code' => '862',
			'iso3166a1' => 'VE',
			'iso3166a2' => 'VEN',
			'name'=>'Venezuela'
		] );

		Countries::create( [
		'id'=>15,
		'name'=>'Vietnam'
		] );


					
		Countries::create( [
		'id'=>241,
		'name'=>'Wallis y Futuna'
		] );


					
		Countries::create( [
		'id'=>243,
		'name'=>'Yemen'
		] );


					
		Countries::create( [
		'id'=>116,
		'name'=>'Zambia'
		] );


					
		Countries::create( [
		'id'=>96,
		'name'=>'Zimbabwe'
		] );
    }
}
