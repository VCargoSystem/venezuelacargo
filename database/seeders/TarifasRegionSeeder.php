<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TarifaRegion;


class TarifasRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TarifaRegion::create( [
		'id'=>1,
		'region_id'=>1,
        'trip_id'=>1,
        'cost'=>10,
		] );
				
		TarifaRegion::create( [
            'id'=>2,
            'region_id'=>2,
            'trip_id'=>1,
            'cost'=>12,
		] );
					
		TarifaRegion::create( [
            'id'=>3,
            'region_id'=>3,
            'trip_id'=>1,
            'cost'=>20,
		] );

        TarifaRegion::create( [
            'id'=>4,
            'region_id'=>1,
            'trip_id'=>2,
            'cost'=>10,
            ] );
                           
            TarifaRegion::create( [
                'id'=>5,
                'region_id'=>2,
                'trip_id'=>2,
                'cost'=>10,
            ] );
                             
            TarifaRegion::create( [
                'id'=>6,
                'region_id'=>3,
                'trip_id'=>2,
                'cost'=>10,
            ] );

    }
}