<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RegionApi;

class RegionApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RegionApi::create( [
		'id'=>1,
		'region_name'=>'Caracas, Valencia o Maracay'
		] );


					
		RegionApi::create( [
		'id'=>2,
		'region_name'=>'Margarita, Cumaná o Carúpano'
		] );


					
		RegionApi::create( [
		'id'=>3,
		'region_name'=>'Otras Ciudades'
		] );

    }
}