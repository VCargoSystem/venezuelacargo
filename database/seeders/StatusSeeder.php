<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	//creo un usuario admin
        Status::create([
            'id' => 1,
            'name' => 'EN ALMACÉN MIAMI',
            'status' => 1,
            'latitude' => '25.755898',
            'longitude' => '-80.2709445'
        ]);

        Status::create([
            'id' => 2,
            'name' => 'EN PROCESO DE ENVÍO A VENEZUELA',
            'status' => 1,
            'latitude' => '25.5669439',
            'longitude' => '-80.4148706'
        ]);

        Status::create([
            'id' => 3,
            'name' => 'EN TRÁNSITO',
            'status' => 1,
            'latitude' => '16.3393373',
            'longitude' => '-74.7020041'
        ]);

        Status::create([
            'id' => 4,
            'name' => 'EN ADUANA DE VENEZUELA',
            'status' => 1,
            'latitude' => '10.6017984',
            'longitude' => '-66.9728794'
        ]);

        Status::create([
            'id' => 5,
            'name' => 'EN ALMACÉN DE LA GUAIRA',
            'status' => 1,
            'latitude' => '10.5945007',
            'longitude' => '-66.9526663'
        ]);

        Status::create([
            'id' => 6,
            'name' => 'EN TRÁNSITO VENEZUELA',
            'status' => 1,
            'latitude' => '8.394971',
            'longitude' => '-66.4330047'
        ]);

        Status::create([
            'id' => 7,
            'name' => 'ENTREGADO',
            'status' => 1,
            'latitude' => '8.394971',
            'longitude' => '-66.4330047'
        ]);
    }
}
