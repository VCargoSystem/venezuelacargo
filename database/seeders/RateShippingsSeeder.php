<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RateShippings;
use Illuminate\Support\Facades\DB;

class RateShippingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('rate_shippings')->delete();

        RateShippings::create([
        	'id' => 1,
        	'regions' => 'Caracas, Valencia o Maracay',
        	'cost' => 10,
            'trip_id' => 1
        ]);

        RateShippings::create([
        	'id' => 2,
        	'regions' => 'Margarita, Cumaná o Carúpano',
        	'cost' => 20,
            'trip_id' => 1
        ]);

        RateShippings::create([
        	'id' => 3,
        	'regions' => 'Otros',
        	'cost' => 25,
            'trip_id' => 1
        ]);

          RateShippings::create([
            'id' => 4,
            'regions' => 'Caracas, Valencia o Maracay',
            'cost' => 10,
            'trip_id' => 2
        ]);

        RateShippings::create([
            'id' => 5,
            'regions' => 'Margarita, Cumaná o Carúpano',
            'cost' => 20,
            'trip_id' => 2
        ]);

        RateShippings::create([
            'id' => 6,
            'regions' => 'Otros',
            'cost' => 25,
            'trip_id' => 2
        ]);
    }
}
